.class public Lcom/badlogic/gdx/a/a/f;
.super Lcom/badlogic/gdx/a/a/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/a/a/f$b;,
        Lcom/badlogic/gdx/a/a/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/badlogic/gdx/a/a/b<",
        "Lcom/badlogic/gdx/graphics/l;",
        "Lcom/badlogic/gdx/a/a/f$b;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/badlogic/gdx/a/a/f$a;


# virtual methods
.method public a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/f$b;)Lcom/badlogic/gdx/utils/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/badlogic/gdx/c/a;",
            "Lcom/badlogic/gdx/a/a/f$b;",
            ")",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/a/a;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Lcom/badlogic/gdx/utils/a;
    .locals 0

    check-cast p3, Lcom/badlogic/gdx/a/a/f$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/badlogic/gdx/a/a/f;->a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/f$b;)Lcom/badlogic/gdx/utils/a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/f$b;)V
    .locals 1

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iput-object p2, p1, Lcom/badlogic/gdx/a/a/f$a;->a:Ljava/lang/String;

    if-eqz p4, :cond_1

    iget-object p1, p4, Lcom/badlogic/gdx/a/a/f$b;->e:Lcom/badlogic/gdx/graphics/o;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/f$b;->e:Lcom/badlogic/gdx/graphics/o;

    iput-object p2, p1, Lcom/badlogic/gdx/a/a/f$a;->b:Lcom/badlogic/gdx/graphics/o;

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/f$b;->d:Lcom/badlogic/gdx/graphics/l;

    iput-object p2, p1, Lcom/badlogic/gdx/a/a/f$a;->c:Lcom/badlogic/gdx/graphics/l;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    iget-object p2, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    const/4 v0, 0x0

    iput-object v0, p2, Lcom/badlogic/gdx/a/a/f$a;->c:Lcom/badlogic/gdx/graphics/l;

    if-eqz p4, :cond_2

    iget-object v0, p4, Lcom/badlogic/gdx/a/a/f$b;->b:Lcom/badlogic/gdx/graphics/j$c;

    iget-boolean p1, p4, Lcom/badlogic/gdx/a/a/f$b;->c:Z

    iget-object p2, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p4, p4, Lcom/badlogic/gdx/a/a/f$b;->d:Lcom/badlogic/gdx/graphics/l;

    iput-object p4, p2, Lcom/badlogic/gdx/a/a/f$a;->c:Lcom/badlogic/gdx/graphics/l;

    :cond_2
    iget-object p2, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    invoke-static {p3, v0, p1}, Lcom/badlogic/gdx/graphics/o$a;->a(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j$c;Z)Lcom/badlogic/gdx/graphics/o;

    move-result-object p1

    iput-object p1, p2, Lcom/badlogic/gdx/a/a/f$a;->b:Lcom/badlogic/gdx/graphics/o;

    :goto_1
    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a/f$a;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->a()Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a/f$a;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->b()V

    :cond_3
    return-void
.end method

.method public bridge synthetic a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)V
    .locals 0

    check-cast p4, Lcom/badlogic/gdx/a/a/f$b;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/badlogic/gdx/a/a/f;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/f$b;)V

    return-void
.end method

.method public b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/f$b;)Lcom/badlogic/gdx/graphics/l;
    .locals 0

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a/f$a;->c:Lcom/badlogic/gdx/graphics/l;

    if-eqz p1, :cond_1

    iget-object p2, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p2, p2, Lcom/badlogic/gdx/a/a/f$a;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/o;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/badlogic/gdx/graphics/l;

    iget-object p2, p0, Lcom/badlogic/gdx/a/a/f;->a:Lcom/badlogic/gdx/a/a/f$a;

    iget-object p2, p2, Lcom/badlogic/gdx/a/a/f$a;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/graphics/o;)V

    :goto_0
    if-eqz p4, :cond_2

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/f$b;->f:Lcom/badlogic/gdx/graphics/l$a;

    iget-object p3, p4, Lcom/badlogic/gdx/a/a/f$b;->g:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {p1, p2, p3}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;)V

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/f$b;->h:Lcom/badlogic/gdx/graphics/l$b;

    iget-object p3, p4, Lcom/badlogic/gdx/a/a/f$b;->i:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p1, p2, p3}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;)V

    :cond_2
    return-object p1
.end method

.method public bridge synthetic b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Ljava/lang/Object;
    .locals 0

    check-cast p4, Lcom/badlogic/gdx/a/a/f$b;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/badlogic/gdx/a/a/f;->b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/f$b;)Lcom/badlogic/gdx/graphics/l;

    move-result-object p1

    return-object p1
.end method
