.class public Lcom/badlogic/gdx/a/a/c;
.super Lcom/badlogic/gdx/a/a/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/a/a/c$b;,
        Lcom/badlogic/gdx/a/a/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/badlogic/gdx/a/a/b<",
        "Lcom/badlogic/gdx/graphics/c;",
        "Lcom/badlogic/gdx/a/a/c$b;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/badlogic/gdx/a/a/c$a;


# virtual methods
.method public a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/c$b;)Lcom/badlogic/gdx/utils/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/badlogic/gdx/c/a;",
            "Lcom/badlogic/gdx/a/a/c$b;",
            ")",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/a/a;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Lcom/badlogic/gdx/utils/a;
    .locals 0

    check-cast p3, Lcom/badlogic/gdx/a/a/c$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/badlogic/gdx/a/a/c;->a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/c$b;)Lcom/badlogic/gdx/utils/a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/c$b;)V
    .locals 2

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iput-object p2, p1, Lcom/badlogic/gdx/a/a/c$a;->a:Ljava/lang/String;

    if-eqz p4, :cond_1

    iget-object p1, p4, Lcom/badlogic/gdx/a/a/c$b;->d:Lcom/badlogic/gdx/graphics/d;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/c$b;->d:Lcom/badlogic/gdx/graphics/d;

    iput-object p2, p1, Lcom/badlogic/gdx/a/a/c$a;->b:Lcom/badlogic/gdx/graphics/d;

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/c$b;->c:Lcom/badlogic/gdx/graphics/c;

    iput-object p2, p1, Lcom/badlogic/gdx/a/a/c$a;->c:Lcom/badlogic/gdx/graphics/c;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    iget-object v0, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/badlogic/gdx/a/a/c$a;->c:Lcom/badlogic/gdx/graphics/c;

    if-eqz p4, :cond_2

    iget-object v0, p4, Lcom/badlogic/gdx/a/a/c$b;->b:Lcom/badlogic/gdx/graphics/j$c;

    iget-object v0, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p4, p4, Lcom/badlogic/gdx/a/a/c$b;->c:Lcom/badlogic/gdx/graphics/c;

    iput-object p4, v0, Lcom/badlogic/gdx/a/a/c$a;->c:Lcom/badlogic/gdx/graphics/c;

    :cond_2
    const-string p4, ".ktx"

    invoke-virtual {p2, p4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_3

    const-string p4, ".zktx"

    invoke-virtual {p2, p4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_4

    :cond_3
    iget-object p2, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    new-instance p4, Lcom/badlogic/gdx/graphics/glutils/k;

    invoke-direct {p4, p3, p1}, Lcom/badlogic/gdx/graphics/glutils/k;-><init>(Lcom/badlogic/gdx/c/a;Z)V

    iput-object p4, p2, Lcom/badlogic/gdx/a/a/c$a;->b:Lcom/badlogic/gdx/graphics/d;

    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a/c$a;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/d;->a()Z

    move-result p1

    if-nez p1, :cond_5

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a/c$a;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/d;->b()V

    :cond_5
    return-void
.end method

.method public bridge synthetic a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)V
    .locals 0

    check-cast p4, Lcom/badlogic/gdx/a/a/c$b;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/badlogic/gdx/a/a/c;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/c$b;)V

    return-void
.end method

.method public b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/c$b;)Lcom/badlogic/gdx/graphics/c;
    .locals 0

    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a/c$a;->c:Lcom/badlogic/gdx/graphics/c;

    if-eqz p1, :cond_1

    iget-object p2, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p2, p2, Lcom/badlogic/gdx/a/a/c$a;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/d;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/badlogic/gdx/graphics/c;

    iget-object p2, p0, Lcom/badlogic/gdx/a/a/c;->a:Lcom/badlogic/gdx/a/a/c$a;

    iget-object p2, p2, Lcom/badlogic/gdx/a/a/c$a;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/graphics/c;-><init>(Lcom/badlogic/gdx/graphics/d;)V

    :goto_0
    if-eqz p4, :cond_2

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/c$b;->e:Lcom/badlogic/gdx/graphics/l$a;

    iget-object p3, p4, Lcom/badlogic/gdx/a/a/c$b;->f:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {p1, p2, p3}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;)V

    iget-object p2, p4, Lcom/badlogic/gdx/a/a/c$b;->g:Lcom/badlogic/gdx/graphics/l$b;

    iget-object p3, p4, Lcom/badlogic/gdx/a/a/c$b;->h:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p1, p2, p3}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;)V

    :cond_2
    return-object p1
.end method

.method public bridge synthetic b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Ljava/lang/Object;
    .locals 0

    check-cast p4, Lcom/badlogic/gdx/a/a/c$b;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/badlogic/gdx/a/a/c;->b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/a/c$b;)Lcom/badlogic/gdx/graphics/c;

    move-result-object p1

    return-object p1
.end method
