.class Lcom/badlogic/gdx/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/a/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/badlogic/gdx/utils/a/c<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/badlogic/gdx/a/e;

.field final b:Lcom/badlogic/gdx/a/a;

.field final c:Lcom/badlogic/gdx/a/a/a;

.field final d:Lcom/badlogic/gdx/utils/a/a;

.field final e:J

.field volatile f:Z

.field volatile g:Z

.field volatile h:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/a/a;",
            ">;"
        }
    .end annotation
.end field

.field volatile i:Lcom/badlogic/gdx/utils/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a/b<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field volatile j:Lcom/badlogic/gdx/utils/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a/b<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field volatile k:Ljava/lang/Object;

.field l:I

.field volatile m:Z


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/a/e;Lcom/badlogic/gdx/a/a;Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/utils/a/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/a/d;->f:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/a/d;->g:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/badlogic/gdx/a/d;->i:Lcom/badlogic/gdx/utils/a/b;

    iput-object v1, p0, Lcom/badlogic/gdx/a/d;->j:Lcom/badlogic/gdx/utils/a/b;

    iput-object v1, p0, Lcom/badlogic/gdx/a/d;->k:Ljava/lang/Object;

    iput v0, p0, Lcom/badlogic/gdx/a/d;->l:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/a/d;->m:Z

    iput-object p1, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iput-object p2, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iput-object p3, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iput-object p4, p0, Lcom/badlogic/gdx/a/d;->d:Lcom/badlogic/gdx/utils/a/a;

    iget-object p1, p1, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/f;->a()I

    move-result p1

    const/4 p2, 0x3

    if-ne p1, p2, :cond_0

    invoke-static {}, Lcom/badlogic/gdx/utils/p;->a()J

    move-result-wide p1

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    iput-wide p1, p0, Lcom/badlogic/gdx/a/d;->e:J

    return-void
.end method

.method private a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;
    .locals 1

    iget-object v0, p2, Lcom/badlogic/gdx/a/a;->d:Lcom/badlogic/gdx/c/a;

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/a/a/a;->a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object p1

    iput-object p1, p2, Lcom/badlogic/gdx/a/a;->d:Lcom/badlogic/gdx/c/a;

    :cond_0
    iget-object p1, p2, Lcom/badlogic/gdx/a/a;->d:Lcom/badlogic/gdx/c/a;

    return-object p1
.end method

.method private a(Lcom/badlogic/gdx/utils/a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/a/a;",
            ">;)V"
        }
    .end annotation

    iget-boolean v0, p1, Lcom/badlogic/gdx/utils/a;->c:Z

    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/badlogic/gdx/utils/a;->c:Z

    const/4 v2, 0x0

    :goto_0
    iget v3, p1, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    iget v5, p1, Lcom/badlogic/gdx/utils/a;->b:I

    sub-int/2addr v5, v1

    :goto_1
    if-le v5, v2, :cond_1

    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/badlogic/gdx/a/a;

    iget-object v6, v6, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    if-ne v4, v6, :cond_0

    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/badlogic/gdx/a/a;

    iget-object v6, v6, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/utils/a;->b(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iput-boolean v0, p1, Lcom/badlogic/gdx/utils/a;->c:Z

    return-void
.end method

.method private e()V
    .locals 5

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    check-cast v0, Lcom/badlogic/gdx/a/a/e;

    iget-boolean v1, p0, Lcom/badlogic/gdx/a/d;->g:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/badlogic/gdx/a/d;->g:Z

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v2, v3}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v2

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3}, Lcom/badlogic/gdx/a/a/e;->a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Lcom/badlogic/gdx/utils/a;

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v3, v4}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v3

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/a/a/e;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/a/d;->k:Ljava/lang/Object;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/utils/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v0, v1, v2}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;Lcom/badlogic/gdx/utils/a;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v3, v4}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v3

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/a/a/e;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/a/d;->k:Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    check-cast v0, Lcom/badlogic/gdx/a/a/b;

    iget-boolean v1, p0, Lcom/badlogic/gdx/a/d;->g:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->i:Lcom/badlogic/gdx/utils/a/b;

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->d:Lcom/badlogic/gdx/utils/a/a;

    invoke-virtual {v0, p0}, Lcom/badlogic/gdx/utils/a/a;->a(Lcom/badlogic/gdx/utils/a/c;)Lcom/badlogic/gdx/utils/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/a/d;->i:Lcom/badlogic/gdx/utils/a/b;

    goto/16 :goto_1

    :cond_0
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->i:Lcom/badlogic/gdx/utils/a/b;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a/b;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->i:Lcom/badlogic/gdx/utils/a/b;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a/b;->b()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/badlogic/gdx/a/d;->g:Z

    iget-boolean v1, p0, Lcom/badlogic/gdx/a/d;->f:Z

    if-eqz v1, :cond_4

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/badlogic/gdx/utils/d;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t load dependencies of asset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->j:Lcom/badlogic/gdx/utils/a/b;

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/badlogic/gdx/a/d;->f:Z

    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->d:Lcom/badlogic/gdx/utils/a/a;

    invoke-virtual {v0, p0}, Lcom/badlogic/gdx/utils/a/a;->a(Lcom/badlogic/gdx/utils/a/c;)Lcom/badlogic/gdx/utils/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/a/d;->j:Lcom/badlogic/gdx/utils/a/b;

    goto :goto_1

    :cond_2
    iget-boolean v1, p0, Lcom/badlogic/gdx/a/d;->f:Z

    if-eqz v1, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v3, v4}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v3

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/a/a/b;->b(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/a/d;->k:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->j:Lcom/badlogic/gdx/utils/a/b;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a/b;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    :try_start_1
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->j:Lcom/badlogic/gdx/utils/a/b;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a/b;->b()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/badlogic/gdx/utils/d;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t load asset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 5

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    check-cast v0, Lcom/badlogic/gdx/a/a/b;

    iget-boolean v1, p0, Lcom/badlogic/gdx/a/d;->g:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v2, v3}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v2

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3}, Lcom/badlogic/gdx/a/a/b;->a(Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)Lcom/badlogic/gdx/utils/a;

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/utils/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v0, v1, v2}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;Lcom/badlogic/gdx/utils/a;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v3, v4}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v3

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/a/a/b;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/a/d;->f:Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/a/d;->a:Lcom/badlogic/gdx/a/e;

    iget-object v2, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-direct {p0, v3, v4}, Lcom/badlogic/gdx/a/d;->a(Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/a/a;)Lcom/badlogic/gdx/c/a;

    move-result-object v3

    iget-object v4, p0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/a/a/b;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/a/c;)V

    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Z
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/a/d;->l:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/badlogic/gdx/a/d;->l:I

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->c:Lcom/badlogic/gdx/a/a/a;

    instance-of v0, v0, Lcom/badlogic/gdx/a/a/e;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/badlogic/gdx/a/d;->e()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/badlogic/gdx/a/d;->f()V

    :goto_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->k:Ljava/lang/Object;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public c()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/a/d;->k:Ljava/lang/Object;

    return-object v0
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/a/d;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
