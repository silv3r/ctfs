.class public Lcom/badlogic/gdx/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/b;


# instance fields
.field final a:Lcom/badlogic/gdx/utils/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i<",
            "Ljava/lang/Class;",
            "Lcom/badlogic/gdx/utils/i<",
            "Ljava/lang/String;",
            "Lcom/badlogic/gdx/a/f;",
            ">;>;"
        }
    .end annotation
.end field

.field final b:Lcom/badlogic/gdx/utils/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i<",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/badlogic/gdx/utils/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i<",
            "Ljava/lang/String;",
            "Lcom/badlogic/gdx/utils/a<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final d:Lcom/badlogic/gdx/utils/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/j<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/badlogic/gdx/utils/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i<",
            "Ljava/lang/Class;",
            "Lcom/badlogic/gdx/utils/i<",
            "Ljava/lang/String;",
            "Lcom/badlogic/gdx/a/a/a;",
            ">;>;"
        }
    .end annotation
.end field

.field final f:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/a/a;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lcom/badlogic/gdx/utils/a/a;

.field final h:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lcom/badlogic/gdx/a/d;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/badlogic/gdx/a/b;

.field j:I

.field k:I

.field l:I

.field m:Lcom/badlogic/gdx/utils/f;


# direct methods
.method private a(Lcom/badlogic/gdx/a/a;)V
    .locals 4

    iget-object v0, p1, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    iget-object v1, p1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/badlogic/gdx/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    new-instance v2, Lcom/badlogic/gdx/a/d;

    iget-object v3, p0, Lcom/badlogic/gdx/a/e;->g:Lcom/badlogic/gdx/utils/a/a;

    invoke-direct {v2, p0, p1, v0, v3}, Lcom/badlogic/gdx/a/d;-><init>(Lcom/badlogic/gdx/a/e;Lcom/badlogic/gdx/a/a;Lcom/badlogic/gdx/a/a/a;Lcom/badlogic/gdx/utils/a/a;)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget p1, p0, Lcom/badlogic/gdx/a/e;->l:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/a/e;->l:I

    return-void

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No loader for type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-static {p1}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/badlogic/gdx/a/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, p1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object p1, p2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    iget-object p1, p2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/a/e;->b(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Dependency already loaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    iget-object v0, p2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Class;

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/utils/i;

    iget-object v0, p2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/a/f;

    invoke-virtual {p1}, Lcom/badlogic/gdx/a/f;->a()V

    iget-object p1, p2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/a/e;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loading dependency: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/utils/f;->b(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/badlogic/gdx/a/e;->a(Lcom/badlogic/gdx/a/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    const-string v1, "Error loading asset."

    invoke-virtual {v0, v1, p1}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/a/d;

    iget-object v1, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-boolean v2, v0, Lcom/badlogic/gdx/a/d;->g:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/badlogic/gdx/a/d;->h:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->i:Lcom/badlogic/gdx/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->i:Lcom/badlogic/gdx/a/b;

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/a/b;->a(Lcom/badlogic/gdx/a/a;Ljava/lang/Throwable;)V

    return-void

    :cond_1
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    invoke-direct {v0, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    invoke-direct {v0, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/utils/a;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/a/f;

    invoke-virtual {v1}, Lcom/badlogic/gdx/a/f;->a()V

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/a/e;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/a;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/a/a;

    iget-object v1, v0, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/a/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already loaded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    iget-object v2, v0, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/utils/i;

    iget-object v2, v0, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/a/f;

    invoke-virtual {v1}, Lcom/badlogic/gdx/a/f;->a()V

    iget-object v1, v0, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/a/e;->d(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    iget-object v1, v1, Lcom/badlogic/gdx/a/c;->a:Lcom/badlogic/gdx/a/c$a;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    iget-object v1, v1, Lcom/badlogic/gdx/a/c;->a:Lcom/badlogic/gdx/a/c$a;

    iget-object v2, v0, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-interface {v1, p0, v2, v0}, Lcom/badlogic/gdx/a/c$a;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Ljava/lang/Class;)V

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/a/e;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/a/e;->j:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/utils/f;->b(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/a/e;->a(Lcom/badlogic/gdx/a/a;)V

    :goto_0
    return-void
.end method

.method private f()Z
    .locals 8

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/a/d;

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    iget-boolean v3, v0, Lcom/badlogic/gdx/a/d;->m:Z

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/badlogic/gdx/a/d;->b()Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :catch_0
    move-exception v3

    iput-boolean v2, v0, Lcom/badlogic/gdx/a/d;->m:Z

    iget-object v4, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-virtual {p0, v4, v3}, Lcom/badlogic/gdx/a/e;->a(Lcom/badlogic/gdx/a/a;Ljava/lang/RuntimeException;)V

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    if-ne v3, v2, :cond_2

    iget v3, p0, Lcom/badlogic/gdx/a/e;->j:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/badlogic/gdx/a/e;->j:I

    iput v1, p0, Lcom/badlogic/gdx/a/e;->l:I

    :cond_2
    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    iget-boolean v1, v0, Lcom/badlogic/gdx/a/d;->m:Z

    if-eqz v1, :cond_3

    return v2

    :cond_3
    iget-object v1, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Lcom/badlogic/gdx/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v1, v3, v4}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    iget-object v1, v1, Lcom/badlogic/gdx/a/c;->a:Lcom/badlogic/gdx/a/c$a;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v1, v1, Lcom/badlogic/gdx/a/a;->c:Lcom/badlogic/gdx/a/c;

    iget-object v1, v1, Lcom/badlogic/gdx/a/c;->a:Lcom/badlogic/gdx/a/c$a;

    iget-object v3, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v3, v3, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v4, v4, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-interface {v1, p0, v3, v4}, Lcom/badlogic/gdx/a/c$a;->a(Lcom/badlogic/gdx/a/e;Ljava/lang/String;Ljava/lang/Class;)V

    :cond_4
    invoke-static {}, Lcom/badlogic/gdx/utils/p;->a()J

    move-result-wide v3

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Loaded: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v6, v0, Lcom/badlogic/gdx/a/d;->e:J

    sub-long/2addr v3, v6

    long-to-float v3, v3

    const v4, 0x49742400    # 1000000.0f

    div-float/2addr v3, v4

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v3, "ms "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V

    return v2

    :cond_5
    return v1
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/String;)Lcom/badlogic/gdx/a/a/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/badlogic/gdx/a/a/a;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->e:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/utils/i;

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget v1, p1, Lcom/badlogic/gdx/utils/i;->a:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    goto :goto_1

    :cond_0
    if-nez p2, :cond_1

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/a/a/a;

    return-object p1

    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/i;->c()Lcom/badlogic/gdx/utils/i$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/i$a;->b()Lcom/badlogic/gdx/utils/i$a;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/utils/i$b;

    iget-object v3, v2, Lcom/badlogic/gdx/utils/i$b;->a:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v1, :cond_2

    iget-object v3, v2, Lcom/badlogic/gdx/utils/i$b;->a:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v2, Lcom/badlogic/gdx/utils/i$b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/badlogic/gdx/a/a/a;

    iget-object v1, v2, Lcom/badlogic/gdx/utils/i$b;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0

    :cond_3
    :goto_1
    return-object v0
.end method

.method public declared-synchronized a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i;->d()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i$c;->a()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/i;->d()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/badlogic/gdx/utils/i$c;->a()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/badlogic/gdx/a/f;

    const-class v5, Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/badlogic/gdx/a/f;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    if-eq v4, p1, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    :cond_2
    monitor-exit p0

    return-object v3

    :cond_3
    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected a(Lcom/badlogic/gdx/a/a;Ljava/lang/RuntimeException;)V
    .locals 0

    throw p2
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->firstElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/a/d;

    iget-object v2, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean v1, v0, Lcom/badlogic/gdx/a/d;->m:Z

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unload (from tasks): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/f;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    const/4 v3, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v2, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a/a;

    iget-object v2, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    :goto_1
    if-eq v0, v3, :cond_3

    iget v2, p0, Lcom/badlogic/gdx/a/e;->k:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/badlogic/gdx/a/e;->k:I

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/a;->b(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unload (from queue): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/f;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/a/f;

    invoke-virtual {v1}, Lcom/badlogic/gdx/a/f;->b()V

    invoke-virtual {v1}, Lcom/badlogic/gdx/a/f;->c()I

    move-result v2

    if-gtz v2, :cond_5

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unload (dispose): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/badlogic/gdx/utils/f;->b(Ljava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/a/f;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/badlogic/gdx/utils/b;

    if-eqz v2, :cond_4

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/a/f;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/utils/b;

    invoke-interface {v2}, Lcom/badlogic/gdx/utils/b;->c()V

    :cond_4
    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v2, p1}, Lcom/badlogic/gdx/utils/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v2, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unload (decrement): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/badlogic/gdx/utils/f;->b(Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/badlogic/gdx/a/e;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0, v2}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    invoke-virtual {v1}, Lcom/badlogic/gdx/a/f;->c()I

    move-result v0

    if-gtz v0, :cond_8

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    monitor-exit p0

    return-void

    :cond_9
    :try_start_3
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Asset not loaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Ljava/lang/String;I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/a/f;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/a/f;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance p2, Lcom/badlogic/gdx/utils/d;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Asset not loaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a(Ljava/lang/String;Lcom/badlogic/gdx/utils/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/a/a;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->d:Lcom/badlogic/gdx/utils/j;

    invoke-virtual {p2}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/a/a;

    iget-object v2, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/badlogic/gdx/utils/j;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/badlogic/gdx/utils/j;->a(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, v1}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;Lcom/badlogic/gdx/a/a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/j;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;Lcom/badlogic/gdx/a/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lcom/badlogic/gdx/a/c<",
            "TT;>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2, p1}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/badlogic/gdx/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    iget v0, v0, Lcom/badlogic/gdx/utils/a;->b:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iput v1, p0, Lcom/badlogic/gdx/a/e;->j:I

    iput v1, p0, Lcom/badlogic/gdx/a/e;->k:I

    iput v1, p0, Lcom/badlogic/gdx/a/e;->l:I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v2, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a/a;

    iget-object v3, v2, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    new-instance p3, Lcom/badlogic/gdx/utils/d;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Asset with name \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' already in preload queue, but has different type (expected: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", found: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v2, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-static {p1}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/a/d;

    iget-object v0, v0, Lcom/badlogic/gdx/a/d;->b:Lcom/badlogic/gdx/a/a;

    iget-object v2, v0, Lcom/badlogic/gdx/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    new-instance p3, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Asset with name \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' already in task list, but has different type (expected: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", found: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v0, Lcom/badlogic/gdx/a/a;->b:Ljava/lang/Class;

    invoke-static {p1}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_5
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_8

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    goto :goto_4

    :cond_7
    new-instance p3, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Asset with name \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' already loaded, but has different type (expected: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", found: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_8
    :goto_4
    iget v0, p0, Lcom/badlogic/gdx/a/e;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/a/e;->k:I

    new-instance v0, Lcom/badlogic/gdx/a/a;

    invoke-direct {v0, p1, p2, p3}, Lcom/badlogic/gdx/a/a;-><init>(Ljava/lang/String;Ljava/lang/Class;Lcom/badlogic/gdx/a/c;)V

    iget-object p1, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Queued: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_9
    :try_start_1
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No loader for type: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lcom/badlogic/gdx/utils/b/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1, p2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/i;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/i;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/i;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, p2, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance p2, Lcom/badlogic/gdx/a/f;

    invoke-direct {p2, p3}, Lcom/badlogic/gdx/a/f;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, p2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public declared-synchronized a()Z
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/badlogic/gdx/a/e;->e()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    monitor-exit p0

    return v1

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/badlogic/gdx/a/e;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v2

    :try_start_2
    invoke-direct {p0, v2}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/Throwable;)V

    iget-object v2, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    monitor-exit p0

    return v0

    :goto_1
    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    const-string v1, "Waiting for loading to complete..."

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/a/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/badlogic/gdx/utils/a/d;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    const-string v1, "Loading complete."

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized b(Ljava/lang/String;)Z
    .locals 1

    monitor-enter p0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return p1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->d(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c(Ljava/lang/String;)I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/a/f;

    invoke-virtual {p1}, Lcom/badlogic/gdx/a/f;->c()I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :cond_0
    :try_start_1
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Asset not loaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->m:Lcom/badlogic/gdx/utils/f;

    const-string v1, "Disposing."

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/f;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/a/e;->d()V

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->g:Lcom/badlogic/gdx/utils/a/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a/a;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->c()V

    :goto_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/a/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    iget v1, v1, Lcom/badlogic/gdx/utils/i;->a:I

    const/4 v2, 0x0

    if-lez v1, :cond_7

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/h;->a()V

    iget-object v1, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/i;->d()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/i$c;->b()Lcom/badlogic/gdx/utils/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v4, v2}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/badlogic/gdx/utils/a;

    if-nez v4, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v5, v2}, Lcom/badlogic/gdx/utils/h;->b(Ljava/lang/Object;I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v5, v6}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/badlogic/gdx/utils/h;->b(Ljava/lang/Object;I)I

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {p0, v3}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->a:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i;->a()V

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->b:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i;->a()V

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i;->a()V

    iput v2, p0, Lcom/badlogic/gdx/a/e;->j:I

    iput v2, p0, Lcom/badlogic/gdx/a/e;->k:I

    iput v2, p0, Lcom/badlogic/gdx/a/e;->l:I

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->f:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/a/e;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
