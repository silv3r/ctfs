.class public Lcom/badlogic/gdx/a/f;
.super Ljava/lang/Object;


# instance fields
.field a:Ljava/lang/Object;

.field b:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/a/f;->b:I

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/badlogic/gdx/a/f;->a:Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Object must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    iget-object p1, p0, Lcom/badlogic/gdx/a/f;->a:Ljava/lang/Object;

    return-object p1
.end method

.method public a()V
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/a/f;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/a/f;->b:I

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/badlogic/gdx/a/f;->b:I

    return-void
.end method

.method public b()V
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/a/f;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/a/f;->b:I

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/a/f;->b:I

    return v0
.end method
