.class public Lcom/badlogic/gdx/math/f;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/badlogic/gdx/math/f;

.field public static final b:Lcom/badlogic/gdx/math/f;

.field public static final c:Lcom/badlogic/gdx/math/f;


# instance fields
.field public d:F

.field public e:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/badlogic/gdx/math/f;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/badlogic/gdx/math/f;-><init>(FF)V

    sput-object v0, Lcom/badlogic/gdx/math/f;->a:Lcom/badlogic/gdx/math/f;

    new-instance v0, Lcom/badlogic/gdx/math/f;

    invoke-direct {v0, v2, v1}, Lcom/badlogic/gdx/math/f;-><init>(FF)V

    sput-object v0, Lcom/badlogic/gdx/math/f;->b:Lcom/badlogic/gdx/math/f;

    new-instance v0, Lcom/badlogic/gdx/math/f;

    invoke-direct {v0, v2, v2}, Lcom/badlogic/gdx/math/f;-><init>(FF)V

    sput-object v0, Lcom/badlogic/gdx/math/f;->c:Lcom/badlogic/gdx/math/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/badlogic/gdx/math/f;->d:F

    iput p2, p0, Lcom/badlogic/gdx/math/f;->e:F

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    check-cast p1, Lcom/badlogic/gdx/math/f;

    iget v2, p0, Lcom/badlogic/gdx/math/f;->d:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    iget v3, p1, Lcom/badlogic/gdx/math/f;->d:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v3

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    iget v2, p0, Lcom/badlogic/gdx/math/f;->e:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    iget p1, p1, Lcom/badlogic/gdx/math/f;->e:F

    invoke-static {p1}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result p1

    if-eq v2, p1, :cond_4

    return v1

    :cond_4
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/math/f;->d:F

    invoke-static {v0}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v0

    const/16 v1, 0x1f

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/badlogic/gdx/math/f;->e:F

    invoke-static {v1}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/f;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/f;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
