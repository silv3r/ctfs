.class public final Lcom/badlogic/gdx/math/h;
.super Ljava/lang/Object;


# instance fields
.field a:[F

.field b:I

.field c:I

.field d:F

.field e:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/math/h;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/math/h;->d:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/math/h;->e:Z

    new-array p1, p1, [F

    iput-object p1, p0, Lcom/badlogic/gdx/math/h;->a:[F

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/math/h;->b:I

    iput v0, p0, Lcom/badlogic/gdx/math/h;->c:I

    :goto_0
    iget-object v1, p0, Lcom/badlogic/gdx/math/h;->a:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/math/h;->a:[F

    const/4 v2, 0x0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/math/h;->e:Z

    return-void
.end method

.method public a(F)V
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/math/h;->b:I

    iget-object v1, p0, Lcom/badlogic/gdx/math/h;->a:[F

    array-length v1, v1

    const/4 v2, 0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/math/h;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/badlogic/gdx/math/h;->b:I

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/math/h;->a:[F

    iget v1, p0, Lcom/badlogic/gdx/math/h;->c:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/badlogic/gdx/math/h;->c:I

    aput p1, v0, v1

    iget p1, p0, Lcom/badlogic/gdx/math/h;->c:I

    iget-object v0, p0, Lcom/badlogic/gdx/math/h;->a:[F

    array-length v0, v0

    sub-int/2addr v0, v2

    if-le p1, v0, :cond_1

    const/4 p1, 0x0

    iput p1, p0, Lcom/badlogic/gdx/math/h;->c:I

    :cond_1
    iput-boolean v2, p0, Lcom/badlogic/gdx/math/h;->e:Z

    return-void
.end method
