.class public Lcom/badlogic/gdx/math/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Lcom/badlogic/gdx/math/g;

.field public b:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/math/c;->a:Lcom/badlogic/gdx/math/g;

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/math/c;->b:F

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/math/g;F)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/math/c;->a:Lcom/badlogic/gdx/math/g;

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/math/c;->b:F

    iget-object v0, p0, Lcom/badlogic/gdx/math/c;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    iput p2, p0, Lcom/badlogic/gdx/math/c;->b:F

    return-void
.end method


# virtual methods
.method public a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/math/c;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/badlogic/gdx/math/g;->c(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object v0

    iget v1, p2, Lcom/badlogic/gdx/math/g;->a:F

    iget v2, p3, Lcom/badlogic/gdx/math/g;->a:F

    sub-float/2addr v1, v2

    iget v2, p2, Lcom/badlogic/gdx/math/g;->b:F

    iget v3, p3, Lcom/badlogic/gdx/math/g;->b:F

    sub-float/2addr v2, v3

    iget p2, p2, Lcom/badlogic/gdx/math/g;->c:F

    iget p3, p3, Lcom/badlogic/gdx/math/g;->c:F

    sub-float/2addr p2, p3

    invoke-virtual {v0, v1, v2, p2}, Lcom/badlogic/gdx/math/g;->d(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p2

    invoke-virtual {p2}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    iget-object p2, p0, Lcom/badlogic/gdx/math/c;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/math/g;->d(Lcom/badlogic/gdx/math/g;)F

    move-result p1

    neg-float p1, p1

    iput p1, p0, Lcom/badlogic/gdx/math/c;->b:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/math/c;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v1}, Lcom/badlogic/gdx/math/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/c;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
