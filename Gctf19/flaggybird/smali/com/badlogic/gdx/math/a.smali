.class public Lcom/badlogic/gdx/math/a;
.super Ljava/lang/Object;


# static fields
.field protected static final a:[Lcom/badlogic/gdx/math/g;

.field protected static final b:[F

.field private static final f:Lcom/badlogic/gdx/math/g;


# instance fields
.field public final c:[Lcom/badlogic/gdx/math/c;

.field public final d:[Lcom/badlogic/gdx/math/g;

.field protected final e:[F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/badlogic/gdx/math/g;

    new-instance v1, Lcom/badlogic/gdx/math/g;

    const/high16 v2, -0x40800000    # -1.0f

    invoke-direct {v1, v2, v2, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v3, 0x0

    aput-object v1, v0, v3

    new-instance v1, Lcom/badlogic/gdx/math/g;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v4, v2, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v5, 0x1

    aput-object v1, v0, v5

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1, v4, v4, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v5, 0x2

    aput-object v1, v0, v5

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1, v2, v4, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v5, 0x3

    aput-object v1, v0, v5

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1, v2, v2, v4}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v5, 0x4

    aput-object v1, v0, v5

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1, v4, v2, v4}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v5, 0x5

    aput-object v1, v0, v5

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1, v4, v4, v4}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v5, 0x6

    aput-object v1, v0, v5

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1, v2, v4, v4}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/badlogic/gdx/math/a;->a:[Lcom/badlogic/gdx/math/g;

    const/16 v0, 0x18

    new-array v0, v0, [F

    sput-object v0, Lcom/badlogic/gdx/math/a;->b:[F

    sget-object v0, Lcom/badlogic/gdx/math/a;->a:[Lcom/badlogic/gdx/math/g;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    sget-object v5, Lcom/badlogic/gdx/math/a;->b:[F

    add-int/lit8 v6, v2, 0x1

    iget v7, v4, Lcom/badlogic/gdx/math/g;->a:F

    aput v7, v5, v2

    sget-object v2, Lcom/badlogic/gdx/math/a;->b:[F

    add-int/lit8 v5, v6, 0x1

    iget v7, v4, Lcom/badlogic/gdx/math/g;->b:F

    aput v7, v2, v6

    sget-object v2, Lcom/badlogic/gdx/math/a;->b:[F

    add-int/lit8 v6, v5, 0x1

    iget v4, v4, Lcom/badlogic/gdx/math/g;->c:F

    aput v4, v2, v5

    add-int/lit8 v3, v3, 0x1

    move v2, v6

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/a;->f:Lcom/badlogic/gdx/math/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    new-array v1, v0, [Lcom/badlogic/gdx/math/c;

    iput-object v1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/badlogic/gdx/math/g;

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v4, 0x2

    aput-object v2, v1, v4

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v4, 0x3

    aput-object v2, v1, v4

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v4, 0x4

    aput-object v2, v1, v4

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v4, 0x5

    aput-object v2, v1, v4

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    aput-object v2, v1, v0

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v4, 0x7

    aput-object v2, v1, v4

    iput-object v1, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/16 v1, 0x18

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/badlogic/gdx/math/a;->e:[F

    :goto_0
    if-ge v3, v0, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    new-instance v2, Lcom/badlogic/gdx/math/c;

    new-instance v4, Lcom/badlogic/gdx/math/g;

    invoke-direct {v4}, Lcom/badlogic/gdx/math/g;-><init>()V

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lcom/badlogic/gdx/math/c;-><init>(Lcom/badlogic/gdx/math/g;F)V

    aput-object v2, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/badlogic/gdx/math/Matrix4;)V
    .locals 10

    sget-object v0, Lcom/badlogic/gdx/math/a;->b:[F

    iget-object v1, p0, Lcom/badlogic/gdx/math/a;->e:[F

    sget-object v2, Lcom/badlogic/gdx/math/a;->b:[F

    array-length v2, v2

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p1, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->e:[F

    const/16 v1, 0x8

    const/4 v2, 0x3

    invoke-static {p1, v0, v3, v1, v2}, Lcom/badlogic/gdx/math/Matrix4;->prj([F[FIII)V

    const/4 p1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge p1, v1, :cond_0

    iget-object v4, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v4, v4, p1

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->e:[F

    add-int/lit8 v6, v0, 0x1

    aget v0, v5, v0

    iput v0, v4, Lcom/badlogic/gdx/math/g;->a:F

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->e:[F

    add-int/lit8 v5, v6, 0x1

    aget v0, v0, v6

    iput v0, v4, Lcom/badlogic/gdx/math/g;->b:F

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->e:[F

    add-int/lit8 v6, v5, 0x1

    aget v0, v0, v5

    iput v0, v4, Lcom/badlogic/gdx/math/g;->c:F

    add-int/lit8 p1, p1, 0x1

    move v0, v6

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    aget-object p1, p1, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v4, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v4, v4, v3

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/4 v6, 0x2

    aget-object v5, v5, v6

    invoke-virtual {p1, v0, v4, v5}, Lcom/badlogic/gdx/math/c;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    iget-object p1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    aget-object p1, p1, v1

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/4 v4, 0x4

    aget-object v0, v0, v4

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/4 v7, 0x5

    aget-object v5, v5, v7

    iget-object v8, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/4 v9, 0x7

    aget-object v8, v8, v9

    invoke-virtual {p1, v0, v5, v8}, Lcom/badlogic/gdx/math/c;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    iget-object p1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    aget-object p1, p1, v6

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v0, v0, v3

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v5, v5, v4

    iget-object v8, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v8, v8, v2

    invoke-virtual {p1, v0, v5, v8}, Lcom/badlogic/gdx/math/c;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    iget-object p1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    aget-object p1, p1, v2

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v0, v0, v7

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v5, v5, v1

    iget-object v8, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    const/4 v9, 0x6

    aget-object v8, v8, v9

    invoke-virtual {p1, v0, v5, v8}, Lcom/badlogic/gdx/math/c;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    iget-object p1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    aget-object p1, p1, v4

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v0, v0, v6

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v2, v5, v2

    iget-object v5, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v5, v5, v9

    invoke-virtual {p1, v0, v2, v5}, Lcom/badlogic/gdx/math/c;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    iget-object p1, p0, Lcom/badlogic/gdx/math/a;->c:[Lcom/badlogic/gdx/math/c;

    aget-object p1, p1, v7

    iget-object v0, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v0, v0, v4

    iget-object v2, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/badlogic/gdx/math/a;->d:[Lcom/badlogic/gdx/math/g;

    aget-object v1, v3, v1

    invoke-virtual {p1, v0, v2, v1}, Lcom/badlogic/gdx/math/c;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    return-void
.end method
