.class public Lcom/badlogic/gdx/math/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static c:Lcom/badlogic/gdx/math/g;


# instance fields
.field public final a:Lcom/badlogic/gdx/math/g;

.field public final b:Lcom/badlogic/gdx/math/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/a/a;->c:Lcom/badlogic/gdx/math/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    iget-object v0, p0, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    iget-object p1, p0, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lcom/badlogic/gdx/math/a/a;

    iget-object v2, p0, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    iget-object v3, p1, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v2, v3}, Lcom/badlogic/gdx/math/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    iget-object p1, p1, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v2, p1}, Lcom/badlogic/gdx/math/g;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/math/g;->hashCode()I

    move-result v0

    const/16 v1, 0x49

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x49

    iget-object v1, p0, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v1}, Lcom/badlogic/gdx/math/g;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ray ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/a/a;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/a/a;->b:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
