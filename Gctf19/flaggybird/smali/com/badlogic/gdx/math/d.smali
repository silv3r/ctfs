.class public Lcom/badlogic/gdx/math/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static e:Lcom/badlogic/gdx/math/d;

.field private static f:Lcom/badlogic/gdx/math/d;


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/badlogic/gdx/math/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/badlogic/gdx/math/d;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/math/d;->e:Lcom/badlogic/gdx/math/d;

    new-instance v0, Lcom/badlogic/gdx/math/d;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/badlogic/gdx/math/d;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/math/d;->f:Lcom/badlogic/gdx/math/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lcom/badlogic/gdx/math/d;->a()Lcom/badlogic/gdx/math/d;

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/badlogic/gdx/math/d;->a(FFFF)Lcom/badlogic/gdx/math/d;

    return-void
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/math/d;
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v0, v0, v1}, Lcom/badlogic/gdx/math/d;->a(FFFF)Lcom/badlogic/gdx/math/d;

    move-result-object v0

    return-object v0
.end method

.method public a(FFFF)Lcom/badlogic/gdx/math/d;
    .locals 0

    iput p1, p0, Lcom/badlogic/gdx/math/d;->a:F

    iput p2, p0, Lcom/badlogic/gdx/math/d;->b:F

    iput p3, p0, Lcom/badlogic/gdx/math/d;->c:F

    iput p4, p0, Lcom/badlogic/gdx/math/d;->d:F

    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    :cond_1
    instance-of v2, p1, Lcom/badlogic/gdx/math/d;

    if-nez v2, :cond_2

    return v1

    :cond_2
    check-cast p1, Lcom/badlogic/gdx/math/d;

    iget v2, p0, Lcom/badlogic/gdx/math/d;->d:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v2

    iget v3, p1, Lcom/badlogic/gdx/math/d;->d:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/badlogic/gdx/math/d;->a:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v2

    iget v3, p1, Lcom/badlogic/gdx/math/d;->a:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/badlogic/gdx/math/d;->b:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v2

    iget v3, p1, Lcom/badlogic/gdx/math/d;->b:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/badlogic/gdx/math/d;->c:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v2

    iget p1, p1, Lcom/badlogic/gdx/math/d;->c:F

    invoke-static {p1}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result p1

    if-ne v2, p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/math/d;->d:F

    invoke-static {v0}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v0

    const/16 v1, 0x1f

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/badlogic/gdx/math/d;->a:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/badlogic/gdx/math/d;->b:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/badlogic/gdx/math/d;->c:F

    invoke-static {v1}, Lcom/badlogic/gdx/utils/g;->b(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/d;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/d;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/d;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/d;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
