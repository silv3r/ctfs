.class public Lcom/badlogic/gdx/math/Matrix4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static b:Lcom/badlogic/gdx/math/d;

.field static c:Lcom/badlogic/gdx/math/d;

.field static final d:Lcom/badlogic/gdx/math/g;

.field static final e:Lcom/badlogic/gdx/math/g;

.field static final f:Lcom/badlogic/gdx/math/g;

.field static final g:Lcom/badlogic/gdx/math/g;

.field static final h:Lcom/badlogic/gdx/math/Matrix4;

.field static final i:Lcom/badlogic/gdx/math/g;

.field static final j:Lcom/badlogic/gdx/math/g;

.field static final k:Lcom/badlogic/gdx/math/g;

.field private static final l:[F


# instance fields
.field public final a:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [F

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->l:[F

    new-instance v0, Lcom/badlogic/gdx/math/d;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/d;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->b:Lcom/badlogic/gdx/math/d;

    new-instance v0, Lcom/badlogic/gdx/math/d;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/d;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->c:Lcom/badlogic/gdx/math/d;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->d:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->f:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->g:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->h:Lcom/badlogic/gdx/math/Matrix4;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->i:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->j:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/Matrix4;->k:Lcom/badlogic/gdx/math/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x5

    aput v1, v0, v2

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xa

    aput v1, v0, v2

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xf

    aput v1, v0, v2

    return-void
.end method

.method public static native inv([F)Z
.end method

.method public static native mul([F[F)V
.end method

.method public static native prj([F[FIII)V
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/math/Matrix4;
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x0

    const/4 v3, 0x4

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0x8

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0xc

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v3, 0x1

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v3, 0x5

    aput v1, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0x9

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0xd

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v3, 0x2

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v3, 0x6

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0xa

    aput v1, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0xe

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v3, 0x3

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v3, 0x7

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v3, 0xb

    aput v2, v0, v3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xf

    aput v1, v0, v2

    return-object p0
.end method

.method public a(FFF)Lcom/badlogic/gdx/math/Matrix4;
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/math/Matrix4;->a()Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v1, 0xc

    aput p1, v0, v1

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v0, 0xd

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p2, 0xe

    aput p3, p1, p2

    return-object p0
.end method

.method public a(FFFF)Lcom/badlogic/gdx/math/Matrix4;
    .locals 7

    add-float v2, p1, p3

    add-float v4, p2, p4

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    move v1, p1

    move v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/badlogic/gdx/math/Matrix4;->a(FFFFFF)Lcom/badlogic/gdx/math/Matrix4;

    return-object p0
.end method

.method public a(FFFFFF)Lcom/badlogic/gdx/math/Matrix4;
    .locals 6

    invoke-virtual {p0}, Lcom/badlogic/gdx/math/Matrix4;->a()Lcom/badlogic/gdx/math/Matrix4;

    sub-float v0, p2, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v2, v1, v0

    sub-float v3, p4, p3

    div-float/2addr v1, v3

    sub-float v4, p6, p5

    const/high16 v5, -0x40000000    # -2.0f

    div-float/2addr v5, v4

    add-float/2addr p2, p1

    neg-float p1, p2

    div-float/2addr p1, v0

    add-float/2addr p4, p3

    neg-float p2, p4

    div-float/2addr p2, v3

    add-float/2addr p6, p5

    neg-float p3, p6

    div-float/2addr p3, v4

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p5, 0x0

    aput v2, p4, p5

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p5, 0x0

    const/4 p6, 0x1

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p6, 0x2

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p6, 0x3

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p6, 0x4

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p6, 0x5

    aput v1, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p6, 0x6

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 p6, 0x7

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p6, 0x8

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p6, 0x9

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p6, 0xa

    aput v5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p6, 0xb

    aput p5, p4, p6

    iget-object p4, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p5, 0xc

    aput p1, p4, p5

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p4, 0xd

    aput p2, p1, p4

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p2, 0xe

    aput p3, p1, p2

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 p2, 0xf

    const/high16 p3, 0x3f800000    # 1.0f

    aput p3, p1, p2

    return-object p0
.end method

.method public a(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;
    .locals 0

    iget-object p1, p1, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/math/Matrix4;->a([F)Lcom/badlogic/gdx/math/Matrix4;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/Matrix4;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/math/Matrix4;->d:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    sget-object v0, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    sget-object p1, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/math/g;->e(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    sget-object p1, Lcom/badlogic/gdx/math/Matrix4;->f:Lcom/badlogic/gdx/math/g;

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->d:Lcom/badlogic/gdx/math/g;

    invoke-virtual {p1, p2}, Lcom/badlogic/gdx/math/g;->e(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/math/g;->b()Lcom/badlogic/gdx/math/g;

    invoke-virtual {p0}, Lcom/badlogic/gdx/math/Matrix4;->a()Lcom/badlogic/gdx/math/Matrix4;

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->a:F

    const/4 v0, 0x0

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->b:F

    const/4 v0, 0x4

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->e:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->c:F

    const/16 v0, 0x8

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->f:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->a:F

    const/4 v0, 0x1

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->f:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->b:F

    const/4 v0, 0x5

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->f:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->c:F

    const/16 v0, 0x9

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->d:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->a:F

    neg-float p2, p2

    const/4 v0, 0x2

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->d:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->b:F

    neg-float p2, p2

    const/4 v0, 0x6

    aput p2, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->d:Lcom/badlogic/gdx/math/g;

    iget p2, p2, Lcom/badlogic/gdx/math/g;->c:F

    neg-float p2, p2

    const/16 v0, 0xa

    aput p2, p1, v0

    return-object p0
.end method

.method public a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/Matrix4;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/math/Matrix4;->g:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, p2}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/badlogic/gdx/math/g;->c(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->g:Lcom/badlogic/gdx/math/g;

    invoke-virtual {p0, p2, p3}, Lcom/badlogic/gdx/math/Matrix4;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/Matrix4;

    sget-object p2, Lcom/badlogic/gdx/math/Matrix4;->h:Lcom/badlogic/gdx/math/Matrix4;

    iget p3, p1, Lcom/badlogic/gdx/math/g;->a:F

    neg-float p3, p3

    iget v0, p1, Lcom/badlogic/gdx/math/g;->b:F

    neg-float v0, v0

    iget p1, p1, Lcom/badlogic/gdx/math/g;->c:F

    neg-float p1, p1

    invoke-virtual {p2, p3, v0, p1}, Lcom/badlogic/gdx/math/Matrix4;->a(FFF)Lcom/badlogic/gdx/math/Matrix4;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/math/Matrix4;->b(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;

    return-object p0
.end method

.method public a([F)Lcom/badlogic/gdx/math/Matrix4;
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    array-length v1, v1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p0
.end method

.method public b(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    iget-object p1, p1, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    invoke-static {v0, p1}, Lcom/badlogic/gdx/math/Matrix4;->mul([F[F)V

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0x8

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xc

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]\n["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x5

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0x9

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xd

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]\n["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xa

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xe

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]\n["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x7

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xb

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/16 v2, 0xf

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
