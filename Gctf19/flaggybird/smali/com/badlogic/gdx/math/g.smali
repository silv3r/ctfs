.class public Lcom/badlogic/gdx/math/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final d:Lcom/badlogic/gdx/math/g;

.field public static final e:Lcom/badlogic/gdx/math/g;

.field public static final f:Lcom/badlogic/gdx/math/g;

.field public static final g:Lcom/badlogic/gdx/math/g;

.field private static final h:Lcom/badlogic/gdx/math/Matrix4;


# instance fields
.field public a:F

.field public b:F

.field public c:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/badlogic/gdx/math/g;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    sput-object v0, Lcom/badlogic/gdx/math/g;->d:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0, v2, v1, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    sput-object v0, Lcom/badlogic/gdx/math/g;->e:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0, v2, v2, v1}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    sput-object v0, Lcom/badlogic/gdx/math/g;->f:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0, v2, v2, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    sput-object v0, Lcom/badlogic/gdx/math/g;->g:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/math/g;->h:Lcom/badlogic/gdx/math/Matrix4;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    return-void
.end method


# virtual methods
.method public a()F
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/math/g;->a:F

    iget v1, p0, Lcom/badlogic/gdx/math/g;->a:F

    mul-float v0, v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->b:F

    iget v2, p0, Lcom/badlogic/gdx/math/g;->b:F

    mul-float v1, v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    iget v2, p0, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public a(F)Lcom/badlogic/gdx/math/g;
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/math/g;->a:F

    mul-float v0, v0, p1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->b:F

    mul-float v1, v1, p1

    iget v2, p0, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v2, v2, p1

    invoke-virtual {p0, v0, v1, v2}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public a(FFF)Lcom/badlogic/gdx/math/g;
    .locals 0

    iput p1, p0, Lcom/badlogic/gdx/math/g;->a:F

    iput p2, p0, Lcom/badlogic/gdx/math/g;->b:F

    iput p3, p0, Lcom/badlogic/gdx/math/g;->c:F

    return-object p0
.end method

.method public a(Lcom/badlogic/gdx/math/f;F)Lcom/badlogic/gdx/math/g;
    .locals 1

    iget v0, p1, Lcom/badlogic/gdx/math/f;->d:F

    iget p1, p1, Lcom/badlogic/gdx/math/f;->e:F

    invoke-virtual {p0, v0, p1, p2}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;
    .locals 2

    iget v0, p1, Lcom/badlogic/gdx/math/g;->a:F

    iget v1, p1, Lcom/badlogic/gdx/math/g;->b:F

    iget p1, p1, Lcom/badlogic/gdx/math/g;->c:F

    invoke-virtual {p0, v0, v1, p1}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public b()Lcom/badlogic/gdx/math/g;
    .locals 4

    invoke-virtual {p0}, Lcom/badlogic/gdx/math/g;->a()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v1

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    div-float/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/math/g;->a(F)Lcom/badlogic/gdx/math/g;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    return-object p0
.end method

.method public b(FFF)Lcom/badlogic/gdx/math/g;
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/math/g;->a:F

    add-float/2addr v0, p1

    iget p1, p0, Lcom/badlogic/gdx/math/g;->b:F

    add-float/2addr p1, p2

    iget p2, p0, Lcom/badlogic/gdx/math/g;->c:F

    add-float/2addr p2, p3

    invoke-virtual {p0, v0, p1, p2}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public b(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;
    .locals 2

    iget v0, p1, Lcom/badlogic/gdx/math/g;->a:F

    iget v1, p1, Lcom/badlogic/gdx/math/g;->b:F

    iget p1, p1, Lcom/badlogic/gdx/math/g;->c:F

    invoke-virtual {p0, v0, v1, p1}, Lcom/badlogic/gdx/math/g;->b(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public c(FFF)Lcom/badlogic/gdx/math/g;
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/math/g;->a:F

    sub-float/2addr v0, p1

    iget p1, p0, Lcom/badlogic/gdx/math/g;->b:F

    sub-float/2addr p1, p2

    iget p2, p0, Lcom/badlogic/gdx/math/g;->c:F

    sub-float/2addr p2, p3

    invoke-virtual {p0, v0, p1, p2}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public c(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;
    .locals 2

    iget v0, p1, Lcom/badlogic/gdx/math/g;->a:F

    iget v1, p1, Lcom/badlogic/gdx/math/g;->b:F

    iget p1, p1, Lcom/badlogic/gdx/math/g;->c:F

    invoke-virtual {p0, v0, v1, p1}, Lcom/badlogic/gdx/math/g;->c(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public d(Lcom/badlogic/gdx/math/g;)F
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/math/g;->a:F

    iget v1, p1, Lcom/badlogic/gdx/math/g;->a:F

    mul-float v0, v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->b:F

    iget v2, p1, Lcom/badlogic/gdx/math/g;->b:F

    mul-float v1, v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    iget p1, p1, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v1, v1, p1

    add-float/2addr v0, v1

    return v0
.end method

.method public d(FFF)Lcom/badlogic/gdx/math/g;
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/math/g;->b:F

    mul-float v0, v0, p3

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v1, v1, p2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v1, v1, p1

    iget v2, p0, Lcom/badlogic/gdx/math/g;->a:F

    mul-float v2, v2, p3

    sub-float/2addr v1, v2

    iget p3, p0, Lcom/badlogic/gdx/math/g;->a:F

    mul-float p3, p3, p2

    iget p2, p0, Lcom/badlogic/gdx/math/g;->b:F

    mul-float p2, p2, p1

    sub-float/2addr p3, p2

    invoke-virtual {p0, v0, v1, p3}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public e(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/math/g;->b:F

    iget v1, p1, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v0, v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    iget v2, p1, Lcom/badlogic/gdx/math/g;->b:F

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    iget v2, p1, Lcom/badlogic/gdx/math/g;->a:F

    mul-float v1, v1, v2

    iget v2, p0, Lcom/badlogic/gdx/math/g;->a:F

    iget v3, p1, Lcom/badlogic/gdx/math/g;->c:F

    mul-float v2, v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/badlogic/gdx/math/g;->a:F

    iget v3, p1, Lcom/badlogic/gdx/math/g;->b:F

    mul-float v2, v2, v3

    iget v3, p0, Lcom/badlogic/gdx/math/g;->b:F

    iget p1, p1, Lcom/badlogic/gdx/math/g;->a:F

    mul-float v3, v3, p1

    sub-float/2addr v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/badlogic/gdx/math/g;->a(FFF)Lcom/badlogic/gdx/math/g;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    check-cast p1, Lcom/badlogic/gdx/math/g;

    iget v2, p0, Lcom/badlogic/gdx/math/g;->a:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    iget v3, p1, Lcom/badlogic/gdx/math/g;->a:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v3

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    iget v2, p0, Lcom/badlogic/gdx/math/g;->b:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    iget v3, p1, Lcom/badlogic/gdx/math/g;->b:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v3

    if-eq v2, v3, :cond_4

    return v1

    :cond_4
    iget v2, p0, Lcom/badlogic/gdx/math/g;->c:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    iget p1, p1, Lcom/badlogic/gdx/math/g;->c:F

    invoke-static {p1}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result p1

    if-eq v2, p1, :cond_5

    return v1

    :cond_5
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/math/g;->a:F

    invoke-static {v0}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v0

    const/16 v1, 0x1f

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/badlogic/gdx/math/g;->b:F

    invoke-static {v2}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    invoke-static {v1}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/g;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/g;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/badlogic/gdx/math/g;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
