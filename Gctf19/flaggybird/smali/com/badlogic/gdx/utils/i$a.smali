.class public Lcom/badlogic/gdx/utils/i$a;
.super Lcom/badlogic/gdx/utils/i$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/badlogic/gdx/utils/i$d<",
        "TK;TV;",
        "Lcom/badlogic/gdx/utils/i$b<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field a:Lcom/badlogic/gdx/utils/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i$b<",
            "TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/i<",
            "TK;TV;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i$d;-><init>(Lcom/badlogic/gdx/utils/i;)V

    new-instance p1, Lcom/badlogic/gdx/utils/i$b;

    invoke-direct {p1}, Lcom/badlogic/gdx/utils/i$b;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/utils/i$a;->a:Lcom/badlogic/gdx/utils/i$b;

    return-void
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/utils/i$b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/i$b<",
            "TK;TV;>;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$a;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$a;->c:Lcom/badlogic/gdx/utils/i;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i$a;->a:Lcom/badlogic/gdx/utils/i$b;

    iget v2, p0, Lcom/badlogic/gdx/utils/i$a;->d:I

    aget-object v0, v0, v2

    iput-object v0, v1, Lcom/badlogic/gdx/utils/i$b;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$a;->a:Lcom/badlogic/gdx/utils/i$b;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i$a;->c:Lcom/badlogic/gdx/utils/i;

    iget-object v1, v1, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/i$a;->d:I

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/badlogic/gdx/utils/i$b;->b:Ljava/lang/Object;

    iget v0, p0, Lcom/badlogic/gdx/utils/i$a;->d:I

    iput v0, p0, Lcom/badlogic/gdx/utils/i$a;->e:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$a;->d()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$a;->a:Lcom/badlogic/gdx/utils/i$b;

    return-object v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public b()Lcom/badlogic/gdx/utils/i$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/i$a<",
            "TK;TV;>;"
        }
    .end annotation

    return-object p0
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/badlogic/gdx/utils/i$d;->c()V

    return-void
.end method

.method public hasNext()Z
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$a;->b:Z

    return v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$a;->b()Lcom/badlogic/gdx/utils/i$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$a;->a()Lcom/badlogic/gdx/utils/i$b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove()V
    .locals 0

    invoke-super {p0}, Lcom/badlogic/gdx/utils/i$d;->remove()V

    return-void
.end method
