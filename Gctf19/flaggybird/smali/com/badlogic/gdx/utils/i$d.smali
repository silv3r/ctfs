.class abstract Lcom/badlogic/gdx/utils/i$d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        "I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TI;>;",
        "Ljava/util/Iterator<",
        "TI;>;"
    }
.end annotation


# instance fields
.field public b:Z

.field final c:Lcom/badlogic/gdx/utils/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field d:I

.field e:I

.field f:Z


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/i<",
            "TK;TV;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/utils/i$d;->f:Z

    iput-object p1, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$d;->c()V

    return-void
.end method


# virtual methods
.method public c()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    iput v0, p0, Lcom/badlogic/gdx/utils/i$d;->d:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$d;->d()V

    return-void
.end method

.method d()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/utils/i$d;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget v1, v1, Lcom/badlogic/gdx/utils/i;->d:I

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget v2, v2, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v1, v2

    :cond_0
    iget v2, p0, Lcom/badlogic/gdx/utils/i$d;->d:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lcom/badlogic/gdx/utils/i$d;->d:I

    if-ge v2, v1, :cond_1

    iget v2, p0, Lcom/badlogic/gdx/utils/i$d;->d:I

    aget-object v2, v0, v2

    if-eqz v2, :cond_0

    iput-boolean v3, p0, Lcom/badlogic/gdx/utils/i$d;->b:Z

    :cond_1
    return-void
.end method

.method public remove()V
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget v1, v1, Lcom/badlogic/gdx/utils/i;->d:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget v1, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/i;->a(I)V

    iget v0, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/i$d;->d:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$d;->d()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    aput-object v2, v0, v1

    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/i$d;->e:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$d;->c:Lcom/badlogic/gdx/utils/i;

    iget v1, v0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/badlogic/gdx/utils/i;->a:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "next must be called before remove."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
