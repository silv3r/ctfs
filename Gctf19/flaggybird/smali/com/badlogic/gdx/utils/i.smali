.class public Lcom/badlogic/gdx/utils/i;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/utils/i$c;,
        Lcom/badlogic/gdx/utils/i$a;,
        Lcom/badlogic/gdx/utils/i$d;,
        Lcom/badlogic/gdx/utils/i$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/badlogic/gdx/utils/i$b<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public a:I

.field b:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TK;"
        }
    .end annotation
.end field

.field c:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TV;"
        }
    .end annotation
.end field

.field d:I

.field e:I

.field private f:F

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Lcom/badlogic/gdx/utils/i$a;

.field private m:Lcom/badlogic/gdx/utils/i$a;

.field private n:Lcom/badlogic/gdx/utils/i$c;

.field private o:Lcom/badlogic/gdx/utils/i$c;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x33

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {p0, v0, v1}, Lcom/badlogic/gdx/utils/i;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_2

    int-to-float p1, p1

    div-float/2addr p1, p2

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p1, v0

    invoke-static {p1}, Lcom/badlogic/gdx/math/b;->b(I)I

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    if-gt p1, v0, :cond_1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    const/4 p1, 0x0

    cmpg-float p1, p2, p1

    if-lez p1, :cond_0

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->f:F

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    int-to-float p1, p1

    mul-float p1, p1, p2

    float-to-int p1, p1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->i:I

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->h:I

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result p1

    rsub-int/lit8 p1, p1, 0x1f

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->g:I

    const/4 p1, 0x3

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->d:I

    int-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p2, v0

    mul-int/lit8 p2, p2, 0x2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->j:I

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    const/16 p2, 0x8

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->d:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    div-int/2addr v0, p2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->k:I

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->j:I

    add-int/2addr p1, p2

    new-array p1, p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    array-length p1, p1

    new-array p1, p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadFactor must be > 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initialCapacity is too large: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initialCapacity must be >= 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    const-string p1, "{}"

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Lcom/badlogic/gdx/utils/o;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/o;-><init>(I)V

    if-eqz p2, :cond_2

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/o;->b(C)Lcom/badlogic/gdx/utils/o;

    :cond_2
    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    array-length v3, v1

    :goto_1
    add-int/lit8 v4, v3, -0x1

    const/16 v5, 0x3d

    if-lez v3, :cond_4

    aget-object v3, v1, v4

    if-nez v3, :cond_3

    move v3, v4

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v3}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;

    invoke-virtual {v0, v5}, Lcom/badlogic/gdx/utils/o;->b(C)Lcom/badlogic/gdx/utils/o;

    aget-object v3, v2, v4

    invoke-virtual {v0, v3}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;

    :cond_4
    :goto_2
    add-int/lit8 v3, v4, -0x1

    if-lez v4, :cond_6

    aget-object v4, v1, v3

    if-nez v4, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/o;->b(Ljava/lang/String;)Lcom/badlogic/gdx/utils/o;

    invoke-virtual {v0, v4}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;

    invoke-virtual {v0, v5}, Lcom/badlogic/gdx/utils/o;->b(C)Lcom/badlogic/gdx/utils/o;

    aget-object v4, v2, v3

    invoke-virtual {v0, v4}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;

    :goto_3
    move v4, v3

    goto :goto_2

    :cond_6
    if-eqz p2, :cond_7

    const/16 p1, 0x7d

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/o;->b(C)Lcom/badlogic/gdx/utils/o;

    :cond_7
    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/o;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;ITK;ITK;ITK;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->h:I

    iget v3, p0, Lcom/badlogic/gdx/utils/i;->k:I

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x2

    invoke-static {v5}, Lcom/badlogic/gdx/math/b;->a(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    aget-object p3, v1, p7

    aput-object p1, v0, p7

    aput-object p2, v1, p7

    move-object p2, p3

    move-object p1, p8

    goto :goto_1

    :pswitch_0
    aget-object p3, v1, p5

    aput-object p1, v0, p5

    aput-object p2, v1, p5

    move-object p2, p3

    move-object p1, p6

    goto :goto_1

    :pswitch_1
    aget-object p5, v1, p3

    aput-object p1, v0, p3

    aput-object p2, v1, p3

    move-object p1, p4

    move-object p2, p5

    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p3

    and-int p4, p3, v2

    aget-object p5, v0, p4

    if-nez p5, :cond_1

    aput-object p1, v0, p4

    aput-object p2, v1, p4

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_0

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p3}, Lcom/badlogic/gdx/utils/i;->c(I)I

    move-result p6

    aget-object p7, v0, p6

    if-nez p7, :cond_3

    aput-object p1, v0, p6

    aput-object p2, v1, p6

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_2

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_2
    return-void

    :cond_3
    invoke-direct {p0, p3}, Lcom/badlogic/gdx/utils/i;->d(I)I

    move-result p3

    aget-object p8, v0, p3

    if-nez p8, :cond_5

    aput-object p1, v0, p3

    aput-object p2, v1, p3

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_4

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_4
    return-void

    :cond_5
    add-int/lit8 v4, v4, 0x1

    if-ne v4, v3, :cond_6

    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/utils/i;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_6
    move-object v6, p7

    move p7, p3

    move p3, p4

    move-object p4, p5

    move p5, p6

    move-object p6, v6

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)V
    .locals 5

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v0, v1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    int-to-float v1, p1

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->f:F

    mul-float v1, v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/badlogic/gdx/utils/i;->i:I

    add-int/lit8 v1, p1, -0x1

    iput v1, p0, Lcom/badlogic/gdx/utils/i;->h:I

    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x1f

    iput v1, p0, Lcom/badlogic/gdx/utils/i;->g:I

    int-to-double v1, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->log(D)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/badlogic/gdx/utils/i;->j:I

    const/16 v3, 0x8

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-int v1, v1

    div-int/2addr v1, v3

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/badlogic/gdx/utils/i;->k:I

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v3, p0, Lcom/badlogic/gdx/utils/i;->j:I

    add-int/2addr v3, p1

    new-array v3, v3, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    iput-object v3, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget v3, p0, Lcom/badlogic/gdx/utils/i;->j:I

    add-int/2addr p1, v3

    new-array p1, p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iput v3, p0, Lcom/badlogic/gdx/utils/i;->e:I

    if-lez p1, :cond_1

    :goto_0
    if-ge v3, v0, :cond_1

    aget-object p1, v1, v3

    if-eqz p1, :cond_0

    aget-object v4, v2, v3

    invoke-direct {p0, p1, v4}, Lcom/badlogic/gdx/utils/i;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int v5, v0, v1

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v6, v1, v5

    if-nez v6, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object p1, v0, v5

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v5

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_0

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->c(I)I

    move-result v7

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v8, v1, v7

    if-nez v8, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object p1, v0, v7

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v7

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_2

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_2
    return-void

    :cond_3
    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->d(I)I

    move-result v9

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v10, v0, v9

    if-nez v10, :cond_5

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object p1, v0, v9

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v9

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_4

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_4
    return-void

    :cond_5
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v10}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method

.method private c(I)I
    .locals 1

    const v0, -0x4b47d1c7

    mul-int p1, p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->g:I

    ushr-int v0, p1, v0

    xor-int/2addr p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int/2addr p1, v0

    return p1
.end method

.method private c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->e:I

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->j:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->b(I)V

    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/utils/i;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object p1, v1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v0

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->e:I

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    return-void
.end method

.method private d(I)I
    .locals 1

    const v0, -0x312e3dbf

    mul-int p1, p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->g:I

    ushr-int v0, p1, v0

    xor-int/2addr p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int/2addr p1, v0

    return p1
.end method

.method private d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v2, v1

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v1

    return-object p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object p2
.end method

.method private e(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v2, v1

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int/2addr v1, v0

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->c(I)I

    move-result v1

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->d(I)I

    move-result v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/badlogic/gdx/utils/i;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v1

    return-object p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int v6, v1, v2

    aget-object v7, v0, v6

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v6

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, v0, v6

    return-object p1

    :cond_0
    invoke-direct {p0, v1}, Lcom/badlogic/gdx/utils/i;->c(I)I

    move-result v8

    aget-object v9, v0, v8

    invoke-virtual {p1, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v8

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, v0, v8

    return-object p1

    :cond_1
    invoke-direct {p0, v1}, Lcom/badlogic/gdx/utils/i;->d(I)I

    move-result v10

    aget-object v11, v0, v10

    invoke-virtual {p1, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v10

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, v0, v10

    return-object p1

    :cond_2
    iget v1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v2, v1

    :goto_0
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, v0, v1

    return-object p1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    if-nez v7, :cond_6

    aput-object p1, v0, v6

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v6

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_5

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_5
    return-object v1

    :cond_6
    if-nez v9, :cond_8

    aput-object p1, v0, v8

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v8

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_7

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_7
    return-object v1

    :cond_8
    if-nez v11, :cond_a

    aput-object p1, v0, v10

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object p2, p1, v10

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/i;->i:I

    if-lt p1, p2, :cond_9

    iget p1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->b(I)V

    :cond_9
    return-object v1

    :cond_a
    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v3 .. v11}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V

    return-object v1

    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "key cannot be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()V
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v3, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v2, v3

    :goto_0
    add-int/lit8 v3, v2, -0x1

    if-lez v2, :cond_1

    const/4 v2, 0x0

    aput-object v2, v0, v3

    aput-object v2, v1, v3

    move v2, v3

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    iput v0, p0, Lcom/badlogic/gdx/utils/i;->e:I

    return-void
.end method

.method a(I)V
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/i;->e:I

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v0, v1

    const/4 v1, 0x0

    if-ge p1, v0, :cond_0

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    aput-object v3, v2, p1

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object v3, v3, v0

    aput-object v3, v2, p1

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object v1, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object v1, p1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object v1, v0, p1

    :goto_0
    return-void
.end method

.method public b()Lcom/badlogic/gdx/utils/i$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/i$a<",
            "TK;TV;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i;->c()Lcom/badlogic/gdx/utils/i$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int/2addr v1, v0

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object v3, p1, v1

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object v3, v0, v1

    :goto_0
    iget v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    return-object p1

    :cond_0
    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->c(I)I

    move-result v1

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object v3, p1, v1

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object v3, v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aput-object v3, p1, v0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v0

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aput-object v3, v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/i;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public c()Lcom/badlogic/gdx/utils/i$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/i$a<",
            "TK;TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/i$a;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/i$a;-><init>(Lcom/badlogic/gdx/utils/i;)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    new-instance v0, Lcom/badlogic/gdx/utils/i$a;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/i$a;-><init>(Lcom/badlogic/gdx/utils/i;)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/i;->m:Lcom/badlogic/gdx/utils/i$a;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    iget-boolean v0, v0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i$a;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->m:Lcom/badlogic/gdx/utils/i$a;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->m:Lcom/badlogic/gdx/utils/i$a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i$a;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->m:Lcom/badlogic/gdx/utils/i$a;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->l:Lcom/badlogic/gdx/utils/i$a;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/i$a;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->m:Lcom/badlogic/gdx/utils/i$a;

    return-object v0
.end method

.method c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v2, v1

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    aget-object p1, p1, v1

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/utils/i;->a(I)V

    iget v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/i;->a:I

    return-object p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public d()Lcom/badlogic/gdx/utils/i$c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/i$c<",
            "TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/i$c;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/i$c;-><init>(Lcom/badlogic/gdx/utils/i;)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    new-instance v0, Lcom/badlogic/gdx/utils/i$c;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/i$c;-><init>(Lcom/badlogic/gdx/utils/i;)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/i;->o:Lcom/badlogic/gdx/utils/i$c;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    iget-boolean v0, v0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i$c;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->o:Lcom/badlogic/gdx/utils/i$c;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->o:Lcom/badlogic/gdx/utils/i$c;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/i$c;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->o:Lcom/badlogic/gdx/utils/i$c;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->n:Lcom/badlogic/gdx/utils/i$c;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->o:Lcom/badlogic/gdx/utils/i$c;

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/i;->h:I

    and-int/2addr v1, v0

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v1, v2, v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->c(I)I

    move-result v1

    iget-object v2, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v1, v2, v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/i;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i;->e(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/badlogic/gdx/utils/i;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/badlogic/gdx/utils/i;

    iget v1, p1, Lcom/badlogic/gdx/utils/i;->a:I

    iget v3, p0, Lcom/badlogic/gdx/utils/i;->a:I

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v4, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v5, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v4, v5

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_6

    aget-object v6, v1, v5

    if-eqz v6, :cond_5

    aget-object v7, v3, v5

    if-nez v7, :cond_4

    invoke-virtual {p1, v6}, Lcom/badlogic/gdx/utils/i;->d(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p1, v6}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_5

    :cond_3
    return v2

    :cond_4
    invoke-virtual {p1, v6}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    return v2

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_6
    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i;->c:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/i;->d:I

    iget v3, p0, Lcom/badlogic/gdx/utils/i;->e:I

    add-int/2addr v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v5, v0, v3

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1f

    add-int/2addr v4, v5

    aget-object v5, v1, v3

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    add-int/2addr v4, v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v4
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i;->b()Lcom/badlogic/gdx/utils/i$a;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, ", "

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
