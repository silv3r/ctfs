.class public Lcom/badlogic/gdx/utils/h$a;
.super Lcom/badlogic/gdx/utils/h$c;

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/badlogic/gdx/utils/h$c<",
        "TK;>;",
        "Ljava/lang/Iterable<",
        "Lcom/badlogic/gdx/utils/h$b<",
        "TK;>;>;",
        "Ljava/util/Iterator<",
        "Lcom/badlogic/gdx/utils/h$b<",
        "TK;>;>;"
    }
.end annotation


# instance fields
.field private f:Lcom/badlogic/gdx/utils/h$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h$b<",
            "TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/h<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/h$c;-><init>(Lcom/badlogic/gdx/utils/h;)V

    new-instance p1, Lcom/badlogic/gdx/utils/h$b;

    invoke-direct {p1}, Lcom/badlogic/gdx/utils/h$b;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/utils/h$a;->f:Lcom/badlogic/gdx/utils/h$b;

    return-void
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/utils/h$b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/h$b<",
            "TK;>;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/h$a;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/h$a;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$a;->b:Lcom/badlogic/gdx/utils/h;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/h;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/h$a;->f:Lcom/badlogic/gdx/utils/h$b;

    iget v2, p0, Lcom/badlogic/gdx/utils/h$a;->c:I

    aget-object v0, v0, v2

    iput-object v0, v1, Lcom/badlogic/gdx/utils/h$b;->a:Ljava/lang/Object;

    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$a;->f:Lcom/badlogic/gdx/utils/h$b;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/h$a;->b:Lcom/badlogic/gdx/utils/h;

    iget-object v1, v1, Lcom/badlogic/gdx/utils/h;->c:[I

    iget v2, p0, Lcom/badlogic/gdx/utils/h$a;->c:I

    aget v1, v1, v2

    iput v1, v0, Lcom/badlogic/gdx/utils/h$b;->b:I

    iget v0, p0, Lcom/badlogic/gdx/utils/h$a;->c:I

    iput v0, p0, Lcom/badlogic/gdx/utils/h$a;->d:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/h$a;->d()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$a;->f:Lcom/badlogic/gdx/utils/h$b;

    return-object v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public b()Lcom/badlogic/gdx/utils/h$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/h$a<",
            "TK;>;"
        }
    .end annotation

    return-object p0
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/badlogic/gdx/utils/h$c;->c()V

    return-void
.end method

.method public hasNext()Z
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/h$a;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/h$a;->a:Z

    return v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/h$a;->b()Lcom/badlogic/gdx/utils/h$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/h$a;->a()Lcom/badlogic/gdx/utils/h$b;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    invoke-super {p0}, Lcom/badlogic/gdx/utils/h$c;->remove()V

    return-void
.end method
