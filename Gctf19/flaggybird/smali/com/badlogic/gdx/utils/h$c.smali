.class Lcom/badlogic/gdx/utils/h$c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Z

.field final b:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "TK;>;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field e:Z


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/h<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/utils/h$c;->e:Z

    iput-object p1, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/h$c;->c()V

    return-void
.end method


# virtual methods
.method public c()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    iput v0, p0, Lcom/badlogic/gdx/utils/h$c;->c:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/h$c;->d()V

    return-void
.end method

.method d()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/utils/h$c;->a:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/h;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget v1, v1, Lcom/badlogic/gdx/utils/h;->d:I

    iget-object v2, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget v2, v2, Lcom/badlogic/gdx/utils/h;->e:I

    add-int/2addr v1, v2

    :cond_0
    iget v2, p0, Lcom/badlogic/gdx/utils/h$c;->c:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lcom/badlogic/gdx/utils/h$c;->c:I

    if-ge v2, v1, :cond_1

    iget v2, p0, Lcom/badlogic/gdx/utils/h$c;->c:I

    aget-object v2, v0, v2

    if-eqz v2, :cond_0

    iput-boolean v3, p0, Lcom/badlogic/gdx/utils/h$c;->a:Z

    :cond_1
    return-void
.end method

.method public remove()V
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    iget-object v1, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget v1, v1, Lcom/badlogic/gdx/utils/h;->d:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget v1, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/h;->a(I)V

    iget v0, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/h$c;->c:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/h$c;->d()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/h;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/h$c;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/h$c;->b:Lcom/badlogic/gdx/utils/h;

    iget v1, v0, Lcom/badlogic/gdx/utils/h;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/badlogic/gdx/utils/h;->a:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "next must be called before remove."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
