.class public Lcom/badlogic/gdx/utils/f;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:I


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/utils/f;->b:I

    return v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/utils/f;->b:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/f;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/utils/f;->b:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/f;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/utils/f;->b:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/f;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
