.class public Lcom/badlogic/gdx/utils/i$c;
.super Lcom/badlogic/gdx/utils/i$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/badlogic/gdx/utils/i$d<",
        "TK;",
        "Ljava/lang/Object;",
        "TK;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/i<",
            "TK;*>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/i$d;-><init>(Lcom/badlogic/gdx/utils/i;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/badlogic/gdx/utils/a;)Lcom/badlogic/gdx/utils/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/a<",
            "TK;>;)",
            "Lcom/badlogic/gdx/utils/a<",
            "TK;>;"
        }
    .end annotation

    :goto_0
    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$c;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$c;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method public a()Lcom/badlogic/gdx/utils/i$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/i$c<",
            "TK;>;"
        }
    .end annotation

    return-object p0
.end method

.method public b()Lcom/badlogic/gdx/utils/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/a<",
            "TK;>;"
        }
    .end annotation

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/i$c;->c:Lcom/badlogic/gdx/utils/i;

    iget v1, v1, Lcom/badlogic/gdx/utils/i;->a:I

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lcom/badlogic/gdx/utils/a;-><init>(ZI)V

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/i$c;->a(Lcom/badlogic/gdx/utils/a;)Lcom/badlogic/gdx/utils/a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lcom/badlogic/gdx/utils/i$d;->c()V

    return-void
.end method

.method public hasNext()Z
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$c;->b:Z

    return v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$c;->a()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$c;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/i$c;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/i$c;->c:Lcom/badlogic/gdx/utils/i;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/i;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/i$c;->d:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/badlogic/gdx/utils/i$c;->d:I

    iput v1, p0, Lcom/badlogic/gdx/utils/i$c;->e:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/i$c;->d()V

    return-object v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public bridge synthetic remove()V
    .locals 0

    invoke-super {p0}, Lcom/badlogic/gdx/utils/i$d;->remove()V

    return-void
.end method
