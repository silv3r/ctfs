.class public Lcom/badlogic/gdx/utils/o;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Appendable;
.implements Ljava/lang/CharSequence;


# static fields
.field private static final c:[C


# instance fields
.field public a:[C

.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/badlogic/gdx/utils/o;->c:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    new-array p1, p1, [C

    iput-object p1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    return-void

    :cond_0
    new-instance p1, Ljava/lang/NegativeArraySizeException;

    invoke-direct {p1}, Ljava/lang/NegativeArraySizeException;-><init>()V

    throw p1
.end method

.method public static a(II)I
    .locals 1

    if-gez p0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    div-int/2addr p0, p1

    if-eqz p0, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    array-length v0, v0

    shr-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    new-array p1, p1, [C

    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    return-void
.end method


# virtual methods
.method public a(I)Lcom/badlogic/gdx/utils/o;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/badlogic/gdx/utils/o;->c(II)Lcom/badlogic/gdx/utils/o;

    move-result-object p1

    return-object p1
.end method

.method public a(IIC)Lcom/badlogic/gdx/utils/o;
    .locals 6

    const/high16 v0, -0x80000000

    if-ne p1, v0, :cond_0

    const-string p1, "-2147483648"

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/String;)V

    return-object p0

    :cond_0
    if-gez p1, :cond_1

    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/o;->a(C)V

    neg-int p1, p1

    :cond_1
    const/4 v0, 0x1

    const/16 v1, 0xa

    if-le p2, v0, :cond_2

    invoke-static {p1, v1}, Lcom/badlogic/gdx/utils/o;->a(II)I

    move-result v0

    sub-int/2addr p2, v0

    :goto_0
    if-lez p2, :cond_2

    invoke-virtual {p0, p3}, Lcom/badlogic/gdx/utils/o;->b(C)Lcom/badlogic/gdx/utils/o;

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_2
    const/16 p2, 0x2710

    if-lt p1, p2, :cond_8

    const p3, 0x3b9aca00

    if-lt p1, p3, :cond_3

    sget-object v0, Lcom/badlogic/gdx/utils/o;->c:[C

    int-to-long v2, p1

    const-wide v4, 0x2540be400L

    rem-long/2addr v2, v4

    const-wide/32 v4, 0x3b9aca00

    div-long/2addr v2, v4

    long-to-int v2, v2

    aget-char v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_3
    const v0, 0x5f5e100

    if-lt p1, v0, :cond_4

    sget-object v2, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int p3, p1, p3

    div-int/2addr p3, v0

    aget-char p3, v2, p3

    invoke-virtual {p0, p3}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_4
    const p3, 0x989680

    if-lt p1, p3, :cond_5

    sget-object v2, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int v0, p1, v0

    div-int/2addr v0, p3

    aget-char v0, v2, v0

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_5
    const v0, 0xf4240

    if-lt p1, v0, :cond_6

    sget-object v2, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int p3, p1, p3

    div-int/2addr p3, v0

    aget-char p3, v2, p3

    invoke-virtual {p0, p3}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_6
    const p3, 0x186a0

    if-lt p1, p3, :cond_7

    sget-object v2, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int v0, p1, v0

    div-int/2addr v0, p3

    aget-char v0, v2, v0

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_7
    sget-object v0, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int p3, p1, p3

    div-int/2addr p3, p2

    aget-char p2, v0, p3

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_8
    const/16 p2, 0x3e8

    if-lt p1, p2, :cond_9

    sget-object p3, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int/lit16 v0, p1, 0x2710

    div-int/2addr v0, p2

    aget-char p2, p3, v0

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_9
    const/16 p2, 0x64

    if-lt p1, p2, :cond_a

    sget-object p3, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int/lit16 v0, p1, 0x3e8

    div-int/2addr v0, p2

    aget-char p2, p3, v0

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_a
    if-lt p1, v1, :cond_b

    sget-object p2, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int/lit8 p3, p1, 0x64

    div-int/2addr p3, v1

    aget-char p2, p2, p3

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/utils/o;->a(C)V

    :cond_b
    sget-object p2, Lcom/badlogic/gdx/utils/o;->c:[C

    rem-int/2addr p1, v1

    aget-char p1, p2, p1

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(C)V

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/badlogic/gdx/utils/o;
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/o;->a()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/badlogic/gdx/utils/o;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/badlogic/gdx/utils/o;

    iget-object v0, p1, Lcom/badlogic/gdx/utils/o;->a:[C

    const/4 v1, 0x0

    iget p1, p1, Lcom/badlogic/gdx/utils/o;->b:I

    invoke-virtual {p0, v0, v1, p1}, Lcom/badlogic/gdx/utils/o;->a([CII)V

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/o;->a()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method final a()V
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    array-length v1, v1

    if-le v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/o;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/badlogic/gdx/utils/o;->b:I

    const/16 v2, 0x6e

    aput-char v2, v0, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/badlogic/gdx/utils/o;->b:I

    const/16 v2, 0x75

    aput-char v2, v0, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/badlogic/gdx/utils/o;->b:I

    const/16 v2, 0x6c

    aput-char v2, v0, v1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/badlogic/gdx/utils/o;->b:I

    aput-char v2, v0, v1

    return-void
.end method

.method final a(C)V
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    array-length v1, v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/o;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/badlogic/gdx/utils/o;->b:I

    aput-char p1, v0, v1

    return-void
.end method

.method final a(Ljava/lang/CharSequence;II)V
    .locals 1

    if-nez p1, :cond_0

    const-string p1, "null"

    :cond_0
    if-ltz p2, :cond_1

    if-ltz p3, :cond_1

    if-gt p2, p3, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gt p3, v0, :cond_1

    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/o;->a()V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    array-length v2, v2

    if-le v1, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/utils/o;->b(I)V

    :cond_1
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v4, p0, Lcom/badlogic/gdx/utils/o;->b:I

    invoke-virtual {p1, v2, v0, v3, v4}, Ljava/lang/String;->getChars(II[CI)V

    iput v1, p0, Lcom/badlogic/gdx/utils/o;->b:I

    return-void
.end method

.method final a([CII)V
    .locals 3

    array-length v0, p1

    if-gt p2, v0, :cond_2

    if-ltz p2, :cond_2

    if-ltz p3, :cond_1

    array-length v0, p1

    sub-int/2addr v0, p2

    if-lt v0, p3, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    array-length v1, v1

    if-le v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/o;->b(I)V

    :cond_0
    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget v2, p0, Lcom/badlogic/gdx/utils/o;->b:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    return-void

    :cond_1
    new-instance p1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Length out of bounds: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Offset out of bounds: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic append(C)Ljava/lang/Appendable;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->b(C)Lcom/badlogic/gdx/utils/o;

    move-result-object p1

    return-object p1
.end method

.method public synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/CharSequence;)Lcom/badlogic/gdx/utils/o;

    move-result-object p1

    return-object p1
.end method

.method public synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/badlogic/gdx/utils/o;->b(Ljava/lang/CharSequence;II)Lcom/badlogic/gdx/utils/o;

    move-result-object p1

    return-object p1
.end method

.method public b(C)Lcom/badlogic/gdx/utils/o;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(C)V

    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;II)Lcom/badlogic/gdx/utils/o;
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/CharSequence;II)V

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/badlogic/gdx/utils/o;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/String;)V

    return-object p0
.end method

.method public b(II)Ljava/lang/String;
    .locals 2

    if-ltz p1, :cond_1

    if-gt p1, p2, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    if-gt p2, v0, :cond_1

    if-ne p1, p2, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    sub-int/2addr p2, p1

    invoke-direct {v0, v1, p1, p2}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public c(II)Lcom/badlogic/gdx/utils/o;
    .locals 1

    const/16 v0, 0x30

    invoke-virtual {p0, p1, p2, v0}, Lcom/badlogic/gdx/utils/o;->a(IIC)Lcom/badlogic/gdx/utils/o;

    move-result-object p1

    return-object p1
.end method

.method public charAt(I)C
    .locals 1

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    aget-char p1, v0, p1

    return p1

    :cond_0
    new-instance v0, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    check-cast p1, Lcom/badlogic/gdx/utils/o;

    iget v2, p0, Lcom/badlogic/gdx/utils/o;->b:I

    iget v3, p1, Lcom/badlogic/gdx/utils/o;->b:I

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    iget-object v3, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    iget-object p1, p1, Lcom/badlogic/gdx/utils/o;->a:[C

    if-ne v3, p1, :cond_4

    return v0

    :cond_4
    if-eqz v3, :cond_8

    if-nez p1, :cond_5

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_7

    aget-char v5, v3, v4

    aget-char v6, p1, v4

    if-eq v5, v6, :cond_6

    return v1

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_7
    return v0

    :cond_8
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([C)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public length()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/badlogic/gdx/utils/o;->b(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/o;->b:I

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/o;->a:[C

    const/4 v2, 0x0

    iget v3, p0, Lcom/badlogic/gdx/utils/o;->b:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
