.class public Lcom/badlogic/gdx/utils/m;
.super Lcom/badlogic/gdx/utils/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/badlogic/gdx/utils/a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private d:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private e:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/a;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private f()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    array-length v0, v0

    iget v1, p0, Lcom/badlogic/gdx/utils/m;->b:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/m;->b:I

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    array-length v0, v0

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/m;->c(I)[Ljava/lang/Object;

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/m;->f()V

    invoke-super {p0}, Lcom/badlogic/gdx/utils/a;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)Z"
        }
    .end annotation

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/m;->f()V

    invoke-super {p0, p1, p2}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;Z)Z

    move-result p1

    return p1
.end method

.method public b(I)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/m;->f()V

    invoke-super {p0, p1}, Lcom/badlogic/gdx/utils/a;->b(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public c()V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/m;->f()V

    invoke-super {p0}, Lcom/badlogic/gdx/utils/a;->c()V

    return-void
.end method

.method public d()[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TT;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/m;->f()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    iget v0, p0, Lcom/badlogic/gdx/utils/m;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/m;->f:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    return-object v0
.end method

.method public e()V
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/m;->f:I

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/utils/m;->f:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/badlogic/gdx/utils/m;->a:[Ljava/lang/Object;

    const/4 v3, 0x0

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/utils/m;->f:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    iget-object v0, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    array-length v0, v0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/badlogic/gdx/utils/m;->e:[Ljava/lang/Object;

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iput-object v3, p0, Lcom/badlogic/gdx/utils/m;->d:[Ljava/lang/Object;

    return-void
.end method
