.class public Lcom/badlogic/gdx/utils/j$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TK;>;",
        "Ljava/util/Iterator<",
        "TK;>;"
    }
.end annotation


# instance fields
.field public a:Z

.field final b:Lcom/badlogic/gdx/utils/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/j<",
            "TK;>;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field e:Z


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/j<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    iput-object p1, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/j$a;->a()V

    return-void
.end method

.method private c()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/utils/j$a;->a:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget v1, v1, Lcom/badlogic/gdx/utils/j;->c:I

    iget-object v2, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget v2, v2, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v1, v2

    :cond_0
    iget v2, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    if-ge v2, v1, :cond_1

    iget v2, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    aget-object v2, v0, v2

    if-eqz v2, :cond_0

    iput-boolean v3, p0, Lcom/badlogic/gdx/utils/j$a;->a:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    iput v0, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/j$a;->c()V

    return-void
.end method

.method public b()Lcom/badlogic/gdx/utils/j$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/j$a<",
            "TK;>;"
        }
    .end annotation

    return-object p0
.end method

.method public hasNext()Z
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/j$a;->a:Z

    return v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/j$a;->b()Lcom/badlogic/gdx/utils/j$a;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/j$a;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    iput v1, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/j$a;->c()V

    return-object v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "#iterator() cannot be used nested."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget v1, v1, Lcom/badlogic/gdx/utils/j;->c:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget v1, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/j;->a(I)V

    iget v0, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j$a;->c:I

    invoke-direct {p0}, Lcom/badlogic/gdx/utils/j$a;->c()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget-object v0, v0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j$a;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j$a;->b:Lcom/badlogic/gdx/utils/j;

    iget v1, v0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/badlogic/gdx/utils/j;->a:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "next must be called before remove."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
