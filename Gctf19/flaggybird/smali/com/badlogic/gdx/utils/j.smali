.class public Lcom/badlogic/gdx/utils/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/utils/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public a:I

.field b:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field private e:F

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Lcom/badlogic/gdx/utils/j$a;

.field private l:Lcom/badlogic/gdx/utils/j$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x33

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {p0, v0, v1}, Lcom/badlogic/gdx/utils/j;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_2

    int-to-float p1, p1

    div-float/2addr p1, p2

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p1, v0

    invoke-static {p1}, Lcom/badlogic/gdx/math/b;->b(I)I

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    if-gt p1, v0, :cond_1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    const/4 p1, 0x0

    cmpg-float p1, p2, p1

    if-lez p1, :cond_0

    iput p2, p0, Lcom/badlogic/gdx/utils/j;->e:F

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    int-to-float p1, p1

    mul-float p1, p1, p2

    float-to-int p1, p1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->h:I

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->g:I

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result p1

    rsub-int/lit8 p1, p1, 0x1f

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->f:I

    const/4 p1, 0x3

    iget p2, p0, Lcom/badlogic/gdx/utils/j;->c:I

    int-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p2, v0

    mul-int/lit8 p2, p2, 0x2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->i:I

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    const/16 p2, 0x8

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->c:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    div-int/2addr v0, p2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->j:I

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget p2, p0, Lcom/badlogic/gdx/utils/j;->i:I

    add-int/2addr p1, p2

    new-array p1, p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadFactor must be > 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initialCapacity is too large: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initialCapacity must be >= 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private a(Ljava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ITT;ITT;ITT;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->g:I

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->j:I

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/badlogic/gdx/math/b;->a(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    aput-object p1, v0, p6

    move-object p1, p7

    goto :goto_1

    :pswitch_0
    aput-object p1, v0, p4

    move-object p1, p5

    goto :goto_1

    :pswitch_1
    aput-object p1, v0, p2

    move-object p1, p3

    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p2

    and-int p3, p2, v1

    aget-object p4, v0, p3

    if-nez p4, :cond_1

    aput-object p1, v0, p3

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, p2, :cond_0

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p2}, Lcom/badlogic/gdx/utils/j;->c(I)I

    move-result p5

    aget-object p6, v0, p5

    if-nez p6, :cond_3

    aput-object p1, v0, p5

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, p2, :cond_2

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_2
    return-void

    :cond_3
    invoke-direct {p0, p2}, Lcom/badlogic/gdx/utils/j;->d(I)I

    move-result p2

    aget-object p7, v0, p2

    if-nez p7, :cond_5

    aput-object p1, v0, p2

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget p2, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, p2, :cond_4

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_4
    return-void

    :cond_5
    add-int/lit8 v3, v3, 0x1

    if-ne v3, v2, :cond_6

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->d(Ljava/lang/Object;)V

    return-void

    :cond_6
    move-object v5, p6

    move p6, p2

    move p2, p3

    move-object p3, p4

    move p4, p5

    move-object p5, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)V
    .locals 5

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v0, v1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    int-to-float v1, p1

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->e:F

    mul-float v1, v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/badlogic/gdx/utils/j;->h:I

    add-int/lit8 v1, p1, -0x1

    iput v1, p0, Lcom/badlogic/gdx/utils/j;->g:I

    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x1f

    iput v1, p0, Lcom/badlogic/gdx/utils/j;->f:I

    int-to-double v1, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->log(D)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/badlogic/gdx/utils/j;->i:I

    const/16 v3, 0x8

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-int v1, v1

    div-int/2addr v1, v3

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/badlogic/gdx/utils/j;->j:I

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->i:I

    add-int/2addr p1, v2

    new-array p1, p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iput v2, p0, Lcom/badlogic/gdx/utils/j;->d:I

    if-lez p1, :cond_1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object p1, v1, v2

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->c(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(I)I
    .locals 1

    const v0, -0x4b47d1c7

    mul-int p1, p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->f:I

    ushr-int v0, p1, v0

    xor-int/2addr p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->g:I

    and-int/2addr p1, v0

    return p1
.end method

.method private c(Ljava/lang/Object;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->g:I

    and-int v4, v0, v1

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v5, v1, v4

    if-nez v5, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aput-object p1, v0, v4

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, v0, :cond_0

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/j;->c(I)I

    move-result v6

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v7, v1, v6

    if-nez v7, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aput-object p1, v0, v6

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, v0, :cond_2

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_2
    return-void

    :cond_3
    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/j;->d(I)I

    move-result v8

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v9, v0, v8

    if-nez v9, :cond_5

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aput-object p1, v0, v8

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, v0, :cond_4

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_4
    return-void

    :cond_5
    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v2 .. v9}, Lcom/badlogic/gdx/utils/j;->a(Ljava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method

.method private d(I)I
    .locals 1

    const v0, -0x312e3dbf

    mul-int p1, p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->f:I

    ushr-int v0, p1, v0

    xor-int/2addr p1, v0

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->g:I

    and-int/2addr p1, v0

    return p1
.end method

.method private d(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->d:I

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->i:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/j;->b(I)V

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->c(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aput-object p1, v1, v0

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->d:I

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    return-void
.end method

.method private e(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v2, v1

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    aget-object p1, v0, v1

    return-object p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/o;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/o;-><init>(I)V

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    array-length v2, v1

    :goto_0
    add-int/lit8 v3, v2, -0x1

    if-lez v2, :cond_2

    aget-object v2, v1, v3

    if-nez v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;

    :cond_2
    :goto_1
    add-int/lit8 v2, v3, -0x1

    if-lez v3, :cond_4

    aget-object v3, v1, v2

    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/o;->b(Ljava/lang/String;)Lcom/badlogic/gdx/utils/o;

    invoke-virtual {v0, v3}, Lcom/badlogic/gdx/utils/o;->a(Ljava/lang/Object;)Lcom/badlogic/gdx/utils/o;

    :goto_2
    move v3, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/o;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v1, v2

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    aput-object v1, v0, v2

    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->d:I

    return-void
.end method

.method a(I)V
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->d:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    aput-object v2, v1, p1

    iget-object p1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v1, p1, v0

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->g:I

    and-int v5, v1, v2

    aget-object v6, v0, v5

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    return v3

    :cond_0
    invoke-direct {p0, v1}, Lcom/badlogic/gdx/utils/j;->c(I)I

    move-result v7

    aget-object v8, v0, v7

    invoke-virtual {p1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v3

    :cond_1
    invoke-direct {p0, v1}, Lcom/badlogic/gdx/utils/j;->d(I)I

    move-result v9

    aget-object v10, v0, v9

    invoke-virtual {p1, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return v3

    :cond_2
    iget v1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v2, v1

    :goto_0
    if-ge v1, v2, :cond_4

    aget-object v4, v0, v1

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    return v3

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x1

    if-nez v6, :cond_6

    aput-object p1, v0, v5

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, v0, :cond_5

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/2addr p1, v1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_5
    return v1

    :cond_6
    if-nez v8, :cond_8

    aput-object p1, v0, v7

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, v0, :cond_7

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/2addr p1, v1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_7
    return v1

    :cond_8
    if-nez v10, :cond_a

    aput-object p1, v0, v9

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->a:I

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/badlogic/gdx/utils/j;->a:I

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->h:I

    if-lt p1, v0, :cond_9

    iget p1, p0, Lcom/badlogic/gdx/utils/j;->c:I

    shl-int/2addr p1, v1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->b(I)V

    :cond_9
    return v1

    :cond_a
    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v3 .. v10}, Lcom/badlogic/gdx/utils/j;->a(Ljava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V

    return v1

    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "key cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Lcom/badlogic/gdx/utils/j$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/j$a<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/j$a;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/j$a;-><init>(Lcom/badlogic/gdx/utils/j;)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    new-instance v0, Lcom/badlogic/gdx/utils/j$a;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/j$a;-><init>(Lcom/badlogic/gdx/utils/j;)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/j;->l:Lcom/badlogic/gdx/utils/j$a;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    iget-boolean v0, v0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/j$a;->a()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->l:Lcom/badlogic/gdx/utils/j$a;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->l:Lcom/badlogic/gdx/utils/j$a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/j$a;->a()V

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->l:Lcom/badlogic/gdx/utils/j$a;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->k:Lcom/badlogic/gdx/utils/j$a;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/j$a;->e:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->l:Lcom/badlogic/gdx/utils/j$a;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->g:I

    and-int/2addr v1, v0

    iget-object v2, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v1, v2, v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/j;->c(I)I

    move-result v1

    iget-object v3, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v1, v3, v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/j;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/utils/j;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    instance-of v0, p1, Lcom/badlogic/gdx/utils/j;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    check-cast p1, Lcom/badlogic/gdx/utils/j;

    iget v0, p1, Lcom/badlogic/gdx/utils/j;->a:I

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->a:I

    if-eq v0, v2, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    iget v2, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v3, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v2, v3

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    if-eqz v4, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {p1, v4}, Lcom/badlogic/gdx/utils/j;->b(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    return v1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/utils/j;->c:I

    iget v1, p0, Lcom/badlogic/gdx/utils/j;->d:I

    add-int/2addr v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/badlogic/gdx/utils/j;->b:[Ljava/lang/Object;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/j;->b()Lcom/badlogic/gdx/utils/j$a;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/utils/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
