.class public Lcom/badlogic/gdx/utils/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/utils/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Z

.field private c:Lcom/badlogic/gdx/utils/a$b;

.field private d:Lcom/badlogic/gdx/utils/a$b;


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/utils/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/a<",
            "TT;>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/badlogic/gdx/utils/a$a;-><init>(Lcom/badlogic/gdx/utils/a;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/utils/a;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/utils/a<",
            "TT;>;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/utils/a$a;->a:Lcom/badlogic/gdx/utils/a;

    iput-boolean p2, p0, Lcom/badlogic/gdx/utils/a$a;->b:Z

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/a$b;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/a$a;->a:Lcom/badlogic/gdx/utils/a;

    iget-boolean v2, p0, Lcom/badlogic/gdx/utils/a$a;->b:Z

    invoke-direct {v0, v1, v2}, Lcom/badlogic/gdx/utils/a$b;-><init>(Lcom/badlogic/gdx/utils/a;Z)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    new-instance v0, Lcom/badlogic/gdx/utils/a$b;

    iget-object v1, p0, Lcom/badlogic/gdx/utils/a$a;->a:Lcom/badlogic/gdx/utils/a;

    iget-boolean v2, p0, Lcom/badlogic/gdx/utils/a$a;->b:Z

    invoke-direct {v0, v1, v2}, Lcom/badlogic/gdx/utils/a$b;-><init>(Lcom/badlogic/gdx/utils/a;Z)V

    iput-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->d:Lcom/badlogic/gdx/utils/a$b;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    iget-boolean v0, v0, Lcom/badlogic/gdx/utils/a$b;->b:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    iput v2, v0, Lcom/badlogic/gdx/utils/a$b;->a:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/a$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->d:Lcom/badlogic/gdx/utils/a$b;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/a$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->d:Lcom/badlogic/gdx/utils/a$b;

    iput v2, v0, Lcom/badlogic/gdx/utils/a$b;->a:I

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->d:Lcom/badlogic/gdx/utils/a$b;

    iput-boolean v1, v0, Lcom/badlogic/gdx/utils/a$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->c:Lcom/badlogic/gdx/utils/a$b;

    iput-boolean v2, v0, Lcom/badlogic/gdx/utils/a$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/utils/a$a;->d:Lcom/badlogic/gdx/utils/a$b;

    return-object v0
.end method
