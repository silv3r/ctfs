.class public Lcom/badlogic/gdx/graphics/b;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lcom/badlogic/gdx/graphics/b;

.field public static final B:Lcom/badlogic/gdx/graphics/b;

.field public static final C:Lcom/badlogic/gdx/graphics/b;

.field public static final D:Lcom/badlogic/gdx/graphics/b;

.field public static final E:Lcom/badlogic/gdx/graphics/b;

.field public static final F:Lcom/badlogic/gdx/graphics/b;

.field public static final G:Lcom/badlogic/gdx/graphics/b;

.field public static final H:Lcom/badlogic/gdx/graphics/b;

.field public static final I:Lcom/badlogic/gdx/graphics/b;

.field public static final a:Lcom/badlogic/gdx/graphics/b;

.field public static final b:Lcom/badlogic/gdx/graphics/b;

.field public static final c:Lcom/badlogic/gdx/graphics/b;

.field public static final d:Lcom/badlogic/gdx/graphics/b;

.field public static final e:Lcom/badlogic/gdx/graphics/b;

.field public static final f:F

.field public static final g:Lcom/badlogic/gdx/graphics/b;

.field public static final h:Lcom/badlogic/gdx/graphics/b;

.field public static final i:Lcom/badlogic/gdx/graphics/b;

.field public static final j:Lcom/badlogic/gdx/graphics/b;

.field public static final k:Lcom/badlogic/gdx/graphics/b;

.field public static final l:Lcom/badlogic/gdx/graphics/b;

.field public static final m:Lcom/badlogic/gdx/graphics/b;

.field public static final n:Lcom/badlogic/gdx/graphics/b;

.field public static final o:Lcom/badlogic/gdx/graphics/b;

.field public static final p:Lcom/badlogic/gdx/graphics/b;

.field public static final q:Lcom/badlogic/gdx/graphics/b;

.field public static final r:Lcom/badlogic/gdx/graphics/b;

.field public static final s:Lcom/badlogic/gdx/graphics/b;

.field public static final t:Lcom/badlogic/gdx/graphics/b;

.field public static final u:Lcom/badlogic/gdx/graphics/b;

.field public static final v:Lcom/badlogic/gdx/graphics/b;

.field public static final w:Lcom/badlogic/gdx/graphics/b;

.field public static final x:Lcom/badlogic/gdx/graphics/b;

.field public static final y:Lcom/badlogic/gdx/graphics/b;

.field public static final z:Lcom/badlogic/gdx/graphics/b;


# instance fields
.field public J:F

.field public K:F

.field public L:F

.field public M:F


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->a:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v2, -0x40404001

    invoke-direct {v0, v2}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->b:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v2, 0x7f7f7fff

    invoke-direct {v0, v2}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->c:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v2, 0x3f3f3fff

    invoke-direct {v0, v2}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->d:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v2, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->e:Lcom/badlogic/gdx/graphics/b;

    sget-object v0, Lcom/badlogic/gdx/graphics/b;->a:Lcom/badlogic/gdx/graphics/b;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/b;->b()F

    move-result v0

    sput v0, Lcom/badlogic/gdx/graphics/b;->f:F

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    invoke-direct {v0, v2, v2, v2, v2}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->g:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    invoke-direct {v0, v2, v2, v1, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->h:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v0, v2, v2, v3, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->i:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v4, 0x4169e1ff

    invoke-direct {v0, v4}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->j:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v4, 0x708090ff

    invoke-direct {v0, v4}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->k:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v4, -0x78311401

    invoke-direct {v0, v4}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->l:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    invoke-direct {v0, v2, v1, v1, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->m:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    invoke-direct {v0, v2, v3, v3, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->n:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, 0xff00ff

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->o:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, 0x7fff00ff

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->p:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, 0x32cd32ff

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->q:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, 0x228b22ff

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->r:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, 0x6b8e23ff

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->s:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0xff01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->t:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x28ff01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->u:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x255adf01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->v:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x5aff01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->w:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x74baec01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->x:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x2d4b7301

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->y:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x4ddddd01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->z:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0xffff01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->A:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0xcbe301

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->B:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x80af01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->C:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x57f8d01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->D:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v3, -0x964b01

    invoke-direct {v0, v3}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->E:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    invoke-direct {v0, v1, v2, v1, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->F:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v1, -0x5fdf0f01

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->G:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v1, -0x117d1101

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->H:Lcom/badlogic/gdx/graphics/b;

    new-instance v0, Lcom/badlogic/gdx/graphics/b;

    const v1, -0x4fcf9f01

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/b;-><init>(I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/b;->I:Lcom/badlogic/gdx/graphics/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    iput p2, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    iput p3, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    iput p4, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/b;->a()Lcom/badlogic/gdx/graphics/b;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p0, p1}, Lcom/badlogic/gdx/graphics/b;->a(Lcom/badlogic/gdx/graphics/b;I)V

    return-void
.end method

.method public static a(FFFF)I
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float p0, p0, v0

    float-to-int p0, p0

    shl-int/lit8 p0, p0, 0x18

    mul-float p1, p1, v0

    float-to-int p1, p1

    shl-int/lit8 p1, p1, 0x10

    or-int/2addr p0, p1

    mul-float p2, p2, v0

    float-to-int p1, p2

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p0, p1

    mul-float p3, p3, v0

    float-to-int p1, p3

    or-int/2addr p0, p1

    return p0
.end method

.method public static a(Lcom/badlogic/gdx/graphics/b;I)V
    .locals 2

    const/high16 v0, -0x1000000

    and-int/2addr v0, p1

    ushr-int/lit8 v0, v0, 0x18

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    const/high16 v0, 0xff0000

    and-int/2addr v0, p1

    ushr-int/lit8 v0, v0, 0x10

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    const v0, 0xff00

    and-int/2addr v0, p1

    ushr-int/lit8 v0, v0, 0x8

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    and-int/lit16 p1, p1, 0xff

    int-to-float p1, p1

    div-float/2addr p1, v1

    iput p1, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    return-void
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/graphics/b;
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    const/high16 v2, 0x3f800000    # 1.0f

    if-gez v0, :cond_0

    iput v1, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    iput v2, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    :cond_1
    :goto_0
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iput v1, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_3

    iput v2, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    :cond_3
    :goto_1
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    iput v1, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    goto :goto_2

    :cond_4
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    iput v2, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    :cond_5
    :goto_2
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    iput v1, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    goto :goto_3

    :cond_6
    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    iput v2, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    :cond_7
    :goto_3
    return-object p0
.end method

.method public b()F
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float v0, v0, v1

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    mul-float v2, v2, v1

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v0, v2

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    mul-float v2, v2, v1

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    mul-float v2, v2, v1

    float-to-int v1, v2

    or-int/2addr v0, v1

    invoke-static {v0}, Lcom/badlogic/gdx/utils/g;->a(I)F

    move-result v0

    return v0
.end method

.method public c()I
    .locals 3

    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float v0, v0, v1

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    mul-float v2, v2, v1

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v0, v2

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    mul-float v2, v2, v1

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    mul-float v2, v2, v1

    float-to-int v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lcom/badlogic/gdx/graphics/b;

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/b;->c()I

    move-result v2

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/b;->c()I

    move-result p1

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    invoke-static {v0}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    cmpl-float v3, v3, v1

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v3

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    cmpl-float v3, v3, v1

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    invoke-static {v3}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v3

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    cmpl-float v1, v3, v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    invoke-static {v1}, Lcom/badlogic/gdx/utils/g;->a(F)I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/graphics/b;->J:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float v0, v0, v1

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->K:F

    mul-float v2, v2, v1

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v0, v2

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->L:F

    mul-float v2, v2, v1

    float-to-int v2, v2

    const/16 v3, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v0, v2

    iget v2, p0, Lcom/badlogic/gdx/graphics/b;->M:F

    mul-float v2, v2, v1

    float-to-int v1, v2

    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method
