.class public Lcom/badlogic/gdx/graphics/o$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/graphics/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public static a(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j$c;Z)Lcom/badlogic/gdx/graphics/o;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/c/a;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".cim"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/badlogic/gdx/graphics/glutils/b;

    invoke-static {p0}, Lcom/badlogic/gdx/graphics/k;->a(Lcom/badlogic/gdx/c/a;)Lcom/badlogic/gdx/graphics/j;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/b;-><init>(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j;Lcom/badlogic/gdx/graphics/j$c;Z)V

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/badlogic/gdx/c/a;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".etc1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/a;

    invoke-direct {p1, p0, p2}, Lcom/badlogic/gdx/graphics/glutils/a;-><init>(Lcom/badlogic/gdx/c/a;Z)V

    return-object p1

    :cond_2
    invoke-virtual {p0}, Lcom/badlogic/gdx/c/a;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".ktx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/badlogic/gdx/c/a;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".zktx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/badlogic/gdx/graphics/glutils/b;

    new-instance v1, Lcom/badlogic/gdx/graphics/j;

    invoke-direct {v1, p0}, Lcom/badlogic/gdx/graphics/j;-><init>(Lcom/badlogic/gdx/c/a;)V

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/b;-><init>(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j;Lcom/badlogic/gdx/graphics/j$c;Z)V

    return-object v0

    :cond_4
    :goto_0
    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/k;

    invoke-direct {p1, p0, p2}, Lcom/badlogic/gdx/graphics/glutils/k;-><init>(Lcom/badlogic/gdx/c/a;Z)V

    return-object p1
.end method
