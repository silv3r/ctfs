.class public Lcom/badlogic/gdx/graphics/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/h$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/badlogic/gdx/a;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/graphics/h;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field final b:Lcom/badlogic/gdx/graphics/glutils/r;

.field final c:Lcom/badlogic/gdx/graphics/glutils/j;

.field d:Z

.field final e:Z

.field private final f:Lcom/badlogic/gdx/math/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/graphics/h$a;ZIILcom/badlogic/gdx/graphics/q;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/h;->d:Z

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v1, p0, Lcom/badlogic/gdx/graphics/h;->f:Lcom/badlogic/gdx/math/g;

    sget-object v1, Lcom/badlogic/gdx/graphics/h$1;->a:[I

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/h$a;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/n;

    invoke-direct {p1, p3, p5}, Lcom/badlogic/gdx/graphics/glutils/n;-><init>(ILcom/badlogic/gdx/graphics/q;)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/g;

    invoke-direct {p1, p4}, Lcom/badlogic/gdx/graphics/glutils/g;-><init>(I)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/h;->e:Z

    goto :goto_1

    :pswitch_0
    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/q;

    invoke-direct {p1, p2, p3, p5}, Lcom/badlogic/gdx/graphics/glutils/q;-><init>(ZILcom/badlogic/gdx/graphics/q;)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/i;

    invoke-direct {p1, p2, p4}, Lcom/badlogic/gdx/graphics/glutils/i;-><init>(ZI)V

    goto :goto_0

    :pswitch_1
    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/p;

    invoke-direct {p1, p2, p3, p5}, Lcom/badlogic/gdx/graphics/glutils/p;-><init>(ZILcom/badlogic/gdx/graphics/q;)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/i;

    invoke-direct {p1, p2, p4}, Lcom/badlogic/gdx/graphics/glutils/i;-><init>(ZI)V

    goto :goto_0

    :pswitch_2
    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/o;

    invoke-direct {p1, p2, p3, p5}, Lcom/badlogic/gdx/graphics/glutils/o;-><init>(ZILcom/badlogic/gdx/graphics/q;)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    new-instance p1, Lcom/badlogic/gdx/graphics/glutils/h;

    invoke-direct {p1, p2, p4}, Lcom/badlogic/gdx/graphics/glutils/h;-><init>(ZI)V

    :goto_0
    iput-object p1, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    iput-boolean v1, p0, Lcom/badlogic/gdx/graphics/h;->e:Z

    :goto_1
    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-static {p1, p0}, Lcom/badlogic/gdx/graphics/h;->a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/h;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public varargs constructor <init>(Lcom/badlogic/gdx/graphics/h$a;ZII[Lcom/badlogic/gdx/graphics/p;)V
    .locals 6

    new-instance v5, Lcom/badlogic/gdx/graphics/q;

    invoke-direct {v5, p5}, Lcom/badlogic/gdx/graphics/q;-><init>([Lcom/badlogic/gdx/graphics/p;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/badlogic/gdx/graphics/h;-><init>(Lcom/badlogic/gdx/graphics/h$a;ZIILcom/badlogic/gdx/graphics/q;)V

    return-void
.end method

.method public static a(Lcom/badlogic/gdx/a;)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/utils/a;

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/h;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    invoke-interface {v1}, Lcom/badlogic/gdx/graphics/glutils/r;->a()V

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/h;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v1}, Lcom/badlogic/gdx/graphics/glutils/j;->g()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/h;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    sget-object p1, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    invoke-interface {p1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static b()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Managed meshes/app: { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a;

    sget-object v3, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/badlogic/gdx/a;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a([FII)Lcom/badlogic/gdx/graphics/h;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    invoke-interface {v0, p1, p2, p3}, Lcom/badlogic/gdx/graphics/glutils/r;->a([FII)V

    return-object p0
.end method

.method public a([S)Lcom/badlogic/gdx/graphics/h;
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2, v1}, Lcom/badlogic/gdx/graphics/glutils/j;->a([SII)V

    return-object p0
.end method

.method public a()Ljava/nio/ShortBuffer;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/glutils/j;->d()Ljava/nio/ShortBuffer;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/badlogic/gdx/graphics/h;->a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;III)V
    .locals 6

    iget-boolean v5, p0, Lcom/badlogic/gdx/graphics/h;->d:Z

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/badlogic/gdx/graphics/h;->a(Lcom/badlogic/gdx/graphics/glutils/m;IIIZ)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;IIIZ)V
    .locals 4

    if-nez p4, :cond_0

    return-void

    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/h;->a(Lcom/badlogic/gdx/graphics/glutils/m;)V

    :cond_1
    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/h;->e:Z

    const/16 v1, 0x1403

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/glutils/j;->a()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/glutils/j;->d()Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->position()I

    move-result v2

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->limit()I

    move-result v3

    invoke-virtual {v0, p3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    add-int/2addr p3, p4

    invoke-virtual {v0, p3}, Ljava/nio/ShortBuffer;->limit(I)Ljava/nio/Buffer;

    sget-object p3, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {p3, p2, p4, v1, v0}, Lcom/badlogic/gdx/graphics/e;->glDrawElements(IIILjava/nio/Buffer;)V

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v0, v3}, Ljava/nio/ShortBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0, p2, p3, p4}, Lcom/badlogic/gdx/graphics/e;->glDrawArrays(III)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/glutils/j;->a()I

    move-result v0

    if-lez v0, :cond_2

    add-int v0, p4, p3

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v2}, Lcom/badlogic/gdx/graphics/glutils/j;->b()I

    move-result v2

    if-gt v0, v2, :cond_4

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    mul-int/lit8 p3, p3, 0x2

    invoke-interface {v0, p2, p4, v1, p3}, Lcom/badlogic/gdx/graphics/e;->glDrawElements(IIII)V

    goto :goto_0

    :cond_4
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "Mesh attempting to access memory outside of the index buffer (count: "

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, ", offset: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ", max: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {p3}, Lcom/badlogic/gdx/graphics/glutils/j;->b()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ")"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1

    :goto_0
    if-eqz p5, :cond_5

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/h;->b(Lcom/badlogic/gdx/graphics/glutils/m;)V

    :cond_5
    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/r;->a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/glutils/j;->a()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/glutils/j;->e()V

    :cond_0
    return-void
.end method

.method public b(Lcom/badlogic/gdx/graphics/glutils/m;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/badlogic/gdx/graphics/h;->b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V

    return-void
.end method

.method public b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/r;->b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/glutils/j;->a()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/glutils/j;->f()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/graphics/h;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;Z)Z

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->b:Lcom/badlogic/gdx/graphics/glutils/r;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/glutils/r;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/h;->c:Lcom/badlogic/gdx/graphics/glutils/j;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/glutils/j;->c()V

    return-void
.end method
