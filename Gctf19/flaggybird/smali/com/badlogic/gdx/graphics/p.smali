.class public final Lcom/badlogic/gdx/graphics/p;
.super Ljava/lang/Object;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Z

.field public final d:I

.field public e:I

.field public f:Ljava/lang/String;

.field public g:I

.field private final h:I


# direct methods
.method public constructor <init>(IIIZLjava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/badlogic/gdx/graphics/p;->a:I

    iput p2, p0, Lcom/badlogic/gdx/graphics/p;->b:I

    iput p3, p0, Lcom/badlogic/gdx/graphics/p;->d:I

    iput-boolean p4, p0, Lcom/badlogic/gdx/graphics/p;->c:Z

    iput-object p5, p0, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    iput p6, p0, Lcom/badlogic/gdx/graphics/p;->g:I

    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/p;->h:I

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/badlogic/gdx/graphics/p;-><init>(IILjava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;I)V
    .locals 9

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/16 v1, 0x1401

    const/16 v5, 0x1401

    goto :goto_0

    :cond_0
    const/16 v1, 0x1406

    const/16 v5, 0x1406

    :goto_0
    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    const/4 v6, 0x0

    :goto_1
    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v7, p3

    move v8, p4

    invoke-direct/range {v2 .. v8}, Lcom/badlogic/gdx/graphics/p;-><init>(IIIZLjava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->h:I

    shl-int/lit8 v0, v0, 0x8

    iget v1, p0, Lcom/badlogic/gdx/graphics/p;->g:I

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    return v0
.end method

.method public a(Lcom/badlogic/gdx/graphics/p;)Z
    .locals 2

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->a:I

    iget v1, p1, Lcom/badlogic/gdx/graphics/p;->a:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v1, p1, Lcom/badlogic/gdx/graphics/p;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->d:I

    iget v1, p1, Lcom/badlogic/gdx/graphics/p;->d:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-boolean v1, p1, Lcom/badlogic/gdx/graphics/p;->c:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->g:I

    iget p1, p1, Lcom/badlogic/gdx/graphics/p;->g:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()I
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->d:I

    const/16 v1, 0x1406

    if-eq v0, v1, :cond_0

    const/16 v1, 0x140c

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->b:I

    mul-int/lit8 v0, v0, 0x2

    return v0

    :pswitch_1
    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->b:I

    return v0

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/graphics/p;->b:I

    mul-int/lit8 v0, v0, 0x4

    return v0

    :pswitch_data_0
    .packed-switch 0x1400
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/badlogic/gdx/graphics/p;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    check-cast p1, Lcom/badlogic/gdx/graphics/p;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/p;->a(Lcom/badlogic/gdx/graphics/p;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/p;->a()I

    move-result v0

    mul-int/lit16 v0, v0, 0x21d

    iget v1, p0, Lcom/badlogic/gdx/graphics/p;->b:I

    add-int/2addr v0, v1

    mul-int/lit16 v0, v0, 0x21d

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
