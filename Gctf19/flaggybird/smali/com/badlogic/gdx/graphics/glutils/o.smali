.class public Lcom/badlogic/gdx/graphics/glutils/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/glutils/r;


# instance fields
.field a:Z

.field b:Z

.field private c:Lcom/badlogic/gdx/graphics/q;

.field private d:Ljava/nio/FloatBuffer;

.field private e:Ljava/nio/ByteBuffer;

.field private f:Z

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(ZILcom/badlogic/gdx/graphics/q;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->a:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->b:Z

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v1}, Lcom/badlogic/gdx/graphics/e;->glGenBuffer()I

    move-result v1

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->g:I

    iget v1, p3, Lcom/badlogic/gdx/graphics/q;->a:I

    mul-int v1, v1, p2

    invoke-static {v1}, Lcom/badlogic/gdx/utils/BufferUtils;->d(I)Ljava/nio/ByteBuffer;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    const/4 v0, 0x1

    invoke-virtual {p0, p2, v0, p3}, Lcom/badlogic/gdx/graphics/glutils/o;->a(Ljava/nio/Buffer;ZLcom/badlogic/gdx/graphics/q;)V

    if-eqz p1, :cond_0

    const p1, 0x88e4

    goto :goto_0

    :cond_0
    const p1, 0x88e8

    :goto_0
    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/o;->a(I)V

    return-void
.end method

.method private b()V
    .locals 5

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const v1, 0x8892

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/o;->h:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->a:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenBuffer()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->g:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->a:Z

    return-void
.end method

.method protected a(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->b:Z

    if-nez v0, :cond_0

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->h:I

    return-void

    :cond_0
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string v0, "Cannot change usage while VBO is bound"

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 11

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->g:I

    const v2, 0x8892

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->a:Z

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/o;->d:Ljava/nio/FloatBuffer;

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->limit()I

    move-result v4

    mul-int/lit8 v4, v4, 0x4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    iget v5, p0, Lcom/badlogic/gdx/graphics/glutils/o;->h:I

    invoke-interface {v0, v2, v1, v4, v5}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    iput-boolean v3, p0, Lcom/badlogic/gdx/graphics/glutils/o;->a:Z

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v0

    if-nez p2, :cond_2

    :goto_0
    if-ge v3, v0, :cond_4

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {p2, v3}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object p2

    iget-object v1, p2, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/badlogic/gdx/graphics/glutils/m;->b(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v6, p2, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, p2, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, p2, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    iget v9, v1, Lcom/badlogic/gdx/graphics/q;->a:I

    iget v10, p2, Lcom/badlogic/gdx/graphics/p;->e:I

    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZII)V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    if-ge v3, v0, :cond_4

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v1, v3}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v1

    aget v5, p2, v3

    if-gez v5, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v6, v1, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, v1, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, v1, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    iget v9, v2, Lcom/badlogic/gdx/graphics/q;->a:I

    iget v10, v1, Lcom/badlogic/gdx/graphics/p;->e:I

    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZII)V

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->b:Z

    return-void
.end method

.method protected a(Ljava/nio/Buffer;ZLcom/badlogic/gdx/graphics/q;)V
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->a(Ljava/nio/ByteBuffer;)V

    :cond_0
    iput-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    instance-of p3, p1, Ljava/nio/ByteBuffer;

    if-eqz p3, :cond_1

    check-cast p1, Ljava/nio/ByteBuffer;

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->f:Z

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result p1

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    iget-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->d:Ljava/nio/FloatBuffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {p2, p1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->d:Ljava/nio/FloatBuffer;

    div-int/lit8 p1, p1, 0x4

    invoke-virtual {p2, p1}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    return-void

    :cond_1
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string p2, "Only ByteBuffer is currently supported"

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string p2, "Cannot change attributes while VBO is bound"

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a([FII)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->a:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-static {p1, v0, p3, p2}, Lcom/badlogic/gdx/utils/BufferUtils;->a([FLjava/nio/Buffer;II)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->d:Ljava/nio/FloatBuffer;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->d:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, p3}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/o;->b()V

    return-void
.end method

.method public b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 5

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    const/4 p2, 0x0

    :goto_0
    if-ge p2, v1, :cond_2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/o;->c:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v3, p2}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v3

    iget-object v3, v3, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_2

    aget v4, p2, v3

    if-ltz v4, :cond_1

    invoke-virtual {p1, v4}, Lcom/badlogic/gdx/graphics/glutils/m;->a(I)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const p1, 0x8892

    invoke-interface {v0, p1, v2}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iput-boolean v2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->b:Z

    return-void
.end method

.method public c()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x0

    const v2, 0x8892

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/o;->g:I

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteBuffer(I)V

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/o;->g:I

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/o;->e:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->a(Ljava/nio/ByteBuffer;)V

    :cond_0
    return-void
.end method
