.class public Lcom/badlogic/gdx/graphics/glutils/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/glutils/f$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/badlogic/gdx/graphics/glutils/f$a;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GLVersion"

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/f;->g:Ljava/lang/String;

    sget-object v0, Lcom/badlogic/gdx/a$a;->a:Lcom/badlogic/gdx/a$a;

    if-ne p1, v0, :cond_0

    :goto_0
    sget-object p1, Lcom/badlogic/gdx/graphics/glutils/f$a;->b:Lcom/badlogic/gdx/graphics/glutils/f$a;

    :goto_1
    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->f:Lcom/badlogic/gdx/graphics/glutils/f$a;

    goto :goto_3

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/a$a;->f:Lcom/badlogic/gdx/a$a;

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/badlogic/gdx/a$a;->b:Lcom/badlogic/gdx/a$a;

    if-ne p1, v0, :cond_2

    :goto_2
    sget-object p1, Lcom/badlogic/gdx/graphics/glutils/f$a;->a:Lcom/badlogic/gdx/graphics/glutils/f$a;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/badlogic/gdx/a$a;->d:Lcom/badlogic/gdx/a$a;

    if-ne p1, v0, :cond_3

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/badlogic/gdx/a$a;->e:Lcom/badlogic/gdx/a$a;

    if-ne p1, v0, :cond_4

    sget-object p1, Lcom/badlogic/gdx/graphics/glutils/f$a;->c:Lcom/badlogic/gdx/graphics/glutils/f$a;

    goto :goto_1

    :cond_4
    sget-object p1, Lcom/badlogic/gdx/graphics/glutils/f$a;->d:Lcom/badlogic/gdx/graphics/glutils/f$a;

    goto :goto_1

    :goto_3
    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->f:Lcom/badlogic/gdx/graphics/glutils/f$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/f$a;->b:Lcom/badlogic/gdx/graphics/glutils/f$a;

    if-ne p1, v0, :cond_5

    const-string p1, "OpenGL ES (\\d(\\.\\d){0,2})"

    :goto_4
    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_5
    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->f:Lcom/badlogic/gdx/graphics/glutils/f$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/f$a;->c:Lcom/badlogic/gdx/graphics/glutils/f$a;

    if-ne p1, v0, :cond_6

    const-string p1, "WebGL (\\d(\\.\\d){0,2})"

    goto :goto_4

    :cond_6
    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->f:Lcom/badlogic/gdx/graphics/glutils/f$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/f$a;->a:Lcom/badlogic/gdx/graphics/glutils/f$a;

    if-ne p1, v0, :cond_7

    const-string p1, "(\\d(\\.\\d){0,2})"

    goto :goto_4

    :cond_7
    const/4 p1, -0x1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->a:I

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->b:I

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->c:I

    const-string p3, ""

    const-string p4, ""

    :goto_5
    iput-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/f;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/badlogic/gdx/graphics/glutils/f;->e:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;I)I
    .locals 4

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "LibGDX GL"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing number: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", assuming: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    return p2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    invoke-virtual {p1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\\."

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    aget-object v0, p1, v2

    invoke-direct {p0, v0, v1}, Lcom/badlogic/gdx/graphics/glutils/f;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/f;->a:I

    array-length v0, p1

    if-ge v0, v1, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    aget-object p2, p1, p2

    invoke-direct {p0, p2, v2}, Lcom/badlogic/gdx/graphics/glutils/f;->a(Ljava/lang/String;I)I

    move-result p2

    :goto_0
    iput p2, p0, Lcom/badlogic/gdx/graphics/glutils/f;->b:I

    array-length p2, p1

    const/4 v0, 0x3

    if-ge p2, v0, :cond_1

    goto :goto_1

    :cond_1
    aget-object p1, p1, v1

    invoke-direct {p0, p1, v2}, Lcom/badlogic/gdx/graphics/glutils/f;->a(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v0, "GLVersion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid version string: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/f;->a:I

    iput v2, p0, Lcom/badlogic/gdx/graphics/glutils/f;->b:I

    :goto_1
    iput v2, p0, Lcom/badlogic/gdx/graphics/glutils/f;->c:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/f;->a:I

    return v0
.end method
