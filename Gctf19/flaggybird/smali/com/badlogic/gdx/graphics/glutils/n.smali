.class public Lcom/badlogic/gdx/graphics/glutils/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/glutils/r;


# instance fields
.field final a:Lcom/badlogic/gdx/graphics/q;

.field final b:Ljava/nio/FloatBuffer;

.field final c:Ljava/nio/ByteBuffer;

.field d:Z


# direct methods
.method public constructor <init>(ILcom/badlogic/gdx/graphics/q;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/n;->d:Z

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    iget p2, p2, Lcom/badlogic/gdx/graphics/q;->a:I

    mul-int p2, p2, p1

    invoke-static {p2}, Lcom/badlogic/gdx/utils/BufferUtils;->d(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 11

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    const/16 v1, 0x1406

    const/4 v2, 0x0

    if-nez p2, :cond_2

    :goto_0
    if-ge v2, v0, :cond_5

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {p2, v2}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object p2

    iget-object v3, p2, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/badlogic/gdx/graphics/glutils/m;->b(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v3, p2, Lcom/badlogic/gdx/graphics/p;->d:I

    if-ne v3, v1, :cond_1

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    iget v4, p2, Lcom/badlogic/gdx/graphics/p;->e:I

    div-int/lit8 v4, v4, 0x4

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget v6, p2, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, p2, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, p2, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    iget v9, p2, Lcom/badlogic/gdx/graphics/q;->a:I

    iget-object v10, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    :goto_1
    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZILjava/nio/Buffer;)V

    goto :goto_2

    :cond_1
    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    iget v4, p2, Lcom/badlogic/gdx/graphics/p;->e:I

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v6, p2, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, p2, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, p2, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    iget v9, p2, Lcom/badlogic/gdx/graphics/q;->a:I

    iget-object v10, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    goto :goto_1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_3
    if-ge v2, v0, :cond_5

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v3, v2}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v3

    aget v5, p2, v2

    if-gez v5, :cond_3

    goto :goto_5

    :cond_3
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v4, v3, Lcom/badlogic/gdx/graphics/p;->d:I

    if-ne v4, v1, :cond_4

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    iget v6, v3, Lcom/badlogic/gdx/graphics/p;->e:I

    div-int/lit8 v6, v6, 0x4

    invoke-virtual {v4, v6}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget v6, v3, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, v3, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, v3, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    iget v9, v3, Lcom/badlogic/gdx/graphics/q;->a:I

    iget-object v10, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    :goto_4
    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZILjava/nio/Buffer;)V

    goto :goto_5

    :cond_4
    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    iget v6, v3, Lcom/badlogic/gdx/graphics/p;->e:I

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v6, v3, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, v3, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, v3, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    iget v9, v3, Lcom/badlogic/gdx/graphics/q;->a:I

    iget-object v10, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    goto :goto_4

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->d:Z

    return-void
.end method

.method public a([FII)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    invoke-static {p1, v0, p3, p2}, Lcom/badlogic/gdx/utils/BufferUtils;->a([FLjava/nio/Buffer;II)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, p3}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    return-void
.end method

.method public b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v0

    const/4 v1, 0x0

    if-nez p2, :cond_0

    const/4 p2, 0x0

    :goto_0
    if-ge p2, v0, :cond_2

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/n;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v2, p2}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v2

    iget-object v2, v2, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    aget v3, p2, v2

    if-ltz v3, :cond_1

    invoke-virtual {p1, v3}, Lcom/badlogic/gdx/graphics/glutils/m;->a(I)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iput-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/n;->d:Z

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/n;->c:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->a(Ljava/nio/ByteBuffer;)V

    return-void
.end method
