.class public Lcom/badlogic/gdx/graphics/glutils/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/o;


# instance fields
.field final a:Lcom/badlogic/gdx/c/a;

.field b:I

.field c:I

.field d:Lcom/badlogic/gdx/graphics/j$c;

.field e:Lcom/badlogic/gdx/graphics/j;

.field f:Z

.field g:Z


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j;Lcom/badlogic/gdx/graphics/j$c;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->b:I

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->c:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->g:Z

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->a:Lcom/badlogic/gdx/c/a;

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    iput-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/b;->d:Lcom/badlogic/gdx/graphics/j$c;

    iput-boolean p4, p0, Lcom/badlogic/gdx/graphics/glutils/b;->f:Z

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->b:I

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->c:I

    if-nez p3, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->i()Lcom/badlogic/gdx/graphics/j$c;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->d:Lcom/badlogic/gdx/graphics/j$c;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string v0, "This TextureData implementation does not upload data itself"

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->g:Z

    return v0
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->g:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->a:Lcom/badlogic/gdx/c/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/c/a;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cim"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->a:Lcom/badlogic/gdx/c/a;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/k;->a(Lcom/badlogic/gdx/c/a;)Lcom/badlogic/gdx/graphics/j;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    goto :goto_1

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/graphics/j;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->a:Lcom/badlogic/gdx/c/a;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/j;-><init>(Lcom/badlogic/gdx/c/a;)V

    goto :goto_0

    :goto_1
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->b:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->c:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->d:Lcom/badlogic/gdx/graphics/j$c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->i()Lcom/badlogic/gdx/graphics/j$c;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->d:Lcom/badlogic/gdx/graphics/j$c;

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->g:Z

    return-void

    :cond_2
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Already prepared"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()Lcom/badlogic/gdx/graphics/o$b;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/o$b;->a:Lcom/badlogic/gdx/graphics/o$b;

    return-object v0
.end method

.method public f()Lcom/badlogic/gdx/graphics/j;
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->g:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/b;->e:Lcom/badlogic/gdx/graphics/j;

    return-object v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Call prepare() before calling getPixmap()"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->b:I

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->c:I

    return v0
.end method

.method public j()Lcom/badlogic/gdx/graphics/j$c;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->d:Lcom/badlogic/gdx/graphics/j$c;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->f:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/b;->a:Lcom/badlogic/gdx/c/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/c/a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
