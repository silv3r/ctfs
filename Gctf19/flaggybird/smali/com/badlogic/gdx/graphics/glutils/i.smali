.class public Lcom/badlogic/gdx/graphics/glutils/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/glutils/j;


# instance fields
.field final a:Ljava/nio/ShortBuffer;

.field final b:Ljava/nio/ByteBuffer;

.field c:I

.field final d:Z

.field e:Z

.field f:Z

.field final g:I


# direct methods
.method public constructor <init>(ZI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->f:Z

    mul-int/lit8 p2, p2, 0x2

    invoke-static {p2}, Lcom/badlogic/gdx/utils/BufferUtils;->b(I)Ljava/nio/ByteBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->d:Z

    if-eqz p1, :cond_0

    const p1, 0x88e4

    goto :goto_0

    :cond_0
    const p1, 0x88e8

    :goto_0
    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->g:I

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {p1}, Ljava/nio/ShortBuffer;->flip()Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/i;->h()I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->c:I

    return-void
.end method

.method private h()I
    .locals 6

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenBuffer()I

    move-result v0

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const v2, 0x8893

    invoke-interface {v1, v2, v0}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/i;->g:I

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v5, v4}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->limit()I

    move-result v0

    return v0
.end method

.method public a([SII)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {v1, p1, p2, p3}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {p1}, Ljava/nio/ShortBuffer;->flip()Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    shl-int/2addr p3, v0

    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->f:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const p3, 0x8893

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    invoke-interface {p1, p3, p2, v0, v1}, Lcom/badlogic/gdx/graphics/e;->glBufferSubData(IIILjava/nio/Buffer;)V

    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->capacity()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x0

    const v2, 0x8893

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/i;->c:I

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteBuffer(I)V

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->c:I

    return-void
.end method

.method public d()Ljava/nio/ShortBuffer;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    return-object v0
.end method

.method public e()V
    .locals 5

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->c:I

    if-eqz v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->c:I

    const v2, 0x8893

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->a:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/i;->b:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-interface {v0, v2, v4, v1, v3}, Lcom/badlogic/gdx/graphics/e;->glBufferSubData(IIILjava/nio/Buffer;)V

    iput-boolean v4, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->f:Z

    return-void

    :cond_1
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "IndexBufferObject cannot be used after it has been disposed."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x0

    const v2, 0x8893

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iput-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/i;->f:Z

    return-void
.end method

.method public g()V
    .locals 1

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/i;->h()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/i;->e:Z

    return-void
.end method
