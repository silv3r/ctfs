.class public Lcom/badlogic/gdx/graphics/glutils/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/o;


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:I

.field e:I

.field f:I

.field g:I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->a:I

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->b:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->c:Z

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->d:I

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/e;->a:I

    iput p2, p0, Lcom/badlogic/gdx/graphics/glutils/e;->b:I

    iput p3, p0, Lcom/badlogic/gdx/graphics/glutils/e;->d:I

    iput p4, p0, Lcom/badlogic/gdx/graphics/glutils/e;->e:I

    iput p5, p0, Lcom/badlogic/gdx/graphics/glutils/e;->f:I

    iput p6, p0, Lcom/badlogic/gdx/graphics/glutils/e;->g:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 10

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/e;->d:I

    iget v3, p0, Lcom/badlogic/gdx/graphics/glutils/e;->e:I

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/e;->a:I

    iget v5, p0, Lcom/badlogic/gdx/graphics/glutils/e;->b:I

    iget v7, p0, Lcom/badlogic/gdx/graphics/glutils/e;->f:I

    iget v8, p0, Lcom/badlogic/gdx/graphics/glutils/e;->g:I

    const/4 v6, 0x0

    const/4 v9, 0x0

    move v1, p1

    invoke-interface/range {v0 .. v9}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->c:Z

    return v0
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->c:Z

    return-void

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Already prepared"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/badlogic/gdx/graphics/o$b;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/o$b;->b:Lcom/badlogic/gdx/graphics/o$b;

    return-object v0
.end method

.method public f()Lcom/badlogic/gdx/graphics/j;
    .locals 2

    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "This TextureData implementation does not return a Pixmap"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g()Z
    .locals 2

    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "This TextureData implementation does not return a Pixmap"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->a:I

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/e;->b:I

    return v0
.end method

.method public j()Lcom/badlogic/gdx/graphics/j$c;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->g:Lcom/badlogic/gdx/graphics/j$c;

    return-object v0
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
