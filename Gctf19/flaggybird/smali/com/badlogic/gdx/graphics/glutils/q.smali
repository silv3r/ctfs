.class public Lcom/badlogic/gdx/graphics/glutils/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/glutils/r;


# static fields
.field static final a:Ljava/nio/IntBuffer;


# instance fields
.field final b:Lcom/badlogic/gdx/graphics/q;

.field final c:Ljava/nio/FloatBuffer;

.field final d:Ljava/nio/ByteBuffer;

.field e:I

.field final f:Z

.field final g:I

.field h:Z

.field i:Z

.field j:I

.field k:Lcom/badlogic/gdx/utils/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->c(I)Ljava/nio/IntBuffer;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    return-void
.end method

.method public constructor <init>(ZILcom/badlogic/gdx/graphics/q;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->h:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->i:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->j:I

    new-instance v0, Lcom/badlogic/gdx/utils/e;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/e;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->f:Z

    iput-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    iget-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    iget p3, p3, Lcom/badlogic/gdx/graphics/q;->a:I

    mul-int p3, p3, p2

    invoke-static {p3}, Lcom/badlogic/gdx/utils/BufferUtils;->d(I)Ljava/nio/ByteBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->c:Ljava/nio/FloatBuffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {p2}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    sget-object p2, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {p2}, Lcom/badlogic/gdx/graphics/e;->glGenBuffer()I

    move-result p2

    iput p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->e:I

    if-eqz p1, :cond_0

    const p1, 0x88e4

    goto :goto_0

    :cond_0
    const p1, 0x88e8

    :goto_0
    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->g:I

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/q;->d()V

    return-void
.end method

.method private a(Lcom/badlogic/gdx/graphics/e;)V
    .locals 4

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->e:I

    const v1, 0x8892

    invoke-interface {p1, v1, v0}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    iget v3, p0, Lcom/badlogic/gdx/graphics/glutils/q;->g:I

    invoke-interface {p1, v1, v0, v2, v3}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->h:Z

    :cond_0
    return-void
.end method

.method private a(Lcom/badlogic/gdx/graphics/glutils/m;)V
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    iget v0, v0, Lcom/badlogic/gdx/utils/e;->b:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/e;->b(I)I

    move-result v2

    if-gez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v2}, Lcom/badlogic/gdx/graphics/glutils/m;->a(I)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private b()V
    .locals 5

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->i:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const v1, 0x8892

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/q;->g:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->h:Z

    :cond_0
    return-void
.end method

.method private c(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 11

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    iget v0, v0, Lcom/badlogic/gdx/utils/e;->b:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v3

    if-eqz v0, :cond_5

    if-nez p2, :cond_2

    const/4 v4, 0x0

    :goto_1
    if-eqz v0, :cond_5

    if-ge v4, v3, :cond_5

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0, v4}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v0

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/graphics/glutils/m;->b(Ljava/lang/String;)I

    move-result v0

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/utils/e;->b(I)I

    move-result v5

    if-ne v0, v5, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    array-length v0, p2

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    iget v4, v4, Lcom/badlogic/gdx/utils/e;->b:I

    if-ne v0, v4, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    const/4 v4, 0x0

    :goto_4
    if-eqz v0, :cond_5

    if-ge v4, v3, :cond_5

    aget v0, p2, v4

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/utils/e;->b(I)I

    move-result v5

    if-ne v0, v5, :cond_4

    const/4 v0, 0x1

    goto :goto_5

    :cond_4
    const/4 v0, 0x0

    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    if-nez v0, :cond_8

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const v2, 0x8892

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/q;->e:I

    invoke-interface {v0, v2, v4}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/q;->a(Lcom/badlogic/gdx/graphics/glutils/m;)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/e;->a()V

    :goto_6
    if-ge v1, v3, :cond_8

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v0

    if-nez p2, :cond_6

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    iget-object v4, v0, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/badlogic/gdx/graphics/glutils/m;->b(Ljava/lang/String;)I

    move-result v4

    :goto_7
    invoke-virtual {v2, v4}, Lcom/badlogic/gdx/utils/e;->a(I)V

    goto :goto_8

    :cond_6
    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    aget v4, p2, v1

    goto :goto_7

    :goto_8
    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->k:Lcom/badlogic/gdx/utils/e;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/e;->b(I)I

    move-result v5

    if-gez v5, :cond_7

    goto :goto_9

    :cond_7
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v6, v0, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, v0, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, v0, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->b:Lcom/badlogic/gdx/graphics/q;

    iget v9, v2, Lcom/badlogic/gdx/graphics/q;->a:I

    iget v10, v0, Lcom/badlogic/gdx/graphics/p;->e:I

    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZII)V

    :goto_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_8
    return-void
.end method

.method private d()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    sget-object v0, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    sget-object v1, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/f;->c(ILjava/nio/IntBuffer;)V

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->get()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->j:I

    return-void
.end method

.method private e()V
    .locals 4

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->j:I

    invoke-virtual {v0, v2}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->flip()Ljava/nio/Buffer;

    sget-object v0, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    const/4 v2, 0x1

    sget-object v3, Lcom/badlogic/gdx/graphics/glutils/q;->a:Ljava/nio/IntBuffer;

    invoke-interface {v0, v2, v3}, Lcom/badlogic/gdx/graphics/f;->b(ILjava/nio/IntBuffer;)V

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->j:I

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/f;->glGenBuffer()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->e:I

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/q;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->h:Z

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->j:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/f;->a(I)V

    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/q;->c(Lcom/badlogic/gdx/graphics/glutils/m;[I)V

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/graphics/glutils/q;->a(Lcom/badlogic/gdx/graphics/e;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->i:Z

    return-void
.end method

.method public a([FII)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->h:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    invoke-static {p1, v0, p3, p2}, Lcom/badlogic/gdx/utils/BufferUtils;->a([FLjava/nio/Buffer;II)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->c:Ljava/nio/FloatBuffer;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, p3}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/q;->b()V

    return-void
.end method

.method public b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 0

    sget-object p1, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lcom/badlogic/gdx/graphics/f;->a(I)V

    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->i:Z

    return-void
.end method

.method public c()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    const/4 v1, 0x0

    const v2, 0x8892

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/f;->glBindBuffer(II)V

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/q;->e:I

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/graphics/f;->glDeleteBuffer(I)V

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/q;->e:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/q;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->a(Ljava/nio/ByteBuffer;)V

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/q;->e()V

    return-void
.end method
