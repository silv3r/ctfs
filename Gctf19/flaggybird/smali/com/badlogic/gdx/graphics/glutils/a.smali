.class public Lcom/badlogic/gdx/graphics/glutils/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/o;


# instance fields
.field a:Lcom/badlogic/gdx/c/a;

.field b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

.field c:Z

.field d:I

.field e:I

.field f:Z


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/c/a;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->d:I

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->e:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->f:Z

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/a;->a:Lcom/badlogic/gdx/c/a;

    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/a;->c:Z

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 13

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->f:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    const-string v1, "GL_OES_compressed_ETC1_RGB8_texture"

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/h;->a(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    sget-object v2, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    invoke-static {v0, v2}, Lcom/badlogic/gdx/graphics/glutils/ETC1;->a(Lcom/badlogic/gdx/graphics/glutils/ETC1$a;Lcom/badlogic/gdx/graphics/j$c;)Lcom/badlogic/gdx/graphics/j;

    move-result-object v0

    sget-object v2, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->f()I

    move-result v5

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v6

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->e()I

    move-result v9

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->g()I

    move-result v10

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v11

    move v3, p1

    invoke-interface/range {v2 .. v11}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    iget-boolean v2, p0, Lcom/badlogic/gdx/graphics/glutils/a;->c:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v2

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v3

    invoke-static {p1, v0, v2, v3}, Lcom/badlogic/gdx/graphics/glutils/l;->a(ILcom/badlogic/gdx/graphics/j;II)V

    :cond_0
    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->c()V

    iput-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/a;->c:Z

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/4 v6, 0x0

    sget v7, Lcom/badlogic/gdx/graphics/glutils/ETC1;->b:I

    iget v8, p0, Lcom/badlogic/gdx/graphics/glutils/a;->d:I

    iget v9, p0, Lcom/badlogic/gdx/graphics/glutils/a;->e:I

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iget v2, v2, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->d:I

    sub-int v11, v0, v2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iget-object v12, v0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->c:Ljava/nio/ByteBuffer;

    move v5, p1

    invoke-interface/range {v4 .. v12}, Lcom/badlogic/gdx/graphics/e;->glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/glutils/a;->k()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Lcom/badlogic/gdx/graphics/e;->glGenerateMipmap(I)V

    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->c()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iput-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/a;->f:Z

    return-void

    :cond_3
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string v0, "Call prepare() before calling consumeCompressedData()"

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->f:Z

    return v0
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->f:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->a:Lcom/badlogic/gdx/c/a;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Can only load once from ETC1Data"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->a:Lcom/badlogic/gdx/c/a;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/a;->a:Lcom/badlogic/gdx/c/a;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;-><init>(Lcom/badlogic/gdx/c/a;)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iget v0, v0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->a:I

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->b:Lcom/badlogic/gdx/graphics/glutils/ETC1$a;

    iget v0, v0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->b:I

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->e:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->f:Z

    return-void

    :cond_3
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Already prepared"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()Lcom/badlogic/gdx/graphics/o$b;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/o$b;->b:Lcom/badlogic/gdx/graphics/o$b;

    return-object v0
.end method

.method public f()Lcom/badlogic/gdx/graphics/j;
    .locals 2

    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "This TextureData implementation does not return a Pixmap"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g()Z
    .locals 2

    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "This TextureData implementation does not return a Pixmap"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->d:I

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->e:I

    return v0
.end method

.method public j()Lcom/badlogic/gdx/graphics/j$c;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/a;->c:Z

    return v0
.end method
