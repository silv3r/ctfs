.class public Lcom/badlogic/gdx/graphics/glutils/c;
.super Lcom/badlogic/gdx/graphics/glutils/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/badlogic/gdx/graphics/glutils/d<",
        "Lcom/badlogic/gdx/graphics/l;",
        ">;"
    }
.end annotation


# virtual methods
.method protected a(Lcom/badlogic/gdx/graphics/glutils/d$b;)Lcom/badlogic/gdx/graphics/l;
    .locals 8

    new-instance v7, Lcom/badlogic/gdx/graphics/glutils/e;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/c;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d$c;->a:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/c;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget v2, v0, Lcom/badlogic/gdx/graphics/glutils/d$c;->b:I

    iget v4, p1, Lcom/badlogic/gdx/graphics/glutils/d$b;->a:I

    iget v5, p1, Lcom/badlogic/gdx/graphics/glutils/d$b;->b:I

    iget v6, p1, Lcom/badlogic/gdx/graphics/glutils/d$b;->c:I

    const/4 v3, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/badlogic/gdx/graphics/glutils/e;-><init>(IIIIII)V

    new-instance p1, Lcom/badlogic/gdx/graphics/l;

    invoke-direct {p1, v7}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/graphics/o;)V

    sget-object v0, Lcom/badlogic/gdx/graphics/l$a;->b:Lcom/badlogic/gdx/graphics/l$a;

    sget-object v1, Lcom/badlogic/gdx/graphics/l$a;->b:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {p1, v0, v1}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;)V

    sget-object v0, Lcom/badlogic/gdx/graphics/l$b;->b:Lcom/badlogic/gdx/graphics/l$b;

    sget-object v1, Lcom/badlogic/gdx/graphics/l$b;->b:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p1, v0, v1}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;)V

    return-object p1
.end method

.method protected synthetic a(Lcom/badlogic/gdx/graphics/g;)V
    .locals 0

    check-cast p1, Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/c;->b(Lcom/badlogic/gdx/graphics/l;)V

    return-void
.end method

.method protected a(Lcom/badlogic/gdx/graphics/l;)V
    .locals 0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->c()V

    return-void
.end method

.method protected synthetic b(Lcom/badlogic/gdx/graphics/glutils/d$b;)Lcom/badlogic/gdx/graphics/g;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/c;->a(Lcom/badlogic/gdx/graphics/glutils/d$b;)Lcom/badlogic/gdx/graphics/l;

    move-result-object p1

    return-object p1
.end method

.method protected synthetic b(Lcom/badlogic/gdx/graphics/g;)V
    .locals 0

    check-cast p1, Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/c;->a(Lcom/badlogic/gdx/graphics/l;)V

    return-void
.end method

.method protected b(Lcom/badlogic/gdx/graphics/l;)V
    .locals 6

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->k()I

    move-result v4

    const v1, 0x8d40

    const v2, 0x8ce0

    const/16 v3, 0xde1

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/badlogic/gdx/graphics/e;->glFramebufferTexture2D(IIIII)V

    return-void
.end method
