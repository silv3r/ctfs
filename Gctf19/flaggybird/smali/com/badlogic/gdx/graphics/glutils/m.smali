.class public Lcom/badlogic/gdx/graphics/glutils/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/b;


# static fields
.field public static a:Z = true

.field public static b:Ljava/lang/String; = ""

.field public static c:Ljava/lang/String; = ""

.field static final d:Ljava/nio/IntBuffer;

.field private static final g:Lcom/badlogic/gdx/utils/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/i<",
            "Lcom/badlogic/gdx/a;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/graphics/glutils/m;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field e:Ljava/nio/IntBuffer;

.field f:Ljava/nio/IntBuffer;

.field private h:Ljava/lang/String;

.field private i:Z

.field private final j:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:[Ljava/lang/String;

.field private final n:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/badlogic/gdx/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:[Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:I

.field private final u:Ljava/nio/FloatBuffer;

.field private final v:Ljava/lang/String;

.field private final w:Ljava/lang/String;

.field private x:Z

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/badlogic/gdx/utils/i;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/i;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->c(I)Ljava/nio/IntBuffer;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->d:Ljava/nio/IntBuffer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->j:Lcom/badlogic/gdx/utils/h;

    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->k:Lcom/badlogic/gdx/utils/h;

    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->l:Lcom/badlogic/gdx/utils/h;

    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->n:Lcom/badlogic/gdx/utils/h;

    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->o:Lcom/badlogic/gdx/utils/h;

    new-instance v0, Lcom/badlogic/gdx/utils/h;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/h;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->p:Lcom/badlogic/gdx/utils/h;

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->y:I

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->c(I)Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->c(I)Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    if-eqz p1, :cond_4

    if-eqz p2, :cond_3

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/badlogic/gdx/graphics/glutils/m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/badlogic/gdx/graphics/glutils/m;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_1
    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->v:Ljava/lang/String;

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->w:Ljava/lang/String;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/badlogic/gdx/utils/BufferUtils;->a(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->u:Ljava/nio/FloatBuffer;

    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->d()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->j()V

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->i()V

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-direct {p0, p1, p0}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/glutils/m;)V

    :cond_2
    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "fragment shader must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "vertex shader must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(ILjava/lang/String;)I
    .locals 4

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/badlogic/gdx/utils/BufferUtils;->c(I)Ljava/nio/IntBuffer;

    move-result-object v1

    invoke-interface {v0, p1}, Lcom/badlogic/gdx/graphics/e;->glCreateShader(I)I

    move-result v2

    const/4 v3, -0x1

    if-nez v2, :cond_0

    return v3

    :cond_0
    invoke-interface {v0, v2, p2}, Lcom/badlogic/gdx/graphics/e;->glShaderSource(ILjava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/graphics/e;->glCompileShader(I)V

    const p2, 0x8b81

    invoke-interface {v0, v2, p2, v1}, Lcom/badlogic/gdx/graphics/e;->glGetShaderiv(IILjava/nio/IntBuffer;)V

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, Ljava/nio/IntBuffer;->get(I)I

    move-result p2

    if-nez p2, :cond_2

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/graphics/e;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x8b31

    if-ne p1, v1, :cond_1

    const-string p1, "Vertex shader\n"

    goto :goto_0

    :cond_1
    const-string p1, "Fragment shader:\n"

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    return v3

    :cond_2
    return v2
.end method

.method public static a(Lcom/badlogic/gdx/a;)V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/utils/a;

    if-nez p0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v0, v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/glutils/m;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/badlogic/gdx/graphics/glutils/m;->x:Z

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/glutils/m;

    invoke-direct {v1}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/glutils/m;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    sget-object p2, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {p2, p1, v0}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const v0, 0x8b31

    invoke-direct {p0, v0, p1}, Lcom/badlogic/gdx/graphics/glutils/m;->a(ILjava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->s:I

    const p1, 0x8b30

    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/m;->a(ILjava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->t:I

    iget p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->s:I

    const/4 p2, 0x0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    iget p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->t:I

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->a()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/m;->c(I)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    iget p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    if-ne p1, v0, :cond_1

    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->i:Z

    return-void

    :cond_1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->i:Z

    return-void

    :cond_2
    :goto_0
    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->i:Z

    return-void
.end method

.method public static b(Lcom/badlogic/gdx/a;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v0, p0}, Lcom/badlogic/gdx/utils/i;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private c(I)I
    .locals 4

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    return v1

    :cond_0
    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->s:I

    invoke-interface {v0, p1, v2}, Lcom/badlogic/gdx/graphics/e;->glAttachShader(II)V

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->t:I

    invoke-interface {v0, p1, v2}, Lcom/badlogic/gdx/graphics/e;->glAttachShader(II)V

    invoke-interface {v0, p1}, Lcom/badlogic/gdx/graphics/e;->glLinkProgram(I)V

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v2

    const v3, 0x8b82

    invoke-interface {v0, p1, v3, v2}, Lcom/badlogic/gdx/graphics/e;->glGetProgramiv(IILjava/nio/IntBuffer;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0, p1}, Lcom/badlogic/gdx/graphics/e;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    return v1

    :cond_1
    return p1
.end method

.method private c(Ljava/lang/String;)I
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->n:Lcom/badlogic/gdx/utils/h;

    const/4 v2, -0x2

    invoke-virtual {v1, p1, v2}, Lcom/badlogic/gdx/utils/h;->b(Ljava/lang/Object;I)I

    move-result v1

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/graphics/e;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->n:Lcom/badlogic/gdx/utils/h;

    invoke-virtual {v0, p1, v1}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    :cond_0
    return v1
.end method

.method private d(Ljava/lang/String;)I
    .locals 1

    sget-boolean v0, Lcom/badlogic/gdx/graphics/glutils/m;->a:Z

    invoke-virtual {p0, p1, v0}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;Z)I

    move-result p1

    return p1
.end method

.method public static g()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Managed shaders/app: { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/i;->d()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/i$c;->a()Lcom/badlogic/gdx/utils/i$c;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a;

    sget-object v3, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    invoke-virtual {v3, v2}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->v:Ljava/lang/String;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->w:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->x:Z

    :cond_0
    return-void
.end method

.method private i()V
    .locals 7

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    const v3, 0x8b86

    invoke-interface {v0, v1, v3, v2}, Lcom/badlogic/gdx/graphics/e;->glGetProgramiv(IILjava/nio/IntBuffer;)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->m:[Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v3}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    invoke-virtual {v3}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    sget-object v3, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    iget-object v6, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    invoke-interface {v3, v4, v2, v5, v6}, Lcom/badlogic/gdx/graphics/e;->glGetActiveUniform(IILjava/nio/IntBuffer;Ljava/nio/Buffer;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v4, v5, v3}, Lcom/badlogic/gdx/graphics/e;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->j:Lcom/badlogic/gdx/utils/h;

    invoke-virtual {v5, v3, v4}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->k:Lcom/badlogic/gdx/utils/h;

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    invoke-virtual {v5, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->l:Lcom/badlogic/gdx/utils/h;

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v5, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->m:[Ljava/lang/String;

    aput-object v3, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private j()V
    .locals 7

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    const v3, 0x8b89

    invoke-interface {v0, v1, v3, v2}, Lcom/badlogic/gdx/graphics/e;->glGetProgramiv(IILjava/nio/IntBuffer;)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->q:[Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v3}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    invoke-virtual {v3}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    sget-object v3, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    iget-object v6, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    invoke-interface {v3, v4, v2, v5, v6}, Lcom/badlogic/gdx/graphics/e;->glGetActiveAttrib(IILjava/nio/IntBuffer;Ljava/nio/Buffer;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v4, v5, v3}, Lcom/badlogic/gdx/graphics/e;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->n:Lcom/badlogic/gdx/utils/h;

    invoke-virtual {v5, v3, v4}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->o:Lcom/badlogic/gdx/utils/h;

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->f:Ljava/nio/IntBuffer;

    invoke-virtual {v5, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->p:Lcom/badlogic/gdx/utils/h;

    iget-object v5, p0, Lcom/badlogic/gdx/graphics/glutils/m;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v5, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/m;->q:[Ljava/lang/String;

    aput-object v3, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glCreateProgram()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public a(Ljava/lang/String;Z)I
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->j:Lcom/badlogic/gdx/utils/h;

    const/4 v2, -0x2

    invoke-virtual {v1, p1, v2}, Lcom/badlogic/gdx/utils/h;->b(Ljava/lang/Object;I)I

    move-result v1

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/graphics/e;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "no uniform with name \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' in shader"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/m;->j:Lcom/badlogic/gdx/utils/h;

    invoke-virtual {p2, p1, v1}, Lcom/badlogic/gdx/utils/h;->a(Ljava/lang/Object;I)V

    :cond_2
    return v1
.end method

.method public a(I)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    invoke-interface {v0, p1}, Lcom/badlogic/gdx/graphics/e;->glDisableVertexAttribArray(I)V

    return-void
.end method

.method public a(IIIZII)V
    .locals 7

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/badlogic/gdx/graphics/e;->glVertexAttribPointer(IIIZII)V

    return-void
.end method

.method public a(IIIZILjava/nio/Buffer;)V
    .locals 7

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/badlogic/gdx/graphics/e;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    return-void
.end method

.method public a(ILcom/badlogic/gdx/math/Matrix4;Z)V
    .locals 6

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    iget-object v4, p2, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    const/4 v2, 0x1

    const/4 v5, 0x0

    move v1, p1

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lcom/badlogic/gdx/graphics/e;->glUniformMatrix4fv(IIZ[FI)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/m;->c(Ljava/lang/String;)I

    move-result p1

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    return-void

    :cond_0
    invoke-interface {v0, p1}, Lcom/badlogic/gdx/graphics/e;->glDisableVertexAttribArray(I)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/m;->d(Ljava/lang/String;)I

    move-result p1

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/graphics/e;->glUniform1i(II)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/badlogic/gdx/math/Matrix4;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;Lcom/badlogic/gdx/math/Matrix4;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/badlogic/gdx/math/Matrix4;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/m;->d(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/badlogic/gdx/graphics/glutils/m;->a(ILcom/badlogic/gdx/math/Matrix4;Z)V

    return-void
.end method

.method public b(Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->n:Lcom/badlogic/gdx/utils/h;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/badlogic/gdx/utils/h;->b(Ljava/lang/Object;I)I

    move-result p1

    return p1
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->i:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->h:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    invoke-interface {v0, p1}, Lcom/badlogic/gdx/graphics/e;->glEnableVertexAttribArray(I)V

    return-void
.end method

.method public c()V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glUseProgram(I)V

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->s:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteShader(I)V

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->t:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteShader(I)V

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteProgram(I)V

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/m;->g:Lcom/badlogic/gdx/utils/i;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/utils/i;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;Z)Z

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/m;->i:Z

    return v0
.end method

.method public e()V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/m;->h()V

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/m;->r:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glUseProgram(I)V

    return-void
.end method

.method public f()V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glUseProgram(I)V

    return-void
.end method
