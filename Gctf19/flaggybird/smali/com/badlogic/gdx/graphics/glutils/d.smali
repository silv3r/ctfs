.class public abstract Lcom/badlogic/gdx/graphics/glutils/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/glutils/d$c;,
        Lcom/badlogic/gdx/graphics/glutils/d$a;,
        Lcom/badlogic/gdx/graphics/glutils/d$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/badlogic/gdx/graphics/g;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/badlogic/gdx/utils/b;"
    }
.end annotation


# static fields
.field protected static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/badlogic/gdx/a;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/graphics/glutils/d;",
            ">;>;"
        }
    .end annotation
.end field

.field protected static c:I

.field protected static d:Z


# instance fields
.field protected b:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field protected e:I

.field protected f:I

.field protected g:I

.field protected h:I

.field protected i:Z

.field protected j:Z

.field protected k:Lcom/badlogic/gdx/graphics/glutils/d$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/graphics/glutils/d$c<",
            "+",
            "Lcom/badlogic/gdx/graphics/glutils/d<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/badlogic/gdx/graphics/glutils/d;->d:Z

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3

    const-string v0, "Managed buffers/app: { "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/a;

    sget-object v2, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/utils/a;

    iget v1, v1, Lcom/badlogic/gdx/utils/a;->b:I

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v0, "}"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public static a(Lcom/badlogic/gdx/a;)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/utils/a;

    if-nez p0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v0, v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/glutils/d;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/glutils/d;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/glutils/d;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    sget-object p1, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    invoke-interface {p1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/glutils/d;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/badlogic/gdx/a;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private d()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v0}, Lcom/badlogic/gdx/h;->a()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v0, v0, Lcom/badlogic/gdx/graphics/glutils/d$c;->i:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/glutils/d$c;->c:Lcom/badlogic/gdx/utils/a;

    iget v0, v0, Lcom/badlogic/gdx/utils/a;->b:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_4

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/glutils/d$c;->c:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/glutils/d$b;

    iget-boolean v2, v1, Lcom/badlogic/gdx/graphics/glutils/d$b;->e:Z

    if-nez v2, :cond_3

    iget-boolean v2, v1, Lcom/badlogic/gdx/graphics/glutils/d$b;->f:Z

    if-nez v2, :cond_2

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$b;->d:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    const-string v2, "OES_texture_float"

    invoke-interface {v1, v2}, Lcom/badlogic/gdx/h;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Float texture FrameBuffer Attachment not available on GLES 2.0"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Stencil texture FrameBuffer Attachment not available on GLES 2.0"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Depth texture FrameBuffer Attachment not available on GLES 2.0"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Multiple render targets not available on GLES 2.0"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Packed Stencil/Render render buffers are not available on GLES 2.0"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 18

    move-object/from16 v0, p0

    sget-object v7, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/graphics/glutils/d;->d()V

    sget-boolean v1, Lcom/badlogic/gdx/graphics/glutils/d;->d:Z

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-nez v1, :cond_1

    sput-boolean v8, Lcom/badlogic/gdx/graphics/glutils/d;->d:Z

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v1}, Lcom/badlogic/gdx/a;->c()Lcom/badlogic/gdx/a$a;

    move-result-object v1

    sget-object v2, Lcom/badlogic/gdx/a$a;->f:Lcom/badlogic/gdx/a$a;

    if-ne v1, v2, :cond_0

    const/16 v1, 0x40

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    const v2, 0x8ca6

    invoke-interface {v7, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    invoke-virtual {v1, v9}, Ljava/nio/IntBuffer;->get(I)I

    move-result v1

    sput v1, Lcom/badlogic/gdx/graphics/glutils/d;->c:I

    goto :goto_0

    :cond_0
    sput v9, Lcom/badlogic/gdx/graphics/glutils/d;->c:I

    :cond_1
    :goto_0
    invoke-interface {v7}, Lcom/badlogic/gdx/graphics/e;->glGenFramebuffer()I

    move-result v1

    iput v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->e:I

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->e:I

    const v10, 0x8d40

    invoke-interface {v7, v10, v1}, Lcom/badlogic/gdx/graphics/e;->glBindFramebuffer(II)V

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget v11, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->a:I

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget v12, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->b:I

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->h:Z

    const v13, 0x8d41

    if-eqz v1, :cond_2

    invoke-interface {v7}, Lcom/badlogic/gdx/graphics/e;->glGenRenderbuffer()I

    move-result v1

    iput v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    invoke-interface {v7, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glBindRenderbuffer(II)V

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->e:Lcom/badlogic/gdx/graphics/glutils/d$a;

    iget v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$a;->a:I

    invoke-interface {v7, v13, v1, v11, v12}, Lcom/badlogic/gdx/graphics/e;->glRenderbufferStorage(IIII)V

    :cond_2
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->g:Z

    if-eqz v1, :cond_3

    invoke-interface {v7}, Lcom/badlogic/gdx/graphics/e;->glGenRenderbuffer()I

    move-result v1

    iput v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    invoke-interface {v7, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glBindRenderbuffer(II)V

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->d:Lcom/badlogic/gdx/graphics/glutils/d$a;

    iget v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$a;->a:I

    invoke-interface {v7, v13, v1, v11, v12}, Lcom/badlogic/gdx/graphics/e;->glRenderbufferStorage(IIII)V

    :cond_3
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->i:Z

    if-eqz v1, :cond_4

    invoke-interface {v7}, Lcom/badlogic/gdx/graphics/e;->glGenRenderbuffer()I

    move-result v1

    iput v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glBindRenderbuffer(II)V

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->f:Lcom/badlogic/gdx/graphics/glutils/d$a;

    iget v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$a;->a:I

    invoke-interface {v7, v13, v1, v11, v12}, Lcom/badlogic/gdx/graphics/e;->glRenderbufferStorage(IIII)V

    :cond_4
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->c:Lcom/badlogic/gdx/utils/a;

    iget v1, v1, Lcom/badlogic/gdx/utils/a;->b:I

    if-le v1, v8, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->j:Z

    iget-boolean v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->j:Z

    const v14, 0x8ce0

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->c:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v15

    const/16 v16, 0x0

    :cond_6
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/glutils/d$b;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/graphics/glutils/d;->b(Lcom/badlogic/gdx/graphics/glutils/d$b;)Lcom/badlogic/gdx/graphics/g;

    move-result-object v2

    iget-object v3, v0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v3, v2}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/glutils/d$b;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    const v3, 0x8d40

    add-int v4, v16, v14

    const/16 v5, 0xde1

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/g;->k()I

    move-result v6

    const/16 v17, 0x0

    move-object v1, v7

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move/from16 v6, v17

    invoke-interface/range {v1 .. v6}, Lcom/badlogic/gdx/graphics/e;->glFramebufferTexture2D(IIIII)V

    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :cond_7
    iget-boolean v3, v1, Lcom/badlogic/gdx/graphics/glutils/d$b;->e:Z

    if-eqz v3, :cond_8

    const v3, 0x8d40

    const v4, 0x8d00

    :goto_3
    const/16 v5, 0xde1

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/g;->k()I

    move-result v6

    const/16 v17, 0x0

    move-object v1, v7

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move/from16 v6, v17

    invoke-interface/range {v1 .. v6}, Lcom/badlogic/gdx/graphics/e;->glFramebufferTexture2D(IIIII)V

    goto :goto_2

    :cond_8
    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$b;->f:Z

    if-eqz v1, :cond_6

    const v3, 0x8d40

    const v4, 0x8d20

    goto :goto_3

    :cond_9
    move/from16 v1, v16

    goto :goto_4

    :cond_a
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->c:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/glutils/d$b;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/graphics/glutils/d;->b(Lcom/badlogic/gdx/graphics/glutils/d$b;)Lcom/badlogic/gdx/graphics/g;

    move-result-object v1

    iget-object v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    iget v2, v1, Lcom/badlogic/gdx/graphics/g;->c:I

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/g;->k()I

    move-result v1

    invoke-interface {v7, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindTexture(II)V

    const/4 v1, 0x0

    :goto_4
    iget-boolean v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->j:Z

    if-eqz v2, :cond_c

    invoke-static {v1}, Lcom/badlogic/gdx/utils/BufferUtils;->c(I)Ljava/nio/IntBuffer;

    move-result-object v2

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v1, :cond_b

    add-int v4, v3, v14

    invoke-virtual {v2, v4}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_b
    invoke-virtual {v2, v9}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    sget-object v3, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    invoke-interface {v3, v1, v2}, Lcom/badlogic/gdx/graphics/f;->a(ILjava/nio/IntBuffer;)V

    goto :goto_6

    :cond_c
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/g;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/graphics/glutils/d;->a(Lcom/badlogic/gdx/graphics/g;)V

    :goto_6
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->h:Z

    const v2, 0x8d00

    if-eqz v1, :cond_d

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    invoke-interface {v7, v10, v2, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glFramebufferRenderbuffer(IIII)V

    :cond_d
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->g:Z

    const v3, 0x8d20

    if-eqz v1, :cond_e

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    invoke-interface {v7, v10, v3, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glFramebufferRenderbuffer(IIII)V

    :cond_e
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->i:Z

    if-eqz v1, :cond_f

    const v1, 0x821a

    iget v4, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v10, v1, v13, v4}, Lcom/badlogic/gdx/graphics/e;->glFramebufferRenderbuffer(IIII)V

    :cond_f
    invoke-interface {v7, v13, v9}, Lcom/badlogic/gdx/graphics/e;->glBindRenderbuffer(II)V

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/badlogic/gdx/graphics/g;

    iget v4, v4, Lcom/badlogic/gdx/graphics/g;->c:I

    invoke-interface {v7, v4, v9}, Lcom/badlogic/gdx/graphics/e;->glBindTexture(II)V

    goto :goto_7

    :cond_10
    invoke-interface {v7, v10}, Lcom/badlogic/gdx/graphics/e;->glCheckFramebufferStatus(I)I

    move-result v1

    const v4, 0x8cdd

    if-ne v1, v4, :cond_15

    iget-object v5, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v5, v5, Lcom/badlogic/gdx/graphics/glutils/d$c;->h:Z

    if-eqz v5, :cond_15

    iget-object v5, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v5, v5, Lcom/badlogic/gdx/graphics/glutils/d$c;->g:Z

    if-eqz v5, :cond_15

    sget-object v5, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    const-string v6, "GL_OES_packed_depth_stencil"

    invoke-interface {v5, v6}, Lcom/badlogic/gdx/h;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_11

    sget-object v5, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    const-string v6, "GL_EXT_packed_depth_stencil"

    invoke-interface {v5, v6}, Lcom/badlogic/gdx/h;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    :cond_11
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->h:Z

    if-eqz v1, :cond_12

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    invoke-interface {v7, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    iput v9, v0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    :cond_12
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->g:Z

    if-eqz v1, :cond_13

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    invoke-interface {v7, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    iput v9, v0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    :cond_13
    iget-object v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->i:Z

    if-eqz v1, :cond_14

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    iput v9, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    :cond_14
    invoke-interface {v7}, Lcom/badlogic/gdx/graphics/e;->glGenRenderbuffer()I

    move-result v1

    iput v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    iput-boolean v8, v0, Lcom/badlogic/gdx/graphics/glutils/d;->i:Z

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glBindRenderbuffer(II)V

    const v1, 0x88f0

    invoke-interface {v7, v13, v1, v11, v12}, Lcom/badlogic/gdx/graphics/e;->glRenderbufferStorage(IIII)V

    invoke-interface {v7, v13, v9}, Lcom/badlogic/gdx/graphics/e;->glBindRenderbuffer(II)V

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v10, v2, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glFramebufferRenderbuffer(IIII)V

    iget v1, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v10, v3, v13, v1}, Lcom/badlogic/gdx/graphics/e;->glFramebufferRenderbuffer(IIII)V

    invoke-interface {v7, v10}, Lcom/badlogic/gdx/graphics/e;->glCheckFramebufferStatus(I)I

    move-result v1

    :cond_15
    sget v2, Lcom/badlogic/gdx/graphics/glutils/d;->c:I

    invoke-interface {v7, v10, v2}, Lcom/badlogic/gdx/graphics/e;->glBindFramebuffer(II)V

    const v2, 0x8cd5

    if-eq v1, v2, :cond_1e

    iget-object v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v2}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/badlogic/gdx/graphics/g;

    invoke-virtual {v0, v3}, Lcom/badlogic/gdx/graphics/glutils/d;->b(Lcom/badlogic/gdx/graphics/g;)V

    goto :goto_8

    :cond_16
    iget-boolean v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->i:Z

    if-eqz v2, :cond_17

    iget v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    invoke-interface {v7, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteBuffer(I)V

    goto :goto_9

    :cond_17
    iget-object v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v2, v2, Lcom/badlogic/gdx/graphics/glutils/d$c;->h:Z

    if-eqz v2, :cond_18

    iget v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    invoke-interface {v7, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    :cond_18
    iget-object v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v2, v2, Lcom/badlogic/gdx/graphics/glutils/d$c;->g:Z

    if-eqz v2, :cond_19

    iget v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    invoke-interface {v7, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    :cond_19
    :goto_9
    iget v2, v0, Lcom/badlogic/gdx/graphics/glutils/d;->e:I

    invoke-interface {v7, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteFramebuffer(I)V

    const v2, 0x8cd6

    if-eq v1, v2, :cond_1d

    const v2, 0x8cd9

    if-eq v1, v2, :cond_1c

    const v2, 0x8cd7

    if-eq v1, v2, :cond_1b

    if-ne v1, v4, :cond_1a

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Frame buffer couldn\'t be constructed: unsupported combination of formats"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1a
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Frame buffer couldn\'t be constructed: unknown error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1b
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Frame buffer couldn\'t be constructed: missing attachment"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1c
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Frame buffer couldn\'t be constructed: incomplete dimensions"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1d
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Frame buffer couldn\'t be constructed: incomplete attachment"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1e
    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-static {v1, v0}, Lcom/badlogic/gdx/graphics/glutils/d;->a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/glutils/d;)V

    return-void
.end method

.method protected abstract a(Lcom/badlogic/gdx/graphics/g;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method protected abstract b(Lcom/badlogic/gdx/graphics/glutils/d$b;)Lcom/badlogic/gdx/graphics/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/badlogic/gdx/graphics/glutils/d$b;",
            ")TT;"
        }
    .end annotation
.end method

.method protected abstract b(Lcom/badlogic/gdx/graphics/g;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public c()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->b:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/graphics/g;

    invoke-virtual {p0, v2}, Lcom/badlogic/gdx/graphics/glutils/d;->b(Lcom/badlogic/gdx/graphics/g;)V

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->i:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->h:I

    :goto_1
    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    goto :goto_2

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->h:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->f:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteRenderbuffer(I)V

    :cond_2
    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->k:Lcom/badlogic/gdx/graphics/glutils/d$c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/graphics/glutils/d$c;->g:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->g:I

    goto :goto_1

    :cond_3
    :goto_2
    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/d;->e:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteFramebuffer(I)V

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/badlogic/gdx/graphics/glutils/d;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;Z)Z

    :cond_4
    return-void
.end method
