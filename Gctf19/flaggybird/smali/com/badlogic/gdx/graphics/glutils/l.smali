.class public Lcom/badlogic/gdx/graphics/glutils/l;
.super Ljava/lang/Object;


# static fields
.field private static a:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private static a(ILcom/badlogic/gdx/graphics/j;)V
    .locals 10

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->f()I

    move-result v3

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v4

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v5

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->e()I

    move-result v7

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->g()I

    move-result v8

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v9

    const/4 v2, 0x0

    const/4 v6, 0x0

    move v1, p0

    invoke-interface/range {v0 .. v9}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    sget-object p1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {p1, p0}, Lcom/badlogic/gdx/graphics/e;->glGenerateMipmap(I)V

    return-void
.end method

.method public static a(ILcom/badlogic/gdx/graphics/j;II)V
    .locals 2

    sget-boolean v0, Lcom/badlogic/gdx/graphics/glutils/l;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0, p1, p2, p3}, Lcom/badlogic/gdx/graphics/glutils/l;->c(ILcom/badlogic/gdx/graphics/j;II)V

    return-void

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0}, Lcom/badlogic/gdx/a;->c()Lcom/badlogic/gdx/a$a;

    move-result-object v0

    sget-object v1, Lcom/badlogic/gdx/a$a;->a:Lcom/badlogic/gdx/a$a;

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0}, Lcom/badlogic/gdx/a;->c()Lcom/badlogic/gdx/a$a;

    move-result-object v0

    sget-object v1, Lcom/badlogic/gdx/a$a;->e:Lcom/badlogic/gdx/a$a;

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0}, Lcom/badlogic/gdx/a;->c()Lcom/badlogic/gdx/a$a;

    move-result-object v0

    sget-object v1, Lcom/badlogic/gdx/a$a;->f:Lcom/badlogic/gdx/a$a;

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/badlogic/gdx/graphics/glutils/l;->b(ILcom/badlogic/gdx/graphics/j;II)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-static {p0, p1}, Lcom/badlogic/gdx/graphics/glutils/l;->a(ILcom/badlogic/gdx/graphics/j;)V

    :goto_1
    return-void
.end method

.method private static b(ILcom/badlogic/gdx/graphics/j;II)V
    .locals 11

    sget-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    const-string v1, "GL_ARB_framebuffer_object"

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/h;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    const-string v1, "GL_EXT_framebuffer_object"

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/h;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/badlogic/gdx/graphics/glutils/l;->c(ILcom/badlogic/gdx/graphics/j;II)V

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->f()I

    move-result v4

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v5

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->e()I

    move-result v8

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->g()I

    move-result v9

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v10

    move v2, p0

    invoke-interface/range {v1 .. v10}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    sget-object p1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {p1, p0}, Lcom/badlogic/gdx/graphics/e;->glGenerateMipmap(I)V

    :goto_1
    return-void
.end method

.method private static c(ILcom/badlogic/gdx/graphics/j;II)V
    .locals 23

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->f()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->e()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->g()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v9

    const/4 v2, 0x0

    const/4 v6, 0x0

    move/from16 v1, p0

    invoke-interface/range {v0 .. v9}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    if-nez v0, :cond_1

    move/from16 v0, p2

    move/from16 v1, p3

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "texture width and height must be square when using mipmapping."

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    const/4 v2, 0x1

    move v11, v0

    move v12, v1

    const/4 v1, 0x1

    move-object/from16 v0, p1

    :goto_1
    if-lez v11, :cond_3

    if-lez v12, :cond_3

    new-instance v15, Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->i()Lcom/badlogic/gdx/graphics/j$c;

    move-result-object v3

    invoke-direct {v15, v11, v12, v3}, Lcom/badlogic/gdx/graphics/j;-><init>(IILcom/badlogic/gdx/graphics/j$c;)V

    sget-object v3, Lcom/badlogic/gdx/graphics/j$a;->a:Lcom/badlogic/gdx/graphics/j$a;

    invoke-virtual {v15, v3}, Lcom/badlogic/gdx/graphics/j;->a(Lcom/badlogic/gdx/graphics/j$a;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v7

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v3, v15

    move-object v4, v0

    invoke-virtual/range {v3 .. v12}, Lcom/badlogic/gdx/graphics/j;->a(Lcom/badlogic/gdx/graphics/j;IIIIIIII)V

    if-le v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->c()V

    :cond_2
    sget-object v13, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-virtual {v15}, Lcom/badlogic/gdx/graphics/j;->f()I

    move-result v16

    invoke-virtual {v15}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v17

    invoke-virtual {v15}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual {v15}, Lcom/badlogic/gdx/graphics/j;->e()I

    move-result v20

    invoke-virtual {v15}, Lcom/badlogic/gdx/graphics/j;->g()I

    move-result v21

    invoke-virtual {v15}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v22

    move/from16 v14, p0

    move-object v0, v15

    move v15, v1

    invoke-interface/range {v13 .. v22}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v3

    div-int/lit8 v11, v3, 0x2

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v3

    div-int/lit8 v12, v3, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method
