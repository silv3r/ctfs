.class public Lcom/badlogic/gdx/graphics/glutils/ETC1;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/glutils/ETC1$a;
    }
.end annotation


# static fields
.field public static a:I = 0x10

.field public static b:I = 0x8d64


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private static a(Lcom/badlogic/gdx/graphics/j$c;)I
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x2

    return p0

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->f:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_1

    const/4 p0, 0x3

    return p0

    :cond_1
    new-instance p0, Lcom/badlogic/gdx/utils/d;

    const-string v0, "Can only handle RGB565 or RGB888 images"

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lcom/badlogic/gdx/graphics/glutils/ETC1$a;Lcom/badlogic/gdx/graphics/j$c;)Lcom/badlogic/gdx/graphics/j;
    .locals 10

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->c:Ljava/nio/ByteBuffer;

    invoke-static {v2, v1}, Lcom/badlogic/gdx/graphics/glutils/ETC1;->getWidthPKM(Ljava/nio/ByteBuffer;I)I

    move-result v2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->c:Ljava/nio/ByteBuffer;

    invoke-static {v3, v1}, Lcom/badlogic/gdx/graphics/glutils/ETC1;->getHeightPKM(Ljava/nio/ByteBuffer;I)I

    move-result v1

    move v8, v1

    move v7, v2

    const/16 v4, 0x10

    goto :goto_0

    :cond_0
    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->a:I

    iget v0, p0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->b:I

    move v8, v0

    move v7, v2

    const/4 v4, 0x0

    :goto_0
    invoke-static {p1}, Lcom/badlogic/gdx/graphics/glutils/ETC1;->a(Lcom/badlogic/gdx/graphics/j$c;)I

    move-result v9

    new-instance v0, Lcom/badlogic/gdx/graphics/j;

    invoke-direct {v0, v7, v8, p1}, Lcom/badlogic/gdx/graphics/j;-><init>(IILcom/badlogic/gdx/graphics/j$c;)V

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/ETC1$a;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static/range {v3 .. v9}, Lcom/badlogic/gdx/graphics/glutils/ETC1;->decodeImage(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;IIII)V

    return-object v0
.end method

.method private static native decodeImage(Ljava/nio/ByteBuffer;ILjava/nio/ByteBuffer;IIII)V
.end method

.method static native getHeightPKM(Ljava/nio/ByteBuffer;I)I
.end method

.method static native getWidthPKM(Ljava/nio/ByteBuffer;I)I
.end method

.method static native isValidPKM(Ljava/nio/ByteBuffer;I)Z
.end method
