.class public Lcom/badlogic/gdx/graphics/glutils/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/glutils/r;


# instance fields
.field final a:Lcom/badlogic/gdx/graphics/q;

.field final b:Ljava/nio/FloatBuffer;

.field final c:Ljava/nio/ByteBuffer;

.field d:I

.field final e:Z

.field final f:Z

.field final g:I

.field h:Z

.field i:Z


# direct methods
.method public constructor <init>(ZILcom/badlogic/gdx/graphics/q;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->h:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->i:Z

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->f:Z

    iput-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    iget-object p3, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    iget p3, p3, Lcom/badlogic/gdx/graphics/q;->a:I

    mul-int p3, p3, p2

    invoke-static {p3}, Lcom/badlogic/gdx/utils/BufferUtils;->b(I)Ljava/nio/ByteBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->e:Z

    if-eqz p1, :cond_0

    const p1, 0x88e4

    goto :goto_0

    :cond_0
    const p1, 0x88e8

    :goto_0
    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->g:I

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/p;->b()I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->d:I

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-void
.end method

.method private b()I
    .locals 6

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenBuffer()I

    move-result v0

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const v2, 0x8892

    invoke-interface {v1, v2, v0}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    iget v4, p0, Lcom/badlogic/gdx/graphics/glutils/p;->g:I

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v5, v4}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    sget-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    return v0
.end method

.method private d()V
    .locals 5

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->i:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const v1, 0x8892

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4, v2, v3}, Lcom/badlogic/gdx/graphics/e;->glBufferSubData(IIILjava/nio/Buffer;)V

    iput-boolean v4, p0, Lcom/badlogic/gdx/graphics/glutils/p;->h:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/p;->b()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->h:Z

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 11

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->d:I

    const v2, 0x8892

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget-boolean v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->h:Z

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->limit()I

    move-result v4

    mul-int/lit8 v4, v4, 0x4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    iget v5, p0, Lcom/badlogic/gdx/graphics/glutils/p;->g:I

    invoke-interface {v0, v2, v1, v4, v5}, Lcom/badlogic/gdx/graphics/e;->glBufferData(IILjava/nio/Buffer;I)V

    iput-boolean v3, p0, Lcom/badlogic/gdx/graphics/glutils/p;->h:Z

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v0

    if-nez p2, :cond_2

    :goto_0
    if-ge v3, v0, :cond_4

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {p2, v3}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object p2

    iget-object v1, p2, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/badlogic/gdx/graphics/glutils/m;->b(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v6, p2, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, p2, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, p2, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    iget v9, v1, Lcom/badlogic/gdx/graphics/q;->a:I

    iget v10, p2, Lcom/badlogic/gdx/graphics/p;->e:I

    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZII)V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    if-ge v3, v0, :cond_4

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v1, v3}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v1

    aget v5, p2, v3

    if-gez v5, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p1, v5}, Lcom/badlogic/gdx/graphics/glutils/m;->b(I)V

    iget v6, v1, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v7, v1, Lcom/badlogic/gdx/graphics/p;->d:I

    iget-boolean v8, v1, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    iget v9, v2, Lcom/badlogic/gdx/graphics/q;->a:I

    iget v10, v1, Lcom/badlogic/gdx/graphics/p;->e:I

    move-object v4, p1

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/graphics/glutils/m;->a(IIIZII)V

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->i:Z

    return-void
.end method

.method public a([FII)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->h:Z

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->e:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-static {p1, v0, p3, p2}, Lcom/badlogic/gdx/utils/BufferUtils;->a([FLjava/nio/Buffer;II)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p1, p3}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->c:Ljava/nio/ByteBuffer;

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {p2}, Ljava/nio/FloatBuffer;->limit()I

    move-result p2

    shl-int/lit8 p2, p2, 0x2

    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :goto_0
    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/glutils/p;->d()V

    return-void
.end method

.method public b(Lcom/badlogic/gdx/graphics/glutils/m;[I)V
    .locals 5

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/q;->a()I

    move-result v1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    const/4 p2, 0x0

    :goto_0
    if-ge p2, v1, :cond_2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/glutils/p;->a:Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {v3, p2}, Lcom/badlogic/gdx/graphics/q;->a(I)Lcom/badlogic/gdx/graphics/p;

    move-result-object v3

    iget-object v3, v3, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_2

    aget v4, p2, v3

    if-ltz v4, :cond_1

    invoke-virtual {p1, v4}, Lcom/badlogic/gdx/graphics/glutils/m;->a(I)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const p1, 0x8892

    invoke-interface {v0, p1, v2}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iput-boolean v2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->i:Z

    return-void
.end method

.method public c()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x0

    const v2, 0x8892

    invoke-interface {v0, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glBindBuffer(II)V

    iget v2, p0, Lcom/badlogic/gdx/graphics/glutils/p;->d:I

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/graphics/e;->glDeleteBuffer(I)V

    iput v1, p0, Lcom/badlogic/gdx/graphics/glutils/p;->d:I

    return-void
.end method
