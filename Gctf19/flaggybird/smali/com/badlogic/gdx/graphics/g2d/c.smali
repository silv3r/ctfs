.class public Lcom/badlogic/gdx/graphics/g2d/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/g2d/b;


# static fields
.field public static a:Lcom/badlogic/gdx/graphics/h$a;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field final b:[F

.field c:I

.field d:Lcom/badlogic/gdx/graphics/l;

.field e:F

.field f:F

.field g:Z

.field h:F

.field public i:I

.field public j:I

.field public k:I

.field private l:Lcom/badlogic/gdx/graphics/h;

.field private final m:Lcom/badlogic/gdx/math/Matrix4;

.field private final n:Lcom/badlogic/gdx/math/Matrix4;

.field private final o:Lcom/badlogic/gdx/math/Matrix4;

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private final u:Lcom/badlogic/gdx/graphics/glutils/m;

.field private v:Lcom/badlogic/gdx/graphics/glutils/m;

.field private w:Z

.field private final x:Lcom/badlogic/gdx/graphics/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/h$a;->a:Lcom/badlogic/gdx/graphics/h$a;

    sput-object v0, Lcom/badlogic/gdx/graphics/g2d/c;->a:Lcom/badlogic/gdx/graphics/h$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x3e8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/badlogic/gdx/graphics/g2d/c;-><init>(ILcom/badlogic/gdx/graphics/glutils/m;)V

    return-void
.end method

.method public constructor <init>(ILcom/badlogic/gdx/graphics/glutils/m;)V
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v3, 0x0

    iput v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    const/4 v4, 0x0

    iput-object v4, v0, Lcom/badlogic/gdx/graphics/g2d/c;->d:Lcom/badlogic/gdx/graphics/l;

    const/4 v5, 0x0

    iput v5, v0, Lcom/badlogic/gdx/graphics/g2d/c;->e:F

    iput v5, v0, Lcom/badlogic/gdx/graphics/g2d/c;->f:F

    iput-boolean v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    new-instance v6, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v6}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v6, v0, Lcom/badlogic/gdx/graphics/g2d/c;->m:Lcom/badlogic/gdx/math/Matrix4;

    new-instance v6, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v6}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v6, v0, Lcom/badlogic/gdx/graphics/g2d/c;->n:Lcom/badlogic/gdx/math/Matrix4;

    new-instance v6, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v6}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v6, v0, Lcom/badlogic/gdx/graphics/g2d/c;->o:Lcom/badlogic/gdx/math/Matrix4;

    iput-boolean v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->p:Z

    const/16 v6, 0x302

    iput v6, v0, Lcom/badlogic/gdx/graphics/g2d/c;->q:I

    const/16 v7, 0x303

    iput v7, v0, Lcom/badlogic/gdx/graphics/g2d/c;->r:I

    iput v6, v0, Lcom/badlogic/gdx/graphics/g2d/c;->s:I

    iput v7, v0, Lcom/badlogic/gdx/graphics/g2d/c;->t:I

    iput-object v4, v0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    new-instance v4, Lcom/badlogic/gdx/graphics/b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v4, v6, v6, v6, v6}, Lcom/badlogic/gdx/graphics/b;-><init>(FFFF)V

    iput-object v4, v0, Lcom/badlogic/gdx/graphics/g2d/c;->x:Lcom/badlogic/gdx/graphics/b;

    sget v4, Lcom/badlogic/gdx/graphics/b;->f:F

    iput v4, v0, Lcom/badlogic/gdx/graphics/g2d/c;->h:F

    iput v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->i:I

    iput v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->j:I

    iput v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->k:I

    const/16 v4, 0x1fff

    if-gt v1, v4, :cond_3

    sget-object v4, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/badlogic/gdx/graphics/h$a;->d:Lcom/badlogic/gdx/graphics/h$a;

    :goto_0
    move-object v7, v4

    goto :goto_1

    :cond_0
    sget-object v4, Lcom/badlogic/gdx/graphics/g2d/c;->a:Lcom/badlogic/gdx/graphics/h$a;

    goto :goto_0

    :goto_1
    new-instance v4, Lcom/badlogic/gdx/graphics/h;

    const/4 v8, 0x0

    mul-int/lit8 v9, v1, 0x4

    mul-int/lit8 v12, v1, 0x6

    const/4 v6, 0x3

    new-array v11, v6, [Lcom/badlogic/gdx/graphics/p;

    new-instance v6, Lcom/badlogic/gdx/graphics/p;

    const-string v10, "a_position"

    const/4 v13, 0x2

    const/4 v14, 0x1

    invoke-direct {v6, v14, v13, v10}, Lcom/badlogic/gdx/graphics/p;-><init>(IILjava/lang/String;)V

    aput-object v6, v11, v3

    new-instance v6, Lcom/badlogic/gdx/graphics/p;

    const-string v10, "a_color"

    const/4 v15, 0x4

    invoke-direct {v6, v15, v15, v10}, Lcom/badlogic/gdx/graphics/p;-><init>(IILjava/lang/String;)V

    aput-object v6, v11, v14

    new-instance v6, Lcom/badlogic/gdx/graphics/p;

    const/16 v10, 0x10

    const-string v3, "a_texCoord0"

    invoke-direct {v6, v10, v13, v3}, Lcom/badlogic/gdx/graphics/p;-><init>(IILjava/lang/String;)V

    aput-object v6, v11, v13

    move-object v6, v4

    move v10, v12

    invoke-direct/range {v6 .. v11}, Lcom/badlogic/gdx/graphics/h;-><init>(Lcom/badlogic/gdx/graphics/h$a;ZII[Lcom/badlogic/gdx/graphics/p;)V

    iput-object v4, v0, Lcom/badlogic/gdx/graphics/g2d/c;->l:Lcom/badlogic/gdx/graphics/h;

    iget-object v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->n:Lcom/badlogic/gdx/math/Matrix4;

    sget-object v4, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v4}, Lcom/badlogic/gdx/h;->b()I

    move-result v4

    int-to-float v4, v4

    sget-object v6, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v6}, Lcom/badlogic/gdx/h;->c()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v3, v5, v5, v4, v6}, Lcom/badlogic/gdx/math/Matrix4;->a(FFFF)Lcom/badlogic/gdx/math/Matrix4;

    mul-int/lit8 v1, v1, 0x14

    new-array v1, v1, [F

    iput-object v1, v0, Lcom/badlogic/gdx/graphics/g2d/c;->b:[F

    new-array v1, v12, [S

    const/4 v3, 0x0

    const/16 v16, 0x0

    :goto_2
    if-ge v3, v12, :cond_1

    aput-short v16, v1, v3

    add-int/lit8 v4, v3, 0x1

    add-int/lit8 v5, v16, 0x1

    int-to-short v5, v5

    aput-short v5, v1, v4

    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v5, v16, 0x2

    int-to-short v5, v5

    aput-short v5, v1, v4

    add-int/lit8 v4, v3, 0x3

    aput-short v5, v1, v4

    add-int/lit8 v4, v3, 0x4

    add-int/lit8 v5, v16, 0x3

    int-to-short v5, v5

    aput-short v5, v1, v4

    add-int/lit8 v4, v3, 0x5

    aput-short v16, v1, v4

    add-int/lit8 v3, v3, 0x6

    add-int/lit8 v4, v16, 0x4

    int-to-short v4, v4

    move/from16 v16, v4

    goto :goto_2

    :cond_1
    iget-object v3, v0, Lcom/badlogic/gdx/graphics/g2d/c;->l:Lcom/badlogic/gdx/graphics/h;

    invoke-virtual {v3, v1}, Lcom/badlogic/gdx/graphics/h;->a([S)Lcom/badlogic/gdx/graphics/h;

    if-nez v2, :cond_2

    invoke-static {}, Lcom/badlogic/gdx/graphics/g2d/c;->e()Lcom/badlogic/gdx/graphics/glutils/m;

    move-result-object v1

    iput-object v1, v0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    iput-boolean v14, v0, Lcom/badlogic/gdx/graphics/g2d/c;->w:Z

    goto :goto_3

    :cond_2
    iput-object v2, v0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    :goto_3
    return-void

    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t have more than 8191 sprites per batch: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static e()Lcom/badlogic/gdx/graphics/glutils/m;
    .locals 4

    const-string v0, "attribute vec4 a_position;\nattribute vec4 a_color;\nattribute vec2 a_texCoord0;\nuniform mat4 u_projTrans;\nvarying vec4 v_color;\nvarying vec2 v_texCoords;\n\nvoid main()\n{\n   v_color = a_color;\n   v_color.a = v_color.a * (255.0/254.0);\n   v_texCoords = a_texCoord0;\n   gl_Position =  u_projTrans * a_position;\n}\n"

    const-string v1, "#ifdef GL_ES\n#define LOWP lowp\nprecision mediump float;\n#else\n#define LOWP \n#endif\nvarying LOWP vec4 v_color;\nvarying vec2 v_texCoords;\nuniform sampler2D u_texture;\nvoid main()\n{\n  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n}"

    new-instance v2, Lcom/badlogic/gdx/graphics/glutils/m;

    invoke-direct {v2, v0, v1}, Lcom/badlogic/gdx/graphics/glutils/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/glutils/m;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v2

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error compiling shader: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/glutils/m;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private h()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->o:Lcom/badlogic/gdx/math/Matrix4;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->n:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/math/Matrix4;->a(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;

    move-result-object v0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->m:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/math/Matrix4;->b(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    const-string v2, "u_projTrans"

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/g2d/c;->o:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {v0, v2, v3}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;Lcom/badlogic/gdx/math/Matrix4;)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    :goto_0
    const-string v2, "u_texture"

    invoke-virtual {v0, v2, v1}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;I)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    const-string v2, "u_projTrans"

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/g2d/c;->o:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {v0, v2, v3}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Ljava/lang/String;Lcom/badlogic/gdx/math/Matrix4;)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    goto :goto_0

    :goto_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->i:I

    sget-object v1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v1, v0}, Lcom/badlogic/gdx/graphics/e;->glDepthMask(Z)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    :goto_0
    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/glutils/m;->e()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    goto :goto_0

    :goto_1
    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SpriteBatch.end must be called before begin."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/badlogic/gdx/graphics/g2d/d;FF)V
    .locals 7

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/g2d/d;->c()I

    move-result v0

    int-to-float v5, v0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/g2d/d;->d()I

    move-result v0

    int-to-float v6, v0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/badlogic/gdx/graphics/g2d/c;->a(Lcom/badlogic/gdx/graphics/g2d/d;FFFF)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/g2d/d;FFFF)V
    .locals 7

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->b:[F

    iget-object v1, p1, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->d:Lcom/badlogic/gdx/graphics/l;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/graphics/g2d/c;->a(Lcom/badlogic/gdx/graphics/l;)V

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    array-length v2, v0

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->f()V

    :cond_1
    :goto_0
    add-float/2addr p4, p2

    add-float/2addr p5, p3

    iget v1, p1, Lcom/badlogic/gdx/graphics/g2d/d;->b:F

    iget v2, p1, Lcom/badlogic/gdx/graphics/g2d/d;->e:F

    iget v3, p1, Lcom/badlogic/gdx/graphics/g2d/d;->d:F

    iget p1, p1, Lcom/badlogic/gdx/graphics/g2d/d;->c:F

    iget v4, p0, Lcom/badlogic/gdx/graphics/g2d/c;->h:F

    iget v5, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    aput p2, v0, v5

    add-int/lit8 v6, v5, 0x1

    aput p3, v0, v6

    add-int/lit8 v6, v5, 0x2

    aput v4, v0, v6

    add-int/lit8 v6, v5, 0x3

    aput v1, v0, v6

    add-int/lit8 v6, v5, 0x4

    aput v2, v0, v6

    add-int/lit8 v6, v5, 0x5

    aput p2, v0, v6

    add-int/lit8 p2, v5, 0x6

    aput p5, v0, p2

    add-int/lit8 p2, v5, 0x7

    aput v4, v0, p2

    add-int/lit8 p2, v5, 0x8

    aput v1, v0, p2

    add-int/lit8 p2, v5, 0x9

    aput p1, v0, p2

    add-int/lit8 p2, v5, 0xa

    aput p4, v0, p2

    add-int/lit8 p2, v5, 0xb

    aput p5, v0, p2

    add-int/lit8 p2, v5, 0xc

    aput v4, v0, p2

    add-int/lit8 p2, v5, 0xd

    aput v3, v0, p2

    add-int/lit8 p2, v5, 0xe

    aput p1, v0, p2

    add-int/lit8 p1, v5, 0xf

    aput p4, v0, p1

    add-int/lit8 p1, v5, 0x10

    aput p3, v0, p1

    add-int/lit8 p1, v5, 0x11

    aput v4, v0, p1

    add-int/lit8 p1, v5, 0x12

    aput v3, v0, p1

    add-int/lit8 p1, v5, 0x13

    aput v2, v0, p1

    add-int/lit8 v5, v5, 0x14

    iput v5, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "SpriteBatch.begin must be called before draw."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected a(Lcom/badlogic/gdx/graphics/l;)V
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->f()V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->d:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    div-float v0, v1, v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->e:F

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr v1, p1

    iput v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->f:F

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/l;FF)V
    .locals 7

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    int-to-float v5, v0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v0

    int-to-float v6, v0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/badlogic/gdx/graphics/g2d/c;->a(Lcom/badlogic/gdx/graphics/l;FFFF)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/l;FFFF)V
    .locals 5

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->b:[F

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->d:Lcom/badlogic/gdx/graphics/l;

    if-eq p1, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/g2d/c;->a(Lcom/badlogic/gdx/graphics/l;)V

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    array-length v1, v0

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->f()V

    :cond_1
    :goto_0
    add-float/2addr p4, p2

    add-float/2addr p5, p3

    iget p1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->h:F

    iget v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    aput p2, v0, v1

    add-int/lit8 v2, v1, 0x1

    aput p3, v0, v2

    add-int/lit8 v2, v1, 0x2

    aput p1, v0, v2

    add-int/lit8 v2, v1, 0x3

    const/4 v3, 0x0

    aput v3, v0, v2

    add-int/lit8 v2, v1, 0x4

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v0, v2

    add-int/lit8 v2, v1, 0x5

    aput p2, v0, v2

    add-int/lit8 p2, v1, 0x6

    aput p5, v0, p2

    add-int/lit8 p2, v1, 0x7

    aput p1, v0, p2

    add-int/lit8 p2, v1, 0x8

    aput v3, v0, p2

    add-int/lit8 p2, v1, 0x9

    aput v3, v0, p2

    add-int/lit8 p2, v1, 0xa

    aput p4, v0, p2

    add-int/lit8 p2, v1, 0xb

    aput p5, v0, p2

    add-int/lit8 p2, v1, 0xc

    aput p1, v0, p2

    add-int/lit8 p2, v1, 0xd

    aput v4, v0, p2

    add-int/lit8 p2, v1, 0xe

    aput v3, v0, p2

    add-int/lit8 p2, v1, 0xf

    aput p4, v0, p2

    add-int/lit8 p2, v1, 0x10

    aput p3, v0, p2

    add-int/lit8 p2, v1, 0x11

    aput p1, v0, p2

    add-int/lit8 p1, v1, 0x12

    aput v4, v0, p1

    add-int/lit8 p1, v1, 0x13

    aput v4, v0, p1

    add-int/lit8 v1, v1, 0x14

    iput v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "SpriteBatch.begin must be called before draw."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/badlogic/gdx/math/Matrix4;)V
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->f()V

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->n:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/math/Matrix4;->a(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;

    iget-boolean p1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->h()V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->f()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->d:Lcom/badlogic/gdx/graphics/l;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->g:Z

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDepthMask(Z)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xbe2

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDisable(I)V

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    :goto_0
    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/glutils/m;->f()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    goto :goto_0

    :goto_1
    return-void

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SpriteBatch.begin must be called before end."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->l:Lcom/badlogic/gdx/graphics/h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/h;->c()V

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/glutils/m;->c()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->p:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g2d/c;->f()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->p:Z

    return-void
.end method

.method public f()V
    .locals 8

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->i:I

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->j:I

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    div-int/lit8 v0, v0, 0x14

    iget v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->k:I

    if-le v0, v1, :cond_1

    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->k:I

    :cond_1
    mul-int/lit8 v0, v0, 0x6

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->d:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/l;->f()V

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/c;->l:Lcom/badlogic/gdx/graphics/h;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->b:[F

    iget v3, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Lcom/badlogic/gdx/graphics/h;->a([FII)Lcom/badlogic/gdx/graphics/h;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/h;->a()Ljava/nio/ShortBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/h;->a()Ljava/nio/ShortBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/nio/ShortBuffer;->limit(I)Ljava/nio/Buffer;

    iget-boolean v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->p:Z

    const/16 v3, 0xbe2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v2, v3}, Lcom/badlogic/gdx/graphics/e;->glDisable(I)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v2, v3}, Lcom/badlogic/gdx/graphics/e;->glEnable(I)V

    iget v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->q:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    sget-object v2, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v3, p0, Lcom/badlogic/gdx/graphics/g2d/c;->q:I

    iget v5, p0, Lcom/badlogic/gdx/graphics/g2d/c;->r:I

    iget v6, p0, Lcom/badlogic/gdx/graphics/g2d/c;->s:I

    iget v7, p0, Lcom/badlogic/gdx/graphics/g2d/c;->t:I

    invoke-interface {v2, v3, v5, v6, v7}, Lcom/badlogic/gdx/graphics/e;->glBlendFuncSeparate(IIII)V

    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->v:Lcom/badlogic/gdx/graphics/glutils/m;

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/c;->u:Lcom/badlogic/gdx/graphics/glutils/m;

    :goto_1
    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/badlogic/gdx/graphics/h;->a(Lcom/badlogic/gdx/graphics/glutils/m;III)V

    iput v4, p0, Lcom/badlogic/gdx/graphics/g2d/c;->c:I

    return-void
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/g2d/c;->p:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
