.class public Lcom/badlogic/gdx/graphics/g2d/d;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/badlogic/gdx/graphics/l;

.field b:F

.field c:F

.field d:F

.field e:F

.field f:I

.field g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/graphics/l;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result p1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v1, v0, p1}, Lcom/badlogic/gdx/graphics/g2d/d;->a(IIII)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "texture cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/badlogic/gdx/graphics/l;IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/badlogic/gdx/graphics/g2d/d;->a(IIII)V

    return-void
.end method

.method public static a(Lcom/badlogic/gdx/graphics/l;II)[[Lcom/badlogic/gdx/graphics/g2d/d;
    .locals 1

    new-instance v0, Lcom/badlogic/gdx/graphics/g2d/d;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/graphics/g2d/d;-><init>(Lcom/badlogic/gdx/graphics/l;)V

    invoke-virtual {v0, p1, p2}, Lcom/badlogic/gdx/graphics/g2d/d;->a(II)[[Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->b:F

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v1

    int-to-float v1, v1

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public a(FFFF)V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v1

    sub-float v2, p3, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v0, v0

    mul-float v2, v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->f:I

    sub-float v2, p4, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v1, v1

    mul-float v2, v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->g:I

    iget v2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->f:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->g:I

    if-ne v2, v3, :cond_0

    const/high16 v2, 0x3e800000    # 0.25f

    div-float v0, v2, v0

    add-float/2addr p1, v0

    sub-float/2addr p3, v0

    div-float/2addr v2, v1

    add-float/2addr p2, v2

    sub-float/2addr p4, v2

    :cond_0
    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->b:F

    iput p2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->c:F

    iput p3, p0, Lcom/badlogic/gdx/graphics/g2d/d;->d:F

    iput p4, p0, Lcom/badlogic/gdx/graphics/g2d/d;->e:F

    return-void
.end method

.method public a(IIII)V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    div-float v0, v1, v0

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float v2, p1

    mul-float v2, v2, v0

    int-to-float v3, p2

    mul-float v3, v3, v1

    add-int/2addr p1, p3

    int-to-float p1, p1

    mul-float p1, p1, v0

    add-int/2addr p2, p4

    int-to-float p2, p2

    mul-float p2, p2, v1

    invoke-virtual {p0, v2, v3, p1, p2}, Lcom/badlogic/gdx/graphics/g2d/d;->a(FFFF)V

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->f:I

    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->g:I

    return-void
.end method

.method public a(ZZ)V
    .locals 1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->b:F

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->d:F

    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->b:F

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->d:F

    :cond_0
    if-eqz p2, :cond_1

    iget p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->c:F

    iget p2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->e:F

    iput p2, p0, Lcom/badlogic/gdx/graphics/g2d/d;->c:F

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->e:F

    :cond_1
    return-void
.end method

.method public a(II)[[Lcom/badlogic/gdx/graphics/g2d/d;
    .locals 18

    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Lcom/badlogic/gdx/graphics/g2d/d;->a()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/badlogic/gdx/graphics/g2d/d;->b()I

    move-result v1

    iget v2, v0, Lcom/badlogic/gdx/graphics/g2d/d;->f:I

    iget v3, v0, Lcom/badlogic/gdx/graphics/g2d/d;->g:I

    div-int v8, v3, p2

    div-int v9, v2, p1

    filled-new-array {v8, v9}, [I

    move-result-object v2

    const-class v3, Lcom/badlogic/gdx/graphics/g2d/d;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, [[Lcom/badlogic/gdx/graphics/g2d/d;

    const/4 v11, 0x0

    move v13, v1

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v8, :cond_1

    move v14, v7

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v9, :cond_0

    aget-object v15, v10, v12

    new-instance v16, Lcom/badlogic/gdx/graphics/g2d/d;

    iget-object v2, v0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    move-object/from16 v1, v16

    move v3, v14

    move v4, v13

    move/from16 v5, p1

    move/from16 v17, v6

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/graphics/g2d/d;-><init>(Lcom/badlogic/gdx/graphics/l;IIII)V

    aput-object v16, v15, v17

    add-int/lit8 v6, v17, 0x1

    add-int v14, v14, p1

    goto :goto_1

    :cond_0
    add-int/lit8 v12, v12, 0x1

    add-int v13, v13, p2

    goto :goto_0

    :cond_1
    return-object v10
.end method

.method public b()I
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->c:F

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/d;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v1

    int-to-float v1, v1

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->f:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/d;->g:I

    return v0
.end method
