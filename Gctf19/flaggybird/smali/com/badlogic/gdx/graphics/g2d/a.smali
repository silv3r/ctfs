.class public Lcom/badlogic/gdx/graphics/g2d/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/g2d/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private b:F

.field private c:F

.field private d:I

.field private e:F

.field private f:Lcom/badlogic/gdx/graphics/g2d/a$a;


# direct methods
.method public varargs constructor <init>(F[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F[TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/badlogic/gdx/graphics/g2d/a$a;->a:Lcom/badlogic/gdx/graphics/g2d/a$a;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->b:F

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/graphics/g2d/a;->a([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(F)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/g2d/a;->b(F)I

    move-result p1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public a(FZ)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FZ)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-eqz p2, :cond_3

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    sget-object v2, Lcom/badlogic/gdx/graphics/g2d/a$a;->a:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    sget-object v2, Lcom/badlogic/gdx/graphics/g2d/a$a;->b:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-ne v1, v2, :cond_3

    :cond_0
    iget-object p2, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    sget-object v1, Lcom/badlogic/gdx/graphics/g2d/a$a;->a:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-ne p2, v1, :cond_2

    :cond_1
    sget-object p2, Lcom/badlogic/gdx/graphics/g2d/a$a;->c:Lcom/badlogic/gdx/graphics/g2d/a$a;

    :goto_0
    iput-object p2, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    goto :goto_1

    :cond_2
    sget-object p2, Lcom/badlogic/gdx/graphics/g2d/a$a;->d:Lcom/badlogic/gdx/graphics/g2d/a$a;

    goto :goto_0

    :cond_3
    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    sget-object v1, Lcom/badlogic/gdx/graphics/g2d/a$a;->a:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-eq p2, v1, :cond_4

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    sget-object v1, Lcom/badlogic/gdx/graphics/g2d/a$a;->b:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-eq p2, v1, :cond_4

    iget-object p2, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    sget-object v1, Lcom/badlogic/gdx/graphics/g2d/a$a;->d:Lcom/badlogic/gdx/graphics/g2d/a$a;

    if-ne p2, v1, :cond_1

    sget-object p2, Lcom/badlogic/gdx/graphics/g2d/a$a;->b:Lcom/badlogic/gdx/graphics/g2d/a$a;

    goto :goto_0

    :cond_4
    :goto_1
    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/g2d/a;->a(F)Ljava/lang/Object;

    move-result-object p1

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    return-object p1
.end method

.method protected varargs a([Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length p1, p1

    int-to-float p1, p1

    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->b:F

    mul-float p1, p1, v0

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->c:F

    return-void
.end method

.method public b(F)I
    .locals 5

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    return v1

    :cond_0
    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->b:F

    div-float v0, p1, v0

    float-to-int v0, v0

    sget-object v3, Lcom/badlogic/gdx/graphics/g2d/a$1;->a:[I

    iget-object v4, p0, Lcom/badlogic/gdx/graphics/g2d/a;->f:Lcom/badlogic/gdx/graphics/g2d/a$a;

    invoke-virtual {v4}, Lcom/badlogic/gdx/graphics/g2d/a$a;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    rem-int/2addr v0, v1

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    sub-int/2addr v1, v0

    add-int/lit8 v0, v1, -0x1

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v3, v3

    sub-int/2addr v3, v0

    sub-int/2addr v3, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->e:F

    iget v3, p0, Lcom/badlogic/gdx/graphics/g2d/a;->b:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v0, v0

    sub-int/2addr v0, v2

    invoke-static {v0}, Lcom/badlogic/gdx/math/b;->a(I)I

    move-result v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->d:I

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x2

    rem-int/2addr v0, v1

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x2

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v2, v2

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    rem-int/2addr v0, v1

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    sub-int/2addr v1, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_2
    :goto_0
    iput v0, p0, Lcom/badlogic/gdx/graphics/g2d/a;->d:I

    iput p1, p0, Lcom/badlogic/gdx/graphics/g2d/a;->e:F

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
