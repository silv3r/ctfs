.class public abstract Lcom/badlogic/gdx/graphics/a;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/badlogic/gdx/math/g;

.field public final b:Lcom/badlogic/gdx/math/g;

.field public final c:Lcom/badlogic/gdx/math/g;

.field public final d:Lcom/badlogic/gdx/math/Matrix4;

.field public final e:Lcom/badlogic/gdx/math/Matrix4;

.field public final f:Lcom/badlogic/gdx/math/Matrix4;

.field public final g:Lcom/badlogic/gdx/math/Matrix4;

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public final l:Lcom/badlogic/gdx/math/a;

.field private final m:Lcom/badlogic/gdx/math/g;

.field private final n:Lcom/badlogic/gdx/math/a/a;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->a:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    const/4 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    invoke-direct {v0, v1, v1, v2}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->b:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v1}, Lcom/badlogic/gdx/math/g;-><init>(FFF)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->c:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->d:Lcom/badlogic/gdx/math/Matrix4;

    new-instance v0, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->e:Lcom/badlogic/gdx/math/Matrix4;

    new-instance v0, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->f:Lcom/badlogic/gdx/math/Matrix4;

    new-instance v0, Lcom/badlogic/gdx/math/Matrix4;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/Matrix4;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->g:Lcom/badlogic/gdx/math/Matrix4;

    iput v2, p0, Lcom/badlogic/gdx/graphics/a;->h:F

    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/badlogic/gdx/graphics/a;->i:F

    iput v1, p0, Lcom/badlogic/gdx/graphics/a;->j:F

    iput v1, p0, Lcom/badlogic/gdx/graphics/a;->k:F

    new-instance v0, Lcom/badlogic/gdx/math/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/a;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->l:Lcom/badlogic/gdx/math/a;

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->m:Lcom/badlogic/gdx/math/g;

    new-instance v0, Lcom/badlogic/gdx/math/a/a;

    new-instance v1, Lcom/badlogic/gdx/math/g;

    invoke-direct {v1}, Lcom/badlogic/gdx/math/g;-><init>()V

    new-instance v2, Lcom/badlogic/gdx/math/g;

    invoke-direct {v2}, Lcom/badlogic/gdx/math/g;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/badlogic/gdx/math/a/a;-><init>(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/a;->n:Lcom/badlogic/gdx/math/a/a;

    return-void
.end method
