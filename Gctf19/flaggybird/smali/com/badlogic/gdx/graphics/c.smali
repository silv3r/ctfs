.class public Lcom/badlogic/gdx/graphics/c;
.super Lcom/badlogic/gdx/graphics/g;


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/badlogic/gdx/a;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/graphics/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private static i:Lcom/badlogic/gdx/a/e;


# instance fields
.field protected b:Lcom/badlogic/gdx/graphics/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/graphics/d;)V
    .locals 1

    const v0, 0x8513

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/graphics/g;-><init>(I)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/c;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/d;)V

    return-void
.end method

.method public static a(Lcom/badlogic/gdx/a;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static b(Lcom/badlogic/gdx/a;)V
    .locals 8

    sget-object v0, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/utils/a;

    if-nez p0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    :goto_0
    iget v0, p0, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v1, v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/graphics/c;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/c;->d()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v0}, Lcom/badlogic/gdx/a/e;->b()V

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/a;-><init>(Lcom/badlogic/gdx/utils/a;)V

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/badlogic/gdx/graphics/c;

    sget-object v4, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v4, v3}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/c;->d()V

    goto :goto_1

    :cond_2
    sget-object v5, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/a/e;->c(Ljava/lang/String;)I

    move-result v5

    sget-object v6, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v6, v4, v1}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;I)V

    iput v1, v3, Lcom/badlogic/gdx/graphics/c;->d:I

    new-instance v6, Lcom/badlogic/gdx/a/a/c$b;

    invoke-direct {v6}, Lcom/badlogic/gdx/a/a/c$b;-><init>()V

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/c;->a()Lcom/badlogic/gdx/graphics/d;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/c$b;->d:Lcom/badlogic/gdx/graphics/d;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/c;->g()Lcom/badlogic/gdx/graphics/l$a;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/c$b;->e:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/c;->h()Lcom/badlogic/gdx/graphics/l$a;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/c$b;->f:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/c;->i()Lcom/badlogic/gdx/graphics/l$b;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/c$b;->g:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/c;->j()Lcom/badlogic/gdx/graphics/l$b;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/c$b;->h:Lcom/badlogic/gdx/graphics/l$b;

    iput-object v3, v6, Lcom/badlogic/gdx/a/a/c$b;->c:Lcom/badlogic/gdx/graphics/c;

    new-instance v7, Lcom/badlogic/gdx/graphics/c$1;

    invoke-direct {v7, v5}, Lcom/badlogic/gdx/graphics/c$1;-><init>(I)V

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/c$b;->a:Lcom/badlogic/gdx/a/c$a;

    sget-object v5, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;)V

    sget-object v5, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v5}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v5

    iput v5, v3, Lcom/badlogic/gdx/graphics/c;->d:I

    sget-object v3, Lcom/badlogic/gdx/graphics/c;->i:Lcom/badlogic/gdx/a/e;

    const-class v5, Lcom/badlogic/gdx/graphics/c;

    invoke-virtual {v3, v4, v5, v6}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/badlogic/gdx/a/c;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/a;->c()V

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(Lcom/badlogic/gdx/utils/a;)V

    :cond_4
    return-void
.end method

.method public static e()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Managed cubemap/app: { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a;

    sget-object v3, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/graphics/d;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/c;->b:Lcom/badlogic/gdx/graphics/d;

    return-object v0
.end method

.method public a(Lcom/badlogic/gdx/graphics/d;)V
    .locals 3

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/d;->b()V

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/c;->f()V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/c;->e:Lcom/badlogic/gdx/graphics/l$a;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/c;->f:Lcom/badlogic/gdx/graphics/l$a;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;Z)V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/c;->g:Lcom/badlogic/gdx/graphics/l$b;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/c;->h:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p0, v0, v1, v2}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;Z)V

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/d;->c()V

    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v0, p0, Lcom/badlogic/gdx/graphics/c;->c:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/badlogic/gdx/graphics/e;->glBindTexture(II)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/c;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/d;->d()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/c;->d:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/c;->l()V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/c;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/graphics/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;Z)Z

    :cond_1
    return-void
.end method

.method protected d()V
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/c;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/c;->b:Lcom/badlogic/gdx/graphics/d;

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/graphics/d;)V

    return-void

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Tried to reload an unmanaged Cubemap"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method
