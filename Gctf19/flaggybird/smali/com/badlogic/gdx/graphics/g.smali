.class public abstract Lcom/badlogic/gdx/graphics/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/b;


# instance fields
.field public final c:I

.field protected d:I

.field protected e:Lcom/badlogic/gdx/graphics/l$a;

.field protected f:Lcom/badlogic/gdx/graphics/l$a;

.field protected g:Lcom/badlogic/gdx/graphics/l$b;

.field protected h:Lcom/badlogic/gdx/graphics/l$b;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/badlogic/gdx/graphics/g;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/badlogic/gdx/graphics/l$a;->a:Lcom/badlogic/gdx/graphics/l$a;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g;->e:Lcom/badlogic/gdx/graphics/l$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/l$a;->a:Lcom/badlogic/gdx/graphics/l$a;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g;->f:Lcom/badlogic/gdx/graphics/l$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/l$b;->b:Lcom/badlogic/gdx/graphics/l$b;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g;->g:Lcom/badlogic/gdx/graphics/l$b;

    sget-object v0, Lcom/badlogic/gdx/graphics/l$b;->b:Lcom/badlogic/gdx/graphics/l$b;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/g;->h:Lcom/badlogic/gdx/graphics/l$b;

    iput p1, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    iput p2, p0, Lcom/badlogic/gdx/graphics/g;->d:I

    return-void
.end method

.method protected static a(ILcom/badlogic/gdx/graphics/o;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/badlogic/gdx/graphics/g;->a(ILcom/badlogic/gdx/graphics/o;I)V

    return-void
.end method

.method public static a(ILcom/badlogic/gdx/graphics/o;I)V
    .locals 12

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->b()V

    :cond_1
    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->e()Lcom/badlogic/gdx/graphics/o$b;

    move-result-object v0

    sget-object v1, Lcom/badlogic/gdx/graphics/o$b;->b:Lcom/badlogic/gdx/graphics/o$b;

    if-ne v0, v1, :cond_2

    invoke-interface {p1, p0}, Lcom/badlogic/gdx/graphics/o;->a(I)V

    return-void

    :cond_2
    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->f()Lcom/badlogic/gdx/graphics/j;

    move-result-object v0

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->g()Z

    move-result v1

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->j()Lcom/badlogic/gdx/graphics/j$c;

    move-result-object v2

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->i()Lcom/badlogic/gdx/graphics/j$c;

    move-result-object v3

    const/4 v10, 0x1

    if-eq v2, v3, :cond_4

    new-instance v1, Lcom/badlogic/gdx/graphics/j;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v2

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v3

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->j()Lcom/badlogic/gdx/graphics/j$c;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/badlogic/gdx/graphics/j;-><init>(IILcom/badlogic/gdx/graphics/j$c;)V

    sget-object v2, Lcom/badlogic/gdx/graphics/j$a;->a:Lcom/badlogic/gdx/graphics/j$a;

    invoke-virtual {v1, v2}, Lcom/badlogic/gdx/graphics/j;->a(Lcom/badlogic/gdx/graphics/j$a;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v8

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v9

    move-object v2, v1

    move-object v3, v0

    invoke-virtual/range {v2 .. v9}, Lcom/badlogic/gdx/graphics/j;->a(Lcom/badlogic/gdx/graphics/j;IIIIII)V

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->c()V

    :cond_3
    move-object v0, v1

    const/4 v1, 0x1

    :cond_4
    sget-object v2, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/16 v3, 0xcf5

    invoke-interface {v2, v3, v10}, Lcom/badlogic/gdx/graphics/e;->glPixelStorei(II)V

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->k()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result p1

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result p2

    invoke-static {p0, v0, p1, p2}, Lcom/badlogic/gdx/graphics/glutils/l;->a(ILcom/badlogic/gdx/graphics/j;II)V

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->f()I

    move-result v5

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->b()I

    move-result v6

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->d()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->e()I

    move-result v9

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->g()I

    move-result v10

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->h()Ljava/nio/ByteBuffer;

    move-result-object v11

    move v3, p0

    move v4, p2

    invoke-interface/range {v2 .. v11}, Lcom/badlogic/gdx/graphics/e;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    :goto_0
    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/j;->c()V

    :cond_6
    return-void
.end method


# virtual methods
.method public a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;)V
    .locals 3

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g;->e:Lcom/badlogic/gdx/graphics/l$a;

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/g;->f:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g;->f()V

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l$a;->a()I

    move-result p1

    const/16 v2, 0x2801

    invoke-interface {v0, v1, v2, p1}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v0, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    invoke-virtual {p2}, Lcom/badlogic/gdx/graphics/l$a;->a()I

    move-result p2

    const/16 v1, 0x2800

    invoke-interface {p1, v0, v1, p2}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;Z)V
    .locals 4

    if-eqz p1, :cond_1

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g;->e:Lcom/badlogic/gdx/graphics/l$a;

    if-eq v0, p1, :cond_1

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    const/16 v2, 0x2801

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l$a;->a()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g;->e:Lcom/badlogic/gdx/graphics/l$a;

    :cond_1
    if-eqz p2, :cond_3

    if-nez p3, :cond_2

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/g;->f:Lcom/badlogic/gdx/graphics/l$a;

    if-eq p1, p2, :cond_3

    :cond_2
    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget p3, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    const/16 v0, 0x2800

    invoke-virtual {p2}, Lcom/badlogic/gdx/graphics/l$a;->a()I

    move-result v1

    invoke-interface {p1, p3, v0, v1}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/g;->f:Lcom/badlogic/gdx/graphics/l$a;

    :cond_3
    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;)V
    .locals 3

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g;->g:Lcom/badlogic/gdx/graphics/l$b;

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/g;->h:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g;->f()V

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l$b;->a()I

    move-result p1

    const/16 v2, 0x2802

    invoke-interface {v0, v1, v2, p1}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v0, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    invoke-virtual {p2}, Lcom/badlogic/gdx/graphics/l$b;->a()I

    move-result p2

    const/16 v1, 0x2803

    invoke-interface {p1, v0, v1, p2}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;Z)V
    .locals 4

    if-eqz p1, :cond_1

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g;->g:Lcom/badlogic/gdx/graphics/l$b;

    if-eq v0, p1, :cond_1

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    const/16 v2, 0x2802

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l$b;->a()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/g;->g:Lcom/badlogic/gdx/graphics/l$b;

    :cond_1
    if-eqz p2, :cond_3

    if-nez p3, :cond_2

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/g;->h:Lcom/badlogic/gdx/graphics/l$b;

    if-eq p1, p2, :cond_3

    :cond_2
    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget p3, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    const/16 v0, 0x2803

    invoke-virtual {p2}, Lcom/badlogic/gdx/graphics/l$b;->a()I

    move-result v1

    invoke-interface {p1, p3, v0, v1}, Lcom/badlogic/gdx/graphics/e;->glTexParameteri(III)V

    iput-object p2, p0, Lcom/badlogic/gdx/graphics/g;->h:Lcom/badlogic/gdx/graphics/l$b;

    :cond_3
    return-void
.end method

.method public c()V
    .locals 0

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/g;->l()V

    return-void
.end method

.method protected abstract d()V
.end method

.method public f()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/g;->c:I

    iget v2, p0, Lcom/badlogic/gdx/graphics/g;->d:I

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/graphics/e;->glBindTexture(II)V

    return-void
.end method

.method public g()Lcom/badlogic/gdx/graphics/l$a;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g;->e:Lcom/badlogic/gdx/graphics/l$a;

    return-object v0
.end method

.method public h()Lcom/badlogic/gdx/graphics/l$a;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g;->f:Lcom/badlogic/gdx/graphics/l$a;

    return-object v0
.end method

.method public i()Lcom/badlogic/gdx/graphics/l$b;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g;->g:Lcom/badlogic/gdx/graphics/l$b;

    return-object v0
.end method

.method public j()Lcom/badlogic/gdx/graphics/l$b;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/g;->h:Lcom/badlogic/gdx/graphics/l$b;

    return-object v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/g;->d:I

    return v0
.end method

.method protected l()V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/g;->d:I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v1, p0, Lcom/badlogic/gdx/graphics/g;->d:I

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glDeleteTexture(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/g;->d:I

    :cond_0
    return-void
.end method
