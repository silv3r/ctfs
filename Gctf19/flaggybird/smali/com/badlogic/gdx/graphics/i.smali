.class public Lcom/badlogic/gdx/graphics/i;
.super Lcom/badlogic/gdx/graphics/a;


# instance fields
.field public m:F

.field private final n:Lcom/badlogic/gdx/math/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/a;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/badlogic/gdx/graphics/i;->m:F

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/i;->n:Lcom/badlogic/gdx/math/g;

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/i;->h:F

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/a;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/badlogic/gdx/graphics/i;->m:F

    new-instance v0, Lcom/badlogic/gdx/math/g;

    invoke-direct {v0}, Lcom/badlogic/gdx/math/g;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/i;->n:Lcom/badlogic/gdx/math/g;

    iput p1, p0, Lcom/badlogic/gdx/graphics/i;->j:F

    iput p2, p0, Lcom/badlogic/gdx/graphics/i;->k:F

    const/4 p1, 0x0

    iput p1, p0, Lcom/badlogic/gdx/graphics/i;->h:F

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/i;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/graphics/i;->a(Z)V

    return-void
.end method

.method public a(Z)V
    .locals 8

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/i;->d:Lcom/badlogic/gdx/math/Matrix4;

    iget v1, p0, Lcom/badlogic/gdx/graphics/i;->m:F

    iget v2, p0, Lcom/badlogic/gdx/graphics/i;->j:F

    neg-float v2, v2

    mul-float v1, v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v3, p0, Lcom/badlogic/gdx/graphics/i;->m:F

    iget v4, p0, Lcom/badlogic/gdx/graphics/i;->j:F

    div-float/2addr v4, v2

    mul-float v3, v3, v4

    iget v4, p0, Lcom/badlogic/gdx/graphics/i;->m:F

    iget v5, p0, Lcom/badlogic/gdx/graphics/i;->k:F

    div-float/2addr v5, v2

    neg-float v5, v5

    mul-float v4, v4, v5

    iget v5, p0, Lcom/badlogic/gdx/graphics/i;->m:F

    iget v6, p0, Lcom/badlogic/gdx/graphics/i;->k:F

    mul-float v5, v5, v6

    div-float/2addr v5, v2

    iget v6, p0, Lcom/badlogic/gdx/graphics/i;->h:F

    iget v7, p0, Lcom/badlogic/gdx/graphics/i;->i:F

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/badlogic/gdx/math/Matrix4;->a(FFFFFF)Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/i;->e:Lcom/badlogic/gdx/math/Matrix4;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/i;->a:Lcom/badlogic/gdx/math/g;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/i;->n:Lcom/badlogic/gdx/math/g;

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/i;->a:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v2, v3}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object v2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/i;->b:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v2, v3}, Lcom/badlogic/gdx/math/g;->b(Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/g;

    move-result-object v2

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/i;->c:Lcom/badlogic/gdx/math/g;

    invoke-virtual {v0, v1, v2, v3}, Lcom/badlogic/gdx/math/Matrix4;->a(Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;Lcom/badlogic/gdx/math/g;)Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/i;->f:Lcom/badlogic/gdx/math/Matrix4;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/i;->d:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/math/Matrix4;->a(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/i;->f:Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, v0, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/i;->e:Lcom/badlogic/gdx/math/Matrix4;

    iget-object v1, v1, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    invoke-static {v0, v1}, Lcom/badlogic/gdx/math/Matrix4;->mul([F[F)V

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/i;->g:Lcom/badlogic/gdx/math/Matrix4;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/i;->f:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/math/Matrix4;->a(Lcom/badlogic/gdx/math/Matrix4;)Lcom/badlogic/gdx/math/Matrix4;

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/i;->g:Lcom/badlogic/gdx/math/Matrix4;

    iget-object p1, p1, Lcom/badlogic/gdx/math/Matrix4;->a:[F

    invoke-static {p1}, Lcom/badlogic/gdx/math/Matrix4;->inv([F)Z

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/i;->l:Lcom/badlogic/gdx/math/a;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/i;->g:Lcom/badlogic/gdx/math/Matrix4;

    invoke-virtual {p1, v0}, Lcom/badlogic/gdx/math/a;->a(Lcom/badlogic/gdx/math/Matrix4;)V

    :cond_0
    return-void
.end method
