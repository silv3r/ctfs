.class public Lcom/badlogic/gdx/graphics/m;
.super Lcom/badlogic/gdx/graphics/g;


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/badlogic/gdx/a;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/graphics/m;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/badlogic/gdx/graphics/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/graphics/m;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Lcom/badlogic/gdx/a;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/m;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/badlogic/gdx/graphics/n;)V
    .locals 12

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/m;->b:Lcom/badlogic/gdx/graphics/n;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->g()Z

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/m;->b:Lcom/badlogic/gdx/graphics/n;

    invoke-interface {v1}, Lcom/badlogic/gdx/graphics/n;->g()Z

    move-result v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string v0, "New data must have the same managed status as the old data"

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/badlogic/gdx/graphics/m;->b:Lcom/badlogic/gdx/graphics/n;

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/m;->f()V

    sget-object v1, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    const v2, 0x8c1a

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->h()I

    move-result v4

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->d()I

    move-result v5

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->e()I

    move-result v6

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->f()I

    move-result v7

    const/4 v8, 0x0

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->h()I

    move-result v9

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->i()I

    move-result v10

    const/4 v11, 0x0

    invoke-interface/range {v1 .. v11}, Lcom/badlogic/gdx/graphics/f;->a(IIIIIIIIILjava/nio/Buffer;)V

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->b()V

    :cond_2
    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/n;->c()V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/m;->e:Lcom/badlogic/gdx/graphics/l$a;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/m;->f:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {p0, p1, v0}, Lcom/badlogic/gdx/graphics/m;->a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/m;->g:Lcom/badlogic/gdx/graphics/l$b;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/m;->h:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p0, p1, v0}, Lcom/badlogic/gdx/graphics/m;->a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;)V

    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v0, p0, Lcom/badlogic/gdx/graphics/m;->c:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/badlogic/gdx/graphics/e;->glBindTexture(II)V

    return-void
.end method

.method public static b(Lcom/badlogic/gdx/a;)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/graphics/m;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/utils/a;

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/m;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/m;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/m;->b:Lcom/badlogic/gdx/graphics/n;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/n;->g()Z

    move-result v0

    return v0
.end method

.method protected d()V
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/m;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/m;->b:Lcom/badlogic/gdx/graphics/n;

    invoke-direct {p0, v0}, Lcom/badlogic/gdx/graphics/m;->a(Lcom/badlogic/gdx/graphics/n;)V

    return-void

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Tried to reload an unmanaged TextureArray"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method
