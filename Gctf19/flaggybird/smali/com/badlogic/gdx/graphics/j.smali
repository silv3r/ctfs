.class public Lcom/badlogic/gdx/graphics/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/utils/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/j$b;,
        Lcom/badlogic/gdx/graphics/j$a;,
        Lcom/badlogic/gdx/graphics/j$c;
    }
.end annotation


# instance fields
.field final a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

.field b:I

.field private c:Lcom/badlogic/gdx/graphics/j$a;

.field private d:Lcom/badlogic/gdx/graphics/j$b;

.field private e:Z


# direct methods
.method public constructor <init>(IILcom/badlogic/gdx/graphics/j$c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/badlogic/gdx/graphics/j$a;->b:Lcom/badlogic/gdx/graphics/j$a;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/j;->c:Lcom/badlogic/gdx/graphics/j$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/j$b;->b:Lcom/badlogic/gdx/graphics/j$b;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/j;->d:Lcom/badlogic/gdx/graphics/j$b;

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/j;->b:I

    new-instance v0, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-static {p3}, Lcom/badlogic/gdx/graphics/j$c;->a(Lcom/badlogic/gdx/graphics/j$c;)I

    move-result p3

    invoke-direct {v0, p1, p2, p3}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;-><init>(III)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p1, p1, p1}, Lcom/badlogic/gdx/graphics/j;->a(FFFF)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/j;->a()V

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/c/a;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/badlogic/gdx/graphics/j$a;->b:Lcom/badlogic/gdx/graphics/j$a;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/j;->c:Lcom/badlogic/gdx/graphics/j$a;

    sget-object v0, Lcom/badlogic/gdx/graphics/j$b;->b:Lcom/badlogic/gdx/graphics/j$b;

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/j;->d:Lcom/badlogic/gdx/graphics/j$b;

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/graphics/j;->b:I

    :try_start_0
    invoke-virtual {p1}, Lcom/badlogic/gdx/c/a;->i()[B

    move-result-object v1

    new-instance v2, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    array-length v3, v1

    invoke-direct {v2, v1, v0, v3, v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;-><init>([BIII)V

    iput-object v2, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/badlogic/gdx/utils/d;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t load file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    iget v1, p0, Lcom/badlogic/gdx/graphics/j;->b:I

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->c(I)V

    return-void
.end method

.method public a(FFFF)V
    .locals 0

    invoke-static {p1, p2, p3, p4}, Lcom/badlogic/gdx/graphics/b;->a(FFFF)I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/j;->b:I

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/j$a;)V
    .locals 2

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/j;->c:Lcom/badlogic/gdx/graphics/j$a;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    sget-object v1, Lcom/badlogic/gdx/graphics/j$a;->a:Lcom/badlogic/gdx/graphics/j$a;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->d(I)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/j;IIIIII)V
    .locals 8

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    iget-object v1, p1, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    move v2, p4

    move v3, p5

    move v4, p2

    move v5, p3

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->a(Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;IIIIII)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/graphics/j;IIIIIIII)V
    .locals 11

    move-object v0, p0

    iget-object v1, v0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    move-object v2, p1

    iget-object v2, v2, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-virtual/range {v1 .. v10}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->a(Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;IIIIIIII)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->d()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/j;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->c()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/graphics/j;->e:Z

    return-void

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Pixmap already disposed!"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->b()I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->g()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->f()I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->h()I

    move-result v0

    return v0
.end method

.method public h()Ljava/nio/ByteBuffer;
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/graphics/j;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Pixmap already disposed"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public i()Lcom/badlogic/gdx/graphics/j$c;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/j;->a:Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/g2d/Gdx2DPixmap;->e()I

    move-result v0

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/j$c;->a(I)Lcom/badlogic/gdx/graphics/j$c;

    move-result-object v0

    return-object v0
.end method
