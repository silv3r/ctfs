.class public interface abstract Lcom/badlogic/gdx/graphics/e;
.super Ljava/lang/Object;


# virtual methods
.method public abstract glAttachShader(II)V
.end method

.method public abstract glBindBuffer(II)V
.end method

.method public abstract glBindFramebuffer(II)V
.end method

.method public abstract glBindRenderbuffer(II)V
.end method

.method public abstract glBindTexture(II)V
.end method

.method public abstract glBlendFuncSeparate(IIII)V
.end method

.method public abstract glBufferData(IILjava/nio/Buffer;I)V
.end method

.method public abstract glBufferSubData(IIILjava/nio/Buffer;)V
.end method

.method public abstract glCheckFramebufferStatus(I)I
.end method

.method public abstract glClear(I)V
.end method

.method public abstract glClearColor(FFFF)V
.end method

.method public abstract glCompileShader(I)V
.end method

.method public abstract glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V
.end method

.method public abstract glCreateProgram()I
.end method

.method public abstract glCreateShader(I)I
.end method

.method public abstract glDeleteBuffer(I)V
.end method

.method public abstract glDeleteFramebuffer(I)V
.end method

.method public abstract glDeleteProgram(I)V
.end method

.method public abstract glDeleteRenderbuffer(I)V
.end method

.method public abstract glDeleteShader(I)V
.end method

.method public abstract glDeleteTexture(I)V
.end method

.method public abstract glDepthMask(Z)V
.end method

.method public abstract glDisable(I)V
.end method

.method public abstract glDisableVertexAttribArray(I)V
.end method

.method public abstract glDrawArrays(III)V
.end method

.method public abstract glDrawElements(IIII)V
.end method

.method public abstract glDrawElements(IIILjava/nio/Buffer;)V
.end method

.method public abstract glEnable(I)V
.end method

.method public abstract glEnableVertexAttribArray(I)V
.end method

.method public abstract glFramebufferRenderbuffer(IIII)V
.end method

.method public abstract glFramebufferTexture2D(IIIII)V
.end method

.method public abstract glGenBuffer()I
.end method

.method public abstract glGenFramebuffer()I
.end method

.method public abstract glGenRenderbuffer()I
.end method

.method public abstract glGenTexture()I
.end method

.method public abstract glGenerateMipmap(I)V
.end method

.method public abstract glGetActiveAttrib(IILjava/nio/IntBuffer;Ljava/nio/Buffer;)Ljava/lang/String;
.end method

.method public abstract glGetActiveUniform(IILjava/nio/IntBuffer;Ljava/nio/Buffer;)Ljava/lang/String;
.end method

.method public abstract glGetAttribLocation(ILjava/lang/String;)I
.end method

.method public abstract glGetIntegerv(ILjava/nio/IntBuffer;)V
.end method

.method public abstract glGetProgramInfoLog(I)Ljava/lang/String;
.end method

.method public abstract glGetProgramiv(IILjava/nio/IntBuffer;)V
.end method

.method public abstract glGetShaderInfoLog(I)Ljava/lang/String;
.end method

.method public abstract glGetShaderiv(IILjava/nio/IntBuffer;)V
.end method

.method public abstract glGetString(I)Ljava/lang/String;
.end method

.method public abstract glGetUniformLocation(ILjava/lang/String;)I
.end method

.method public abstract glLinkProgram(I)V
.end method

.method public abstract glPixelStorei(II)V
.end method

.method public abstract glRenderbufferStorage(IIII)V
.end method

.method public abstract glShaderSource(ILjava/lang/String;)V
.end method

.method public abstract glTexImage2D(IIIIIIIILjava/nio/Buffer;)V
.end method

.method public abstract glTexParameteri(III)V
.end method

.method public abstract glUniform1i(II)V
.end method

.method public abstract glUniformMatrix4fv(IIZ[FI)V
.end method

.method public abstract glUseProgram(I)V
.end method

.method public abstract glVertexAttribPointer(IIIZII)V
.end method

.method public abstract glVertexAttribPointer(IIIZILjava/nio/Buffer;)V
.end method
