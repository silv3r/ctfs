.class public final Lcom/badlogic/gdx/graphics/q;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/q$a;,
        Lcom/badlogic/gdx/graphics/q$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/badlogic/gdx/graphics/q;",
        ">;",
        "Ljava/lang/Iterable<",
        "Lcom/badlogic/gdx/graphics/p;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field private final b:[Lcom/badlogic/gdx/graphics/p;

.field private c:J

.field private d:Lcom/badlogic/gdx/graphics/q$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/graphics/q$a<",
            "Lcom/badlogic/gdx/graphics/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lcom/badlogic/gdx/graphics/p;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/badlogic/gdx/graphics/q;->c:J

    array-length v0, p1

    if-eqz v0, :cond_1

    array-length v0, p1

    new-array v0, v0, [Lcom/badlogic/gdx/graphics/p;

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    aget-object v2, p1, v1

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iput-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    invoke-direct {p0}, Lcom/badlogic/gdx/graphics/q;->c()I

    move-result p1

    iput p1, p0, Lcom/badlogic/gdx/graphics/q;->a:I

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "attributes must be >= 1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private c()I
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v2, v2, v0

    iput v1, v2, Lcom/badlogic/gdx/graphics/p;->e:I

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/p;->b()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v0, v0

    return v0
.end method

.method public a(Lcom/badlogic/gdx/graphics/q;)I
    .locals 7

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v0, v0

    iget-object v1, p1, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v0, v0

    iget-object p1, p1, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length p1, p1

    sub-int/2addr v0, p1

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/q;->b()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/q;->b()J

    move-result-wide v2

    cmp-long v4, v0, v2

    const/4 v5, -0x1

    const/4 v6, 0x1

    if-eqz v4, :cond_2

    cmp-long p1, v0, v2

    if-gez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v0, v0

    sub-int/2addr v0, v6

    :goto_1
    if-ltz v0, :cond_9

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v1, v1, v0

    iget-object v2, p1, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v2, v2, v0

    iget v3, v1, Lcom/badlogic/gdx/graphics/p;->a:I

    iget v4, v2, Lcom/badlogic/gdx/graphics/p;->a:I

    if-eq v3, v4, :cond_3

    iget p1, v1, Lcom/badlogic/gdx/graphics/p;->a:I

    iget v0, v2, Lcom/badlogic/gdx/graphics/p;->a:I

    :goto_2
    sub-int/2addr p1, v0

    return p1

    :cond_3
    iget v3, v1, Lcom/badlogic/gdx/graphics/p;->g:I

    iget v4, v2, Lcom/badlogic/gdx/graphics/p;->g:I

    if-eq v3, v4, :cond_4

    iget p1, v1, Lcom/badlogic/gdx/graphics/p;->g:I

    iget v0, v2, Lcom/badlogic/gdx/graphics/p;->g:I

    goto :goto_2

    :cond_4
    iget v3, v1, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v4, v2, Lcom/badlogic/gdx/graphics/p;->b:I

    if-eq v3, v4, :cond_5

    iget p1, v1, Lcom/badlogic/gdx/graphics/p;->b:I

    iget v0, v2, Lcom/badlogic/gdx/graphics/p;->b:I

    goto :goto_2

    :cond_5
    iget-boolean v3, v1, Lcom/badlogic/gdx/graphics/p;->c:Z

    iget-boolean v4, v2, Lcom/badlogic/gdx/graphics/p;->c:Z

    if-eq v3, v4, :cond_7

    iget-boolean p1, v1, Lcom/badlogic/gdx/graphics/p;->c:Z

    if-eqz p1, :cond_6

    const/4 v5, 0x1

    :cond_6
    return v5

    :cond_7
    iget v3, v1, Lcom/badlogic/gdx/graphics/p;->d:I

    iget v4, v2, Lcom/badlogic/gdx/graphics/p;->d:I

    if-eq v3, v4, :cond_8

    iget p1, v1, Lcom/badlogic/gdx/graphics/p;->d:I

    iget v0, v2, Lcom/badlogic/gdx/graphics/p;->d:I

    goto :goto_2

    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_9
    const/4 p1, 0x0

    return p1
.end method

.method public a(I)Lcom/badlogic/gdx/graphics/p;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public b()J
    .locals 5

    iget-wide v0, p0, Lcom/badlogic/gdx/graphics/q;->c:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v3, v3, v2

    iget v3, v3, Lcom/badlogic/gdx/graphics/p;->a:I

    int-to-long v3, v3

    or-long/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iput-wide v0, p0, Lcom/badlogic/gdx/graphics/q;->c:J

    :cond_1
    iget-wide v0, p0, Lcom/badlogic/gdx/graphics/q;->c:J

    return-wide v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/badlogic/gdx/graphics/q;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/graphics/q;->a(Lcom/badlogic/gdx/graphics/q;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/badlogic/gdx/graphics/q;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/badlogic/gdx/graphics/q;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v1, v1

    iget-object v3, p1, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v3, v3

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v3, v3, v1

    iget-object v4, p1, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Lcom/badlogic/gdx/graphics/p;->a(Lcom/badlogic/gdx/graphics/p;)Z

    move-result v3

    if-nez v3, :cond_3

    return v2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return v0
.end method

.method public hashCode()I
    .locals 5

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x3d

    int-to-long v0, v0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    const-wide/16 v3, 0x3d

    mul-long v0, v0, v3

    iget-object v3, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/p;->hashCode()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/badlogic/gdx/graphics/p;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->d:Lcom/badlogic/gdx/graphics/q$a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/graphics/q$a;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/q$a;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/q;->d:Lcom/badlogic/gdx/graphics/q$a;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q;->d:Lcom/badlogic/gdx/graphics/q$a;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/q$a;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/badlogic/gdx/graphics/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/badlogic/gdx/graphics/p;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/badlogic/gdx/graphics/p;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/badlogic/gdx/graphics/q;->b:[Lcom/badlogic/gdx/graphics/p;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/badlogic/gdx/graphics/p;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
