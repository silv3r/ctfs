.class public final enum Lcom/badlogic/gdx/graphics/j$c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/graphics/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/badlogic/gdx/graphics/j$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/badlogic/gdx/graphics/j$c;

.field public static final enum b:Lcom/badlogic/gdx/graphics/j$c;

.field public static final enum c:Lcom/badlogic/gdx/graphics/j$c;

.field public static final enum d:Lcom/badlogic/gdx/graphics/j$c;

.field public static final enum e:Lcom/badlogic/gdx/graphics/j$c;

.field public static final enum f:Lcom/badlogic/gdx/graphics/j$c;

.field public static final enum g:Lcom/badlogic/gdx/graphics/j$c;

.field private static final synthetic h:[Lcom/badlogic/gdx/graphics/j$c;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "Alpha"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->a:Lcom/badlogic/gdx/graphics/j$c;

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "Intensity"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->b:Lcom/badlogic/gdx/graphics/j$c;

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "LuminanceAlpha"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->c:Lcom/badlogic/gdx/graphics/j$c;

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "RGB565"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "RGBA4444"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->e:Lcom/badlogic/gdx/graphics/j$c;

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "RGB888"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->f:Lcom/badlogic/gdx/graphics/j$c;

    new-instance v0, Lcom/badlogic/gdx/graphics/j$c;

    const-string v1, "RGBA8888"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/badlogic/gdx/graphics/j$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->g:Lcom/badlogic/gdx/graphics/j$c;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/badlogic/gdx/graphics/j$c;

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->a:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->b:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->c:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->e:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->f:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v7

    sget-object v1, Lcom/badlogic/gdx/graphics/j$c;->g:Lcom/badlogic/gdx/graphics/j$c;

    aput-object v1, v0, v8

    sput-object v0, Lcom/badlogic/gdx/graphics/j$c;->h:[Lcom/badlogic/gdx/graphics/j$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/badlogic/gdx/graphics/j$c;)I
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->a:Lcom/badlogic/gdx/graphics/j$c;

    const/4 v1, 0x1

    if-ne p0, v0, :cond_0

    return v1

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->b:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_1

    return v1

    :cond_1
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->c:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_2

    const/4 p0, 0x2

    return p0

    :cond_2
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_3

    const/4 p0, 0x5

    return p0

    :cond_3
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->e:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_4

    const/4 p0, 0x6

    return p0

    :cond_4
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->f:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_5

    const/4 p0, 0x3

    return p0

    :cond_5
    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->g:Lcom/badlogic/gdx/graphics/j$c;

    if-ne p0, v0, :cond_6

    const/4 p0, 0x4

    return p0

    :cond_6
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Format: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(I)Lcom/badlogic/gdx/graphics/j$c;
    .locals 3

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    sget-object p0, Lcom/badlogic/gdx/graphics/j$c;->a:Lcom/badlogic/gdx/graphics/j$c;

    return-object p0

    :cond_0
    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    sget-object p0, Lcom/badlogic/gdx/graphics/j$c;->c:Lcom/badlogic/gdx/graphics/j$c;

    return-object p0

    :cond_1
    const/4 v0, 0x5

    if-ne p0, v0, :cond_2

    sget-object p0, Lcom/badlogic/gdx/graphics/j$c;->d:Lcom/badlogic/gdx/graphics/j$c;

    return-object p0

    :cond_2
    const/4 v0, 0x6

    if-ne p0, v0, :cond_3

    sget-object p0, Lcom/badlogic/gdx/graphics/j$c;->e:Lcom/badlogic/gdx/graphics/j$c;

    return-object p0

    :cond_3
    const/4 v0, 0x3

    if-ne p0, v0, :cond_4

    sget-object p0, Lcom/badlogic/gdx/graphics/j$c;->f:Lcom/badlogic/gdx/graphics/j$c;

    return-object p0

    :cond_4
    const/4 v0, 0x4

    if-ne p0, v0, :cond_5

    sget-object p0, Lcom/badlogic/gdx/graphics/j$c;->g:Lcom/badlogic/gdx/graphics/j$c;

    return-object p0

    :cond_5
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Gdx2DPixmap Format: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/badlogic/gdx/graphics/j$c;
    .locals 1

    const-class v0, Lcom/badlogic/gdx/graphics/j$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/graphics/j$c;

    return-object p0
.end method

.method public static values()[Lcom/badlogic/gdx/graphics/j$c;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/j$c;->h:[Lcom/badlogic/gdx/graphics/j$c;

    invoke-virtual {v0}, [Lcom/badlogic/gdx/graphics/j$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/badlogic/gdx/graphics/j$c;

    return-object v0
.end method
