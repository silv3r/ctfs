.class public final enum Lcom/badlogic/gdx/graphics/l$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/graphics/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/badlogic/gdx/graphics/l$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/badlogic/gdx/graphics/l$b;

.field public static final enum b:Lcom/badlogic/gdx/graphics/l$b;

.field public static final enum c:Lcom/badlogic/gdx/graphics/l$b;

.field private static final synthetic e:[Lcom/badlogic/gdx/graphics/l$b;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/badlogic/gdx/graphics/l$b;

    const-string v1, "MirroredRepeat"

    const/4 v2, 0x0

    const v3, 0x8370

    invoke-direct {v0, v1, v2, v3}, Lcom/badlogic/gdx/graphics/l$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/badlogic/gdx/graphics/l$b;->a:Lcom/badlogic/gdx/graphics/l$b;

    new-instance v0, Lcom/badlogic/gdx/graphics/l$b;

    const-string v1, "ClampToEdge"

    const/4 v3, 0x1

    const v4, 0x812f

    invoke-direct {v0, v1, v3, v4}, Lcom/badlogic/gdx/graphics/l$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/badlogic/gdx/graphics/l$b;->b:Lcom/badlogic/gdx/graphics/l$b;

    new-instance v0, Lcom/badlogic/gdx/graphics/l$b;

    const-string v1, "Repeat"

    const/4 v4, 0x2

    const/16 v5, 0x2901

    invoke-direct {v0, v1, v4, v5}, Lcom/badlogic/gdx/graphics/l$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/badlogic/gdx/graphics/l$b;->c:Lcom/badlogic/gdx/graphics/l$b;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/badlogic/gdx/graphics/l$b;

    sget-object v1, Lcom/badlogic/gdx/graphics/l$b;->a:Lcom/badlogic/gdx/graphics/l$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/badlogic/gdx/graphics/l$b;->b:Lcom/badlogic/gdx/graphics/l$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/badlogic/gdx/graphics/l$b;->c:Lcom/badlogic/gdx/graphics/l$b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/badlogic/gdx/graphics/l$b;->e:[Lcom/badlogic/gdx/graphics/l$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/badlogic/gdx/graphics/l$b;->d:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/badlogic/gdx/graphics/l$b;
    .locals 1

    const-class v0, Lcom/badlogic/gdx/graphics/l$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/graphics/l$b;

    return-object p0
.end method

.method public static values()[Lcom/badlogic/gdx/graphics/l$b;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/l$b;->e:[Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {v0}, [Lcom/badlogic/gdx/graphics/l$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/badlogic/gdx/graphics/l$b;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/graphics/l$b;->d:I

    return v0
.end method
