.class public Lcom/badlogic/gdx/graphics/l;
.super Lcom/badlogic/gdx/graphics/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/graphics/l$b;,
        Lcom/badlogic/gdx/graphics/l$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/badlogic/gdx/a;",
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/graphics/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private static i:Lcom/badlogic/gdx/a/e;


# instance fields
.field b:Lcom/badlogic/gdx/graphics/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>(IILcom/badlogic/gdx/graphics/o;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/badlogic/gdx/graphics/g;-><init>(II)V

    invoke-virtual {p0, p3}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/o;)V

    invoke-interface {p3}, Lcom/badlogic/gdx/graphics/o;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-static {p1, p0}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/l;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/c/a;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j$c;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j$c;Z)V
    .locals 0

    invoke-static {p1, p2, p3}, Lcom/badlogic/gdx/graphics/o$a;->a(Lcom/badlogic/gdx/c/a;Lcom/badlogic/gdx/graphics/j$c;Z)Lcom/badlogic/gdx/graphics/o;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/graphics/o;)V

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/graphics/o;)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v0

    const/16 v1, 0xde1

    invoke-direct {p0, v1, v0, p1}, Lcom/badlogic/gdx/graphics/l;-><init>(IILcom/badlogic/gdx/graphics/o;)V

    return-void
.end method

.method public static a(Lcom/badlogic/gdx/a;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static a(Lcom/badlogic/gdx/a;Lcom/badlogic/gdx/graphics/l;)V
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    sget-object p1, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    invoke-interface {p1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static b(Lcom/badlogic/gdx/a;)V
    .locals 8

    sget-object v0, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/badlogic/gdx/utils/a;

    if-nez p0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    :goto_0
    iget v0, p0, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v1, v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/l;->d()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v0}, Lcom/badlogic/gdx/a/e;->b()V

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/utils/a;-><init>(Lcom/badlogic/gdx/utils/a;)V

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/a;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/badlogic/gdx/graphics/l;

    sget-object v4, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v4, v3}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->d()V

    goto :goto_1

    :cond_2
    sget-object v5, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/a/e;->c(Ljava/lang/String;)I

    move-result v5

    sget-object v6, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v6, v4, v1}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;I)V

    iput v1, v3, Lcom/badlogic/gdx/graphics/l;->d:I

    new-instance v6, Lcom/badlogic/gdx/a/a/f$b;

    invoke-direct {v6}, Lcom/badlogic/gdx/a/a/f$b;-><init>()V

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->e()Lcom/badlogic/gdx/graphics/o;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/f$b;->e:Lcom/badlogic/gdx/graphics/o;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->g()Lcom/badlogic/gdx/graphics/l$a;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/f$b;->f:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->h()Lcom/badlogic/gdx/graphics/l$a;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/f$b;->g:Lcom/badlogic/gdx/graphics/l$a;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->i()Lcom/badlogic/gdx/graphics/l$b;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/f$b;->h:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->j()Lcom/badlogic/gdx/graphics/l$b;

    move-result-object v7

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/f$b;->i:Lcom/badlogic/gdx/graphics/l$b;

    iget-object v7, v3, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {v7}, Lcom/badlogic/gdx/graphics/o;->k()Z

    move-result v7

    iput-boolean v7, v6, Lcom/badlogic/gdx/a/a/f$b;->c:Z

    iput-object v3, v6, Lcom/badlogic/gdx/a/a/f$b;->d:Lcom/badlogic/gdx/graphics/l;

    new-instance v7, Lcom/badlogic/gdx/graphics/l$1;

    invoke-direct {v7, v5}, Lcom/badlogic/gdx/graphics/l$1;-><init>(I)V

    iput-object v7, v6, Lcom/badlogic/gdx/a/a/f$b;->a:Lcom/badlogic/gdx/a/c$a;

    sget-object v5, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;)V

    sget-object v5, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v5}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v5

    iput v5, v3, Lcom/badlogic/gdx/graphics/l;->d:I

    sget-object v3, Lcom/badlogic/gdx/graphics/l;->i:Lcom/badlogic/gdx/a/e;

    const-class v5, Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v3, v4, v5, v6}, Lcom/badlogic/gdx/a/e;->a(Ljava/lang/String;Ljava/lang/Class;Lcom/badlogic/gdx/a/c;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/badlogic/gdx/utils/a;->c()V

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/utils/a;->a(Lcom/badlogic/gdx/utils/a;)V

    :cond_4
    return-void
.end method

.method public static n()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Managed textures/app: { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/a;

    sget-object v3, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/o;->h()I

    move-result v0

    return v0
.end method

.method public a(Lcom/badlogic/gdx/graphics/o;)V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->d()Z

    move-result v0

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {v1}, Lcom/badlogic/gdx/graphics/o;->d()Z

    move-result v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string v0, "New data must have the same managed status as the old data"

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/badlogic/gdx/graphics/o;->b()V

    :cond_2
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/l;->f()V

    const/16 v0, 0xde1

    invoke-static {v0, p1}, Lcom/badlogic/gdx/graphics/l;->a(ILcom/badlogic/gdx/graphics/o;)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/l;->e:Lcom/badlogic/gdx/graphics/l$a;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->f:Lcom/badlogic/gdx/graphics/l$a;

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/l$a;Lcom/badlogic/gdx/graphics/l$a;Z)V

    iget-object p1, p0, Lcom/badlogic/gdx/graphics/l;->g:Lcom/badlogic/gdx/graphics/l$b;

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->h:Lcom/badlogic/gdx/graphics/l$b;

    invoke-virtual {p0, p1, v0, v1}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/l$b;Lcom/badlogic/gdx/graphics/l$b;Z)V

    sget-object p1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget v0, p0, Lcom/badlogic/gdx/graphics/l;->c:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/badlogic/gdx/graphics/e;->glBindTexture(II)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/o;->i()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/graphics/l;->d:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/l;->l()V

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/o;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/graphics/l;->a:Ljava/util/Map;

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/utils/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;Z)Z

    :cond_1
    return-void
.end method

.method protected d()V
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/graphics/l;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/e;->glGenTexture()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/graphics/l;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/graphics/o;)V

    return-void

    :cond_0
    new-instance v0, Lcom/badlogic/gdx/utils/d;

    const-string v1, "Tried to reload unmanaged Texture"

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()Lcom/badlogic/gdx/graphics/o;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    return-object v0
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/o;->d()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    instance-of v0, v0, Lcom/badlogic/gdx/graphics/glutils/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/l;->b:Lcom/badlogic/gdx/graphics/o;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
