.class Lcom/badlogic/gdx/graphics/q$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/graphics/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private b:Lcom/badlogic/gdx/graphics/q$b;

.field private c:Lcom/badlogic/gdx/graphics/q$b;


# direct methods
.method public constructor <init>([Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/graphics/q$a;->a:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/graphics/q$b;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/q$a;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/q$b;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    new-instance v0, Lcom/badlogic/gdx/graphics/q$b;

    iget-object v1, p0, Lcom/badlogic/gdx/graphics/q$a;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/q$b;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->c:Lcom/badlogic/gdx/graphics/q$b;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    iget-boolean v0, v0, Lcom/badlogic/gdx/graphics/q$b;->b:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    iput v2, v0, Lcom/badlogic/gdx/graphics/q$b;->a:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    iput-boolean v1, v0, Lcom/badlogic/gdx/graphics/q$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->c:Lcom/badlogic/gdx/graphics/q$b;

    iput-boolean v2, v0, Lcom/badlogic/gdx/graphics/q$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->c:Lcom/badlogic/gdx/graphics/q$b;

    iput v2, v0, Lcom/badlogic/gdx/graphics/q$b;->a:I

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->c:Lcom/badlogic/gdx/graphics/q$b;

    iput-boolean v1, v0, Lcom/badlogic/gdx/graphics/q$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->b:Lcom/badlogic/gdx/graphics/q$b;

    iput-boolean v2, v0, Lcom/badlogic/gdx/graphics/q$b;->b:Z

    iget-object v0, p0, Lcom/badlogic/gdx/graphics/q$a;->c:Lcom/badlogic/gdx/graphics/q$b;

    return-object v0
.end method
