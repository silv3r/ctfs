.class public Lcom/badlogic/gdx/backends/android/o;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/backends/android/o;->a:I

    iput v0, p0, Lcom/badlogic/gdx/backends/android/o;->b:I

    return-void
.end method

.method private a(Lcom/badlogic/gdx/backends/android/l;IIIIJ)V
    .locals 1

    iget-object v0, p1, Lcom/badlogic/gdx/backends/android/l;->b:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/backends/android/l$c;

    iput-wide p6, v0, Lcom/badlogic/gdx/backends/android/l$c;->a:J

    iput p3, v0, Lcom/badlogic/gdx/backends/android/l$c;->c:I

    iput p4, v0, Lcom/badlogic/gdx/backends/android/l$c;->d:I

    iput p2, v0, Lcom/badlogic/gdx/backends/android/l$c;->b:I

    iput p5, v0, Lcom/badlogic/gdx/backends/android/l$c;->e:I

    iget-object p1, p1, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;Lcom/badlogic/gdx/backends/android/l;)Z
    .locals 9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v7

    monitor-enter p2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result p1

    neg-float p1, p1

    float-to-int v6, p1

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/badlogic/gdx/backends/android/o;->a(Lcom/badlogic/gdx/backends/android/l;IIIIJ)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    iget v1, p0, Lcom/badlogic/gdx/backends/android/o;->a:I

    if-ne v0, v1, :cond_1

    iget v1, p0, Lcom/badlogic/gdx/backends/android/o;->b:I

    if-eq p1, v1, :cond_2

    :cond_1
    const/4 v3, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    move v4, v0

    move v5, p1

    invoke-direct/range {v1 .. v8}, Lcom/badlogic/gdx/backends/android/o;->a(Lcom/badlogic/gdx/backends/android/l;IIIIJ)V

    iput v0, p0, Lcom/badlogic/gdx/backends/android/o;->a:I

    iput p1, p0, Lcom/badlogic/gdx/backends/android/o;->b:I

    :cond_2
    :goto_0
    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {p1}, Lcom/badlogic/gdx/a;->b()Lcom/badlogic/gdx/h;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/h;->g()V

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
