.class public Lcom/badlogic/gdx/backends/android/j;
.super Lcom/badlogic/gdx/backends/android/AndroidGL20;

# interfaces
.implements Lcom/badlogic/gdx/graphics/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/AndroidGL20;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    invoke-static {p1}, Landroid/opengl/GLES30;->glBindVertexArray(I)V

    return-void
.end method

.method public a(IIIIIIIIILjava/nio/Buffer;)V
    .locals 10

    if-nez p10, :cond_0

    const/4 v9, 0x0

    move v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-static/range {v0 .. v9}, Landroid/opengl/GLES30;->glTexImage3D(IIIIIIIIII)V

    goto :goto_0

    :cond_0
    invoke-static/range {p1 .. p10}, Landroid/opengl/GLES30;->glTexImage3D(IIIIIIIIILjava/nio/Buffer;)V

    :goto_0
    return-void
.end method

.method public a(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {p1, p2}, Landroid/opengl/GLES30;->glDrawBuffers(ILjava/nio/IntBuffer;)V

    return-void
.end method

.method public b(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {p1, p2}, Landroid/opengl/GLES30;->glDeleteVertexArrays(ILjava/nio/IntBuffer;)V

    return-void
.end method

.method public c(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {p1, p2}, Landroid/opengl/GLES30;->glGenVertexArrays(ILjava/nio/IntBuffer;)V

    return-void
.end method
