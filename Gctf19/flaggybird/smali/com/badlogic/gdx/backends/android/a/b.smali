.class public Lcom/badlogic/gdx/backends/android/a/b;
.super Landroid/opengl/GLSurfaceView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/backends/android/a/b$a;,
        Lcom/badlogic/gdx/backends/android/a/b$b;
    }
.end annotation


# static fields
.field static a:Ljava/lang/String; = "GL2JNIView"

.field static c:I


# instance fields
.field final b:Lcom/badlogic/gdx/backends/android/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/badlogic/gdx/backends/android/a/f;I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    sput p3, Lcom/badlogic/gdx/backends/android/a/b;->c:I

    iput-object p2, p0, Lcom/badlogic/gdx/backends/android/a/b;->b:Lcom/badlogic/gdx/backends/android/a/f;

    const/4 p1, 0x0

    const/16 p2, 0x10

    invoke-direct {p0, p1, p2, p1}, Lcom/badlogic/gdx/backends/android/a/b;->a(ZII)V

    return-void
.end method

.method private a(ZII)V
    .locals 15

    move-object v0, p0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a/b;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    const/4 v3, -0x3

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :cond_0
    new-instance v2, Lcom/badlogic/gdx/backends/android/a/b$b;

    invoke-direct {v2}, Lcom/badlogic/gdx/backends/android/a/b$b;-><init>()V

    invoke-virtual {p0, v2}, Lcom/badlogic/gdx/backends/android/a/b;->setEGLContextFactory(Landroid/opengl/GLSurfaceView$EGLContextFactory;)V

    if-eqz p1, :cond_1

    new-instance v1, Lcom/badlogic/gdx/backends/android/a/b$a;

    const/16 v4, 0x8

    const/16 v5, 0x8

    const/16 v6, 0x8

    const/16 v7, 0x8

    move-object v3, v1

    move/from16 v8, p2

    move/from16 v9, p3

    invoke-direct/range {v3 .. v9}, Lcom/badlogic/gdx/backends/android/a/b$a;-><init>(IIIIII)V

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/badlogic/gdx/backends/android/a/b$a;

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x5

    const/4 v12, 0x0

    move-object v8, v1

    move/from16 v13, p2

    move/from16 v14, p3

    invoke-direct/range {v8 .. v14}, Lcom/badlogic/gdx/backends/android/a/b$a;-><init>(IIIIII)V

    :goto_0
    invoke-virtual {p0, v1}, Lcom/badlogic/gdx/backends/android/a/b;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    return-void
.end method

.method static a(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x1

    :goto_0
    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v3

    const/16 v4, 0x3000

    if-eq v3, v4, :cond_0

    sget-object v2, Lcom/badlogic/gdx/backends/android/a/b;->a:Ljava/lang/String;

    const-string v4, "%s: EGL error: 0x%x"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    return v2
.end method


# virtual methods
.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    if-eqz p1, :cond_0

    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v1, 0x10000000

    or-int/2addr v0, v1

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    :cond_0
    new-instance p1, Lcom/badlogic/gdx/backends/android/a/b$1;

    const/4 v0, 0x0

    invoke-direct {p1, p0, p0, v0}, Lcom/badlogic/gdx/backends/android/a/b$1;-><init>(Lcom/badlogic/gdx/backends/android/a/b;Landroid/view/View;Z)V

    return-object p1
.end method

.method protected onMeasure(II)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/b;->b:Lcom/badlogic/gdx/backends/android/a/f;

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/backends/android/a/f;->a(II)Lcom/badlogic/gdx/backends/android/a/f$a;

    move-result-object p1

    iget p2, p1, Lcom/badlogic/gdx/backends/android/a/f$a;->a:I

    iget p1, p1, Lcom/badlogic/gdx/backends/android/a/f$a;->b:I

    invoke-virtual {p0, p2, p1}, Lcom/badlogic/gdx/backends/android/a/b;->setMeasuredDimension(II)V

    return-void
.end method
