.class public Lcom/badlogic/gdx/backends/android/q;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Lcom/badlogic/gdx/b/a;


# instance fields
.field protected a:Z

.field protected b:Lcom/badlogic/gdx/b/a$a;

.field private final c:Lcom/badlogic/gdx/backends/android/e;

.field private d:Landroid/media/MediaPlayer;

.field private e:Z


# virtual methods
.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/q;->a:Z

    return-void
.end method

.method public c()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->b:Lcom/badlogic/gdx/b/a$a;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->c:Lcom/badlogic/gdx/backends/android/e;

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/e;->a:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->c:Lcom/badlogic/gdx/backends/android/e;

    iget-object v0, v0, Lcom/badlogic/gdx/backends/android/e;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v1

    goto :goto_1

    :catch_0
    :try_start_2
    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidMusic"

    const-string v3, "error while disposing AndroidMusic instance, non-fatal"

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->b:Lcom/badlogic/gdx/b/a$a;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->c:Lcom/badlogic/gdx/backends/android/e;

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/e;->a:Ljava/util/List;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->c:Lcom/badlogic/gdx/backends/android/e;

    iget-object v0, v0, Lcom/badlogic/gdx/backends/android/e;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    :goto_0
    return-void

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    :goto_1
    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->b:Lcom/badlogic/gdx/b/a$a;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->c:Lcom/badlogic/gdx/backends/android/e;

    iget-object v0, v0, Lcom/badlogic/gdx/backends/android/e;->a:Ljava/util/List;

    monitor-enter v0

    :try_start_4
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/q;->c:Lcom/badlogic/gdx/backends/android/e;

    iget-object v2, v2, Lcom/badlogic/gdx/backends/android/e;->a:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    :catchall_3
    move-exception v1

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v1
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/q;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/q;->e:Z

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/q;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :goto_0
    return-void

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/q;->b:Lcom/badlogic/gdx/b/a$a;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    new-instance v0, Lcom/badlogic/gdx/backends/android/q$1;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/backends/android/q$1;-><init>(Lcom/badlogic/gdx/backends/android/q;)V

    invoke-interface {p1, v0}, Lcom/badlogic/gdx/a;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
