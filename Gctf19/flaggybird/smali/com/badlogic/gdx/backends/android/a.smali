.class public Lcom/badlogic/gdx/backends/android/a;
.super Landroid/app/Activity;

# interfaces
.implements Lcom/badlogic/gdx/backends/android/b;


# instance fields
.field protected a:Lcom/badlogic/gdx/backends/android/k;

.field protected b:Lcom/badlogic/gdx/backends/android/l;

.field protected c:Lcom/badlogic/gdx/backends/android/e;

.field protected d:Lcom/badlogic/gdx/backends/android/i;

.field protected e:Lcom/badlogic/gdx/backends/android/r;

.field protected f:Lcom/badlogic/gdx/backends/android/f;

.field protected g:Lcom/badlogic/gdx/c;

.field public h:Landroid/os/Handler;

.field protected i:Z

.field protected final j:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field protected final k:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field protected final l:Lcom/badlogic/gdx/utils/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/m<",
            "Lcom/badlogic/gdx/l;",
            ">;"
        }
    .end annotation
.end field

.field protected m:I

.field protected n:Lcom/badlogic/gdx/d;

.field protected o:Z

.field protected p:Z

.field private final q:Lcom/badlogic/gdx/utils/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/a<",
            "Lcom/badlogic/gdx/backends/android/g;",
            ">;"
        }
    .end annotation
.end field

.field private r:I

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lcom/badlogic/gdx/utils/c;->a()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->i:Z

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->j:Lcom/badlogic/gdx/utils/a;

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->k:Lcom/badlogic/gdx/utils/a;

    new-instance v0, Lcom/badlogic/gdx/utils/m;

    const-class v1, Lcom/badlogic/gdx/l;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/utils/m;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->l:Lcom/badlogic/gdx/utils/m;

    new-instance v0, Lcom/badlogic/gdx/utils/a;

    invoke-direct {v0}, Lcom/badlogic/gdx/utils/a;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->q:Lcom/badlogic/gdx/utils/a;

    const/4 v0, 0x2

    iput v0, p0, Lcom/badlogic/gdx/backends/android/a;->m:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->o:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->p:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/badlogic/gdx/backends/android/a;->r:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->s:Z

    return-void
.end method

.method private a(Lcom/badlogic/gdx/c;Lcom/badlogic/gdx/backends/android/c;Z)V
    .locals 4

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->j()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_4

    new-instance v0, Lcom/badlogic/gdx/backends/android/d;

    invoke-direct {v0}, Lcom/badlogic/gdx/backends/android/d;-><init>()V

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/backends/android/a;->a(Lcom/badlogic/gdx/d;)V

    new-instance v0, Lcom/badlogic/gdx/backends/android/k;

    iget-object v1, p2, Lcom/badlogic/gdx/backends/android/c;->r:Lcom/badlogic/gdx/backends/android/a/f;

    if-nez v1, :cond_0

    new-instance v1, Lcom/badlogic/gdx/backends/android/a/a;

    invoke-direct {v1}, Lcom/badlogic/gdx/backends/android/a/a;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p2, Lcom/badlogic/gdx/backends/android/c;->r:Lcom/badlogic/gdx/backends/android/a/f;

    :goto_0
    invoke-direct {v0, p0, p2, v1}, Lcom/badlogic/gdx/backends/android/k;-><init>(Lcom/badlogic/gdx/backends/android/b;Lcom/badlogic/gdx/backends/android/c;Lcom/badlogic/gdx/backends/android/a/f;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    iget-object v0, v0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    invoke-static {p0, p0, v0, p2}, Lcom/badlogic/gdx/backends/android/m;->a(Lcom/badlogic/gdx/a;Landroid/content/Context;Ljava/lang/Object;Lcom/badlogic/gdx/backends/android/c;)Lcom/badlogic/gdx/backends/android/l;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->b:Lcom/badlogic/gdx/backends/android/l;

    new-instance v0, Lcom/badlogic/gdx/backends/android/e;

    invoke-direct {v0, p0, p2}, Lcom/badlogic/gdx/backends/android/e;-><init>(Landroid/content/Context;Lcom/badlogic/gdx/backends/android/c;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->c:Lcom/badlogic/gdx/backends/android/e;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getFilesDir()Ljava/io/File;

    new-instance v0, Lcom/badlogic/gdx/backends/android/i;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/badlogic/gdx/backends/android/i;-><init>(Landroid/content/res/AssetManager;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->d:Lcom/badlogic/gdx/backends/android/i;

    new-instance v0, Lcom/badlogic/gdx/backends/android/r;

    invoke-direct {v0, p0}, Lcom/badlogic/gdx/backends/android/r;-><init>(Lcom/badlogic/gdx/backends/android/b;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->e:Lcom/badlogic/gdx/backends/android/r;

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a;->g:Lcom/badlogic/gdx/c;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a;->h:Landroid/os/Handler;

    iget-boolean p1, p2, Lcom/badlogic/gdx/backends/android/c;->t:Z

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a;->o:Z

    iget-boolean p1, p2, Lcom/badlogic/gdx/backends/android/c;->o:Z

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a;->p:Z

    new-instance p1, Lcom/badlogic/gdx/backends/android/f;

    invoke-direct {p1, p0}, Lcom/badlogic/gdx/backends/android/f;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a;->f:Lcom/badlogic/gdx/backends/android/f;

    new-instance p1, Lcom/badlogic/gdx/backends/android/a$1;

    invoke-direct {p1, p0}, Lcom/badlogic/gdx/backends/android/a$1;-><init>(Lcom/badlogic/gdx/backends/android/a;)V

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/backends/android/a;->a(Lcom/badlogic/gdx/l;)V

    sput-object p0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->h()Lcom/badlogic/gdx/backends/android/l;

    move-result-object p1

    sput-object p1, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->f()Lcom/badlogic/gdx/e;

    move-result-object p1

    sput-object p1, Lcom/badlogic/gdx/g;->c:Lcom/badlogic/gdx/e;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->g()Lcom/badlogic/gdx/f;

    move-result-object p1

    sput-object p1, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->b()Lcom/badlogic/gdx/h;

    move-result-object p1

    sput-object p1, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->i()Lcom/badlogic/gdx/m;

    move-result-object p1

    sput-object p1, Lcom/badlogic/gdx/g;->f:Lcom/badlogic/gdx/m;

    const/4 p1, 0x1

    if-nez p3, :cond_1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/backends/android/a;->requestWindowFeature(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p3

    const-string v0, "AndroidApplication"

    const-string v1, "Content already displayed, cannot request FEATURE_NO_TITLE"

    invoke-virtual {p0, v0, v1, p3}, Lcom/badlogic/gdx/backends/android/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getWindow()Landroid/view/Window;

    move-result-object p3

    const/16 v0, 0x400

    invoke-virtual {p3, v0, v0}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getWindow()Landroid/view/Window;

    move-result-object p3

    const/16 v0, 0x800

    invoke-virtual {p3, v0}, Landroid/view/Window;->clearFlags(I)V

    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {p3}, Lcom/badlogic/gdx/backends/android/k;->s()Landroid/view/View;

    move-result-object p3

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->e()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, Lcom/badlogic/gdx/backends/android/a;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-boolean p2, p2, Lcom/badlogic/gdx/backends/android/c;->n:Z

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/backends/android/a;->a(Z)V

    iget-boolean p2, p0, Lcom/badlogic/gdx/backends/android/a;->p:Z

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/backends/android/a;->b(Z)V

    iget-boolean p2, p0, Lcom/badlogic/gdx/backends/android/a;->o:Z

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/backends/android/a;->c(Z)V

    iget-boolean p2, p0, Lcom/badlogic/gdx/backends/android/a;->o:Z

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->j()I

    move-result p2

    const/16 p3, 0x13

    if-lt p2, p3, :cond_2

    :try_start_1
    const-string p2, "com.badlogic.gdx.backends.android.u"

    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p3

    const-string v0, "createListener"

    new-array v1, p1, [Ljava/lang/Class;

    const-class v2, Lcom/badlogic/gdx/backends/android/b;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p2, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p2

    new-array v0, p1, [Ljava/lang/Object;

    aput-object p0, v0, v3

    invoke-virtual {p2, p3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p2

    const-string p3, "AndroidApplication"

    const-string v0, "Failed to create AndroidVisibilityListener"

    invoke-virtual {p0, p3, v0, p2}, Lcom/badlogic/gdx/backends/android/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p2

    iget p2, p2, Landroid/content/res/Configuration;->keyboard:I

    if-eq p2, p1, :cond_3

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->h()Lcom/badlogic/gdx/backends/android/l;

    move-result-object p2

    iput-boolean p1, p2, Lcom/badlogic/gdx/backends/android/l;->w:Z

    :cond_3
    return-void

    :cond_4
    new-instance p1, Lcom/badlogic/gdx/utils/d;

    const-string p2, "LibGDX requires Android API Level 9 or later."

    invoke-direct {p1, p2}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Lcom/badlogic/gdx/c;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->g:Lcom/badlogic/gdx/c;

    return-object v0
.end method

.method public a(Lcom/badlogic/gdx/c;Lcom/badlogic/gdx/backends/android/c;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/badlogic/gdx/backends/android/a;->a(Lcom/badlogic/gdx/c;Lcom/badlogic/gdx/backends/android/c;Z)V

    return-void
.end method

.method public a(Lcom/badlogic/gdx/d;)V
    .locals 0

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a;->n:Lcom/badlogic/gdx/d;

    return-void
.end method

.method public a(Lcom/badlogic/gdx/l;)V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->l:Lcom/badlogic/gdx/utils/m;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/a;->l:Lcom/badlogic/gdx/utils/m;

    invoke-virtual {v1, p1}, Lcom/badlogic/gdx/utils/m;->a(Ljava/lang/Object;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->j:Lcom/badlogic/gdx/utils/a;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/a;->j:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v1, p1}, Lcom/badlogic/gdx/utils/a;->a(Ljava/lang/Object;)V

    sget-object p1, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {p1}, Lcom/badlogic/gdx/h;->g()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a;->m:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->k()Lcom/badlogic/gdx/d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a;->m:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->k()Lcom/badlogic/gdx/d;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/badlogic/gdx/d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x80

    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V

    :cond_0
    return-void
.end method

.method public b()Lcom/badlogic/gdx/h;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a;->m:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->k()Lcom/badlogic/gdx/d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a;->m:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->k()Lcom/badlogic/gdx/d;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/badlogic/gdx/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method protected b(Z)V
    .locals 6

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->j()I

    move-result p1

    const/16 v0, 0xb

    if-ge p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "setSystemUiVisibility"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->j()I

    move-result v1

    const/16 v3, 0xd

    if-gt v1, v3, :cond_1

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "AndroidApplication"

    const-string v1, "Can\'t hide status bar"

    invoke-virtual {p0, v0, v1, p1}, Lcom/badlogic/gdx/backends/android/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public c()Lcom/badlogic/gdx/a$a;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/a$a;->a:Lcom/badlogic/gdx/a$a;

    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a;->m:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->k()Lcom/badlogic/gdx/d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/badlogic/gdx/d;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->j()I

    move-result p1

    const/16 v0, 0x13

    if-ge p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "setSystemUiVisibility"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/16 v1, 0x1706

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v5

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "AndroidApplication"

    const-string v1, "Can\'t set immersive mode"

    invoke-virtual {p0, v0, v1, p1}, Lcom/badlogic/gdx/backends/android/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->h:Landroid/os/Handler;

    new-instance v1, Lcom/badlogic/gdx/backends/android/a$2;

    invoke-direct {v1, p0}, Lcom/badlogic/gdx/backends/android/a$2;-><init>(Lcom/badlogic/gdx/backends/android/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected e()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    return-object v0
.end method

.method public f()Lcom/badlogic/gdx/e;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->c:Lcom/badlogic/gdx/backends/android/e;

    return-object v0
.end method

.method public g()Lcom/badlogic/gdx/f;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->d:Lcom/badlogic/gdx/backends/android/i;

    return-object v0
.end method

.method public h()Lcom/badlogic/gdx/backends/android/l;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->b:Lcom/badlogic/gdx/backends/android/l;

    return-object v0
.end method

.method public i()Lcom/badlogic/gdx/m;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->e:Lcom/badlogic/gdx/backends/android/r;

    return-object v0
.end method

.method public j()I
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public k()Lcom/badlogic/gdx/d;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->n:Lcom/badlogic/gdx/d;

    return-object v0
.end method

.method public l()Landroid/content/Context;
    .locals 0

    return-object p0
.end method

.method public m()Lcom/badlogic/gdx/utils/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/a<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->j:Lcom/badlogic/gdx/utils/a;

    return-object v0
.end method

.method public n()Lcom/badlogic/gdx/utils/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/a<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->k:Lcom/badlogic/gdx/utils/a;

    return-object v0
.end method

.method public o()Lcom/badlogic/gdx/utils/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/badlogic/gdx/utils/m<",
            "Lcom/badlogic/gdx/l;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->l:Lcom/badlogic/gdx/utils/m;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->q:Lcom/badlogic/gdx/utils/a;

    monitor-enter v0

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/a;->q:Lcom/badlogic/gdx/utils/a;

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/a;->q:Lcom/badlogic/gdx/utils/a;

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/backends/android/g;

    invoke-interface {v2, p1, p2, p3}, Lcom/badlogic/gdx/backends/android/g;->a(IILandroid/content/Intent;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget p1, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a;->b:Lcom/badlogic/gdx/backends/android/l;

    iput-boolean v0, p1, Lcom/badlogic/gdx/backends/android/l;->w:Z

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/k;->t()Z

    move-result v0

    sget-boolean v1, Lcom/badlogic/gdx/backends/android/k;->a:Z

    const/4 v2, 0x1

    sput-boolean v2, Lcom/badlogic/gdx/backends/android/k;->a:Z

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v3, v2}, Lcom/badlogic/gdx/backends/android/k;->a(Z)V

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v2}, Lcom/badlogic/gdx/backends/android/k;->o()V

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/a;->b:Lcom/badlogic/gdx/backends/android/l;

    invoke-virtual {v2}, Lcom/badlogic/gdx/backends/android/l;->f()V

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v2}, Lcom/badlogic/gdx/backends/android/k;->q()V

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v2}, Lcom/badlogic/gdx/backends/android/k;->p()V

    :cond_0
    sput-boolean v1, Lcom/badlogic/gdx/backends/android/k;->a:Z

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/backends/android/k;->a(Z)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/k;->i()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    sput-object p0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->h()Lcom/badlogic/gdx/backends/android/l;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->f()Lcom/badlogic/gdx/e;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/g;->c:Lcom/badlogic/gdx/e;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->g()Lcom/badlogic/gdx/f;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->b()Lcom/badlogic/gdx/h;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a;->i()Lcom/badlogic/gdx/m;

    move-result-object v0

    sput-object v0, Lcom/badlogic/gdx/g;->f:Lcom/badlogic/gdx/m;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->b:Lcom/badlogic/gdx/backends/android/l;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/l;->g()V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/k;->j()V

    :cond_0
    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->i:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->a:Lcom/badlogic/gdx/backends/android/k;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/k;->n()V

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a;->i:Z

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->s:Z

    iget v2, p0, Lcom/badlogic/gdx/backends/android/a;->r:I

    if-eq v2, v0, :cond_2

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a;->r:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a;->c:Lcom/badlogic/gdx/backends/android/e;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/e;->b()V

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a;->s:Z

    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->o:Z

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/backends/android/a;->c(Z)V

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->p:Z

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/backends/android/a;->b(Z)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/backends/android/a;->r:I

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a;->s:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a;->c:Lcom/badlogic/gdx/backends/android/e;

    invoke-virtual {p1}, Lcom/badlogic/gdx/backends/android/e;->b()V

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a;->s:Z

    goto :goto_0

    :cond_0
    iput v0, p0, Lcom/badlogic/gdx/backends/android/a;->r:I

    :cond_1
    :goto_0
    return-void
.end method
