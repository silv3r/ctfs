.class public final Lcom/badlogic/gdx/backends/android/w$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/backends/android/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:J


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/badlogic/gdx/backends/android/w$a;->e:J

    return-wide v0
.end method

.method public b()Z
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/w$a;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()Landroid/content/res/AssetFileDescriptor;
    .locals 8

    iget v0, p0, Lcom/badlogic/gdx/backends/android/w$a;->c:I

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/w$a;->a:Ljava/io/File;

    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/w$a;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/badlogic/gdx/backends/android/w$a;->d:J

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/w$a;->a:Ljava/io/File;

    return-object v0
.end method
