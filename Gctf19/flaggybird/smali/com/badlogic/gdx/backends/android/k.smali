.class public Lcom/badlogic/gdx/backends/android/k;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lcom/badlogic/gdx/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/backends/android/k$a;
    }
.end annotation


# static fields
.field static volatile a:Z = false


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:F

.field private E:Lcom/badlogic/gdx/h$a;

.field private F:Z

.field final b:Landroid/view/View;

.field c:I

.field d:I

.field e:Lcom/badlogic/gdx/backends/android/b;

.field f:Lcom/badlogic/gdx/graphics/e;

.field g:Lcom/badlogic/gdx/graphics/f;

.field h:Ljavax/microedition/khronos/egl/EGLContext;

.field i:Lcom/badlogic/gdx/graphics/glutils/f;

.field j:Ljava/lang/String;

.field protected k:J

.field protected l:F

.field protected m:J

.field protected n:J

.field protected o:I

.field protected p:I

.field protected q:Lcom/badlogic/gdx/math/h;

.field volatile r:Z

.field volatile s:Z

.field volatile t:Z

.field volatile u:Z

.field volatile v:Z

.field protected final w:Lcom/badlogic/gdx/backends/android/c;

.field x:[I

.field y:Ljava/lang/Object;

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/backends/android/b;Lcom/badlogic/gdx/backends/android/c;Lcom/badlogic/gdx/backends/android/a/f;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/badlogic/gdx/backends/android/k;-><init>(Lcom/badlogic/gdx/backends/android/b;Lcom/badlogic/gdx/backends/android/c;Lcom/badlogic/gdx/backends/android/a/f;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/badlogic/gdx/backends/android/b;Lcom/badlogic/gdx/backends/android/c;Lcom/badlogic/gdx/backends/android/a/f;Z)V
    .locals 10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/badlogic/gdx/backends/android/k;->k:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->l:F

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/badlogic/gdx/backends/android/k;->m:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/badlogic/gdx/backends/android/k;->n:J

    const/4 v1, 0x0

    iput v1, p0, Lcom/badlogic/gdx/backends/android/k;->o:I

    new-instance v2, Lcom/badlogic/gdx/math/h;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lcom/badlogic/gdx/math/h;-><init>(I)V

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/k;->q:Lcom/badlogic/gdx/math/h;

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->r:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->u:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->v:Z

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->z:F

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->A:F

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->B:F

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->C:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->D:F

    new-instance v0, Lcom/badlogic/gdx/h$a;

    const/4 v2, 0x5

    const/4 v3, 0x6

    const/4 v4, 0x5

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/badlogic/gdx/h$a;-><init>(IIIIIIIZ)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->E:Lcom/badlogic/gdx/h$a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/k;->F:Z

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->x:[I

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    invoke-static {}, Lcom/badlogic/gdx/backends/android/AndroidGL20;->init()V

    iput-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-virtual {p0, p1, p3}, Lcom/badlogic/gdx/backends/android/k;->a(Lcom/badlogic/gdx/backends/android/b;Lcom/badlogic/gdx/backends/android/a/f;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/k;->h()V

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    :cond_0
    return-void
.end method

.method private a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->x:[I

    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->x:[I

    const/4 p2, 0x0

    aget p1, p1, p2

    return p1

    :cond_0
    return p5
.end method


# virtual methods
.method protected a(Lcom/badlogic/gdx/backends/android/b;Lcom/badlogic/gdx/backends/android/a/f;)Landroid/view/View;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/k;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/k;->k()Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xa

    if-gt v3, v4, :cond_1

    iget-object v3, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v3, v3, Lcom/badlogic/gdx/backends/android/c;->v:Z

    if-eqz v3, :cond_1

    new-instance v3, Lcom/badlogic/gdx/backends/android/a/c;

    invoke-interface/range {p1 .. p1}, Lcom/badlogic/gdx/backends/android/b;->l()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/badlogic/gdx/backends/android/a/c;-><init>(Landroid/content/Context;Lcom/badlogic/gdx/backends/android/a/f;)V

    if-eqz v2, :cond_0

    invoke-virtual {v3, v2}, Lcom/badlogic/gdx/backends/android/a/c;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v5, v1, Lcom/badlogic/gdx/backends/android/c;->a:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v6, v1, Lcom/badlogic/gdx/backends/android/c;->b:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v7, v1, Lcom/badlogic/gdx/backends/android/c;->c:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v8, v1, Lcom/badlogic/gdx/backends/android/c;->d:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v9, v1, Lcom/badlogic/gdx/backends/android/c;->e:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v10, v1, Lcom/badlogic/gdx/backends/android/c;->f:I

    move-object v4, v3

    invoke-virtual/range {v4 .. v10}, Lcom/badlogic/gdx/backends/android/a/c;->a(IIIIII)V

    :goto_0
    invoke-virtual {v3, v0}, Lcom/badlogic/gdx/backends/android/a/c;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    return-object v3

    :cond_1
    new-instance v3, Lcom/badlogic/gdx/backends/android/a/b;

    invoke-interface/range {p1 .. p1}, Lcom/badlogic/gdx/backends/android/b;->l()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v5, v5, Lcom/badlogic/gdx/backends/android/c;->u:Z

    if-eqz v5, :cond_2

    const/4 v5, 0x3

    goto :goto_1

    :cond_2
    const/4 v5, 0x2

    :goto_1
    invoke-direct {v3, v4, v1, v5}, Lcom/badlogic/gdx/backends/android/a/b;-><init>(Landroid/content/Context;Lcom/badlogic/gdx/backends/android/a/f;I)V

    if-eqz v2, :cond_3

    invoke-virtual {v3, v2}, Lcom/badlogic/gdx/backends/android/a/b;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v12, v1, Lcom/badlogic/gdx/backends/android/c;->a:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v13, v1, Lcom/badlogic/gdx/backends/android/c;->b:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v14, v1, Lcom/badlogic/gdx/backends/android/c;->c:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v15, v1, Lcom/badlogic/gdx/backends/android/c;->d:I

    iget-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v1, v1, Lcom/badlogic/gdx/backends/android/c;->e:I

    iget-object v2, v0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v2, v2, Lcom/badlogic/gdx/backends/android/c;->f:I

    move-object v11, v3

    move/from16 v16, v1

    move/from16 v17, v2

    invoke-virtual/range {v11 .. v17}, Lcom/badlogic/gdx/backends/android/a/b;->setEGLConfigChooser(IIIIII)V

    :goto_2
    invoke-virtual {v3, v0}, Lcom/badlogic/gdx/backends/android/a/b;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    return-object v3

    :cond_4
    new-instance v1, Lcom/badlogic/gdx/utils/d;

    const-string v2, "Libgdx requires OpenGL ES 2.0"

    invoke-direct {v1, v2}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected a(Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 17

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v7

    const/16 v5, 0x3024

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object v2, v0

    move-object v3, v7

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    const/16 v5, 0x3023

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    const/16 v5, 0x3022

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v11

    const/16 v5, 0x3021

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v12

    const/16 v5, 0x3025

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v13

    const/16 v5, 0x3026

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v14

    const/16 v5, 0x3031

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    const/16 v5, 0x30e1

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v1

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v15

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "framebuffer: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "depthbuffer: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stencilbuffer: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "samples: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "coverage sampling: ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/badlogic/gdx/h$a;

    move-object v8, v1

    move/from16 v16, v0

    invoke-direct/range {v8 .. v16}, Lcom/badlogic/gdx/h$a;-><init>(IIIIIIIZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/badlogic/gdx/backends/android/k;->E:Lcom/badlogic/gdx/h$a;

    return-void
.end method

.method protected a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 8

    const/16 v0, 0x1f02

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1f00

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f01

    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/badlogic/gdx/graphics/glutils/f;

    sget-object v7, Lcom/badlogic/gdx/a$a;->a:Lcom/badlogic/gdx/a$a;

    invoke-direct {v6, v7, v1, v3, v5}, Lcom/badlogic/gdx/graphics/glutils/f;-><init>(Lcom/badlogic/gdx/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/badlogic/gdx/backends/android/k;->i:Lcom/badlogic/gdx/graphics/glutils/f;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v1, v1, Lcom/badlogic/gdx/backends/android/c;->u:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->i:Lcom/badlogic/gdx/graphics/glutils/f;

    invoke-virtual {v1}, Lcom/badlogic/gdx/graphics/glutils/f;->a()I

    move-result v1

    const/4 v3, 0x2

    if-le v1, v3, :cond_1

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->g:Lcom/badlogic/gdx/graphics/f;

    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/badlogic/gdx/backends/android/j;

    invoke-direct {v1}, Lcom/badlogic/gdx/backends/android/j;-><init>()V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->g:Lcom/badlogic/gdx/graphics/f;

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->f:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->g:Lcom/badlogic/gdx/graphics/f;

    sput-object v1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->g:Lcom/badlogic/gdx/graphics/f;

    sput-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->g:Lcom/badlogic/gdx/graphics/f;

    sput-object v1, Lcom/badlogic/gdx/g;->i:Lcom/badlogic/gdx/graphics/f;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->f:Lcom/badlogic/gdx/graphics/e;

    if-eqz v1, :cond_2

    return-void

    :cond_2
    new-instance v1, Lcom/badlogic/gdx/backends/android/AndroidGL20;

    invoke-direct {v1}, Lcom/badlogic/gdx/backends/android/AndroidGL20;-><init>()V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->f:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->f:Lcom/badlogic/gdx/graphics/e;

    sput-object v1, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->f:Lcom/badlogic/gdx/graphics/e;

    sput-object v1, Lcom/badlogic/gdx/g;->h:Lcom/badlogic/gdx/graphics/e;

    :goto_0
    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v3, "AndroidGraphics"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "OGL renderer: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v3, "AndroidGraphics"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OGL vendor: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OGL version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OGL extensions: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x1f03

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/badlogic/gdx/backends/android/k;->a:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/k;->F:Z

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/k;->F:Z

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/backends/android/a/d;->setRenderMode(I)V

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    :cond_3
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->q:Lcom/badlogic/gdx/math/h;

    invoke-virtual {p1}, Lcom/badlogic/gdx/math/h;->a()V

    :cond_4
    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->g:Lcom/badlogic/gdx/graphics/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/16 v1, 0x1f03

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->j:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/k;->c:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/k;->d:I

    return v0
.end method

.method public d()F
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/k;->l:F

    return v0
.end method

.method public e()Lcom/badlogic/gdx/h$b;
    .locals 8

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v1}, Lcom/badlogic/gdx/backends/android/b;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v1, Lcom/badlogic/gdx/backends/android/k$a;

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v1

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/badlogic/gdx/backends/android/k$a;-><init>(Lcom/badlogic/gdx/backends/android/k;IIII)V

    return-object v1
.end method

.method public f()Lcom/badlogic/gdx/h$a;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->E:Lcom/badlogic/gdx/h$a;

    return-object v0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    :cond_1
    return-void
.end method

.method protected h()V
    .locals 6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/badlogic/gdx/backends/android/a/b;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/badlogic/gdx/backends/android/a/c;

    if-eqz v0, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setPreserveEGLContextOnPause"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    const-string v2, "Method GLSurfaceView.setPreserveEGLContextOnPause not found"

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d;->b()V

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    :cond_1
    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d;->c()V

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    instance-of v0, v0, Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onResume()V

    :cond_1
    return-void
.end method

.method protected k()Landroid/opengl/GLSurfaceView$EGLConfigChooser;
    .locals 9

    new-instance v8, Lcom/badlogic/gdx/backends/android/a/e;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v1, v0, Lcom/badlogic/gdx/backends/android/c;->a:I

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v2, v0, Lcom/badlogic/gdx/backends/android/c;->b:I

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v3, v0, Lcom/badlogic/gdx/backends/android/c;->c:I

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v4, v0, Lcom/badlogic/gdx/backends/android/c;->d:I

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v5, v0, Lcom/badlogic/gdx/backends/android/c;->e:I

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v6, v0, Lcom/badlogic/gdx/backends/android/c;->f:I

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->w:Lcom/badlogic/gdx/backends/android/c;

    iget v7, v0, Lcom/badlogic/gdx/backends/android/c;->g:I

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/badlogic/gdx/backends/android/a/e;-><init>(IIIIIII)V

    return-object v8
.end method

.method protected l()V
    .locals 3

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v1}, Lcom/badlogic/gdx/backends/android/b;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->xdpi:F

    iput v1, p0, Lcom/badlogic/gdx/backends/android/k;->z:F

    iget v1, v0, Landroid/util/DisplayMetrics;->ydpi:F

    iput v1, p0, Lcom/badlogic/gdx/backends/android/k;->A:F

    iget v1, v0, Landroid/util/DisplayMetrics;->xdpi:F

    const v2, 0x40228f5c    # 2.54f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/badlogic/gdx/backends/android/k;->B:F

    iget v1, v0, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/badlogic/gdx/backends/android/k;->C:F

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->D:F

    return-void
.end method

.method protected m()Z
    .locals 10

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v7

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-interface {v0, v7, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    const/16 v1, 0x9

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    const/16 v1, 0xa

    new-array v4, v1, [Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v8, 0x1

    new-array v9, v8, [I

    const/16 v5, 0xa

    move-object v1, v0

    move-object v2, v7

    move-object v6, v9

    invoke-interface/range {v1 .. v6}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    invoke-interface {v0, v7}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    const/4 v0, 0x0

    aget v1, v9, v0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :array_0
    .array-data 4
        0x3024
        0x4
        0x3023
        0x4
        0x3022
        0x4
        0x3040
        0x4
        0x3038
    .end array-data
.end method

.method n()V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->u:Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method o()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z

    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    const-string v3, "waiting for pause synchronization took too long; assuming deadlock and killing"

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    const-string v3, "waiting for pause synchronization failed!"

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 10

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/badlogic/gdx/backends/android/k;->k:J

    sub-long v2, v0, v2

    long-to-float p1, v2

    const v2, 0x4e6e6b28    # 1.0E9f

    div-float/2addr p1, v2

    iput p1, p0, Lcom/badlogic/gdx/backends/android/k;->l:F

    iput-wide v0, p0, Lcom/badlogic/gdx/backends/android/k;->k:J

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/k;->u:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->q:Lcom/badlogic/gdx/math/h;

    iget v2, p0, Lcom/badlogic/gdx/backends/android/k;->l:F

    invoke-virtual {p1, v2}, Lcom/badlogic/gdx/math/h;->a(F)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput p1, p0, Lcom/badlogic/gdx/backends/android/k;->l:F

    :goto_0
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    iget-boolean v2, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    iget-boolean v3, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z

    iget-boolean v4, p0, Lcom/badlogic/gdx/backends/android/k;->v:Z

    iget-boolean v5, p0, Lcom/badlogic/gdx/backends/android/k;->u:Z

    iget-boolean v6, p0, Lcom/badlogic/gdx/backends/android/k;->u:Z

    const/4 v7, 0x0

    if-eqz v6, :cond_1

    iput-boolean v7, p0, Lcom/badlogic/gdx/backends/android/k;->u:Z

    :cond_1
    iget-boolean v6, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z

    if-eqz v6, :cond_2

    iput-boolean v7, p0, Lcom/badlogic/gdx/backends/android/k;->t:Z

    iget-object v6, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    :cond_2
    iget-boolean v6, p0, Lcom/badlogic/gdx/backends/android/k;->v:Z

    if-eqz v6, :cond_3

    iput-boolean v7, p0, Lcom/badlogic/gdx/backends/android/k;->v:Z

    iget-object v6, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    if-eqz v5, :cond_5

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->o()Lcom/badlogic/gdx/utils/m;

    move-result-object p1

    monitor-enter p1

    :try_start_1
    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/m;->d()[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/badlogic/gdx/l;

    iget v6, p1, Lcom/badlogic/gdx/utils/m;->b:I

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v6, :cond_4

    aget-object v9, v5, v8

    invoke-interface {v9}, Lcom/badlogic/gdx/l;->b()V

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/m;->e()V

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->a()Lcom/badlogic/gdx/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/c;->d()V

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v5, "AndroidGraphics"

    const-string v6, "resumed"

    invoke-interface {p1, v5, v6}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_5
    :goto_2
    if-eqz v2, :cond_7

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->m()Lcom/badlogic/gdx/utils/a;

    move-result-object p1

    monitor-enter p1

    :try_start_3
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v2}, Lcom/badlogic/gdx/backends/android/b;->n()Lcom/badlogic/gdx/utils/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/badlogic/gdx/utils/a;->c()V

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v2}, Lcom/badlogic/gdx/backends/android/b;->n()Lcom/badlogic/gdx/utils/a;

    move-result-object v2

    iget-object v5, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v5}, Lcom/badlogic/gdx/backends/android/b;->m()Lcom/badlogic/gdx/utils/a;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/badlogic/gdx/utils/a;->a(Lcom/badlogic/gdx/utils/a;)V

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v2}, Lcom/badlogic/gdx/backends/android/b;->m()Lcom/badlogic/gdx/utils/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/badlogic/gdx/utils/a;->c()V

    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 p1, 0x0

    :goto_3
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v2}, Lcom/badlogic/gdx/backends/android/b;->n()Lcom/badlogic/gdx/utils/a;

    move-result-object v2

    iget v2, v2, Lcom/badlogic/gdx/utils/a;->b:I

    if-ge p1, v2, :cond_6

    :try_start_4
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {v2}, Lcom/badlogic/gdx/backends/android/b;->n()Lcom/badlogic/gdx/utils/a;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/badlogic/gdx/utils/a;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_4
    add-int/lit8 p1, p1, 0x1

    goto :goto_3

    :cond_6
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->h()Lcom/badlogic/gdx/backends/android/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/backends/android/l;->a()V

    iget-wide v5, p0, Lcom/badlogic/gdx/backends/android/k;->n:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, p0, Lcom/badlogic/gdx/backends/android/k;->n:J

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->a()Lcom/badlogic/gdx/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/c;->b()V

    goto :goto_5

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_7
    :goto_5
    if-eqz v3, :cond_9

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->o()Lcom/badlogic/gdx/utils/m;

    move-result-object p1

    monitor-enter p1

    :try_start_6
    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/m;->d()[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/badlogic/gdx/l;

    iget v3, p1, Lcom/badlogic/gdx/utils/m;->b:I

    const/4 v5, 0x0

    :goto_6
    if-ge v5, v3, :cond_8

    aget-object v6, v2, v5

    invoke-interface {v6}, Lcom/badlogic/gdx/l;->a()V

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_8
    monitor-exit p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->a()Lcom/badlogic/gdx/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/c;->c()V

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    const-string v3, "paused"

    invoke-interface {p1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    :cond_9
    :goto_7
    if-eqz v4, :cond_b

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->o()Lcom/badlogic/gdx/utils/m;

    move-result-object p1

    monitor-enter p1

    :try_start_8
    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/m;->d()[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/badlogic/gdx/l;

    iget v3, p1, Lcom/badlogic/gdx/utils/m;->b:I

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_a

    aget-object v5, v2, v4

    invoke-interface {v5}, Lcom/badlogic/gdx/l;->c()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_a
    monitor-exit p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->a()Lcom/badlogic/gdx/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/c;->e()V

    sget-object p1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    const-string v3, "destroyed"

    invoke-interface {p1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :catchall_3
    move-exception v0

    :try_start_9
    monitor-exit p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    :cond_b
    :goto_9
    iget-wide v2, p0, Lcom/badlogic/gdx/backends/android/k;->m:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x3b9aca00

    cmp-long p1, v2, v4

    if-lez p1, :cond_c

    iget p1, p0, Lcom/badlogic/gdx/backends/android/k;->o:I

    iput p1, p0, Lcom/badlogic/gdx/backends/android/k;->p:I

    iput v7, p0, Lcom/badlogic/gdx/backends/android/k;->o:I

    iput-wide v0, p0, Lcom/badlogic/gdx/backends/android/k;->m:J

    :cond_c
    iget p1, p0, Lcom/badlogic/gdx/backends/android/k;->o:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/badlogic/gdx/backends/android/k;->o:I

    return-void

    :catchall_4
    move-exception v0

    :try_start_a
    monitor-exit p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    iput p2, p0, Lcom/badlogic/gdx/backends/android/k;->c:I

    iput p3, p0, Lcom/badlogic/gdx/backends/android/k;->d:I

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/k;->l()V

    iget v0, p0, Lcom/badlogic/gdx/backends/android/k;->c:I

    iget v1, p0, Lcom/badlogic/gdx/backends/android/k;->d:I

    const/4 v2, 0x0

    invoke-interface {p1, v2, v2, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/k;->r:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->a()Lcom/badlogic/gdx/c;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/c;->a()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/k;->r:Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p1}, Lcom/badlogic/gdx/backends/android/b;->a()Lcom/badlogic/gdx/c;

    move-result-object p1

    invoke-interface {p1, p2, p3}, Lcom/badlogic/gdx/c;->a(II)V

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->h:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {p0, p1}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {p0, p2}, Lcom/badlogic/gdx/backends/android/k;->a(Ljavax/microedition/khronos/egl/EGLConfig;)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/k;->l()V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {p2}, Lcom/badlogic/gdx/graphics/h;->a(Lcom/badlogic/gdx/a;)V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {p2}, Lcom/badlogic/gdx/graphics/l;->b(Lcom/badlogic/gdx/a;)V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {p2}, Lcom/badlogic/gdx/graphics/c;->b(Lcom/badlogic/gdx/a;)V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {p2}, Lcom/badlogic/gdx/graphics/m;->b(Lcom/badlogic/gdx/a;)V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {p2}, Lcom/badlogic/gdx/graphics/glutils/m;->a(Lcom/badlogic/gdx/a;)V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {p2}, Lcom/badlogic/gdx/graphics/glutils/c;->a(Lcom/badlogic/gdx/a;)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/k;->r()V

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-interface {p2}, Lcom/badlogic/gdx/backends/android/b;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p2

    invoke-interface {p2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/badlogic/gdx/backends/android/k;->c:I

    invoke-virtual {p2}, Landroid/view/Display;->getHeight()I

    move-result p2

    iput p2, p0, Lcom/badlogic/gdx/backends/android/k;->d:I

    new-instance p2, Lcom/badlogic/gdx/math/h;

    const/4 v0, 0x5

    invoke-direct {p2, v0}, Lcom/badlogic/gdx/math/h;-><init>(I)V

    iput-object p2, p0, Lcom/badlogic/gdx/backends/android/k;->q:Lcom/badlogic/gdx/math/h;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/badlogic/gdx/backends/android/k;->k:J

    iget p2, p0, Lcom/badlogic/gdx/backends/android/k;->c:I

    iget v0, p0, Lcom/badlogic/gdx/backends/android/k;->d:I

    const/4 v1, 0x0

    invoke-interface {p1, v1, v1, p2, v0}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    return-void
.end method

.method p()V
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->s:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->v:Z

    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/k;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/k;->y:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    sget-object v1, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v2, "AndroidGraphics"

    const-string v3, "waiting for destroy synchronization failed!"

    invoke-interface {v1, v2, v3}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public q()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/h;->b(Lcom/badlogic/gdx/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/l;->a(Lcom/badlogic/gdx/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/c;->a(Lcom/badlogic/gdx/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/m;->a(Lcom/badlogic/gdx/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/glutils/m;->b(Lcom/badlogic/gdx/a;)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->e:Lcom/badlogic/gdx/backends/android/b;

    invoke-static {v0}, Lcom/badlogic/gdx/graphics/glutils/c;->b(Lcom/badlogic/gdx/a;)V

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/k;->r()V

    return-void
.end method

.method protected r()V
    .locals 3

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    invoke-static {}, Lcom/badlogic/gdx/graphics/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    invoke-static {}, Lcom/badlogic/gdx/graphics/l;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    invoke-static {}, Lcom/badlogic/gdx/graphics/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    invoke-static {}, Lcom/badlogic/gdx/graphics/glutils/m;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidGraphics"

    invoke-static {}, Lcom/badlogic/gdx/graphics/glutils/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public s()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/k;->b:Landroid/view/View;

    return-object v0
.end method

.method public t()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/k;->F:Z

    return v0
.end method
