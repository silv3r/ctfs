.class public Lcom/badlogic/gdx/backends/android/a/d;
.super Landroid/view/SurfaceView;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/backends/android/a/d$i;,
        Lcom/badlogic/gdx/backends/android/a/d$k;,
        Lcom/badlogic/gdx/backends/android/a/d$h;,
        Lcom/badlogic/gdx/backends/android/a/d$g;,
        Lcom/badlogic/gdx/backends/android/a/d$l;,
        Lcom/badlogic/gdx/backends/android/a/d$b;,
        Lcom/badlogic/gdx/backends/android/a/d$a;,
        Lcom/badlogic/gdx/backends/android/a/d$d;,
        Lcom/badlogic/gdx/backends/android/a/d$f;,
        Lcom/badlogic/gdx/backends/android/a/d$c;,
        Lcom/badlogic/gdx/backends/android/a/d$e;,
        Lcom/badlogic/gdx/backends/android/a/d$j;
    }
.end annotation


# static fields
.field private static final a:Lcom/badlogic/gdx/backends/android/a/d$i;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/badlogic/gdx/backends/android/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/badlogic/gdx/backends/android/a/d$h;

.field private d:Landroid/opengl/GLSurfaceView$Renderer;

.field private e:Z

.field private f:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

.field private g:Lcom/badlogic/gdx/backends/android/a/d$e;

.field private h:Lcom/badlogic/gdx/backends/android/a/d$f;

.field private i:Lcom/badlogic/gdx/backends/android/a/d$j;

.field private j:I

.field private k:I

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/badlogic/gdx/backends/android/a/d$i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/backends/android/a/d$i;-><init>(Lcom/badlogic/gdx/backends/android/a/d$1;)V

    sput-object v0, Lcom/badlogic/gdx/backends/android/a/d;->a:Lcom/badlogic/gdx/backends/android/a/d$i;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d;->e()V

    return-void
.end method

.method static synthetic a(Lcom/badlogic/gdx/backends/android/a/d;)I
    .locals 0

    iget p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->k:I

    return p0
.end method

.method static synthetic b(Lcom/badlogic/gdx/backends/android/a/d;)Landroid/opengl/GLSurfaceView$EGLConfigChooser;
    .locals 0

    iget-object p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->f:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    return-object p0
.end method

.method static synthetic c(Lcom/badlogic/gdx/backends/android/a/d;)Lcom/badlogic/gdx/backends/android/a/d$e;
    .locals 0

    iget-object p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->g:Lcom/badlogic/gdx/backends/android/a/d$e;

    return-object p0
.end method

.method static synthetic d(Lcom/badlogic/gdx/backends/android/a/d;)Lcom/badlogic/gdx/backends/android/a/d$f;
    .locals 0

    iget-object p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->h:Lcom/badlogic/gdx/backends/android/a/d$f;

    return-object p0
.end method

.method static synthetic d()Lcom/badlogic/gdx/backends/android/a/d$i;
    .locals 1

    sget-object v0, Lcom/badlogic/gdx/backends/android/a/d;->a:Lcom/badlogic/gdx/backends/android/a/d$i;

    return-object v0
.end method

.method static synthetic e(Lcom/badlogic/gdx/backends/android/a/d;)Lcom/badlogic/gdx/backends/android/a/d$j;
    .locals 0

    iget-object p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->i:Lcom/badlogic/gdx/backends/android/a/d$j;

    return-object p0
.end method

.method private e()V
    .locals 3

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/badlogic/gdx/backends/android/a/d;)I
    .locals 0

    iget p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->j:I

    return p0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic g(Lcom/badlogic/gdx/backends/android/a/d;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->l:Z

    return p0
.end method

.method static synthetic h(Lcom/badlogic/gdx/backends/android/a/d;)Landroid/opengl/GLSurfaceView$Renderer;
    .locals 0

    iget-object p0, p0, Lcom/badlogic/gdx/backends/android/a/d;->d:Landroid/opengl/GLSurfaceView$Renderer;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->c()V

    return-void
.end method

.method public a(IIIIII)V
    .locals 9

    new-instance v8, Lcom/badlogic/gdx/backends/android/a/d$b;

    move-object v0, v8

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/badlogic/gdx/backends/android/a/d$b;-><init>(Lcom/badlogic/gdx/backends/android/a/d;IIIIII)V

    invoke-virtual {p0, v8}, Lcom/badlogic/gdx/backends/android/a/d;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->f()V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->g()V

    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getDebugFlags()I
    .locals 1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->j:I

    return v0
.end method

.method public getPreserveEGLContextOnPause()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->l:Z

    return v0
.end method

.method public getRenderMode()I
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->b()I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->d:Landroid/opengl/GLSurfaceView$Renderer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->b()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lcom/badlogic/gdx/backends/android/a/d$h;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/a/d;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lcom/badlogic/gdx/backends/android/a/d$h;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v1, v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->e:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->h()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->e:Z

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0

    iput p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->j:I

    return-void
.end method

.method public setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d;->f()V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->f:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1

    new-instance v0, Lcom/badlogic/gdx/backends/android/a/d$l;

    invoke-direct {v0, p0, p1}, Lcom/badlogic/gdx/backends/android/a/d$l;-><init>(Lcom/badlogic/gdx/backends/android/a/d;Z)V

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/backends/android/a/d;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d;->f()V

    iput p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->k:I

    return-void
.end method

.method public setEGLContextFactory(Lcom/badlogic/gdx/backends/android/a/d$e;)V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d;->f()V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->g:Lcom/badlogic/gdx/backends/android/a/d$e;

    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lcom/badlogic/gdx/backends/android/a/d$f;)V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d;->f()V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->h:Lcom/badlogic/gdx/backends/android/a/d$f;

    return-void
.end method

.method public setGLWrapper(Lcom/badlogic/gdx/backends/android/a/d$j;)V
    .locals 0

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->i:Lcom/badlogic/gdx/backends/android/a/d$j;

    return-void
.end method

.method public setPreserveEGLContextOnPause(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->l:Z

    return-void
.end method

.method public setRenderMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {v0, p1}, Lcom/badlogic/gdx/backends/android/a/d$h;->a(I)V

    return-void
.end method

.method public setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V
    .locals 2

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d;->f()V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->f:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    if-nez v0, :cond_0

    new-instance v0, Lcom/badlogic/gdx/backends/android/a/d$l;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/badlogic/gdx/backends/android/a/d$l;-><init>(Lcom/badlogic/gdx/backends/android/a/d;Z)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->f:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->g:Lcom/badlogic/gdx/backends/android/a/d$e;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    new-instance v0, Lcom/badlogic/gdx/backends/android/a/d$c;

    invoke-direct {v0, p0, v1}, Lcom/badlogic/gdx/backends/android/a/d$c;-><init>(Lcom/badlogic/gdx/backends/android/a/d;Lcom/badlogic/gdx/backends/android/a/d$1;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->g:Lcom/badlogic/gdx/backends/android/a/d$e;

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->h:Lcom/badlogic/gdx/backends/android/a/d$f;

    if-nez v0, :cond_2

    new-instance v0, Lcom/badlogic/gdx/backends/android/a/d$d;

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/backends/android/a/d$d;-><init>(Lcom/badlogic/gdx/backends/android/a/d$1;)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->h:Lcom/badlogic/gdx/backends/android/a/d$f;

    :cond_2
    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->d:Landroid/opengl/GLSurfaceView$Renderer;

    new-instance p1, Lcom/badlogic/gdx/backends/android/a/d$h;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/backends/android/a/d$h;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {p1}, Lcom/badlogic/gdx/backends/android/a/d$h;->start()V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {p1, p3, p4}, Lcom/badlogic/gdx/backends/android/a/d$h;->a(II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {p1}, Lcom/badlogic/gdx/backends/android/a/d$h;->d()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d;->c:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {p1}, Lcom/badlogic/gdx/backends/android/a/d$h;->e()V

    return-void
.end method
