.class Lcom/badlogic/gdx/backends/android/a/d$i;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/backends/android/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "i"
.end annotation


# static fields
.field private static a:Ljava/lang/String; = "GLThreadManager"


# instance fields
.field private b:Z

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Lcom/badlogic/gdx/backends/android/a/d$h;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/badlogic/gdx/backends/android/a/d$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d$i;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->b:Z

    if-nez v0, :cond_1

    const/high16 v0, 0x20000

    iput v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->c:I

    iget v1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->c:I

    const/4 v2, 0x1

    if-lt v1, v0, :cond_0

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->e:Z

    :cond_0
    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->b:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/badlogic/gdx/backends/android/a/d$h;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p1, v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->a(Lcom/badlogic/gdx/backends/android/a/d$h;Z)Z

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->d:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d$i;->c()V

    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object p1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->c:I

    const/high16 v1, 0x20000

    const/4 v2, 0x1

    if-ge v0, v1, :cond_0

    const-string v0, "Q3Dimension MSM7500 "

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    xor-int/2addr p1, v2

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->e:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    :cond_0
    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->e:Z

    xor-int/2addr p1, v2

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->f:Z

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d$i;->c()V

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    xor-int/lit8 v0, v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/badlogic/gdx/backends/android/a/d$h;)Z
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    const/4 v1, 0x1

    if-eq v0, p1, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d$i;->c()V

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->e:Z

    if-eqz p1, :cond_1

    return v1

    :cond_1
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {p1}, Lcom/badlogic/gdx/backends/android/a/d$h;->i()V

    :cond_2
    const/4 p1, 0x0

    return p1

    :cond_3
    :goto_0
    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    return v1
.end method

.method public c(Lcom/badlogic/gdx/backends/android/a/d$h;)V
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d$i;->g:Lcom/badlogic/gdx/backends/android/a/d$h;

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method
