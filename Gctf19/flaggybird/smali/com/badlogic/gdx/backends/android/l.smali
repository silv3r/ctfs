.class public Lcom/badlogic/gdx/backends/android/l;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/badlogic/gdx/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/badlogic/gdx/backends/android/l$b;,
        Lcom/badlogic/gdx/backends/android/l$c;,
        Lcom/badlogic/gdx/backends/android/l$a;
    }
.end annotation


# instance fields
.field A:Z

.field final B:[F

.field final C:[F

.field private D:I

.field private E:[Z

.field private F:Z

.field private G:[Z

.field private H:Landroid/hardware/SensorManager;

.field private I:Ljava/lang/String;

.field private J:Lcom/badlogic/gdx/i$b;

.field private K:Landroid/os/Handler;

.field private L:I

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:F

.field private R:F

.field private S:F

.field private T:F

.field private U:Z

.field private V:Lcom/badlogic/gdx/k;

.field private final W:Lcom/badlogic/gdx/backends/android/c;

.field private X:J

.field private final Y:Lcom/badlogic/gdx/backends/android/s;

.field private Z:Landroid/hardware/SensorEventListener;

.field a:Lcom/badlogic/gdx/utils/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/k<",
            "Lcom/badlogic/gdx/backends/android/l$a;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Landroid/hardware/SensorEventListener;

.field private ab:Landroid/hardware/SensorEventListener;

.field private ac:Landroid/hardware/SensorEventListener;

.field b:Lcom/badlogic/gdx/utils/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/utils/k<",
            "Lcom/badlogic/gdx/backends/android/l$c;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View$OnKeyListener;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/badlogic/gdx/backends/android/l$a;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/badlogic/gdx/backends/android/l$c;",
            ">;"
        }
    .end annotation
.end field

.field f:[I

.field g:[I

.field h:[I

.field i:[I

.field j:[Z

.field k:[I

.field l:[I

.field m:[F

.field final n:Z

.field public o:Z

.field protected final p:[F

.field public q:Z

.field protected final r:[F

.field final s:Lcom/badlogic/gdx/a;

.field final t:Landroid/content/Context;

.field protected final u:Lcom/badlogic/gdx/backends/android/t;

.field protected final v:Landroid/os/Vibrator;

.field w:Z

.field protected final x:[F

.field protected final y:[F

.field protected final z:Lcom/badlogic/gdx/i$a;


# direct methods
.method public constructor <init>(Lcom/badlogic/gdx/a;Landroid/content/Context;Ljava/lang/Object;Lcom/badlogic/gdx/backends/android/c;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/backends/android/l$1;

    const/16 v1, 0x3e8

    const/16 v2, 0x10

    invoke-direct {v0, p0, v2, v1}, Lcom/badlogic/gdx/backends/android/l$1;-><init>(Lcom/badlogic/gdx/backends/android/l;II)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    new-instance v0, Lcom/badlogic/gdx/backends/android/l$2;

    invoke-direct {v0, p0, v2, v1}, Lcom/badlogic/gdx/backends/android/l$2;-><init>(Lcom/badlogic/gdx/backends/android/l;II)V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->b:Lcom/badlogic/gdx/utils/k;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    const/16 v0, 0x14

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->f:[I

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->g:[I

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->h:[I

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->i:[I

    new-array v1, v0, [Z

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->k:[I

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->m:[F

    const/4 v0, 0x0

    iput v0, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    const/16 v1, 0x104

    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->F:Z

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->G:[Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->o:Z

    const/4 v1, 0x3

    new-array v2, v1, [F

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->p:[F

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->q:Z

    new-array v2, v1, [F

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->r:[F

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->I:Ljava/lang/String;

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->J:Lcom/badlogic/gdx/i$b;

    iput v0, p0, Lcom/badlogic/gdx/backends/android/l;->L:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->M:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->N:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->O:Z

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->P:Z

    new-array v2, v1, [F

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->x:[F

    new-array v2, v1, [F

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->y:[F

    const/4 v2, 0x0

    iput v2, p0, Lcom/badlogic/gdx/backends/android/l;->Q:F

    iput v2, p0, Lcom/badlogic/gdx/backends/android/l;->R:F

    iput v2, p0, Lcom/badlogic/gdx/backends/android/l;->S:F

    iput v2, p0, Lcom/badlogic/gdx/backends/android/l;->T:F

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->U:Z

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/badlogic/gdx/backends/android/l;->X:J

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/l;->A:Z

    const/16 v3, 0x9

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->B:[F

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->C:[F

    instance-of v1, p3, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast p3, Landroid/view/View;

    invoke-virtual {p3, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-virtual {p3, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p3, v2}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {p3, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p3}, Landroid/view/View;->requestFocus()Z

    :cond_0
    iput-object p4, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    new-instance p3, Lcom/badlogic/gdx/backends/android/s;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {p3, p2, v1, p0}, Lcom/badlogic/gdx/backends/android/s;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/badlogic/gdx/backends/android/l;)V

    iput-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->Y:Lcom/badlogic/gdx/backends/android/s;

    :goto_0
    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    array-length p3, p3

    if-ge v0, p3, :cond_1

    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    const/4 v1, -0x1

    aput v1, p3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance p3, Landroid/os/Handler;

    invoke-direct {p3}, Landroid/os/Handler;-><init>()V

    iput-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->K:Landroid/os/Handler;

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->s:Lcom/badlogic/gdx/a;

    iput-object p2, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    iget p1, p4, Lcom/badlogic/gdx/backends/android/c;->m:I

    iput p1, p0, Lcom/badlogic/gdx/backends/android/l;->L:I

    new-instance p1, Lcom/badlogic/gdx/backends/android/p;

    invoke-direct {p1}, Lcom/badlogic/gdx/backends/android/p;-><init>()V

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->u:Lcom/badlogic/gdx/backends/android/t;

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->u:Lcom/badlogic/gdx/backends/android/t;

    invoke-interface {p1, p2}, Lcom/badlogic/gdx/backends/android/t;->a(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/l;->n:Z

    const-string p1, "vibrator"

    invoke-virtual {p2, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Vibrator;

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->v:Landroid/os/Vibrator;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/l;->e()I

    move-result p1

    iget-object p2, p0, Lcom/badlogic/gdx/backends/android/l;->s:Lcom/badlogic/gdx/a;

    invoke-interface {p2}, Lcom/badlogic/gdx/a;->b()Lcom/badlogic/gdx/h;

    move-result-object p2

    invoke-interface {p2}, Lcom/badlogic/gdx/h;->e()Lcom/badlogic/gdx/h$b;

    move-result-object p2

    if-eqz p1, :cond_2

    const/16 p3, 0xb4

    if-ne p1, p3, :cond_3

    :cond_2
    iget p3, p2, Lcom/badlogic/gdx/h$b;->a:I

    iget p4, p2, Lcom/badlogic/gdx/h$b;->b:I

    if-ge p3, p4, :cond_6

    :cond_3
    const/16 p3, 0x5a

    if-eq p1, p3, :cond_4

    const/16 p3, 0x10e

    if-ne p1, p3, :cond_5

    :cond_4
    iget p1, p2, Lcom/badlogic/gdx/h$b;->a:I

    iget p2, p2, Lcom/badlogic/gdx/h$b;->b:I

    if-gt p1, p2, :cond_5

    goto :goto_1

    :cond_5
    sget-object p1, Lcom/badlogic/gdx/i$a;->b:Lcom/badlogic/gdx/i$a;

    goto :goto_2

    :cond_6
    :goto_1
    sget-object p1, Lcom/badlogic/gdx/i$a;->a:Lcom/badlogic/gdx/i$a;

    :goto_2
    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->z:Lcom/badlogic/gdx/i$a;

    return-void
.end method

.method private a([I)[I
    .locals 3

    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method private a([Z)[Z
    .locals 3

    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Z

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->f:[I

    aget p1, v0, p1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method a()V
    .locals 10

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->U:Z

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->F:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->F:Z

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->G:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->G:[Z

    aput-boolean v0, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->V:Lcom/badlogic/gdx/k;

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->V:Lcom/badlogic/gdx/k;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_1

    iget-object v5, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/badlogic/gdx/backends/android/l$a;

    iget-wide v6, v5, Lcom/badlogic/gdx/backends/android/l$a;->a:J

    iput-wide v6, p0, Lcom/badlogic/gdx/backends/android/l;->X:J

    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$a;->b:I

    packed-switch v6, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    iget-char v6, v5, Lcom/badlogic/gdx/backends/android/l$a;->d:C

    invoke-interface {v1, v6}, Lcom/badlogic/gdx/k;->a(C)Z

    goto :goto_2

    :pswitch_1
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    invoke-interface {v1, v6}, Lcom/badlogic/gdx/k;->b(I)Z

    goto :goto_2

    :pswitch_2
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    invoke-interface {v1, v6}, Lcom/badlogic/gdx/k;->a(I)Z

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/l;->F:Z

    iget-object v6, p0, Lcom/badlogic/gdx/backends/android/l;->G:[Z

    iget v7, v5, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    aput-boolean v2, v6, v7

    :goto_2
    iget-object v6, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v6, v5}, Lcom/badlogic/gdx/utils/k;->a(Ljava/lang/Object;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v3, :cond_5

    iget-object v5, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/badlogic/gdx/backends/android/l$c;

    iget-wide v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->a:J

    iput-wide v6, p0, Lcom/badlogic/gdx/backends/android/l;->X:J

    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->b:I

    packed-switch v6, :pswitch_data_1

    goto :goto_4

    :pswitch_3
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->c:I

    iget v7, v5, Lcom/badlogic/gdx/backends/android/l$c;->d:I

    invoke-interface {v1, v6, v7}, Lcom/badlogic/gdx/k;->a(II)Z

    goto :goto_4

    :pswitch_4
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->e:I

    invoke-interface {v1, v6}, Lcom/badlogic/gdx/k;->c(I)Z

    goto :goto_4

    :pswitch_5
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->c:I

    iget v7, v5, Lcom/badlogic/gdx/backends/android/l$c;->d:I

    iget v8, v5, Lcom/badlogic/gdx/backends/android/l$c;->g:I

    invoke-interface {v1, v6, v7, v8}, Lcom/badlogic/gdx/k;->a(III)Z

    goto :goto_4

    :pswitch_6
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->c:I

    iget v7, v5, Lcom/badlogic/gdx/backends/android/l$c;->d:I

    iget v8, v5, Lcom/badlogic/gdx/backends/android/l$c;->g:I

    iget v9, v5, Lcom/badlogic/gdx/backends/android/l$c;->f:I

    invoke-interface {v1, v6, v7, v8, v9}, Lcom/badlogic/gdx/k;->b(IIII)Z

    goto :goto_4

    :pswitch_7
    iget v6, v5, Lcom/badlogic/gdx/backends/android/l$c;->c:I

    iget v7, v5, Lcom/badlogic/gdx/backends/android/l$c;->d:I

    iget v8, v5, Lcom/badlogic/gdx/backends/android/l$c;->g:I

    iget v9, v5, Lcom/badlogic/gdx/backends/android/l$c;->f:I

    invoke-interface {v1, v6, v7, v8, v9}, Lcom/badlogic/gdx/k;->a(IIII)Z

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/l;->U:Z

    :goto_4
    iget-object v6, p0, Lcom/badlogic/gdx/backends/android/l;->b:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v6, v5}, Lcom/badlogic/gdx/utils/k;->a(Ljava/lang/Object;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v1, :cond_4

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/badlogic/gdx/backends/android/l$c;

    iget v5, v4, Lcom/badlogic/gdx/backends/android/l$c;->b:I

    if-nez v5, :cond_3

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/l;->U:Z

    :cond_3
    iget-object v5, p0, Lcom/badlogic/gdx/backends/android/l;->b:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v5, v4}, Lcom/badlogic/gdx/utils/k;->a(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_4
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v1, :cond_5

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/badlogic/gdx/utils/k;->a(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_5
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    :goto_7
    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->h:[I

    array-length v2, v2

    if-ge v1, v2, :cond_6

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->h:[I

    aput v0, v2, v0

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->i:[I

    aput v0, v2, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_6
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public b(I)I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->g:[I

    aget p1, v0, p1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method b()V
    .locals 6

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v0, v0, Lcom/badlogic/gdx/backends/android/c;->h:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    new-instance v2, Lcom/badlogic/gdx/backends/android/l$b;

    invoke-direct {v2, p0}, Lcom/badlogic/gdx/backends/android/l$b;-><init>(Lcom/badlogic/gdx/backends/android/l;)V

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->Z:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->Z:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget v4, v4, Lcom/badlogic/gdx/backends/android/c;->l:I

    invoke-virtual {v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->o:Z

    goto :goto_1

    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->o:Z

    :goto_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v0, v0, Lcom/badlogic/gdx/backends/android/c;->i:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    new-instance v2, Lcom/badlogic/gdx/backends/android/l$b;

    invoke-direct {v2, p0}, Lcom/badlogic/gdx/backends/android/l$b;-><init>(Lcom/badlogic/gdx/backends/android/l;)V

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->aa:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->aa:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget v4, v4, Lcom/badlogic/gdx/backends/android/c;->l:I

    invoke-virtual {v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->q:Z

    goto :goto_3

    :cond_3
    :goto_2
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->q:Z

    :goto_3
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->P:Z

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v0, v0, Lcom/badlogic/gdx/backends/android/c;->k:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    :cond_4
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v2, Lcom/badlogic/gdx/backends/android/l$b;

    invoke-direct {v2, p0}, Lcom/badlogic/gdx/backends/android/l$b;-><init>(Lcom/badlogic/gdx/backends/android/l;)V

    iput-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->ac:Landroid/hardware/SensorEventListener;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Google Inc."

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getVersion()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_5

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->ac:Landroid/hardware/SensorEventListener;

    iget-object v5, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget v5, v5, Lcom/badlogic/gdx/backends/android/c;->l:I

    invoke-virtual {v2, v4, v3, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/l;->P:Z

    :cond_6
    iget-boolean v2, p0, Lcom/badlogic/gdx/backends/android/l;->P:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->ac:Landroid/hardware/SensorEventListener;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget v4, v4, Lcom/badlogic/gdx/backends/android/c;->l:I

    invoke-virtual {v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->P:Z

    :cond_7
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget-boolean v0, v0, Lcom/badlogic/gdx/backends/android/c;->j:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->P:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    :cond_8
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->o:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->O:Z

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->O:Z

    if-eqz v1, :cond_a

    new-instance v1, Lcom/badlogic/gdx/backends/android/l$b;

    invoke-direct {v1, p0}, Lcom/badlogic/gdx/backends/android/l$b;-><init>(Lcom/badlogic/gdx/backends/android/l;)V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->ab:Landroid/hardware/SensorEventListener;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->ab:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->W:Lcom/badlogic/gdx/backends/android/c;

    iget v3, v3, Lcom/badlogic/gdx/backends/android/c;->l:I

    invoke-virtual {v1, v2, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->O:Z

    goto :goto_4

    :cond_9
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/l;->O:Z

    :cond_a
    :goto_4
    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidInput"

    const-string v2, "sensor listener setup"

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method c()V
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->Z:Landroid/hardware/SensorEventListener;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->Z:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->Z:Landroid/hardware/SensorEventListener;

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->aa:Landroid/hardware/SensorEventListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->aa:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->aa:Landroid/hardware/SensorEventListener;

    :cond_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->ac:Landroid/hardware/SensorEventListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->ac:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->ac:Landroid/hardware/SensorEventListener;

    :cond_2
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->ab:Landroid/hardware/SensorEventListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->ab:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->ab:Landroid/hardware/SensorEventListener;

    :cond_3
    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->H:Landroid/hardware/SensorManager;

    :cond_4
    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidInput"

    const-string v2, "sensor listener tear down"

    invoke-interface {v0, v1, v2}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public c(I)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    aget-boolean p1, v0, p1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public d()I
    .locals 4

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    aget v2, v2, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->f:[I

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->f:[I

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->g:[I

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->g:[I

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->h:[I

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->h:[I

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->i:[I

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->i:[I

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([Z)[Z

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->k:[I

    invoke-direct {p0, v1}, Lcom/badlogic/gdx/backends/android/l;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/badlogic/gdx/backends/android/l;->k:[I

    return v0
.end method

.method public declared-synchronized d(I)Z
    .locals 2

    monitor-enter p0

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    :try_start_0
    iget p1, p0, Lcom/badlogic/gdx/backends/android/l;->D:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    goto :goto_0

    :cond_1
    if-ltz p1, :cond_3

    const/16 v0, 0x104

    if-lt p1, v0, :cond_2

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    aget-boolean p1, v0, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return p1

    :goto_0
    monitor-exit p0

    throw p1

    :cond_3
    :goto_1
    monitor-exit p0

    return v1
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->t:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    goto :goto_0

    :goto_1
    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    const/16 v0, 0x10e

    return v0

    :pswitch_1
    const/16 v0, 0xb4

    return v0

    :pswitch_2
    const/16 v0, 0x5a

    return v0

    :pswitch_3
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized e(I)Z
    .locals 1

    monitor-enter p0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    :try_start_0
    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/l;->F:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :cond_0
    if-ltz p1, :cond_2

    const/16 v0, 0x104

    if-lt p1, v0, :cond_1

    goto :goto_1

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->G:[Z

    aget-boolean p1, v0, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return p1

    :goto_0
    monitor-exit p0

    throw p1

    :cond_2
    :goto_1
    const/4 p1, 0x0

    monitor-exit p0

    return p1
.end method

.method public f(I)I
    .locals 5

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    aget v3, v3, v2

    if-ne v3, p1, :cond_0

    return v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    if-ge v1, v0, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    const-string v1, "AndroidInput"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Pointer ID lookup failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/badlogic/gdx/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, -0x1

    return p1
.end method

.method public f()V
    .locals 2

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/l;->c()V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->l:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    return-void
.end method

.method public g()V
    .locals 0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/l;->b()V

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/l;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_1

    iget-object v4, p0, Lcom/badlogic/gdx/backends/android/l;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View$OnKeyListener;

    invoke-interface {v4, p1, p2, p3}, Landroid/view/View$OnKeyListener;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    return v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result p1

    const/4 v0, 0x2

    if-nez p1, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p3

    if-ge p2, p3, :cond_2

    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {p3}, Lcom/badlogic/gdx/utils/k;->c()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/badlogic/gdx/backends/android/l$a;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p3, Lcom/badlogic/gdx/backends/android/l$a;->a:J

    iput v1, p3, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, p3, Lcom/badlogic/gdx/backends/android/l$a;->d:C

    iput v0, p3, Lcom/badlogic/gdx/backends/android/l$a;->b:I

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_2
    monitor-exit p0

    return v1

    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result p1

    int-to-char p1, p1

    const/16 v2, 0x43

    if-ne p2, v2, :cond_4

    const/16 p1, 0x8

    :cond_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ltz v2, :cond_d

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v4, 0x104

    if-lt v2, v4, :cond_5

    goto/16 :goto_3

    :cond_5
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v4, 0x4

    const/16 v5, 0xff

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_2

    :pswitch_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v2}, Lcom/badlogic/gdx/utils/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/backends/android/l$a;

    iput-wide v6, v2, Lcom/badlogic/gdx/backends/android/l$a;->a:J

    iput-char v1, v2, Lcom/badlogic/gdx/backends/android/l$a;->d:C

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    iput v8, v2, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    iput v3, v2, Lcom/badlogic/gdx/backends/android/l$a;->b:I

    if-ne p2, v4, :cond_6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v8

    if-eqz v8, :cond_6

    iput v5, v2, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    const/16 p2, 0xff

    :cond_6
    iget-object v8, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v2}, Lcom/badlogic/gdx/utils/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/backends/android/l$a;

    iput-wide v6, v2, Lcom/badlogic/gdx/backends/android/l$a;->a:J

    iput-char p1, v2, Lcom/badlogic/gdx/backends/android/l$a;->d:C

    iput v1, v2, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    iput v0, v2, Lcom/badlogic/gdx/backends/android/l$a;->b:I

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne p2, v5, :cond_7

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    aget-boolean p1, p1, v5

    if-eqz p1, :cond_9

    iget p1, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    sub-int/2addr p1, v3

    iput p1, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    aput-boolean v1, p1, v5

    goto :goto_2

    :cond_7
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    aget-boolean p1, p1, v0

    if-eqz p1, :cond_9

    iget p1, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    sub-int/2addr p1, v3

    iput p1, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result p3

    aput-boolean v1, p1, p3

    goto :goto_2

    :pswitch_1
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->a:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {p1}, Lcom/badlogic/gdx/utils/k;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/badlogic/gdx/backends/android/l$a;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    iput-wide v6, p1, Lcom/badlogic/gdx/backends/android/l$a;->a:J

    iput-char v1, p1, Lcom/badlogic/gdx/backends/android/l$a;->d:C

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    iput v0, p1, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    iput v1, p1, Lcom/badlogic/gdx/backends/android/l$a;->b:I

    if-ne p2, v4, :cond_8

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result p3

    if-eqz p3, :cond_8

    iput v5, p1, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    const/16 p2, 0xff

    :cond_8
    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->d:Ljava/util/ArrayList;

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    iget v0, p1, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    aget-boolean p3, p3, v0

    if-nez p3, :cond_9

    iget p3, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    add-int/2addr p3, v3

    iput p3, p0, Lcom/badlogic/gdx/backends/android/l;->D:I

    iget-object p3, p0, Lcom/badlogic/gdx/backends/android/l;->E:[Z

    iget p1, p1, Lcom/badlogic/gdx/backends/android/l$a;->c:I

    aput-boolean v3, p3, p1

    :cond_9
    :goto_2
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->s:Lcom/badlogic/gdx/a;

    invoke-interface {p1}, Lcom/badlogic/gdx/a;->b()Lcom/badlogic/gdx/h;

    move-result-object p1

    invoke-interface {p1}, Lcom/badlogic/gdx/h;->g()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p2, v5, :cond_a

    return v3

    :cond_a
    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/l;->M:Z

    if-eqz p1, :cond_b

    if-ne p2, v4, :cond_b

    return v3

    :cond_b
    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/l;->N:Z

    if-eqz p1, :cond_c

    const/16 p1, 0x52

    if-ne p2, p1, :cond_c

    return v3

    :cond_c
    return v1

    :cond_d
    :goto_3
    :try_start_1
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/l;->A:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/l;->A:Z

    :cond_0
    iget-object p1, p0, Lcom/badlogic/gdx/backends/android/l;->u:Lcom/badlogic/gdx/backends/android/t;

    invoke-interface {p1, p2, p0}, Lcom/badlogic/gdx/backends/android/t;->a(Landroid/view/MotionEvent;Lcom/badlogic/gdx/backends/android/l;)V

    iget p1, p0, Lcom/badlogic/gdx/backends/android/l;->L:I

    if-eqz p1, :cond_1

    :try_start_0
    iget p1, p0, Lcom/badlogic/gdx/backends/android/l;->L:I

    int-to-long p1, p1

    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return v1
.end method
