.class public Lcom/badlogic/gdx/backends/android/AndroidGL20;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/graphics/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native init()V
.end method


# virtual methods
.method public native glAttachShader(II)V
.end method

.method public native glBindBuffer(II)V
.end method

.method public native glBindFramebuffer(II)V
.end method

.method public native glBindRenderbuffer(II)V
.end method

.method public native glBindTexture(II)V
.end method

.method public native glBlendFuncSeparate(IIII)V
.end method

.method public native glBufferData(IILjava/nio/Buffer;I)V
.end method

.method public native glBufferSubData(IIILjava/nio/Buffer;)V
.end method

.method public native glCheckFramebufferStatus(I)I
.end method

.method public native glClear(I)V
.end method

.method public native glClearColor(FFFF)V
.end method

.method public native glCompileShader(I)V
.end method

.method public native glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V
.end method

.method public native glCreateProgram()I
.end method

.method public native glCreateShader(I)I
.end method

.method public native glDeleteBuffer(I)V
.end method

.method public native glDeleteFramebuffer(I)V
.end method

.method public native glDeleteProgram(I)V
.end method

.method public native glDeleteRenderbuffer(I)V
.end method

.method public native glDeleteShader(I)V
.end method

.method public native glDeleteTexture(I)V
.end method

.method public native glDepthMask(Z)V
.end method

.method public native glDisable(I)V
.end method

.method public native glDisableVertexAttribArray(I)V
.end method

.method public native glDrawArrays(III)V
.end method

.method public native glDrawElements(IIII)V
.end method

.method public native glDrawElements(IIILjava/nio/Buffer;)V
.end method

.method public native glEnable(I)V
.end method

.method public native glEnableVertexAttribArray(I)V
.end method

.method public native glFramebufferRenderbuffer(IIII)V
.end method

.method public native glFramebufferTexture2D(IIIII)V
.end method

.method public native glGenBuffer()I
.end method

.method public native glGenFramebuffer()I
.end method

.method public native glGenRenderbuffer()I
.end method

.method public native glGenTexture()I
.end method

.method public native glGenerateMipmap(I)V
.end method

.method public native glGetActiveAttrib(IILjava/nio/IntBuffer;Ljava/nio/Buffer;)Ljava/lang/String;
.end method

.method public native glGetActiveUniform(IILjava/nio/IntBuffer;Ljava/nio/Buffer;)Ljava/lang/String;
.end method

.method public native glGetAttribLocation(ILjava/lang/String;)I
.end method

.method public native glGetIntegerv(ILjava/nio/IntBuffer;)V
.end method

.method public native glGetProgramInfoLog(I)Ljava/lang/String;
.end method

.method public native glGetProgramiv(IILjava/nio/IntBuffer;)V
.end method

.method public native glGetShaderInfoLog(I)Ljava/lang/String;
.end method

.method public native glGetShaderiv(IILjava/nio/IntBuffer;)V
.end method

.method public native glGetString(I)Ljava/lang/String;
.end method

.method public native glGetUniformLocation(ILjava/lang/String;)I
.end method

.method public native glLinkProgram(I)V
.end method

.method public native glPixelStorei(II)V
.end method

.method public native glRenderbufferStorage(IIII)V
.end method

.method public native glShaderSource(ILjava/lang/String;)V
.end method

.method public native glTexImage2D(IIIIIIIILjava/nio/Buffer;)V
.end method

.method public native glTexParameteri(III)V
.end method

.method public native glUniform1i(II)V
.end method

.method public native glUniformMatrix4fv(IIZ[FI)V
.end method

.method public native glUseProgram(I)V
.end method

.method public native glVertexAttribPointer(IIIZII)V
.end method

.method public native glVertexAttribPointer(IIIZILjava/nio/Buffer;)V
.end method
