.class Lcom/badlogic/gdx/backends/android/a/d$h;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/badlogic/gdx/backends/android/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "h"
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Lcom/badlogic/gdx/backends/android/a/d$g;

.field private t:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/badlogic/gdx/backends/android/a/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/badlogic/gdx/backends/android/a/d;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->q:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->r:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->l:I

    iput v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->m:I

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->o:Z

    iput v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->n:I

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->t:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic a(Lcom/badlogic/gdx/backends/android/a/d$h;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z

    return p1
.end method

.method private j()V
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->e()V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->f()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/badlogic/gdx/backends/android/a/d$i;->c(Lcom/badlogic/gdx/backends/android/a/d$h;)V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 16

    move-object/from16 v1, p0

    new-instance v0, Lcom/badlogic/gdx/backends/android/a/d$g;

    iget-object v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->t:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Lcom/badlogic/gdx/backends/android/a/d$g;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v15

    monitor-enter v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    :goto_1
    :try_start_1
    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->a:Z

    if-eqz v2, :cond_0

    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v2

    monitor-enter v2

    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->j()V

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->k()V

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    iget-object v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->q:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    move-object v9, v2

    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_1
    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->d:Z

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->c:Z

    if-eq v2, v0, :cond_2

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->c:Z

    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->c:Z

    iput-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->d:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->k:Z

    if-eqz v2, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->j()V

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->k()V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->k:Z

    const/4 v4, 0x1

    :cond_3
    if-eqz v5, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->j()V

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->k()V

    const/4 v5, 0x0

    :cond_4
    if-eqz v0, :cond_5

    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    if-eqz v2, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->j()V

    :cond_5
    if-eqz v0, :cond_8

    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    if-eqz v2, :cond_8

    iget-object v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/badlogic/gdx/backends/android/a/d;

    if-nez v2, :cond_6

    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    invoke-static {v2}, Lcom/badlogic/gdx/backends/android/a/d;->g(Lcom/badlogic/gdx/backends/android/a/d;)Z

    move-result v2

    :goto_3
    if-eqz v2, :cond_7

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/badlogic/gdx/backends/android/a/d$i;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->k()V

    :cond_8
    if-eqz v0, :cond_9

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$i;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->f()V

    :cond_9
    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->e:Z

    if-nez v0, :cond_b

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->g:Z

    if-nez v0, :cond_b

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    if-eqz v0, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->j()V

    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->g:Z

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->f:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_b
    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->e:Z

    if-eqz v0, :cond_c

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->g:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->g:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_c
    if-eqz v3, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->p:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    const/4 v3, 0x0

    const/4 v14, 0x0

    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->m()Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    if-nez v0, :cond_f

    if-eqz v4, :cond_e

    const/4 v4, 0x0

    goto :goto_4

    :cond_e
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/backends/android/a/d$i;->b(Lcom/badlogic/gdx/backends/android/a/d$h;)Z

    move-result v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    if-eqz v0, :cond_f

    :try_start_4
    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->a()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    const/4 v0, 0x1

    :try_start_5
    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    const/4 v10, 0x1

    goto :goto_4

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/badlogic/gdx/backends/android/a/d$i;->c(Lcom/badlogic/gdx/backends/android/a/d$h;)V

    throw v0

    :cond_f
    :goto_4
    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    if-eqz v0, :cond_10

    iget-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    if-nez v0, :cond_10

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    const/4 v0, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x1

    goto :goto_5

    :cond_10
    move v0, v11

    :goto_5
    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    if-eqz v2, :cond_1e

    iget-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->r:Z

    if-eqz v2, :cond_11

    iget v7, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->l:I

    iget v8, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->m:I

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->r:Z

    const/4 v0, 0x1

    const/4 v13, 0x1

    const/4 v14, 0x1

    goto :goto_6

    :cond_11
    const/4 v2, 0x0

    :goto_6
    iput-boolean v2, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->o:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->notifyAll()V

    move v11, v0

    :goto_7
    monitor-exit v15
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    if-eqz v9, :cond_12

    :try_start_6
    invoke-interface {v9}, Ljava/lang/Runnable;->run()V

    const/4 v0, 0x0

    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_12
    if-eqz v11, :cond_15

    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->b()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v11

    monitor-enter v11
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    const/4 v0, 0x1

    :try_start_7
    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->j:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v11

    const/4 v11, 0x0

    goto :goto_9

    :catchall_1
    move-exception v0

    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0

    :cond_13
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v15

    monitor-enter v15
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    const/4 v0, 0x1

    :try_start_9
    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->j:Z

    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->f:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v15

    :cond_14
    :goto_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v15
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v0

    :cond_15
    :goto_9
    if-eqz v12, :cond_16

    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->c()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/badlogic/gdx/backends/android/a/d$i;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object v6, v0

    const/4 v12, 0x0

    :cond_16
    if-eqz v10, :cond_18

    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_17

    invoke-static {v0}, Lcom/badlogic/gdx/backends/android/a/d;->h(Lcom/badlogic/gdx/backends/android/a/d;)Landroid/opengl/GLSurfaceView$Renderer;

    move-result-object v0

    iget-object v10, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    iget-object v10, v10, Lcom/badlogic/gdx/backends/android/a/d$g;->d:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v0, v6, v10}, Landroid/opengl/GLSurfaceView$Renderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    :cond_17
    const/4 v10, 0x0

    :cond_18
    if-eqz v13, :cond_1a

    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_19

    invoke-static {v0}, Lcom/badlogic/gdx/backends/android/a/d;->h(Lcom/badlogic/gdx/backends/android/a/d;)Landroid/opengl/GLSurfaceView$Renderer;

    move-result-object v0

    invoke-interface {v0, v6, v7, v8}, Landroid/opengl/GLSurfaceView$Renderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    :cond_19
    const/4 v13, 0x0

    :cond_1a
    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/backends/android/a/d;

    if-eqz v0, :cond_1b

    invoke-static {v0}, Lcom/badlogic/gdx/backends/android/a/d;->h(Lcom/badlogic/gdx/backends/android/a/d;)Landroid/opengl/GLSurfaceView$Renderer;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/opengl/GLSurfaceView$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_1b
    iget-object v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->s:Lcom/badlogic/gdx/backends/android/a/d$g;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->d()I

    move-result v0

    const/16 v15, 0x3000

    if-eq v0, v15, :cond_1d

    const/16 v15, 0x300e

    if-eq v0, v15, :cond_1c

    const-string v15, "GLThread"

    const-string v2, "eglSwapBuffers"

    invoke-static {v15, v2, v0}, Lcom/badlogic/gdx/backends/android/a/d$g;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v2

    monitor-enter v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    const/4 v0, 0x1

    :try_start_b
    iput-boolean v0, v1, Lcom/badlogic/gdx/backends/android/a/d$h;->f:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2

    goto :goto_a

    :catchall_3
    move-exception v0

    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :cond_1c
    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_a

    :cond_1d
    const/4 v0, 0x1

    :goto_a
    if-eqz v14, :cond_14

    const/4 v3, 0x1

    goto/16 :goto_8

    :cond_1e
    move v11, v0

    :cond_1f
    :try_start_d
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    const/4 v0, 0x0

    goto/16 :goto_1

    :catchall_4
    move-exception v0

    monitor-exit v15
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :try_start_e
    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    :catchall_5
    move-exception v0

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v2

    monitor-enter v2

    :try_start_f
    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->j()V

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->k()V

    monitor-exit v2
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v0

    :catchall_6
    move-exception v0

    :try_start_10
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    throw v0
.end method

.method private m()Z
    .locals 2

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->d:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->f:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->l:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->m:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->o:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->n:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method


# virtual methods
.method public a(I)V
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iput p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->n:I

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "renderMode"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(II)V
    .locals 1

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iput p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->l:I

    iput p2, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->m:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->r:Z

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->o:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->p:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z

    if-nez p1, :cond_0

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->d:Z

    if-nez p1, :cond_0

    iget-boolean p1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->p:Z

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->a()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->i:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b()I
    .locals 2

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->n:I

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public c()V
    .locals 2

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->o:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->e:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->j:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->g:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->j:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public e()V
    .locals 2

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->e:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->g:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->c:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public g()V
    .locals 3

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->c:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->o:Z

    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->p:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->d:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public h()V
    .locals 2

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->a:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v1, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/badlogic/gdx/backends/android/a/d$h;->k:Z

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    return-void
.end method

.method public run()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GLThread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/badlogic/gdx/backends/android/a/d$h;->setName(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a/d$h;->l()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/badlogic/gdx/backends/android/a/d$i;->a(Lcom/badlogic/gdx/backends/android/a/d$h;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/badlogic/gdx/backends/android/a/d;->d()Lcom/badlogic/gdx/backends/android/a/d$i;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/badlogic/gdx/backends/android/a/d$i;->a(Lcom/badlogic/gdx/backends/android/a/d$h;)V

    throw v0

    :goto_0
    return-void
.end method
