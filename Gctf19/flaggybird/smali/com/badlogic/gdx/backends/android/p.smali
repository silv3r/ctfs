.class public Lcom/badlogic/gdx/backends/android/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/backends/android/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)I
    .locals 2

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    return v0

    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    return v1

    :cond_2
    const/16 v1, 0x8

    if-ne p1, v1, :cond_3

    const/4 p1, 0x3

    return p1

    :cond_3
    const/16 v1, 0x10

    if-ne p1, v1, :cond_4

    return v0

    :cond_4
    const/4 p1, -0x1

    return p1

    :cond_5
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method private a(Lcom/badlogic/gdx/backends/android/l;IIIIIJ)V
    .locals 1

    iget-object v0, p1, Lcom/badlogic/gdx/backends/android/l;->b:Lcom/badlogic/gdx/utils/k;

    invoke-virtual {v0}, Lcom/badlogic/gdx/utils/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/badlogic/gdx/backends/android/l$c;

    iput-wide p7, v0, Lcom/badlogic/gdx/backends/android/l$c;->a:J

    iput p5, v0, Lcom/badlogic/gdx/backends/android/l$c;->g:I

    iput p3, v0, Lcom/badlogic/gdx/backends/android/l$c;->c:I

    iput p4, v0, Lcom/badlogic/gdx/backends/android/l$c;->d:I

    iput p2, v0, Lcom/badlogic/gdx/backends/android/l$c;->b:I

    iput p6, v0, Lcom/badlogic/gdx/backends/android/l$c;->f:I

    iget-object p1, p1, Lcom/badlogic/gdx/backends/android/l;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;Lcom/badlogic/gdx/backends/android/l;)V
    .locals 20

    move-object/from16 v0, p1

    move-object/from16 v10, p2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const v3, 0xff00

    and-int/2addr v2, v3

    shr-int/lit8 v11, v2, 0x8

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    monitor-enter p2

    const/4 v9, 0x0

    const/16 v14, 0x14

    const/4 v15, -0x1

    const/16 v16, 0x0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_7

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->l:[I

    array-length v1, v1

    if-ge v0, v1, :cond_a

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->l:[I

    aput v15, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->f:[I

    aput v16, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->g:[I

    aput v16, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->h:[I

    aput v16, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->i:[I

    aput v16, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    aput-boolean v16, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->k:[I

    aput v16, v1, v0

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->m:[F

    aput v9, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v11

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v11, :cond_a

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v9, v2

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v7, v2

    invoke-virtual {v10, v1}, Lcom/badlogic/gdx/backends/android/l;->f(I)I

    move-result v6

    if-ne v6, v15, :cond_0

    move v14, v8

    goto :goto_3

    :cond_0
    if-lt v6, v14, :cond_1

    goto/16 :goto_7

    :cond_1
    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->k:[I

    aget v5, v1, v6

    if-eq v5, v15, :cond_2

    const/4 v3, 0x2

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move v4, v9

    move/from16 v16, v5

    move v5, v7

    move/from16 v17, v6

    move/from16 v18, v7

    move/from16 v7, v16

    move v14, v8

    move/from16 v16, v9

    move-wide v8, v12

    invoke-direct/range {v1 .. v9}, Lcom/badlogic/gdx/backends/android/p;->a(Lcom/badlogic/gdx/backends/android/l;IIIIIJ)V

    goto :goto_2

    :cond_2
    move/from16 v17, v6

    move/from16 v18, v7

    move v14, v8

    move/from16 v16, v9

    const/4 v3, 0x4

    const/4 v7, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v4, v16

    move/from16 v5, v18

    move-wide v8, v12

    invoke-direct/range {v1 .. v9}, Lcom/badlogic/gdx/backends/android/p;->a(Lcom/badlogic/gdx/backends/android/l;IIIIIJ)V

    :goto_2
    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->h:[I

    iget-object v2, v10, Lcom/badlogic/gdx/backends/android/l;->f:[I

    aget v2, v2, v17

    sub-int v9, v16, v2

    aput v9, v1, v17

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->i:[I

    iget-object v2, v10, Lcom/badlogic/gdx/backends/android/l;->g:[I

    aget v2, v2, v17

    sub-int v7, v18, v2

    aput v7, v1, v17

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->f:[I

    aput v16, v1, v17

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->g:[I

    aput v18, v1, v17

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->m:[F

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v2

    aput v2, v1, v17

    :goto_3
    add-int/lit8 v8, v14, 0x1

    const/16 v14, 0x14

    goto :goto_1

    :pswitch_2
    invoke-virtual {v10, v2}, Lcom/badlogic/gdx/backends/android/l;->f(I)I

    move-result v14

    if-ne v14, v15, :cond_3

    goto/16 :goto_7

    :cond_3
    const/16 v1, 0x14

    if-lt v14, v1, :cond_4

    goto/16 :goto_7

    :cond_4
    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->l:[I

    aput v15, v1, v14

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v7, v1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v11, v0

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->k:[I

    aget v6, v0, v14

    if-eq v6, v15, :cond_5

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move v3, v7

    move v4, v11

    move v5, v14

    move v15, v7

    move-wide v7, v12

    invoke-direct/range {v0 .. v8}, Lcom/badlogic/gdx/backends/android/p;->a(Lcom/badlogic/gdx/backends/android/l;IIIIIJ)V

    goto :goto_4

    :cond_5
    move v15, v7

    :goto_4
    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->f:[I

    aput v15, v0, v14

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->g:[I

    aput v11, v0, v14

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->h:[I

    aput v16, v0, v14

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->i:[I

    aput v16, v0, v14

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    aput-boolean v16, v0, v14

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->k:[I

    aput v16, v0, v14

    iget-object v0, v10, Lcom/badlogic/gdx/backends/android/l;->m:[F

    aput v9, v0, v14

    goto/16 :goto_7

    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Lcom/badlogic/gdx/backends/android/l;->d()I

    move-result v14

    const/16 v1, 0x14

    if-lt v14, v1, :cond_6

    goto/16 :goto_7

    :cond_6
    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->l:[I

    aput v2, v1, v14

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v8, v1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v9, v1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v2, 0xe

    if-lt v1, v2, :cond_7

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v7, p0

    :try_start_2
    invoke-direct {v7, v1}, Lcom/badlogic/gdx/backends/android/p;->a(I)I

    move-result v1

    move v6, v1

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object/from16 v7, p0

    goto :goto_8

    :cond_7
    move-object/from16 v7, p0

    const/4 v6, 0x0

    :goto_5
    if-eq v6, v15, :cond_8

    const/4 v3, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move v4, v8

    move v5, v9

    move/from16 v19, v6

    move v6, v14

    move/from16 v7, v19

    move/from16 v17, v8

    move/from16 v18, v9

    move-wide v8, v12

    invoke-direct/range {v1 .. v9}, Lcom/badlogic/gdx/backends/android/p;->a(Lcom/badlogic/gdx/backends/android/l;IIIIIJ)V

    goto :goto_6

    :cond_8
    move/from16 v19, v6

    move/from16 v17, v8

    move/from16 v18, v9

    :goto_6
    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->f:[I

    aput v17, v1, v14

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->g:[I

    aput v18, v1, v14

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->h:[I

    aput v16, v1, v14

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->i:[I

    aput v16, v1, v14

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->j:[Z

    move/from16 v2, v19

    if-eq v2, v15, :cond_9

    const/16 v16, 0x1

    :cond_9
    aput-boolean v16, v1, v14

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->k:[I

    aput v2, v1, v14

    iget-object v1, v10, Lcom/badlogic/gdx/backends/android/l;->m:[F

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v0

    aput v0, v1, v14

    :cond_a
    :goto_7
    monitor-exit p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0}, Lcom/badlogic/gdx/a;->b()Lcom/badlogic/gdx/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/badlogic/gdx/h;->g()V

    return-void

    :catchall_1
    move-exception v0

    :goto_8
    :try_start_3
    monitor-exit p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/content/Context;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    const-string v0, "android.hardware.touchscreen.multitouch"

    invoke-virtual {p1, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
