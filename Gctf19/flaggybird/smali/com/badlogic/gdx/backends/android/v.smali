.class public Lcom/badlogic/gdx/backends/android/v;
.super Lcom/badlogic/gdx/backends/android/h;


# instance fields
.field private c:Landroid/content/res/AssetFileDescriptor;

.field private d:Lcom/badlogic/gdx/backends/android/w;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/f$a;->b:Lcom/badlogic/gdx/f$a;

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lcom/badlogic/gdx/backends/android/h;-><init>(Landroid/content/res/AssetManager;Ljava/lang/String;Lcom/badlogic/gdx/f$a;)V

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/v;->j()V

    return-void
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5c

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->e:Ljava/lang/String;

    sget-object v0, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    check-cast v0, Lcom/badlogic/gdx/backends/android/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/backends/android/i;->c()Lcom/badlogic/gdx/backends/android/w;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->d:Lcom/badlogic/gdx/backends/android/w;

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->d:Lcom/badlogic/gdx/backends/android/w;

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/v;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/backends/android/w;->b(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->c:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {p0}, Lcom/badlogic/gdx/backends/android/v;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/v;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->e:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->d:Lcom/badlogic/gdx/backends/android/w;

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/v;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/backends/android/w;->c(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/badlogic/gdx/utils/d;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/badlogic/gdx/backends/android/v;->a:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " (ZipResourceFile)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/badlogic/gdx/utils/d;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->c:Landroid/content/res/AssetFileDescriptor;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->c:Landroid/content/res/AssetFileDescriptor;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->d:Lcom/badlogic/gdx/backends/android/w;

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/v;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/badlogic/gdx/backends/android/w;->a(Ljava/lang/String;)[Lcom/badlogic/gdx/backends/android/w$a;

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->c:Landroid/content/res/AssetFileDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/v;->c:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method
