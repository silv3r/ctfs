.class public Lcom/badlogic/gdx/backends/android/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/badlogic/gdx/f;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Landroid/content/res/AssetManager;

.field private d:Lcom/badlogic/gdx/backends/android/w;


# direct methods
.method public constructor <init>(Landroid/content/res/AssetManager;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/i;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/badlogic/gdx/backends/android/i;->d:Lcom/badlogic/gdx/backends/android/w;

    iput-object p1, p0, Lcom/badlogic/gdx/backends/android/i;->c:Landroid/content/res/AssetManager;

    const-string p1, "/"

    invoke-virtual {p2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "/"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lcom/badlogic/gdx/backends/android/i;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/badlogic/gdx/c/a;Ljava/lang/String;)Lcom/badlogic/gdx/c/a;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/i;->c:Landroid/content/res/AssetManager;

    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-instance v0, Lcom/badlogic/gdx/backends/android/v;

    invoke-direct {v0, p2}, Lcom/badlogic/gdx/backends/android/v;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/badlogic/gdx/c/a;->b()Z

    move-result p2

    if-nez p2, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/badlogic/gdx/c/a;->c()Z

    move-result p2

    if-eqz p2, :cond_1

    return-object v0

    :cond_1
    return-object p1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;
    .locals 3

    new-instance v0, Lcom/badlogic/gdx/backends/android/h;

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/i;->c:Landroid/content/res/AssetManager;

    sget-object v2, Lcom/badlogic/gdx/f$a;->b:Lcom/badlogic/gdx/f$a;

    invoke-direct {v0, v1, p1, v2}, Lcom/badlogic/gdx/backends/android/h;-><init>(Landroid/content/res/AssetManager;Ljava/lang/String;Lcom/badlogic/gdx/f$a;)V

    iget-object v1, p0, Lcom/badlogic/gdx/backends/android/i;->d:Lcom/badlogic/gdx/backends/android/w;

    if-eqz v1, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/badlogic/gdx/backends/android/i;->a(Lcom/badlogic/gdx/c/a;Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/badlogic/gdx/backends/android/w;
    .locals 1

    iget-object v0, p0, Lcom/badlogic/gdx/backends/android/i;->d:Lcom/badlogic/gdx/backends/android/w;

    return-object v0
.end method
