.class public Lcom/google/ctf/game/AndroidLauncher;
.super Lcom/badlogic/gdx/backends/android/a;

# interfaces
.implements Lcom/google/ctf/game/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/badlogic/gdx/backends/android/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/google/ctf/game/AndroidLauncher$1;

    invoke-direct {v0, p0, p1}, Lcom/google/ctf/game/AndroidLauncher$1;-><init>(Lcom/google/ctf/game/AndroidLauncher;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/ctf/game/AndroidLauncher;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/badlogic/gdx/backends/android/a;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Lcom/badlogic/gdx/backends/android/c;

    invoke-direct {p1}, Lcom/badlogic/gdx/backends/android/c;-><init>()V

    new-instance v0, Lcom/google/ctf/game/e;

    invoke-direct {v0, p0}, Lcom/google/ctf/game/e;-><init>(Lcom/google/ctf/game/h;)V

    invoke-virtual {p0, v0, p1}, Lcom/google/ctf/game/AndroidLauncher;->a(Lcom/badlogic/gdx/c;Lcom/badlogic/gdx/backends/android/c;)V

    return-void
.end method
