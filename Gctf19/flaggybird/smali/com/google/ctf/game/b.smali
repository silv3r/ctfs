.class Lcom/google/ctf/game/b;
.super Lcom/google/ctf/game/c;


# instance fields
.field private g:Lcom/badlogic/gdx/graphics/g2d/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/badlogic/gdx/graphics/g2d/a<",
            "Lcom/badlogic/gdx/graphics/g2d/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:[Lcom/badlogic/gdx/graphics/g2d/d;


# direct methods
.method constructor <init>(Lcom/badlogic/gdx/math/f;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/google/ctf/game/c;-><init>(Lcom/badlogic/gdx/math/f;)V

    new-instance p1, Lcom/badlogic/gdx/graphics/l;

    sget-object v0, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    const-string v1, "bird.png"

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/f;->a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/c/a;)V

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v1

    div-int/lit8 v1, v1, 0x1

    new-instance v2, Lcom/badlogic/gdx/math/f;

    int-to-float v3, v0

    div-int/lit8 v4, v1, 0x2

    int-to-float v4, v4

    invoke-direct {v2, v3, v4}, Lcom/badlogic/gdx/math/f;-><init>(FF)V

    invoke-virtual {p0, v2}, Lcom/google/ctf/game/b;->a(Lcom/badlogic/gdx/math/f;)V

    invoke-static {p1, v0, v1}, Lcom/badlogic/gdx/graphics/g2d/d;->a(Lcom/badlogic/gdx/graphics/l;II)[[Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object p1

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/badlogic/gdx/graphics/g2d/d;

    iput-object v0, p0, Lcom/google/ctf/game/b;->h:[Lcom/badlogic/gdx/graphics/g2d/d;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    iget-object v1, p0, Lcom/google/ctf/game/b;->h:[Lcom/badlogic/gdx/graphics/g2d/d;

    iget-object v2, p0, Lcom/google/ctf/game/b;->h:[Lcom/badlogic/gdx/graphics/g2d/d;

    array-length v2, v2

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p1, Lcom/badlogic/gdx/graphics/g2d/a;

    iget-object v0, p0, Lcom/google/ctf/game/b;->h:[Lcom/badlogic/gdx/graphics/g2d/d;

    const v1, 0x3d99999a    # 0.075f

    invoke-direct {p1, v1, v0}, Lcom/badlogic/gdx/graphics/g2d/a;-><init>(F[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/ctf/game/b;->g:Lcom/badlogic/gdx/graphics/g2d/a;

    return-void
.end method


# virtual methods
.method a(Lcom/badlogic/gdx/graphics/g2d/b;F)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/ctf/game/b;->e:Z

    if-nez v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    iget-boolean v0, p0, Lcom/google/ctf/game/b;->f:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object p2, p0, Lcom/google/ctf/game/b;->h:[Lcom/badlogic/gdx/graphics/g2d/d;

    const/4 v0, 0x2

    aget-object p2, p2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/ctf/game/b;->g:Lcom/badlogic/gdx/graphics/g2d/a;

    invoke-virtual {v0, p2, v1}, Lcom/badlogic/gdx/graphics/g2d/a;->a(FZ)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/badlogic/gdx/graphics/g2d/d;

    :goto_0
    iget-boolean v0, p0, Lcom/google/ctf/game/b;->d:Z

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Lcom/badlogic/gdx/graphics/g2d/d;->a(ZZ)V

    iget-object v0, p0, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    iget v0, v0, Lcom/badlogic/gdx/math/f;->d:F

    iget-object v3, p0, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    iget v3, v3, Lcom/badlogic/gdx/math/f;->e:F

    invoke-interface {p1, p2, v0, v3}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/graphics/g2d/d;FF)V

    iget-boolean p1, p0, Lcom/google/ctf/game/b;->d:Z

    if-nez p1, :cond_2

    invoke-virtual {p2, v1, v2}, Lcom/badlogic/gdx/graphics/g2d/d;->a(ZZ)V

    :cond_2
    return-void
.end method
