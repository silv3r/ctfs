.class Lcom/google/ctf/game/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/google/ctf/game/f;

.field private b:Lcom/google/ctf/game/i;

.field private c:Lcom/google/ctf/game/b;


# direct methods
.method constructor <init>(Lcom/google/ctf/game/b;Lcom/google/ctf/game/f;Lcom/google/ctf/game/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iput-object p3, p0, Lcom/google/ctf/game/g;->b:Lcom/google/ctf/game/i;

    iput-object p1, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    return-void
.end method

.method private a(Lcom/google/ctf/game/c;)V
    .locals 11

    iget v0, p1, Lcom/google/ctf/game/c;->c:F

    const v1, 0x3dcccccd    # 0.9f

    add-float/2addr v0, v1

    iput v0, p1, Lcom/google/ctf/game/c;->c:F

    iget-object v0, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v1, v0, Lcom/badlogic/gdx/math/f;->e:F

    iget v2, p1, Lcom/google/ctf/game/c;->c:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/badlogic/gdx/math/f;->e:F

    iget-object v0, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v0, v0, Lcom/badlogic/gdx/math/f;->e:F

    iget-object v1, p1, Lcom/google/ctf/game/c;->b:Lcom/badlogic/gdx/math/f;

    iget v1, v1, Lcom/badlogic/gdx/math/f;->e:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v1, v1, Lcom/google/ctf/game/f;->f:I

    div-int/2addr v0, v1

    iget-object v1, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v1, v1, Lcom/badlogic/gdx/math/f;->e:F

    iget-object v2, p1, Lcom/google/ctf/game/c;->b:Lcom/badlogic/gdx/math/f;

    iget v2, v2, Lcom/badlogic/gdx/math/f;->e:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v2, v2, Lcom/google/ctf/game/f;->f:I

    div-int/2addr v1, v2

    iget-object v2, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v2, v2, Lcom/badlogic/gdx/math/f;->e:F

    float-to-int v2, v2

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v4, v4, Lcom/google/ctf/game/f;->f:I

    div-int/2addr v2, v4

    iget-object v4, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v4, v4, Lcom/badlogic/gdx/math/f;->d:F

    float-to-int v4, v4

    iget-object v5, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v5, v5, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v4, v5

    iget-object v5, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v5, v5, Lcom/badlogic/gdx/math/f;->d:F

    iget-object v6, p1, Lcom/google/ctf/game/c;->b:Lcom/badlogic/gdx/math/f;

    iget v6, v6, Lcom/badlogic/gdx/math/f;->d:F

    div-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v3, v5

    iget-object v5, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v5, v5, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v3, v5

    iget-object v5, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v5, v5, Lcom/badlogic/gdx/math/f;->d:F

    iget-object v6, p1, Lcom/google/ctf/game/c;->b:Lcom/badlogic/gdx/math/f;

    iget v6, v6, Lcom/badlogic/gdx/math/f;->d:F

    add-float/2addr v5, v6

    float-to-int v5, v5

    iget-object v6, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v6, v6, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v6, v6, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v6, v6, v2

    aget-object v6, v6, v4

    sget-object v7, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v6, v6, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v6, v6, v2

    aget-object v6, v6, v3

    sget-object v7, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-eq v6, v7, :cond_1

    :cond_0
    iget-object v6, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v6, v6, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v6, v6, v2

    aget-object v6, v6, v5

    sget-object v7, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v6, v6, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v6, v6, v2

    aget-object v6, v6, v3

    sget-object v7, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v6, v7, :cond_2

    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    iget-object v7, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v7, v7, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v7, v7, v0

    aget-object v7, v7, v4

    sget-object v10, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v7, v10, :cond_3

    iget-object v7, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v7, v7, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v7, v7, v0

    aget-object v7, v7, v3

    sget-object v10, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-eq v7, v10, :cond_4

    :cond_3
    iget-object v7, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v7, v7, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v7, v7, v0

    aget-object v7, v7, v5

    sget-object v10, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v7, v10, :cond_5

    iget-object v7, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v7, v7, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v7, v7, v0

    aget-object v3, v7, v3

    sget-object v7, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v3, v7, :cond_5

    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    :goto_1
    iget-object v7, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v7, v7, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v7, v7, v1

    aget-object v4, v7, v4

    sget-object v7, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v4, v7, :cond_6

    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    :goto_2
    iget-object v7, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v7, v7, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v1, v7, v1

    aget-object v1, v1, v5

    sget-object v5, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    if-ne v1, v5, :cond_7

    const/4 v1, 0x1

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    :goto_3
    const/4 v5, 0x0

    if-eqz v3, :cond_8

    iget-object v2, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget-object v3, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v3, v3, Lcom/google/ctf/game/f;->f:I

    sub-int/2addr v0, v9

    mul-int v3, v3, v0

    int-to-float v0, v3

    iput v0, v2, Lcom/badlogic/gdx/math/f;->e:F

    iput v5, p1, Lcom/google/ctf/game/c;->c:F

    goto :goto_4

    :cond_8
    if-eqz v6, :cond_9

    iget-object v0, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget-object v3, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v3, v3, Lcom/google/ctf/game/f;->f:I

    add-int/2addr v2, v9

    mul-int v3, v3, v2

    int-to-float v2, v3

    iput v2, v0, Lcom/badlogic/gdx/math/f;->e:F

    iput v5, p1, Lcom/google/ctf/game/c;->c:F

    iput-boolean v8, p1, Lcom/google/ctf/game/c;->f:Z

    :cond_9
    :goto_4
    iget-object v0, p0, Lcom/google/ctf/game/g;->b:Lcom/google/ctf/game/i;

    invoke-virtual {v0}, Lcom/google/ctf/game/i;->c()Z

    move-result v0

    const/high16 v2, 0x41200000    # 10.0f

    if-eqz v0, :cond_a

    if-nez v4, :cond_c

    iget-object v0, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v1, v0, Lcom/badlogic/gdx/math/f;->d:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/badlogic/gdx/math/f;->d:F

    iput-boolean v8, p1, Lcom/google/ctf/game/c;->d:Z

    :goto_5
    iput-boolean v9, p1, Lcom/google/ctf/game/c;->e:Z

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lcom/google/ctf/game/g;->b:Lcom/google/ctf/game/i;

    invoke-virtual {v0}, Lcom/google/ctf/game/i;->d()Z

    move-result v0

    if-eqz v0, :cond_b

    if-nez v1, :cond_c

    iget-object v0, p1, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v1, v0, Lcom/badlogic/gdx/math/f;->d:F

    add-float/2addr v1, v2

    iput v1, v0, Lcom/badlogic/gdx/math/f;->d:F

    iput-boolean v9, p1, Lcom/google/ctf/game/c;->d:Z

    goto :goto_5

    :cond_b
    iput-boolean v8, p1, Lcom/google/ctf/game/c;->e:Z

    :cond_c
    :goto_6
    iget-object v0, p0, Lcom/google/ctf/game/g;->b:Lcom/google/ctf/game/i;

    invoke-virtual {v0}, Lcom/google/ctf/game/i;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-boolean v0, p1, Lcom/google/ctf/game/c;->f:Z

    if-nez v0, :cond_d

    const/high16 v0, -0x3e200000    # -28.0f

    iput v0, p1, Lcom/google/ctf/game/c;->c:F

    iput-boolean v9, p1, Lcom/google/ctf/game/c;->f:Z

    :cond_d
    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    iget-object v0, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    invoke-direct {p0, v0}, Lcom/google/ctf/game/g;->a(Lcom/google/ctf/game/c;)V

    iget-object v0, p0, Lcom/google/ctf/game/g;->b:Lcom/google/ctf/game/i;

    invoke-virtual {v0}, Lcom/google/ctf/game/i;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v0, v0, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    iget v0, v0, Lcom/badlogic/gdx/math/f;->e:F

    iget-object v1, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v1, v1, Lcom/google/ctf/game/b;->b:Lcom/badlogic/gdx/math/f;

    iget v1, v1, Lcom/badlogic/gdx/math/f;->e:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v1, v1, Lcom/google/ctf/game/f;->f:I

    div-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v1, v1, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    iget v1, v1, Lcom/badlogic/gdx/math/f;->d:F

    float-to-int v1, v1

    iget-object v3, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v3, v3, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v3, v3, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    iget v3, v3, Lcom/badlogic/gdx/math/f;->d:F

    iget-object v4, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v4, v4, Lcom/google/ctf/game/b;->b:Lcom/badlogic/gdx/math/f;

    iget v4, v4, Lcom/badlogic/gdx/math/f;->d:F

    div-float/2addr v4, v2

    add-float/2addr v3, v4

    float-to-int v2, v3

    iget-object v3, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v3, v3, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v3, v3, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    iget v3, v3, Lcom/badlogic/gdx/math/f;->d:F

    iget-object v4, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    iget-object v4, v4, Lcom/google/ctf/game/b;->b:Lcom/badlogic/gdx/math/f;

    iget v4, v4, Lcom/badlogic/gdx/math/f;->d:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget v4, v4, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v1

    sget-object v5, Lcom/google/ctf/game/f$a;->w:Lcom/google/ctf/game/f$a;

    if-eq v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v2

    sget-object v5, Lcom/google/ctf/game/f$a;->w:Lcom/google/ctf/game/f$a;

    if-eq v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v3

    sget-object v5, Lcom/google/ctf/game/f$a;->w:Lcom/google/ctf/game/f$a;

    if-ne v4, v5, :cond_0

    goto :goto_2

    :cond_0
    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v1

    sget-object v5, Lcom/google/ctf/game/f$a;->P:Lcom/google/ctf/game/f$a;

    if-eq v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v2

    sget-object v5, Lcom/google/ctf/game/f$a;->P:Lcom/google/ctf/game/f$a;

    if-eq v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v3

    sget-object v5, Lcom/google/ctf/game/f$a;->P:Lcom/google/ctf/game/f$a;

    if-ne v4, v5, :cond_1

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v4, v4, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v0

    aget-object v4, v4, v1

    sget-object v5, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    if-ne v4, v5, :cond_2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v1, v1, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v1, v1, v0

    aget-object v1, v1, v2

    sget-object v4, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    if-ne v1, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v1, v1, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v1, v1, v0

    aget-object v1, v1, v3

    sget-object v2, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    if-ne v1, v2, :cond_4

    move v1, v3

    :goto_0
    iget-object v2, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v2, v2, Lcom/google/ctf/game/f;->d:[[I

    aget-object v0, v2, v0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/ctf/game/g;->b:Lcom/google/ctf/game/i;

    invoke-virtual {v1, v0}, Lcom/google/ctf/game/i;->d(I)V

    goto :goto_3

    :cond_4
    return-void

    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    iget-object v1, p0, Lcom/google/ctf/game/g;->c:Lcom/google/ctf/game/b;

    invoke-virtual {v0, v1}, Lcom/google/ctf/game/f;->a(Lcom/google/ctf/game/b;)V

    return-void

    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/google/ctf/game/g;->a:Lcom/google/ctf/game/f;

    invoke-virtual {v0}, Lcom/google/ctf/game/f;->a()V

    :cond_7
    :goto_3
    return-void
.end method
