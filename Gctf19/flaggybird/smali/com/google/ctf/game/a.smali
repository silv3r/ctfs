.class Lcom/google/ctf/game/a;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/badlogic/gdx/graphics/l;


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/badlogic/gdx/graphics/l;

    sget-object v1, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    const-string v2, "scenery.png"

    invoke-interface {v1, v2}, Lcom/badlogic/gdx/f;->a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/c/a;)V

    iput-object v0, p0, Lcom/google/ctf/game/a;->a:Lcom/badlogic/gdx/graphics/l;

    return-void
.end method


# virtual methods
.method a(Lcom/badlogic/gdx/graphics/g2d/b;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v2}, Lcom/badlogic/gdx/h;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/ctf/game/a;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v3

    div-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    sget-object v3, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v3}, Lcom/badlogic/gdx/h;->b()I

    move-result v3

    iget-object v4, p0, Lcom/google/ctf/game/a;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v4}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v4

    div-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/ctf/game/a;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v3}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v3

    mul-int v3, v3, v2

    int-to-float v3, v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float v3, v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    iget-object v5, p0, Lcom/google/ctf/game/a;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-virtual {v5}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v5

    mul-int v5, v5, v1

    int-to-float v5, v5

    mul-float v5, v5, v4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v4

    iget-object v4, p0, Lcom/google/ctf/game/a;->a:Lcom/badlogic/gdx/graphics/l;

    invoke-interface {p1, v4, v3, v5}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/graphics/l;FF)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
