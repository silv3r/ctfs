.class public Lcom/google/ctf/game/e;
.super Lcom/badlogic/gdx/b;


# instance fields
.field a:Lcom/google/ctf/game/h;

.field private b:Lcom/google/ctf/game/a;

.field private c:Lcom/badlogic/gdx/graphics/g2d/b;

.field private d:Lcom/google/ctf/game/b;

.field private e:Lcom/google/ctf/game/f;

.field private f:Lcom/badlogic/gdx/graphics/i;

.field private g:Lcom/badlogic/gdx/graphics/i;

.field private h:Lcom/google/ctf/game/g;

.field private i:Lcom/google/ctf/game/i;

.field private j:F

.field private k:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "rary"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/ctf/game/h;)V
    .locals 1

    invoke-direct {p0}, Lcom/badlogic/gdx/b;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ctf/game/e;->j:F

    iput v0, p0, Lcom/google/ctf/game/e;->k:F

    iput-object p1, p0, Lcom/google/ctf/game/e;->a:Lcom/google/ctf/game/h;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    new-instance v0, Lcom/badlogic/gdx/graphics/g2d/c;

    invoke-direct {v0}, Lcom/badlogic/gdx/graphics/g2d/c;-><init>()V

    iput-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/g2d/b;->d()V

    new-instance v0, Lcom/google/ctf/game/a;

    invoke-direct {v0}, Lcom/google/ctf/game/a;-><init>()V

    iput-object v0, p0, Lcom/google/ctf/game/e;->b:Lcom/google/ctf/game/a;

    :try_start_0
    new-instance v0, Lcom/google/ctf/game/f;

    iget-object v1, p0, Lcom/google/ctf/game/e;->a:Lcom/google/ctf/game/h;

    invoke-direct {v0, v1}, Lcom/google/ctf/game/f;-><init>(Lcom/google/ctf/game/h;)V

    iput-object v0, p0, Lcom/google/ctf/game/e;->e:Lcom/google/ctf/game/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    sget-object v0, Lcom/badlogic/gdx/g;->a:Lcom/badlogic/gdx/a;

    invoke-interface {v0}, Lcom/badlogic/gdx/a;->d()V

    :goto_0
    new-instance v0, Lcom/google/ctf/game/i;

    iget-object v1, p0, Lcom/google/ctf/game/e;->e:Lcom/google/ctf/game/f;

    invoke-direct {v0, v1}, Lcom/google/ctf/game/i;-><init>(Lcom/google/ctf/game/f;)V

    iput-object v0, p0, Lcom/google/ctf/game/e;->i:Lcom/google/ctf/game/i;

    new-instance v0, Lcom/google/ctf/game/b;

    new-instance v1, Lcom/badlogic/gdx/math/f;

    const/high16 v2, 0x44b00000    # 1408.0f

    const/high16 v3, 0x45200000    # 2560.0f

    invoke-direct {v1, v2, v3}, Lcom/badlogic/gdx/math/f;-><init>(FF)V

    invoke-direct {v0, v1}, Lcom/google/ctf/game/b;-><init>(Lcom/badlogic/gdx/math/f;)V

    iput-object v0, p0, Lcom/google/ctf/game/e;->d:Lcom/google/ctf/game/b;

    new-instance v0, Lcom/google/ctf/game/g;

    iget-object v1, p0, Lcom/google/ctf/game/e;->d:Lcom/google/ctf/game/b;

    iget-object v2, p0, Lcom/google/ctf/game/e;->e:Lcom/google/ctf/game/f;

    iget-object v3, p0, Lcom/google/ctf/game/e;->i:Lcom/google/ctf/game/i;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ctf/game/g;-><init>(Lcom/google/ctf/game/b;Lcom/google/ctf/game/f;Lcom/google/ctf/game/i;)V

    iput-object v0, p0, Lcom/google/ctf/game/e;->h:Lcom/google/ctf/game/g;

    sget-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v0}, Lcom/badlogic/gdx/h;->b()I

    move-result v0

    int-to-float v0, v0

    sget-object v1, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v1}, Lcom/badlogic/gdx/h;->c()I

    move-result v1

    int-to-float v1, v1

    new-instance v2, Lcom/badlogic/gdx/graphics/i;

    div-float v3, v1, v0

    const v4, 0x45a28000    # 2600.0f

    mul-float v3, v3, v4

    invoke-direct {v2, v4, v3}, Lcom/badlogic/gdx/graphics/i;-><init>(FF)V

    iput-object v2, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    new-instance v2, Lcom/badlogic/gdx/graphics/i;

    sget-object v3, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v3}, Lcom/badlogic/gdx/h;->b()I

    move-result v3

    int-to-float v3, v3

    sget-object v4, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v4}, Lcom/badlogic/gdx/h;->c()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v3, v4}, Lcom/badlogic/gdx/graphics/i;-><init>(FF)V

    iput-object v2, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iget-object v2, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iget-object v2, v2, Lcom/badlogic/gdx/graphics/i;->a:Lcom/badlogic/gdx/math/g;

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    iput v0, v2, Lcom/badlogic/gdx/math/g;->a:F

    iget-object v0, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/i;->a:Lcom/badlogic/gdx/math/g;

    div-float/2addr v1, v3

    iput v1, v0, Lcom/badlogic/gdx/math/g;->b:F

    iget-object v0, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/i;->a()V

    return-void
.end method

.method public a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    const v1, 0x45a28000    # 2600.0f

    iput v1, v0, Lcom/badlogic/gdx/graphics/i;->j:F

    iget-object v0, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iput v1, v0, Lcom/badlogic/gdx/graphics/i;->j:F

    iget-object v0, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    int-to-float p2, p2

    mul-float p2, p2, v1

    int-to-float p1, p1

    div-float/2addr p2, p1

    iput p2, v0, Lcom/badlogic/gdx/graphics/i;->k:F

    iget-object p1, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iput p2, p1, Lcom/badlogic/gdx/graphics/i;->k:F

    return-void
.end method

.method public b()V
    .locals 4

    sget-object v0, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v0}, Lcom/badlogic/gdx/h;->d()F

    move-result v0

    iget v1, p0, Lcom/google/ctf/game/e;->j:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/google/ctf/game/e;->j:F

    iget v1, p0, Lcom/google/ctf/game/e;->k:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/google/ctf/game/e;->k:F

    :goto_0
    iget v0, p0, Lcom/google/ctf/game/e;->k:F

    const v1, 0x3c888889

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/ctf/game/e;->i:Lcom/google/ctf/game/i;

    invoke-virtual {v0}, Lcom/google/ctf/game/i;->a()V

    iget-object v0, p0, Lcom/google/ctf/game/e;->i:Lcom/google/ctf/game/i;

    invoke-virtual {v0}, Lcom/google/ctf/game/i;->b()V

    iget-object v0, p0, Lcom/google/ctf/game/e;->h:Lcom/google/ctf/game/g;

    invoke-virtual {v0}, Lcom/google/ctf/game/g;->a()V

    iget v0, p0, Lcom/google/ctf/game/e;->k:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/ctf/game/e;->k:F

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/i;->a:Lcom/badlogic/gdx/math/g;

    iget-object v1, p0, Lcom/google/ctf/game/e;->d:Lcom/google/ctf/game/b;

    iget-object v1, v1, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/badlogic/gdx/math/g;->a(Lcom/badlogic/gdx/math/f;F)Lcom/badlogic/gdx/math/g;

    iget-object v0, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    iget-object v0, v0, Lcom/badlogic/gdx/graphics/i;->a:Lcom/badlogic/gdx/math/g;

    iget v1, v0, Lcom/badlogic/gdx/math/g;->b:F

    const/high16 v3, 0x43960000    # 300.0f

    add-float/2addr v1, v3

    iput v1, v0, Lcom/badlogic/gdx/math/g;->b:F

    iget-object v0, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    invoke-virtual {v0}, Lcom/badlogic/gdx/graphics/i;->a()V

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v2, v2, v2, v1}, Lcom/badlogic/gdx/graphics/e;->glClearColor(FFFF)V

    sget-object v0, Lcom/badlogic/gdx/g;->g:Lcom/badlogic/gdx/graphics/e;

    sget-object v1, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v1}, Lcom/badlogic/gdx/h;->f()Lcom/badlogic/gdx/h$a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/badlogic/gdx/h$a;->h:Z

    if-eqz v1, :cond_1

    const v1, 0x8000

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    or-int/lit16 v1, v1, 0x4100

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/e;->glClear(I)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/g2d/b;->a()V

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    iget-object v1, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/i;->f:Lcom/badlogic/gdx/math/Matrix4;

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/math/Matrix4;)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->b:Lcom/google/ctf/game/a;

    iget-object v1, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    invoke-virtual {v0, v1}, Lcom/google/ctf/game/a;->a(Lcom/badlogic/gdx/graphics/g2d/b;)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    iget-object v1, p0, Lcom/google/ctf/game/e;->f:Lcom/badlogic/gdx/graphics/i;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/i;->f:Lcom/badlogic/gdx/math/Matrix4;

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/math/Matrix4;)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->e:Lcom/google/ctf/game/f;

    iget-object v1, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    iget-object v2, p0, Lcom/google/ctf/game/e;->d:Lcom/google/ctf/game/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/ctf/game/f;->a(Lcom/badlogic/gdx/graphics/g2d/b;Lcom/google/ctf/game/c;)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->d:Lcom/google/ctf/game/b;

    iget-object v1, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    iget v2, p0, Lcom/google/ctf/game/e;->j:F

    invoke-virtual {v0, v1, v2}, Lcom/google/ctf/game/b;->a(Lcom/badlogic/gdx/graphics/g2d/b;F)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    iget-object v1, p0, Lcom/google/ctf/game/e;->g:Lcom/badlogic/gdx/graphics/i;

    iget-object v1, v1, Lcom/badlogic/gdx/graphics/i;->f:Lcom/badlogic/gdx/math/Matrix4;

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/math/Matrix4;)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->i:Lcom/google/ctf/game/i;

    iget-object v1, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    invoke-virtual {v0, v1}, Lcom/google/ctf/game/i;->a(Lcom/badlogic/gdx/graphics/g2d/b;)V

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/g2d/b;->b()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/ctf/game/e;->c:Lcom/badlogic/gdx/graphics/g2d/b;

    invoke-interface {v0}, Lcom/badlogic/gdx/graphics/g2d/b;->c()V

    return-void
.end method
