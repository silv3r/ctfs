.class final enum Lcom/google/ctf/game/f$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ctf/game/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/ctf/game/f$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/ctf/game/f$a;

.field public static final enum B:Lcom/google/ctf/game/f$a;

.field public static final enum C:Lcom/google/ctf/game/f$a;

.field public static final enum D:Lcom/google/ctf/game/f$a;

.field public static final enum E:Lcom/google/ctf/game/f$a;

.field public static final enum F:Lcom/google/ctf/game/f$a;

.field public static final enum G:Lcom/google/ctf/game/f$a;

.field public static final enum H:Lcom/google/ctf/game/f$a;

.field public static final enum I:Lcom/google/ctf/game/f$a;

.field public static final enum J:Lcom/google/ctf/game/f$a;

.field public static final enum K:Lcom/google/ctf/game/f$a;

.field public static final enum L:Lcom/google/ctf/game/f$a;

.field public static final enum M:Lcom/google/ctf/game/f$a;

.field public static final enum N:Lcom/google/ctf/game/f$a;

.field public static final enum O:Lcom/google/ctf/game/f$a;

.field public static final enum P:Lcom/google/ctf/game/f$a;

.field public static final enum Q:Lcom/google/ctf/game/f$a;

.field public static final enum R:Lcom/google/ctf/game/f$a;

.field public static final enum S:Lcom/google/ctf/game/f$a;

.field public static final enum T:Lcom/google/ctf/game/f$a;

.field public static final enum U:Lcom/google/ctf/game/f$a;

.field public static final enum V:Lcom/google/ctf/game/f$a;

.field public static final enum W:Lcom/google/ctf/game/f$a;

.field public static final enum X:Lcom/google/ctf/game/f$a;

.field public static final enum Y:Lcom/google/ctf/game/f$a;

.field public static final enum Z:Lcom/google/ctf/game/f$a;

.field public static final enum a:Lcom/google/ctf/game/f$a;

.field public static final enum aa:Lcom/google/ctf/game/f$a;

.field public static final enum ab:Lcom/google/ctf/game/f$a;

.field public static final enum ac:Lcom/google/ctf/game/f$a;

.field public static final enum ad:Lcom/google/ctf/game/f$a;

.field public static final enum ae:Lcom/google/ctf/game/f$a;

.field public static final enum af:Lcom/google/ctf/game/f$a;

.field private static final synthetic ag:[Lcom/google/ctf/game/f$a;

.field public static final enum b:Lcom/google/ctf/game/f$a;

.field public static final enum c:Lcom/google/ctf/game/f$a;

.field public static final enum d:Lcom/google/ctf/game/f$a;

.field public static final enum e:Lcom/google/ctf/game/f$a;

.field public static final enum f:Lcom/google/ctf/game/f$a;

.field public static final enum g:Lcom/google/ctf/game/f$a;

.field public static final enum h:Lcom/google/ctf/game/f$a;

.field public static final enum i:Lcom/google/ctf/game/f$a;

.field public static final enum j:Lcom/google/ctf/game/f$a;

.field public static final enum k:Lcom/google/ctf/game/f$a;

.field public static final enum l:Lcom/google/ctf/game/f$a;

.field public static final enum m:Lcom/google/ctf/game/f$a;

.field public static final enum n:Lcom/google/ctf/game/f$a;

.field public static final enum o:Lcom/google/ctf/game/f$a;

.field public static final enum p:Lcom/google/ctf/game/f$a;

.field public static final enum q:Lcom/google/ctf/game/f$a;

.field public static final enum r:Lcom/google/ctf/game/f$a;

.field public static final enum s:Lcom/google/ctf/game/f$a;

.field public static final enum t:Lcom/google/ctf/game/f$a;

.field public static final enum u:Lcom/google/ctf/game/f$a;

.field public static final enum v:Lcom/google/ctf/game/f$a;

.field public static final enum w:Lcom/google/ctf/game/f$a;

.field public static final enum x:Lcom/google/ctf/game/f$a;

.field public static final enum y:Lcom/google/ctf/game/f$a;

.field public static final enum z:Lcom/google/ctf/game/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "AIR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->a:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "GROUND"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_A"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->c:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_AA"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->d:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_B"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->e:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_C"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->f:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_CC"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->g:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_D"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->h:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_DD"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->i:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_E"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->j:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_F"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->k:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_FF"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->l:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_G"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->m:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_GG"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->n:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_H"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->o:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_I"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->p:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_II"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->q:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_J"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->r:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_JJ"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->s:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_K"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->t:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_L"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->u:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "DIAGONAL_LL"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->v:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "COMPUTER"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->w:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_HOLDER"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_0"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->y:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_1"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->z:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_2"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->A:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_3"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->B:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_4"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->C:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_5"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->D:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_6"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->E:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_7"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->F:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_8"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->G:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_9"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->H:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_10"

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->I:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_11"

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->J:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_12"

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->K:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_13"

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->L:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_14"

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->M:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "EGG_15"

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->N:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "BACKGROUND"

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->O:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "PORTAL"

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->P:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_0"

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->Q:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_1"

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->R:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_2"

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->S:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_3"

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->T:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_4"

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->U:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_5"

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->V:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_6"

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->W:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_7"

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->X:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_8"

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->Y:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_9"

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->Z:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_10"

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->aa:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_11"

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->ab:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_12"

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->ac:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_13"

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->ad:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_14"

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->ae:Lcom/google/ctf/game/f$a;

    new-instance v0, Lcom/google/ctf/game/f$a;

    const-string v1, "FLAG_15"

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15}, Lcom/google/ctf/game/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ctf/game/f$a;->af:Lcom/google/ctf/game/f$a;

    const/16 v0, 0x3a

    new-array v0, v0, [Lcom/google/ctf/game/f$a;

    sget-object v1, Lcom/google/ctf/game/f$a;->a:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ctf/game/f$a;->c:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ctf/game/f$a;->d:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ctf/game/f$a;->e:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ctf/game/f$a;->f:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/ctf/game/f$a;->g:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/ctf/game/f$a;->h:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/ctf/game/f$a;->i:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/ctf/game/f$a;->j:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/ctf/game/f$a;->k:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/ctf/game/f$a;->l:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v13

    sget-object v1, Lcom/google/ctf/game/f$a;->m:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v14

    sget-object v1, Lcom/google/ctf/game/f$a;->n:Lcom/google/ctf/game/f$a;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->o:Lcom/google/ctf/game/f$a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->p:Lcom/google/ctf/game/f$a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->q:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->r:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->s:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->t:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->u:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->v:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->w:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->y:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->z:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->A:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->B:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->C:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->D:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->E:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->F:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->G:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->H:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->I:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->J:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->K:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->L:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->M:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->N:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->O:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->P:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->Q:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->R:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->S:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->T:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->U:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->V:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->W:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->X:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->Y:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->Z:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->aa:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->ab:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->ac:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->ad:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->ae:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->af:Lcom/google/ctf/game/f$a;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/ctf/game/f$a;->ag:[Lcom/google/ctf/game/f$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ctf/game/f$a;
    .locals 1

    const-class v0, Lcom/google/ctf/game/f$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/google/ctf/game/f$a;

    return-object p0
.end method

.method public static values()[Lcom/google/ctf/game/f$a;
    .locals 1

    sget-object v0, Lcom/google/ctf/game/f$a;->ag:[Lcom/google/ctf/game/f$a;

    invoke-virtual {v0}, [Lcom/google/ctf/game/f$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ctf/game/f$a;

    return-object v0
.end method
