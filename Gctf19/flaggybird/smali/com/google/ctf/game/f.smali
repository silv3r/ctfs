.class Lcom/google/ctf/game/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ctf/game/f$a;
    }
.end annotation


# static fields
.field static final a:[Lcom/google/ctf/game/f$a;

.field static final b:[Lcom/google/ctf/game/f$a;


# instance fields
.field c:[[Lcom/google/ctf/game/f$a;

.field d:[[I

.field e:I

.field f:I

.field g:Lcom/google/ctf/game/h;

.field private h:I

.field private i:I

.field private j:[[Lcom/badlogic/gdx/graphics/g2d/d;

.field private k:[[I

.field private l:[Lcom/google/ctf/game/f$a;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x10

    new-array v1, v0, [Lcom/google/ctf/game/f$a;

    sget-object v2, Lcom/google/ctf/game/f$a;->y:Lcom/google/ctf/game/f$a;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/ctf/game/f$a;->z:Lcom/google/ctf/game/f$a;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/ctf/game/f$a;->A:Lcom/google/ctf/game/f$a;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/ctf/game/f$a;->B:Lcom/google/ctf/game/f$a;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/ctf/game/f$a;->C:Lcom/google/ctf/game/f$a;

    const/4 v7, 0x4

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/ctf/game/f$a;->D:Lcom/google/ctf/game/f$a;

    const/4 v8, 0x5

    aput-object v2, v1, v8

    sget-object v2, Lcom/google/ctf/game/f$a;->E:Lcom/google/ctf/game/f$a;

    const/4 v9, 0x6

    aput-object v2, v1, v9

    sget-object v2, Lcom/google/ctf/game/f$a;->F:Lcom/google/ctf/game/f$a;

    const/4 v10, 0x7

    aput-object v2, v1, v10

    sget-object v2, Lcom/google/ctf/game/f$a;->G:Lcom/google/ctf/game/f$a;

    const/16 v11, 0x8

    aput-object v2, v1, v11

    sget-object v2, Lcom/google/ctf/game/f$a;->H:Lcom/google/ctf/game/f$a;

    const/16 v12, 0x9

    aput-object v2, v1, v12

    sget-object v2, Lcom/google/ctf/game/f$a;->I:Lcom/google/ctf/game/f$a;

    const/16 v13, 0xa

    aput-object v2, v1, v13

    sget-object v2, Lcom/google/ctf/game/f$a;->J:Lcom/google/ctf/game/f$a;

    const/16 v14, 0xb

    aput-object v2, v1, v14

    sget-object v2, Lcom/google/ctf/game/f$a;->K:Lcom/google/ctf/game/f$a;

    const/16 v15, 0xc

    aput-object v2, v1, v15

    sget-object v2, Lcom/google/ctf/game/f$a;->L:Lcom/google/ctf/game/f$a;

    const/16 v16, 0xd

    aput-object v2, v1, v16

    sget-object v2, Lcom/google/ctf/game/f$a;->M:Lcom/google/ctf/game/f$a;

    const/16 v17, 0xe

    aput-object v2, v1, v17

    sget-object v2, Lcom/google/ctf/game/f$a;->N:Lcom/google/ctf/game/f$a;

    const/16 v17, 0xf

    aput-object v2, v1, v17

    sput-object v1, Lcom/google/ctf/game/f;->a:[Lcom/google/ctf/game/f$a;

    new-array v0, v0, [Lcom/google/ctf/game/f$a;

    sget-object v1, Lcom/google/ctf/game/f$a;->Q:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ctf/game/f$a;->R:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ctf/game/f$a;->S:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/ctf/game/f$a;->T:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/ctf/game/f$a;->U:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/ctf/game/f$a;->V:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/ctf/game/f$a;->W:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/ctf/game/f$a;->X:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/ctf/game/f$a;->Y:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/ctf/game/f$a;->Z:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/ctf/game/f$a;->aa:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v13

    sget-object v1, Lcom/google/ctf/game/f$a;->ab:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v14

    sget-object v1, Lcom/google/ctf/game/f$a;->ac:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v15

    sget-object v1, Lcom/google/ctf/game/f$a;->ad:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, v16

    sget-object v1, Lcom/google/ctf/game/f$a;->ae:Lcom/google/ctf/game/f$a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ctf/game/f$a;->af:Lcom/google/ctf/game/f$a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/ctf/game/f;->b:[Lcom/google/ctf/game/f$a;

    return-void
.end method

.method constructor <init>(Lcom/google/ctf/game/h;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xbe

    iput v0, p0, Lcom/google/ctf/game/f;->h:I

    const/16 v0, 0x2e

    iput v0, p0, Lcom/google/ctf/game/f;->i:I

    iput-object p1, p0, Lcom/google/ctf/game/f;->g:Lcom/google/ctf/game/h;

    new-instance p1, Lcom/badlogic/gdx/graphics/l;

    sget-object v0, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    const-string v1, "tileset.png"

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/f;->a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/c/a;)V

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v0

    div-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/ctf/game/f;->e:I

    invoke-virtual {p1}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/ctf/game/f;->f:I

    iget v0, p0, Lcom/google/ctf/game/f;->e:I

    iget v1, p0, Lcom/google/ctf/game/f;->f:I

    invoke-static {p1, v0, v1}, Lcom/badlogic/gdx/graphics/g2d/d;->a(Lcom/badlogic/gdx/graphics/l;II)[[Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object p1

    iput-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/ctf/game/f;->n:I

    const/4 p1, 0x1

    iput p1, p0, Lcom/google/ctf/game/f;->o:I

    iget p1, p0, Lcom/google/ctf/game/f;->o:I

    invoke-direct {p0, p1}, Lcom/google/ctf/game/f;->a(I)V

    return-void
.end method

.method private a(B)Lcom/google/ctf/game/f$a;
    .locals 1

    const/16 v0, 0x41

    if-eq p1, v0, :cond_1

    const/16 v0, 0x78

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    packed-switch p1, :pswitch_data_2

    packed-switch p1, :pswitch_data_3

    packed-switch p1, :pswitch_data_4

    packed-switch p1, :pswitch_data_5

    sget-object p1, Lcom/google/ctf/game/f$a;->a:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_0
    sget-object p1, Lcom/google/ctf/game/f$a;->ae:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1
    sget-object p1, Lcom/google/ctf/game/f$a;->ac:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/ctf/game/f$a;->aa:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/ctf/game/f$a;->Y:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_4
    sget-object p1, Lcom/google/ctf/game/f$a;->W:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_5
    sget-object p1, Lcom/google/ctf/game/f$a;->U:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_6
    sget-object p1, Lcom/google/ctf/game/f$a;->S:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_7
    sget-object p1, Lcom/google/ctf/game/f$a;->Q:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_8
    sget-object p1, Lcom/google/ctf/game/f$a;->u:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_9
    sget-object p1, Lcom/google/ctf/game/f$a;->t:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_a
    sget-object p1, Lcom/google/ctf/game/f$a;->r:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_b
    sget-object p1, Lcom/google/ctf/game/f$a;->p:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_c
    sget-object p1, Lcom/google/ctf/game/f$a;->o:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_d
    sget-object p1, Lcom/google/ctf/game/f$a;->m:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_e
    sget-object p1, Lcom/google/ctf/game/f$a;->k:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_f
    sget-object p1, Lcom/google/ctf/game/f$a;->j:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_10
    sget-object p1, Lcom/google/ctf/game/f$a;->h:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_11
    sget-object p1, Lcom/google/ctf/game/f$a;->f:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_12
    sget-object p1, Lcom/google/ctf/game/f$a;->e:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_13
    sget-object p1, Lcom/google/ctf/game/f$a;->c:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_14
    sget-object p1, Lcom/google/ctf/game/f$a;->af:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_15
    sget-object p1, Lcom/google/ctf/game/f$a;->ad:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_16
    sget-object p1, Lcom/google/ctf/game/f$a;->ab:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_17
    sget-object p1, Lcom/google/ctf/game/f$a;->Z:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_18
    sget-object p1, Lcom/google/ctf/game/f$a;->X:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_19
    sget-object p1, Lcom/google/ctf/game/f$a;->V:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1a
    sget-object p1, Lcom/google/ctf/game/f$a;->T:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1b
    sget-object p1, Lcom/google/ctf/game/f$a;->R:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1c
    sget-object p1, Lcom/google/ctf/game/f$a;->v:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1d
    sget-object p1, Lcom/google/ctf/game/f$a;->s:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1e
    sget-object p1, Lcom/google/ctf/game/f$a;->q:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_1f
    sget-object p1, Lcom/google/ctf/game/f$a;->n:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_20
    sget-object p1, Lcom/google/ctf/game/f$a;->l:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_21
    sget-object p1, Lcom/google/ctf/game/f$a;->i:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_22
    sget-object p1, Lcom/google/ctf/game/f$a;->g:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_23
    sget-object p1, Lcom/google/ctf/game/f$a;->P:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_24
    sget-object p1, Lcom/google/ctf/game/f$a;->w:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_25
    sget-object p1, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_26
    sget-object p1, Lcom/google/ctf/game/f$a;->b:Lcom/google/ctf/game/f$a;

    return-object p1

    :cond_0
    sget-object p1, Lcom/google/ctf/game/f$a;->O:Lcom/google/ctf/game/f$a;

    return-object p1

    :cond_1
    sget-object p1, Lcom/google/ctf/game/f$a;->d:Lcom/google/ctf/game/f$a;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x43
        :pswitch_22
        :pswitch_21
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x46
        :pswitch_20
        :pswitch_1f
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x49
        :pswitch_1e
        :pswitch_1d
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x4c
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x61
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(I)V
    .locals 4

    sget-object v0, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    const-string v1, "level%d.bin"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/badlogic/gdx/f;->a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/badlogic/gdx/c/a;->i()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/ctf/game/f;->a([B)V

    return-void
.end method

.method private a([B)V
    .locals 8

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance p1, Ljava/util/zip/InflaterInputStream;

    invoke-direct {p1, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    iget v0, p0, Lcom/google/ctf/game/f;->h:I

    iget v1, p0, Lcom/google/ctf/game/f;->i:I

    mul-int v0, v0, v1

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Ljava/util/zip/InflaterInputStream;->read([B)I

    move-result p1

    iget v1, p0, Lcom/google/ctf/game/f;->h:I

    iget v2, p0, Lcom/google/ctf/game/f;->i:I

    mul-int v1, v1, v2

    if-ne p1, v1, :cond_6

    iget p1, p0, Lcom/google/ctf/game/f;->i:I

    new-array p1, p1, [[Lcom/google/ctf/game/f$a;

    iput-object p1, p0, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    iget p1, p0, Lcom/google/ctf/game/f;->i:I

    new-array p1, p1, [[I

    iput-object p1, p0, Lcom/google/ctf/game/f;->d:[[I

    const/16 p1, 0x20

    new-array p1, p1, [[I

    iput-object p1, p0, Lcom/google/ctf/game/f;->k:[[I

    const/4 p1, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/google/ctf/game/f;->i:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    iget v3, p0, Lcom/google/ctf/game/f;->h:I

    new-array v3, v3, [Lcom/google/ctf/game/f$a;

    aput-object v3, v2, v1

    iget-object v2, p0, Lcom/google/ctf/game/f;->d:[[I

    iget v3, p0, Lcom/google/ctf/game/f;->h:I

    new-array v3, v3, [I

    aput-object v3, v2, v1

    const/4 v2, 0x0

    :goto_1
    iget v3, p0, Lcom/google/ctf/game/f;->h:I

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/ctf/game/f;->d:[[I

    aget-object v3, v3, v1

    const/4 v4, -0x1

    aput v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/google/ctf/game/f;->n:I

    const/4 v1, 0x0

    :goto_2
    iget v2, p0, Lcom/google/ctf/game/f;->i:I

    if-ge v1, v2, :cond_4

    const/4 v2, 0x0

    :goto_3
    iget v3, p0, Lcom/google/ctf/game/f;->h:I

    if-ge v2, v3, :cond_3

    iget v3, p0, Lcom/google/ctf/game/f;->h:I

    mul-int v3, v3, v1

    add-int/2addr v3, v2

    aget-byte v3, v0, v3

    invoke-direct {p0, v3}, Lcom/google/ctf/game/f;->a(B)Lcom/google/ctf/game/f$a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    iget v5, p0, Lcom/google/ctf/game/f;->i:I

    sub-int/2addr v5, v1

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    aget-object v4, v4, v5

    aput-object v3, v4, v2

    sget-object v4, Lcom/google/ctf/game/f$a;->x:Lcom/google/ctf/game/f$a;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/ctf/game/f;->d:[[I

    iget v4, p0, Lcom/google/ctf/game/f;->i:I

    sub-int/2addr v4, v1

    sub-int/2addr v4, v6

    aget-object v3, v3, v4

    iget v4, p0, Lcom/google/ctf/game/f;->n:I

    aput v4, v3, v2

    iget-object v3, p0, Lcom/google/ctf/game/f;->k:[[I

    iget v4, p0, Lcom/google/ctf/game/f;->n:I

    const/4 v5, 0x2

    new-array v5, v5, [I

    aput v2, v5, p1

    iget v7, p0, Lcom/google/ctf/game/f;->i:I

    sub-int/2addr v7, v1

    sub-int/2addr v7, v6

    aput v7, v5, v6

    aput-object v5, v3, v4

    iget v3, p0, Lcom/google/ctf/game/f;->n:I

    add-int/2addr v3, v6

    iput v3, p0, Lcom/google/ctf/game/f;->n:I

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iget v0, p0, Lcom/google/ctf/game/f;->n:I

    new-array v0, v0, [Lcom/google/ctf/game/f$a;

    iput-object v0, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    :goto_4
    iget-object v0, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    array-length v0, v0

    if-ge p1, v0, :cond_5

    iget-object v0, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    sget-object v1, Lcom/google/ctf/game/f$a;->y:Lcom/google/ctf/game/f$a;

    aput-object v1, v0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    :cond_5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    return-void

    :cond_6
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1
.end method


# virtual methods
.method a(Lcom/google/ctf/game/f$a;)Lcom/badlogic/gdx/graphics/g2d/d;
    .locals 12

    sget-object v0, Lcom/google/ctf/game/f$1;->a:[I

    invoke-virtual {p1}, Lcom/google/ctf/game/f$a;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/16 v0, 0xb

    const/16 v1, 0xa

    const/16 v2, 0x9

    const/16 v3, 0x8

    const/4 v4, 0x6

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x7

    const/4 v10, 0x4

    const/4 v11, 0x5

    packed-switch p1, :pswitch_data_0

    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v6

    aget-object p1, p1, v6

    return-object p1

    :pswitch_0
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v0

    return-object p1

    :pswitch_1
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v1

    return-object p1

    :pswitch_2
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v2

    return-object p1

    :pswitch_3
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v3

    return-object p1

    :pswitch_4
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v4

    aget-object p1, p1, v0

    return-object p1

    :pswitch_5
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v4

    aget-object p1, p1, v1

    return-object p1

    :pswitch_6
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v4

    aget-object p1, p1, v2

    return-object p1

    :pswitch_7
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v4

    aget-object p1, p1, v3

    return-object p1

    :pswitch_8
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v0

    return-object p1

    :pswitch_9
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v1

    return-object p1

    :pswitch_a
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v2

    return-object p1

    :pswitch_b
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v3

    return-object p1

    :pswitch_c
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v0

    return-object p1

    :pswitch_d
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v1

    return-object p1

    :pswitch_e
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v2

    return-object p1

    :pswitch_f
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v3

    return-object p1

    :pswitch_10
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v11

    return-object p1

    :pswitch_11
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v10

    return-object p1

    :pswitch_12
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v8

    return-object p1

    :pswitch_13
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v7

    return-object p1

    :pswitch_14
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v11

    return-object p1

    :pswitch_15
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v10

    return-object p1

    :pswitch_16
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v8

    return-object p1

    :pswitch_17
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v7

    return-object p1

    :pswitch_18
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v11

    return-object p1

    :pswitch_19
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v10

    return-object p1

    :pswitch_1a
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v8

    return-object p1

    :pswitch_1b
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v7

    return-object p1

    :pswitch_1c
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v7

    aget-object p1, p1, v11

    return-object p1

    :pswitch_1d
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v7

    aget-object p1, p1, v10

    return-object p1

    :pswitch_1e
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v7

    aget-object p1, p1, v8

    return-object p1

    :pswitch_1f
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v7

    aget-object p1, p1, v7

    return-object p1

    :pswitch_20
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v6

    aget-object p1, p1, v8

    return-object p1

    :pswitch_21
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v6

    aget-object p1, p1, v7

    return-object p1

    :pswitch_22
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v5

    aget-object p1, p1, v5

    return-object p1

    :pswitch_23
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v7

    aget-object p1, p1, v6

    return-object p1

    :pswitch_24
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v6

    return-object p1

    :pswitch_25
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v6

    return-object p1

    :pswitch_26
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v6

    return-object p1

    :pswitch_27
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v4

    aget-object p1, p1, v5

    return-object p1

    :pswitch_28
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v7

    return-object p1

    :pswitch_29
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v8

    return-object p1

    :pswitch_2a
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v10

    return-object p1

    :pswitch_2b
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v11

    return-object p1

    :pswitch_2c
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v4

    aget-object p1, p1, v4

    return-object p1

    :pswitch_2d
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v9

    return-object p1

    :pswitch_2e
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v9

    return-object p1

    :pswitch_2f
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v9

    return-object p1

    :pswitch_30
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v7

    aget-object p1, p1, v9

    return-object p1

    :pswitch_31
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v5

    aget-object p1, p1, v4

    return-object p1

    :pswitch_32
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v6

    aget-object p1, p1, v11

    return-object p1

    :pswitch_33
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v6

    aget-object p1, p1, v10

    return-object p1

    :pswitch_34
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v8

    aget-object p1, p1, v5

    return-object p1

    :pswitch_35
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v9

    aget-object p1, p1, v5

    return-object p1

    :pswitch_36
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v10

    aget-object p1, p1, v5

    return-object p1

    :pswitch_37
    iget-object p1, p0, Lcom/google/ctf/game/f;->j:[[Lcom/badlogic/gdx/graphics/g2d/d;

    aget-object p1, p1, v11

    aget-object p1, p1, v5

    return-object p1

    :pswitch_38
    const/4 p1, 0x0

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method a()V
    .locals 6

    const/16 v0, 0x20

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    sget-object v4, Lcom/google/ctf/game/f;->a:[Lcom/google/ctf/game/f$a;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    iget-object v4, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v2

    sget-object v5, Lcom/google/ctf/game/f;->a:[Lcom/google/ctf/game/f$a;

    aget-object v5, v5, v3
    
    if-ne v4, v5, :cond_0

    int-to-byte v4, v3

    aput-byte v4, v0, v2

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/google/ctf/game/Checker;

    invoke-direct {v2}, Lcom/google/ctf/game/Checker;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/ctf/game/Checker;->a([B)[B

    move-result-object v0

    if-eqz v0, :cond_3

    :try_start_0
    iput v1, p0, Lcom/google/ctf/game/f;->o:I

    invoke-direct {p0, v0}, Lcom/google/ctf/game/f;->a([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ctf/game/f;->g:Lcom/google/ctf/game/h;

    const-string v1, "Close, but no cigar."

    invoke-interface {v0, v1}, Lcom/google/ctf/game/h;->a(Ljava/lang/String;)V

    :catch_0
    :goto_2
    return-void
.end method

.method a(II)V
    .locals 5

    iget-object v0, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    sget-object v1, Lcom/google/ctf/game/f;->a:[Lcom/google/ctf/game/f$a;

    aget-object v1, v1, p2

    aput-object v1, v0, p1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    :goto_0
    iget-object v4, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    iget-object v4, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_1

    if-eqz p2, :cond_0

    return-void

    :cond_0
    move v3, v2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-eq v3, v1, :cond_3

    iget-object v1, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_3
    if-nez p2, :cond_4

    return-void

    :cond_4
    iget-object p2, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/16 p2, 0xf

    if-le p1, p2, :cond_5

    iget-object p1, p0, Lcom/google/ctf/game/f;->m:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object p2, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    sget-object v0, Lcom/google/ctf/game/f$a;->y:Lcom/google/ctf/game/f$a;

    aput-object v0, p2, p1

    :cond_5
    return-void
.end method

.method a(Lcom/badlogic/gdx/graphics/g2d/b;Lcom/google/ctf/game/c;)V
    .locals 9

    iget-object v0, p2, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget v0, v0, Lcom/badlogic/gdx/math/f;->d:F

    float-to-int v0, v0

    iget v1, p0, Lcom/google/ctf/game/f;->e:I

    div-int/2addr v0, v1

    iget-object p2, p2, Lcom/google/ctf/game/c;->a:Lcom/badlogic/gdx/math/f;

    iget p2, p2, Lcom/badlogic/gdx/math/f;->e:F

    float-to-int p2, p2

    iget v1, p0, Lcom/google/ctf/game/f;->f:I

    div-int/2addr p2, v1

    add-int/lit8 v1, p2, -0xc

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_0
    add-int/lit8 v3, p2, 0xc

    iget v4, p0, Lcom/google/ctf/game/f;->i:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    if-ge v1, v3, :cond_4

    add-int/lit8 v3, v0, -0x10

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    :goto_1
    add-int/lit8 v4, v0, 0x10

    iget v5, p0, Lcom/google/ctf/game/f;->h:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-ge v3, v4, :cond_3

    iget-object v4, p0, Lcom/google/ctf/game/f;->c:[[Lcom/google/ctf/game/f$a;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    invoke-virtual {p0, v4}, Lcom/google/ctf/game/f;->a(Lcom/google/ctf/game/f$a;)Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object v4

    if-nez v4, :cond_0

    goto :goto_2

    :cond_0
    mul-int/lit16 v5, v3, 0x80

    int-to-float v5, v5

    const/high16 v6, 0x41200000    # 10.0f

    mul-float v5, v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    mul-int/lit16 v7, v1, 0x80

    int-to-float v7, v7

    mul-float v7, v7, v6

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v6

    iget-object v6, p0, Lcom/google/ctf/game/f;->d:[[I

    aget-object v6, v6, v1

    aget v6, v6, v3

    const/4 v8, -0x1

    if-eq v6, v8, :cond_2

    iget-object v6, p0, Lcom/google/ctf/game/f;->l:[Lcom/google/ctf/game/f$a;

    iget-object v8, p0, Lcom/google/ctf/game/f;->d:[[I

    aget-object v8, v8, v1

    aget v8, v8, v3

    aget-object v6, v6, v8

    invoke-virtual {p0, v6}, Lcom/google/ctf/game/f;->a(Lcom/google/ctf/game/f$a;)Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object v6

    if-nez v6, :cond_1

    goto :goto_2

    :cond_1
    invoke-interface {p1, v6, v5, v7}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/graphics/g2d/d;FF)V

    :cond_2
    invoke-interface {p1, v4, v5, v7}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/graphics/g2d/d;FF)V

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method a(Lcom/google/ctf/game/b;)V
    .locals 2

    iget v0, p0, Lcom/google/ctf/game/f;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/ctf/game/f;->o:I

    :try_start_0
    iget v0, p0, Lcom/google/ctf/game/f;->o:I

    invoke-direct {p0, v0}, Lcom/google/ctf/game/f;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object v0, p1, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    const/high16 v1, 0x44b00000    # 1408.0f

    iput v1, v0, Lcom/badlogic/gdx/math/f;->d:F

    iget-object p1, p1, Lcom/google/ctf/game/b;->a:Lcom/badlogic/gdx/math/f;

    const/high16 v0, 0x45200000    # 2560.0f

    iput v0, p1, Lcom/badlogic/gdx/math/f;->e:F

    return-void
.end method
