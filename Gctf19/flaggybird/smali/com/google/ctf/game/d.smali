.class Lcom/google/ctf/game/d;
.super Ljava/lang/Object;


# instance fields
.field a:Z

.field b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/badlogic/gdx/graphics/g2d/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/badlogic/gdx/graphics/g2d/d;III)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/ctf/game/d;->f:Ljava/util/List;

    iget-object v0, p0, Lcom/google/ctf/game/d;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput p2, p0, Lcom/google/ctf/game/d;->c:I

    iput p3, p0, Lcom/google/ctf/game/d;->d:I

    iput p4, p0, Lcom/google/ctf/game/d;->e:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/ctf/game/d;->a:Z

    return-void
.end method

.method constructor <init>(Ljava/util/List;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/badlogic/gdx/graphics/g2d/d;",
            ">;IIII)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/ctf/game/d;->f:Ljava/util/List;

    iput p2, p0, Lcom/google/ctf/game/d;->c:I

    iput p3, p0, Lcom/google/ctf/game/d;->d:I

    iput p4, p0, Lcom/google/ctf/game/d;->e:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/ctf/game/d;->a:Z

    iput p5, p0, Lcom/google/ctf/game/d;->b:I

    return-void
.end method


# virtual methods
.method a(II)V
    .locals 2

    iget v0, p0, Lcom/google/ctf/game/d;->c:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/google/ctf/game/d;->c:I

    iget v1, p0, Lcom/google/ctf/game/d;->e:I

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    iget p1, p0, Lcom/google/ctf/game/d;->d:I

    if-lt p2, p1, :cond_0

    iget p1, p0, Lcom/google/ctf/game/d;->d:I

    iget v0, p0, Lcom/google/ctf/game/d;->e:I

    add-int/2addr p1, v0

    if-gt p2, p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/ctf/game/d;->a:Z

    :cond_0
    return-void
.end method

.method a(Lcom/badlogic/gdx/graphics/g2d/b;)V
    .locals 4

    iget-object v0, p0, Lcom/google/ctf/game/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/badlogic/gdx/graphics/g2d/d;

    iget v2, p0, Lcom/google/ctf/game/d;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/ctf/game/d;->d:I

    int-to-float v3, v3

    invoke-interface {p1, v1, v2, v3}, Lcom/badlogic/gdx/graphics/g2d/b;->a(Lcom/badlogic/gdx/graphics/g2d/d;FF)V

    goto :goto_0

    :cond_0
    return-void
.end method
