.class Lcom/google/ctf/game/i;
.super Lcom/badlogic/gdx/j;


# instance fields
.field private a:Lcom/google/ctf/game/d;

.field private b:Lcom/google/ctf/game/d;

.field private c:Lcom/google/ctf/game/d;

.field private d:Lcom/google/ctf/game/d;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/ctf/game/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/ctf/game/d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:I

.field private i:Lcom/google/ctf/game/f;


# direct methods
.method constructor <init>(Lcom/google/ctf/game/f;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {p0 .. p0}, Lcom/badlogic/gdx/j;-><init>()V

    iput-object v1, v0, Lcom/google/ctf/game/i;->i:Lcom/google/ctf/game/f;

    new-instance v2, Lcom/badlogic/gdx/graphics/l;

    sget-object v3, Lcom/badlogic/gdx/g;->e:Lcom/badlogic/gdx/f;

    const-string v4, "ui.png"

    invoke-interface {v3, v4}, Lcom/badlogic/gdx/f;->a(Ljava/lang/String;)Lcom/badlogic/gdx/c/a;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/badlogic/gdx/graphics/l;-><init>(Lcom/badlogic/gdx/c/a;)V

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/l;->a()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2}, Lcom/badlogic/gdx/graphics/l;->b()I

    move-result v4

    const/4 v10, 0x2

    div-int/lit8 v11, v4, 0x2

    invoke-static {v2, v3, v11}, Lcom/badlogic/gdx/graphics/g2d/d;->a(Lcom/badlogic/gdx/graphics/l;II)[[Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object v2

    sget-object v4, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v4}, Lcom/badlogic/gdx/h;->b()I

    move-result v12

    sget-object v4, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v4}, Lcom/badlogic/gdx/h;->c()I

    move-result v13

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    new-instance v4, Lcom/google/ctf/game/d;

    const/4 v5, 0x1

    aget-object v6, v2, v5

    aget-object v6, v6, v5

    const/16 v7, 0x10

    invoke-direct {v4, v6, v7, v7, v3}, Lcom/google/ctf/game/d;-><init>(Lcom/badlogic/gdx/graphics/g2d/d;III)V

    iput-object v4, v0, Lcom/google/ctf/game/i;->a:Lcom/google/ctf/game/d;

    iget-object v4, v0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    iget-object v6, v0, Lcom/google/ctf/game/i;->a:Lcom/google/ctf/game/d;

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/ctf/game/d;

    aget-object v6, v2, v5

    const/4 v14, 0x0

    aget-object v6, v6, v14

    add-int/lit8 v8, v3, 0x20

    invoke-direct {v4, v6, v8, v7, v3}, Lcom/google/ctf/game/d;-><init>(Lcom/badlogic/gdx/graphics/g2d/d;III)V

    iput-object v4, v0, Lcom/google/ctf/game/i;->b:Lcom/google/ctf/game/d;

    iget-object v4, v0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    iget-object v6, v0, Lcom/google/ctf/game/i;->b:Lcom/google/ctf/game/d;

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/ctf/game/d;

    aget-object v6, v2, v5

    aget-object v6, v6, v10

    add-int/lit8 v8, v12, -0x10

    sub-int/2addr v8, v3

    invoke-direct {v4, v6, v8, v7, v3}, Lcom/google/ctf/game/d;-><init>(Lcom/badlogic/gdx/graphics/g2d/d;III)V

    iput-object v4, v0, Lcom/google/ctf/game/i;->c:Lcom/google/ctf/game/d;

    iget-object v4, v0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    iget-object v6, v0, Lcom/google/ctf/game/i;->c:Lcom/google/ctf/game/d;

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/ctf/game/d;

    aget-object v5, v2, v5

    const/4 v6, 0x3

    aget-object v5, v5, v6

    add-int/lit8 v6, v11, 0x20

    invoke-direct {v4, v5, v8, v6, v3}, Lcom/google/ctf/game/d;-><init>(Lcom/badlogic/gdx/graphics/g2d/d;III)V

    iput-object v4, v0, Lcom/google/ctf/game/i;->d:Lcom/google/ctf/game/d;

    iget-object v4, v0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    iget-object v5, v0, Lcom/google/ctf/game/i;->d:Lcom/google/ctf/game/d;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lcom/google/ctf/game/i;->f:Ljava/util/List;

    const/4 v15, 0x0

    :goto_0
    sget-object v4, Lcom/google/ctf/game/f;->a:[Lcom/google/ctf/game/f$a;

    array-length v4, v4

    if-ge v15, v4, :cond_0

    rem-int/lit8 v4, v15, 0x4

    mul-int v4, v4, v3

    div-int/lit8 v5, v15, 0x4

    mul-int v5, v5, v11

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    aget-object v7, v2, v14

    aget-object v7, v7, v14

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v7, Lcom/google/ctf/game/f;->a:[Lcom/google/ctf/game/f$a;

    aget-object v7, v7, v15

    invoke-virtual {v1, v7}, Lcom/google/ctf/game/f;->a(Lcom/google/ctf/game/f$a;)Lcom/badlogic/gdx/graphics/g2d/d;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v9, v0, Lcom/google/ctf/game/i;->f:Ljava/util/List;

    new-instance v8, Lcom/google/ctf/game/d;

    div-int/lit8 v7, v12, 0x2

    mul-int/lit8 v16, v3, 0x2

    sub-int v7, v7, v16

    add-int/2addr v7, v4

    div-int/lit8 v4, v13, 0x2

    mul-int/lit8 v16, v11, 0x2

    sub-int v4, v4, v16

    add-int v16, v4, v5

    move-object v4, v8

    move-object v5, v6

    move v6, v7

    move/from16 v7, v16

    move-object v10, v8

    move v8, v3

    move-object v14, v9

    move v9, v15

    invoke-direct/range {v4 .. v9}, Lcom/google/ctf/game/d;-><init>(Ljava/util/List;IIII)V

    invoke-interface {v14, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v15, v15, 0x1

    const/4 v10, 0x2

    const/4 v14, 0x0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/ctf/game/i;->g:Z

    iput v4, v0, Lcom/google/ctf/game/i;->h:I

    return-void
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    return-void
.end method


# virtual methods
.method a()V
    .locals 5

    iget-object v0, p0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ctf/game/d;

    iput-boolean v2, v1, Lcom/google/ctf/game/d;->a:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/ctf/game/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ctf/game/d;

    iput-boolean v2, v1, Lcom/google/ctf/game/d;->a:Z

    goto :goto_1

    :cond_1
    :goto_2
    const/16 v0, 0x14

    if-ge v2, v0, :cond_4

    sget-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/i;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    invoke-interface {v0, v2}, Lcom/badlogic/gdx/i;->a(I)I

    move-result v0

    sget-object v1, Lcom/badlogic/gdx/g;->b:Lcom/badlogic/gdx/h;

    invoke-interface {v1}, Lcom/badlogic/gdx/h;->c()I

    move-result v1

    sget-object v3, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    invoke-interface {v3, v2}, Lcom/badlogic/gdx/i;->b(I)I

    move-result v3

    sub-int/2addr v1, v3

    iget-boolean v3, p0, Lcom/google/ctf/game/i;->g:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/ctf/game/d;

    invoke-virtual {v4, v0, v1}, Lcom/google/ctf/game/d;->a(II)V

    goto :goto_3

    :cond_2
    iget-object v3, p0, Lcom/google/ctf/game/i;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/ctf/game/d;

    invoke-virtual {v4, v0, v1}, Lcom/google/ctf/game/d;->a(II)V

    goto :goto_4

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method a(Lcom/badlogic/gdx/graphics/g2d/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/ctf/game/i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ctf/game/d;

    invoke-virtual {v1, p1}, Lcom/google/ctf/game/d;->a(Lcom/badlogic/gdx/graphics/g2d/b;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ctf/game/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ctf/game/d;

    invoke-virtual {v1, p1}, Lcom/google/ctf/game/d;->a(Lcom/badlogic/gdx/graphics/g2d/b;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method b()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/ctf/game/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ctf/game/d;

    iget-boolean v2, v1, Lcom/google/ctf/game/d;->a:Z

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/ctf/game/i;->i:Lcom/google/ctf/game/f;

    iget v2, p0, Lcom/google/ctf/game/i;->h:I

    iget v1, v1, Lcom/google/ctf/game/d;->b:I

    invoke-virtual {v0, v2, v1}, Lcom/google/ctf/game/f;->a(II)V

    invoke-direct {p0}, Lcom/google/ctf/game/i;->g()V

    :cond_2
    return-void
.end method

.method c()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    const/16 v1, 0x15

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/i;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ctf/game/i;->a:Lcom/google/ctf/game/d;

    iget-boolean v0, v0, Lcom/google/ctf/game/d;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method d(I)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    iput p1, p0, Lcom/google/ctf/game/i;->h:I

    return-void
.end method

.method d()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/i;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ctf/game/i;->b:Lcom/google/ctf/game/d;

    iget-boolean v0, v0, Lcom/google/ctf/game/d;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method e()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/ctf/game/i;->g:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    const/16 v1, 0x3e

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/i;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ctf/game/i;->c:Lcom/google/ctf/game/d;

    iget-boolean v0, v0, Lcom/google/ctf/game/d;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method f()Z
    .locals 2

    sget-object v0, Lcom/badlogic/gdx/g;->d:Lcom/badlogic/gdx/i;

    const/16 v1, 0x34

    invoke-interface {v0, v1}, Lcom/badlogic/gdx/i;->e(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/ctf/game/i;->d:Lcom/google/ctf/game/d;

    iget-boolean v0, v0, Lcom/google/ctf/game/d;->a:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
