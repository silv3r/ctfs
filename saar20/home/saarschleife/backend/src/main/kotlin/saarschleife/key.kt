package saarschleife

fun String.hexStringToByteArray() = ByteArray(this.length / 2) { this.substring(it * 2, it * 2 + 2).toInt(16).toByte() }

// Auto-generated at build time. Rebuild project to change the key.
val secretKey = "876c6fe9980a7be53139a8235e7283ff892c32e06a562d7963c74b00cdb3633e".hexStringToByteArray()
