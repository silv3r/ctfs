(function (root, factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', 'kotlin', 'kotlinx-html-js'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('kotlin'), require('kotlinx-html-js'));
  else {
    if (typeof kotlin === 'undefined') {
      throw new Error("Error loading module 'frontend'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'frontend'.");
    }
    if (typeof this['kotlinx-html-js'] === 'undefined') {
      throw new Error("Error loading module 'frontend'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'frontend'.");
    }
    root.frontend = factory(typeof frontend === 'undefined' ? {} : frontend, kotlin, this['kotlinx-html-js']);
  }
}(this, function (_, Kotlin, $module$kotlinx_html_js) {
  'use strict';
  var RuntimeException_init = Kotlin.kotlin.RuntimeException_init_pdl1vj$;
  var Unit = Kotlin.kotlin.Unit;
  var asList = Kotlin.org.w3c.dom.asList_kt9thq$;
  var getCallableRef = Kotlin.getCallableRef;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var toShort = Kotlin.toShort;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var contains = Kotlin.kotlin.ranges.contains_basjzs$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var equals = Kotlin.equals;
  var Sidenav$Companion = M.Sidenav;
  var Parallax$Companion = M.Parallax;
  var toast = M.toast;
  var ensureNotNull = Kotlin.ensureNotNull;
  var removeClass = Kotlin.kotlin.dom.removeClass_hhb33f$;
  var addClass = Kotlin.kotlin.dom.addClass_hhb33f$;
  var throwUPAE = Kotlin.throwUPAE;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var span = $module$kotlinx_html_js.kotlinx.html.span_6djfml$;
  var i = $module$kotlinx_html_js.kotlinx.html.i_5g1p9k$;
  var set_title = $module$kotlinx_html_js.kotlinx.html.set_title_ueiko3$;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var a = $module$kotlinx_html_js.kotlinx.html.a_gu26kr$;
  var h4 = $module$kotlinx_html_js.kotlinx.html.h4_zdyoc7$;
  var strong = $module$kotlinx_html_js.kotlinx.html.strong_okpg28$;
  var p = $module$kotlinx_html_js.kotlinx.html.p_8pggrc$;
  var div = $module$kotlinx_html_js.kotlinx.html.div_ri36nr$;
  var h5 = $module$kotlinx_html_js.kotlinx.html.h5_aplb7s$;
  var div_0 = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var h6 = $module$kotlinx_html_js.kotlinx.html.h6_e7yr7d$;
  var small = $module$kotlinx_html_js.kotlinx.html.small_69ofui$;
  var blockQuote = $module$kotlinx_html_js.kotlinx.html.blockQuote_1wgk0f$;
  var updateTextFields = M.updateTextFields;
  var toString = Kotlin.toString;
  var throwCCE = Kotlin.throwCCE;
  function HomePage() {
    HomePage_instance = this;
  }
  function HomePage$login$lambda(it) {
    window.location.href = '/visitor';
    return Unit;
  }
  HomePage.prototype.login_0 = function (e) {
    var tmp$, tmp$_0;
    e.preventDefault();
    var form = e.currentTarget;
    tmp$_0 = (tmp$ = form.attributes['action']) != null ? tmp$.value : null;
    if (tmp$_0 == null) {
      throw RuntimeException_init('No action');
    }
    var endpoint = tmp$_0;
    LayoutScripts_getInstance().post_3th7v6$(endpoint, LayoutScripts_getInstance().getFormData_nh6ub4$(form), void 0, HomePage$login$lambda);
  };
  HomePage.prototype.install = function () {
    LayoutScripts_getInstance().install();
    var tmp$;
    tmp$ = asList(document.getElementsByClassName('form-login')).iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      element.addEventListener('submit', getCallableRef('login', function ($receiver, e) {
        return $receiver.login_0(e), Unit;
      }.bind(null, HomePage_getInstance())));
    }
  };
  HomePage.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'HomePage',
    interfaces: []
  };
  var HomePage_instance = null;
  function HomePage_getInstance() {
    if (HomePage_instance === null) {
      new HomePage();
    }
    return HomePage_instance;
  }
  function LayoutScripts() {
    LayoutScripts_instance = this;
  }
  LayoutScripts.prototype.defaultErrorHandler_1endcj$ = function (xhr) {
    console.error(xhr);
    if (!(xhr.responseText.length === 0))
      Message_getInstance().error_61zpoe$(xhr.responseText);
  };
  function LayoutScripts$get$lambda(closure$xhr, closure$success, closure$error) {
    return function (it) {
      if (closure$xhr.readyState === toShort(4)) {
        if (contains(new IntRange(200, 299), closure$xhr.status)) {
          closure$success(closure$xhr);
        }
         else {
          closure$error(closure$xhr);
        }
      }
      return Unit;
    };
  }
  LayoutScripts.prototype.get_0 = function (url, error, success) {
    if (error === void 0)
      error = getCallableRef('defaultErrorHandler', function ($receiver, xhr) {
        return $receiver.defaultErrorHandler_1endcj$(xhr), Unit;
      }.bind(null, LayoutScripts_getInstance()));
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onreadystatechange = LayoutScripts$get$lambda(xhr, success, error);
    xhr.send();
  };
  function LayoutScripts$get$lambda_0(closure$success) {
    return function (xhr) {
      closure$success(JSON.parse(xhr.responseText));
      return Unit;
    };
  }
  LayoutScripts.prototype.get_qx7yuv$ = function (url, error, success) {
    if (error === void 0)
      error = getCallableRef('defaultErrorHandler', function ($receiver, xhr) {
        return $receiver.defaultErrorHandler_1endcj$(xhr), Unit;
      }.bind(null, LayoutScripts_getInstance()));
    this.get_0(url, error, LayoutScripts$get$lambda_0(success));
  };
  function LayoutScripts$post$lambda(closure$xhr, closure$success, closure$error) {
    return function (it) {
      if (closure$xhr.readyState === toShort(4)) {
        if (contains(new IntRange(200, 299), closure$xhr.status)) {
          closure$success(closure$xhr);
        }
         else {
          closure$error(closure$xhr);
        }
      }
      return Unit;
    };
  }
  LayoutScripts.prototype.post_3th7v6$ = function (url, body, error, success) {
    if (error === void 0)
      error = getCallableRef('defaultErrorHandler', function ($receiver, xhr) {
        return $receiver.defaultErrorHandler_1endcj$(xhr), Unit;
      }.bind(null, LayoutScripts_getInstance()));
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = LayoutScripts$post$lambda(xhr, success, error);
    xhr.send(JSON.stringify(body));
  };
  function LayoutScripts$getFormData$ObjectLiteral() {
  }
  LayoutScripts$getFormData$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: []
  };
  LayoutScripts.prototype.getFormData_nh6ub4$ = function (form) {
    var data = new LayoutScripts$getFormData$ObjectLiteral();
    var tmp$;
    tmp$ = asList(form.elements).iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      if (Kotlin.isType(element, HTMLInputElement))
        if (equals(element.type, 'checkbox')) {
          data[element.name] = element.checked;
        }
         else {
          data[element.name] = element.value;
        }
       else if (Kotlin.isType(element, HTMLTextAreaElement))
        data[element.name] = element.value;
    }
    return data;
  };
  function LayoutScripts$logout$lambda(it) {
    window.location.href = '/';
    return Unit;
  }
  LayoutScripts.prototype.logout_0 = function (e) {
    e.preventDefault();
    this.post_3th7v6$('/api/logout', null, void 0, LayoutScripts$logout$lambda);
  };
  LayoutScripts.prototype.install = function () {
    var tmp$;
    tmp$ = asList(document.getElementsByClassName('link-logout')).iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      element.addEventListener('click', getCallableRef('logout', function ($receiver, e) {
        return $receiver.logout_0(e), Unit;
      }.bind(null, LayoutScripts_getInstance())));
    }
    var elements = document.querySelector('.sidenav');
    if (elements != null)
      Sidenav$Companion.init(elements);
    var tmp$_0;
    tmp$_0 = asList(document.getElementsByClassName('parallax')).iterator();
    while (tmp$_0.hasNext()) {
      var element_0 = tmp$_0.next();
      Parallax$Companion.init(element_0);
    }
  };
  LayoutScripts.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'LayoutScripts',
    interfaces: []
  };
  var LayoutScripts_instance = null;
  function LayoutScripts_getInstance() {
    if (LayoutScripts_instance === null) {
      new LayoutScripts();
    }
    return LayoutScripts_instance;
  }
  function MyToastOptions(html, displayLength, classes) {
    if (displayLength === void 0)
      displayLength = 5000;
    if (classes === void 0)
      classes = '';
    this.html = html;
    this.displayLength = displayLength;
    this.classes = classes;
  }
  MyToastOptions.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MyToastOptions',
    interfaces: []
  };
  MyToastOptions.prototype.component1 = function () {
    return this.html;
  };
  MyToastOptions.prototype.component2 = function () {
    return this.displayLength;
  };
  MyToastOptions.prototype.component3 = function () {
    return this.classes;
  };
  MyToastOptions.prototype.copy_58xs4a$ = function (html, displayLength, classes) {
    return new MyToastOptions(html === void 0 ? this.html : html, displayLength === void 0 ? this.displayLength : displayLength, classes === void 0 ? this.classes : classes);
  };
  MyToastOptions.prototype.toString = function () {
    return 'MyToastOptions(html=' + Kotlin.toString(this.html) + (', displayLength=' + Kotlin.toString(this.displayLength)) + (', classes=' + Kotlin.toString(this.classes)) + ')';
  };
  MyToastOptions.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.html) | 0;
    result = result * 31 + Kotlin.hashCode(this.displayLength) | 0;
    result = result * 31 + Kotlin.hashCode(this.classes) | 0;
    return result;
  };
  MyToastOptions.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.html, other.html) && Kotlin.equals(this.displayLength, other.displayLength) && Kotlin.equals(this.classes, other.classes)))));
  };
  function Message() {
    Message_instance = this;
  }
  Message.prototype.success_61zpoe$ = function (message) {
    toast(new MyToastOptions(message, void 0, 'light-green'));
  };
  Message.prototype.error_61zpoe$ = function (message) {
    toast(new MyToastOptions(message, void 0, 'red darken-2'));
  };
  Message.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Message',
    interfaces: []
  };
  var Message_instance = null;
  function Message_getInstance() {
    if (Message_instance === null) {
      new Message();
    }
    return Message_instance;
  }
  function SecretPage() {
    SecretPage_instance = this;
    this.secretName = null;
  }
  SecretPage.prototype.selectSecret_0 = function (e) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    e.preventDefault();
    this.secretName = (tmp$_2 = (tmp$_1 = (tmp$_0 = (tmp$ = e.currentTarget) != null ? tmp$ : null) != null ? tmp$_0.attributes : null) != null ? tmp$_1['data-name'] : null) != null ? tmp$_2.value : null;
    var form = ensureNotNull(document.getElementById('form-secret-find'));
    removeClass(form, ['hide']);
    var input = form.elements['solution'];
    input.value = '';
    input.focus();
  };
  function SecretPage$findSecret$ObjectLiteral() {
  }
  SecretPage$findSecret$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: []
  };
  function SecretPage$findSecret$lambda(it) {
    Message_getInstance().success_61zpoe$(it.responseText);
    return Unit;
  }
  SecretPage.prototype.findSecret_0 = function (e) {
    e.preventDefault();
    var form = ensureNotNull(document.getElementById('form-secret-find'));
    addClass(form, ['hide']);
    var data = new SecretPage$findSecret$ObjectLiteral();
    data['name'] = this.secretName;
    data['solution'] = form.elements['solution'].value;
    LayoutScripts_getInstance().post_3th7v6$('/api/secret/found', data, void 0, SecretPage$findSecret$lambda);
  };
  SecretPage.prototype.install = function () {
    var tmp$;
    LayoutScripts_getInstance().install();
    var tmp$_0;
    tmp$_0 = asList(document.getElementsByClassName('link-secret')).iterator();
    while (tmp$_0.hasNext()) {
      var element = tmp$_0.next();
      element.addEventListener('click', getCallableRef('selectSecret', function ($receiver, e) {
        return $receiver.selectSecret_0(e), Unit;
      }.bind(null, SecretPage_getInstance())));
    }
    (tmp$ = document.getElementById('form-secret-find')) != null ? (tmp$.addEventListener('submit', getCallableRef('findSecret', function ($receiver, e) {
      return $receiver.findSecret_0(e), Unit;
    }.bind(null, SecretPage_getInstance()))), Unit) : null;
  };
  SecretPage.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'SecretPage',
    interfaces: []
  };
  var SecretPage_instance = null;
  function SecretPage_getInstance() {
    if (SecretPage_instance === null) {
      new SecretPage();
    }
    return SecretPage_instance;
  }
  function VisitorPage() {
    VisitorPage_instance = this;
    this.myUsername_0 = '';
    this.selectedVisitor_0 = null;
    this.selectedStatusMessage_0 = null;
    this.statusForm_erotal$_0 = this.statusForm_erotal$_0;
  }
  Object.defineProperty(VisitorPage.prototype, 'statusForm_0', {
    get: function () {
      if (this.statusForm_erotal$_0 == null)
        return throwUPAE('statusForm');
      return this.statusForm_erotal$_0;
    },
    set: function (statusForm) {
      this.statusForm_erotal$_0 = statusForm;
    }
  });
  function VisitorPage$displayVisitor$lambda$lambda($receiver) {
    var $receiver_0 = $receiver.attributes;
    var key = 'data-badge-caption';
    var value = "that's you!";
    $receiver_0.put_xwzc9p$(key, value);
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('supervisor_account');
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_0($receiver) {
    i($receiver, 'material-icons left', VisitorPage$displayVisitor$lambda$lambda$lambda);
    var $receiver_0 = $receiver.attributes;
    var key = 'data-badge-caption';
    var value = 'You are friends!';
    $receiver_0.put_xwzc9p$(key, value);
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda$lambda_0($receiver) {
    $receiver.unaryPlus_pdl1vz$('supervisor_account');
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_1($receiver) {
    set_title($receiver, 'Add as friend');
    set_onClickFunction($receiver, getCallableRef('addFriend', function ($receiver, e) {
      return $receiver.addFriend_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    i($receiver, 'material-icons', VisitorPage$displayVisitor$lambda$lambda$lambda_0);
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_2(closure$visitor) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('Visitor: ' + closure$visitor.username);
      return Unit;
    };
  }
  function VisitorPage$displayVisitor$lambda$lambda$lambda_1($receiver) {
    $receiver.unaryPlus_pdl1vz$('Points: ');
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_3(closure$visitor) {
    return function ($receiver) {
      strong($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda$lambda_1);
      $receiver.unaryPlus_pdl1vz$(closure$visitor.points.toString());
      return Unit;
    };
  }
  function VisitorPage$displayVisitor$lambda$lambda$lambda_2($receiver) {
    $receiver.unaryPlus_pdl1vz$('Status: ');
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_4($receiver) {
    strong($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda$lambda_2);
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_5($receiver) {
    var $receiver_0 = $receiver.attributes;
    var value = 'status-message';
    $receiver_0.put_xwzc9p$('id', value);
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda$lambda_6($receiver) {
    $receiver.unaryPlus_pdl1vz$('No visitor found');
    return Unit;
  }
  function VisitorPage$displayVisitor$lambda(closure$visitor, this$VisitorPage) {
    return function ($receiver) {
      if (closure$visitor != null) {
        if (equals(closure$visitor.username, this$VisitorPage.myUsername_0)) {
          span($receiver, 'new badge light-blue', VisitorPage$displayVisitor$lambda$lambda);
        }
         else if (closure$visitor.isFriend) {
          span($receiver, 'new badge green', VisitorPage$displayVisitor$lambda$lambda_0);
        }
         else if (!closure$visitor.isFriend) {
          a($receiver, void 0, void 0, 'btn-floating btn-large halfway-fab top-fab waves-effect waves-light light-blue', VisitorPage$displayVisitor$lambda$lambda_1);
        }
        h4($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda_2(closure$visitor));
        p($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda_3(closure$visitor));
        p($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda_4);
        div($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda_5);
      }
       else {
        h5($receiver, void 0, VisitorPage$displayVisitor$lambda$lambda_6);
      }
      return Unit;
    };
  }
  VisitorPage.prototype.displayVisitor_c0dspo$ = function (visitor) {
    var div = div_0(get_create(document), 'card-content', VisitorPage$displayVisitor$lambda(visitor, this));
    var container = document.getElementById('visitor-content');
    container != null ? (container.innerHTML = '') : null;
    container != null ? (container.append(div), Unit) : null;
    this.selectedVisitor_0 = visitor;
    this.statusForm_0.classList.add('hide');
  };
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda(closure$statusMessage) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$statusMessage.title);
      return Unit;
    };
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda$lambda(closure$statusMessage) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('by ' + closure$statusMessage.writtenBy);
      return Unit;
    };
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda$lambda_0($receiver) {
    var $receiver_0 = $receiver.attributes;
    var key = 'data-badge-caption';
    $receiver_0.put_xwzc9p$(key, 'public');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda$lambda_1($receiver) {
    var $receiver_0 = $receiver.attributes;
    var key = 'data-badge-caption';
    $receiver_0.put_xwzc9p$(key, 'private');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda_0(closure$statusMessage) {
    return function ($receiver) {
      small($receiver, void 0, VisitorPage$displayStatusMessage$lambda$lambda$lambda$lambda(closure$statusMessage));
      if (closure$statusMessage.isPublic) {
        span($receiver, 'new badge light-blue nofloat', VisitorPage$displayStatusMessage$lambda$lambda$lambda$lambda_0);
      }
       else {
        span($receiver, 'new badge red nofloat', VisitorPage$displayStatusMessage$lambda$lambda$lambda$lambda_1);
      }
      return Unit;
    };
  }
  function VisitorPage$displayStatusMessage$lambda$lambda(closure$statusMessage) {
    return function ($receiver) {
      h6($receiver, void 0, VisitorPage$displayStatusMessage$lambda$lambda$lambda(closure$statusMessage));
      $receiver.unaryPlus_pdl1vz$(closure$statusMessage.text);
      p($receiver, void 0, VisitorPage$displayStatusMessage$lambda$lambda$lambda_0(closure$statusMessage));
      return Unit;
    };
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda_1($receiver) {
    $receiver.unaryPlus_pdl1vz$('delete');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda_0($receiver) {
    i($receiver, 'material-icons', VisitorPage$displayStatusMessage$lambda$lambda$lambda_1);
    set_onClickFunction($receiver, getCallableRef('removeStatusMessage', function ($receiver, e) {
      return $receiver.removeStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda_2($receiver) {
    $receiver.unaryPlus_pdl1vz$('edit');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda_1($receiver) {
    i($receiver, 'material-icons', VisitorPage$displayStatusMessage$lambda$lambda$lambda_2);
    set_onClickFunction($receiver, getCallableRef('editStatusMessage', function ($receiver, e) {
      return $receiver.editStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda_3($receiver) {
    $receiver.unaryPlus_pdl1vz$('share');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda_2($receiver) {
    i($receiver, 'material-icons', VisitorPage$displayStatusMessage$lambda$lambda$lambda_3);
    set_onClickFunction($receiver, getCallableRef('spreadStatusMessage', function ($receiver, e) {
      return $receiver.spreadStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda_3($receiver) {
    $receiver.unaryPlus_pdl1vz$('no status message');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda$lambda_4($receiver) {
    $receiver.unaryPlus_pdl1vz$('add');
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda$lambda_4($receiver) {
    i($receiver, 'material-icons', VisitorPage$displayStatusMessage$lambda$lambda$lambda_4);
    set_onClickFunction($receiver, getCallableRef('editStatusMessage', function ($receiver, e) {
      return $receiver.editStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    return Unit;
  }
  function VisitorPage$displayStatusMessage$lambda(closure$statusMessage, this$VisitorPage) {
    return function ($receiver) {
      var tmp$, tmp$_0;
      if (closure$statusMessage != null) {
        blockQuote($receiver, void 0, VisitorPage$displayStatusMessage$lambda$lambda(closure$statusMessage));
        if (equals((tmp$ = this$VisitorPage.selectedVisitor_0) != null ? tmp$.username : null, this$VisitorPage.myUsername_0)) {
          a($receiver, void 0, void 0, 'btn-floating right red', VisitorPage$displayStatusMessage$lambda$lambda_0);
          a($receiver, void 0, void 0, 'btn-floating right waves-effect waves-light light-blue', VisitorPage$displayStatusMessage$lambda$lambda_1);
        }
         else {
          a($receiver, void 0, void 0, 'btn-floating right waves-effect waves-light light-blue', VisitorPage$displayStatusMessage$lambda$lambda_2);
        }
      }
       else {
        span($receiver, void 0, VisitorPage$displayStatusMessage$lambda$lambda_3);
        if (equals((tmp$_0 = this$VisitorPage.selectedVisitor_0) != null ? tmp$_0.username : null, this$VisitorPage.myUsername_0))
          a($receiver, void 0, void 0, 'btn-floating right waves-effect waves-light red', VisitorPage$displayStatusMessage$lambda$lambda_4);
      }
      return Unit;
    };
  }
  VisitorPage.prototype.displayStatusMessage_n1auwl$ = function (statusMessage) {
    var tag = div_0(get_create(document), void 0, VisitorPage$displayStatusMessage$lambda(statusMessage, this));
    var container = document.getElementById('status-message');
    container != null ? (container.innerHTML = '') : null;
    container != null ? (container.append(tag), Unit) : null;
    this.selectedStatusMessage_0 = statusMessage;
  };
  function VisitorPage$displayInaccessibleStatusMessage$lambda$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('delete');
    return Unit;
  }
  function VisitorPage$displayInaccessibleStatusMessage$lambda$lambda($receiver) {
    i($receiver, 'material-icons', VisitorPage$displayInaccessibleStatusMessage$lambda$lambda$lambda);
    set_onClickFunction($receiver, getCallableRef('removeStatusMessage', function ($receiver, e) {
      return $receiver.removeStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    return Unit;
  }
  function VisitorPage$displayInaccessibleStatusMessage$lambda$lambda$lambda_0($receiver) {
    $receiver.unaryPlus_pdl1vz$('edit');
    return Unit;
  }
  function VisitorPage$displayInaccessibleStatusMessage$lambda$lambda_0($receiver) {
    i($receiver, 'material-icons', VisitorPage$displayInaccessibleStatusMessage$lambda$lambda$lambda_0);
    set_onClickFunction($receiver, getCallableRef('editStatusMessage', function ($receiver, e) {
      return $receiver.editStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    return Unit;
  }
  function VisitorPage$displayInaccessibleStatusMessage$lambda$lambda_1($receiver) {
    $receiver.unaryPlus_pdl1vz$("status message inaccessible (you're not a friend of the author)");
    return Unit;
  }
  function VisitorPage$displayInaccessibleStatusMessage$lambda(this$VisitorPage) {
    return function ($receiver) {
      var tmp$;
      if (equals((tmp$ = this$VisitorPage.selectedVisitor_0) != null ? tmp$.username : null, this$VisitorPage.myUsername_0)) {
        a($receiver, void 0, void 0, 'btn-floating right red', VisitorPage$displayInaccessibleStatusMessage$lambda$lambda);
        a($receiver, void 0, void 0, 'btn-floating right waves-effect waves-light light-blue', VisitorPage$displayInaccessibleStatusMessage$lambda$lambda_0);
      }
      i($receiver, void 0, VisitorPage$displayInaccessibleStatusMessage$lambda$lambda_1);
      return Unit;
    };
  }
  VisitorPage.prototype.displayInaccessibleStatusMessage = function () {
    var tag = div_0(get_create(document), void 0, VisitorPage$displayInaccessibleStatusMessage$lambda(this));
    var container = document.getElementById('status-message');
    container != null ? (container.innerHTML = '') : null;
    container != null ? (container.append(tag), Unit) : null;
    this.selectedStatusMessage_0 = null;
  };
  VisitorPage.prototype.searchVisitor_9ojx7i$ = function (e) {
    var tmp$, tmp$_0;
    e.preventDefault();
    var username = (tmp$_0 = (tmp$ = e.currentTarget.elements[0]) != null ? tmp$ : null) != null ? tmp$_0.value : null;
    this.searchVisitor_0(username);
  };
  function VisitorPage$searchVisitor$lambda$lambda(this$VisitorPage) {
    return function (xhr) {
      if (xhr.status === toShort(403)) {
        this$VisitorPage.displayInaccessibleStatusMessage();
      }
       else {
        LayoutScripts_getInstance().defaultErrorHandler_1endcj$(xhr);
      }
      return Unit;
    };
  }
  function VisitorPage$searchVisitor$lambda(this$VisitorPage) {
    return function (visitor) {
      var tmp$, tmp$_0, tmp$_1;
      this$VisitorPage.displayVisitor_c0dspo$(visitor);
      if ((visitor != null ? visitor.statusMessage : null) === true) {
        tmp$_1 = LayoutScripts_getInstance();
        tmp$ = '/api/statusmessage/' + visitor.username;
        tmp$_0 = getCallableRef('displayStatusMessage', function ($receiver, statusMessage) {
          return $receiver.displayStatusMessage_n1auwl$(statusMessage), Unit;
        }.bind(null, this$VisitorPage));
        tmp$_1.get_qx7yuv$(tmp$, VisitorPage$searchVisitor$lambda$lambda(this$VisitorPage), tmp$_0);
      }
       else {
        this$VisitorPage.displayStatusMessage_n1auwl$(null);
      }
      return Unit;
    };
  }
  VisitorPage.prototype.searchVisitor_0 = function (username) {
    this.getVisitor_0(username, VisitorPage$searchVisitor$lambda(this));
  };
  VisitorPage.prototype.clickVisitor_0 = function (e) {
    var tmp$;
    e.preventDefault();
    var username = (tmp$ = e.currentTarget.attributes['data-username']) != null ? tmp$.value : null;
    var searchField = document.getElementById('visitor-search-input');
    this.searchVisitor_0(username);
    searchField.value = username != null ? username : '';
    searchField.scrollIntoView();
    updateTextFields();
  };
  function VisitorPage$getVisitor$lambda(closure$cb) {
    return function (xhr) {
      if (xhr.status === toShort(404))
        closure$cb(null);
      else
        LayoutScripts_getInstance().defaultErrorHandler_1endcj$(xhr);
      return Unit;
    };
  }
  VisitorPage.prototype.getVisitor_0 = function (username, cb) {
    LayoutScripts_getInstance().get_qx7yuv$(username != null ? '/api/visitor/' + toString(username) : '/api/me', VisitorPage$getVisitor$lambda(cb), cb);
  };
  function VisitorPage$addFriend$lambda(this$VisitorPage) {
    return function (it) {
      var tmp$, tmp$_0, tmp$_1;
      Message_getInstance().success_61zpoe$('You are now a friend of ' + toString((tmp$ = this$VisitorPage.selectedVisitor_0) != null ? tmp$.username : null));
      tmp$_1 = (tmp$_0 = this$VisitorPage.selectedVisitor_0) != null ? tmp$_0.username : null;
      this$VisitorPage.searchVisitor_0(tmp$_1);
      return Unit;
    };
  }
  VisitorPage.prototype.addFriend_9ojx7i$ = function (e) {
    var tmp$;
    e.preventDefault();
    LayoutScripts_getInstance().post_3th7v6$('/api/visitor/friends/' + ((tmp$ = this.selectedVisitor_0) != null ? tmp$.username : null), null, void 0, VisitorPage$addFriend$lambda(this));
  };
  VisitorPage.prototype.editStatusMessage_9ojx7i$ = function (e) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9, tmp$_10, tmp$_11, tmp$_12;
    e.preventDefault();
    (tmp$_2 = (tmp$_1 = this.statusForm_0.elements['title']) != null ? tmp$_1 : null) != null ? (tmp$_2.value = (tmp$_0 = (tmp$ = this.selectedStatusMessage_0) != null ? tmp$.title : null) != null ? tmp$_0 : '') : null;
    (tmp$_6 = (tmp$_5 = this.statusForm_0.elements['text']) != null ? tmp$_5 : null) != null ? (tmp$_6.value = (tmp$_4 = (tmp$_3 = this.selectedStatusMessage_0) != null ? tmp$_3.text : null) != null ? tmp$_4 : '') : null;
    (tmp$_10 = (tmp$_9 = this.statusForm_0.elements['isPublic']) != null ? tmp$_9 : null) != null ? (tmp$_10.checked = (tmp$_8 = (tmp$_7 = this.selectedStatusMessage_0) != null ? tmp$_7.isPublic : null) != null ? tmp$_8 : false) : null;
    this.statusForm_0.classList.remove('hide');
    updateTextFields();
    (tmp$_12 = (tmp$_11 = this.statusForm_0.elements['title']) != null ? tmp$_11 : null) != null ? (tmp$_12.focus(), Unit) : null;
  };
  function VisitorPage$submitStatusMessage$lambda(this$VisitorPage) {
    return function (it) {
      Message_getInstance().success_61zpoe$('Status message saved');
      this$VisitorPage.searchVisitor_0(null);
      return Unit;
    };
  }
  VisitorPage.prototype.submitStatusMessage_9ojx7i$ = function (e) {
    var tmp$, tmp$_0;
    e.preventDefault();
    var form = e.currentTarget;
    tmp$_0 = (tmp$ = form.attributes['action']) != null ? tmp$.value : null;
    if (tmp$_0 == null) {
      throw RuntimeException_init('No action');
    }
    var endpoint = tmp$_0;
    LayoutScripts_getInstance().post_3th7v6$(endpoint, LayoutScripts_getInstance().getFormData_nh6ub4$(form), void 0, VisitorPage$submitStatusMessage$lambda(this));
  };
  function VisitorPage$removeStatusMessage$lambda(this$VisitorPage) {
    return function (it) {
      Message_getInstance().success_61zpoe$('Status message removed');
      this$VisitorPage.searchVisitor_0(null);
      return Unit;
    };
  }
  VisitorPage.prototype.removeStatusMessage_9ojx7i$ = function (e) {
    e.preventDefault();
    LayoutScripts_getInstance().post_3th7v6$('/api/statusmessage/remove', null, void 0, VisitorPage$removeStatusMessage$lambda(this));
  };
  function VisitorPage$spreadStatusMessage$lambda(this$VisitorPage) {
    return function (it) {
      Message_getInstance().success_61zpoe$('You shared this status message');
      this$VisitorPage.searchVisitor_0(null);
      return Unit;
    };
  }
  VisitorPage.prototype.spreadStatusMessage_9ojx7i$ = function (e) {
    var tmp$;
    e.preventDefault();
    LayoutScripts_getInstance().post_3th7v6$('/api/statusmessage/spread/' + toString((tmp$ = this.selectedVisitor_0) != null ? tmp$.username : null), null, void 0, VisitorPage$spreadStatusMessage$lambda(this));
  };
  VisitorPage.prototype.install = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    LayoutScripts_getInstance().install();
    this.myUsername_0 = (tmp$_2 = (tmp$_1 = (tmp$_0 = (tmp$ = document.body) != null ? tmp$.attributes : null) != null ? tmp$_0['data-my-username'] : null) != null ? tmp$_1.value : null) != null ? tmp$_2 : '';
    this.statusForm_0 = Kotlin.isType(tmp$_3 = document.getElementsByClassName('form-statusmessage-set')[0], HTMLFormElement) ? tmp$_3 : throwCCE();
    this.statusForm_0.addEventListener('submit', getCallableRef('submitStatusMessage', function ($receiver, e) {
      return $receiver.submitStatusMessage_9ojx7i$(e), Unit;
    }.bind(null, VisitorPage_getInstance())));
    var tmp$_4;
    tmp$_4 = asList(document.getElementsByClassName('form-visitor-search')).iterator();
    while (tmp$_4.hasNext()) {
      var element = tmp$_4.next();
      element.addEventListener('submit', getCallableRef('searchVisitor', function ($receiver, e) {
        return $receiver.searchVisitor_9ojx7i$(e), Unit;
      }.bind(null, VisitorPage_getInstance())));
    }
    var tmp$_5;
    tmp$_5 = asList(document.getElementsByClassName('link-visitor')).iterator();
    while (tmp$_5.hasNext()) {
      var element_0 = tmp$_5.next();
      element_0.addEventListener('click', getCallableRef('clickVisitor', function ($receiver, e) {
        return $receiver.clickVisitor_0(e), Unit;
      }.bind(null, VisitorPage_getInstance())));
    }
    this.searchVisitor_0(null);
  };
  VisitorPage.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'VisitorPage',
    interfaces: []
  };
  var VisitorPage_instance = null;
  function VisitorPage_getInstance() {
    if (VisitorPage_instance === null) {
      new VisitorPage();
    }
    return VisitorPage_instance;
  }
  function ClientVisitor(username, points, statusMessage, isFriend) {
    this.username = username;
    this.points = points;
    this.statusMessage = statusMessage;
    this.isFriend = isFriend;
  }
  ClientVisitor.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ClientVisitor',
    interfaces: []
  };
  ClientVisitor.prototype.component1 = function () {
    return this.username;
  };
  ClientVisitor.prototype.component2 = function () {
    return this.points;
  };
  ClientVisitor.prototype.component3 = function () {
    return this.statusMessage;
  };
  ClientVisitor.prototype.component4 = function () {
    return this.isFriend;
  };
  ClientVisitor.prototype.copy_m20oq8$ = function (username, points, statusMessage, isFriend) {
    return new ClientVisitor(username === void 0 ? this.username : username, points === void 0 ? this.points : points, statusMessage === void 0 ? this.statusMessage : statusMessage, isFriend === void 0 ? this.isFriend : isFriend);
  };
  ClientVisitor.prototype.toString = function () {
    return 'ClientVisitor(username=' + Kotlin.toString(this.username) + (', points=' + Kotlin.toString(this.points)) + (', statusMessage=' + Kotlin.toString(this.statusMessage)) + (', isFriend=' + Kotlin.toString(this.isFriend)) + ')';
  };
  ClientVisitor.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.username) | 0;
    result = result * 31 + Kotlin.hashCode(this.points) | 0;
    result = result * 31 + Kotlin.hashCode(this.statusMessage) | 0;
    result = result * 31 + Kotlin.hashCode(this.isFriend) | 0;
    return result;
  };
  ClientVisitor.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.username, other.username) && Kotlin.equals(this.points, other.points) && Kotlin.equals(this.statusMessage, other.statusMessage) && Kotlin.equals(this.isFriend, other.isFriend)))));
  };
  function ClientStatusMessage(title, text, isPublic, writtenBy) {
    this.title = title;
    this.text = text;
    this.isPublic = isPublic;
    this.writtenBy = writtenBy;
  }
  ClientStatusMessage.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ClientStatusMessage',
    interfaces: []
  };
  ClientStatusMessage.prototype.component1 = function () {
    return this.title;
  };
  ClientStatusMessage.prototype.component2 = function () {
    return this.text;
  };
  ClientStatusMessage.prototype.component3 = function () {
    return this.isPublic;
  };
  ClientStatusMessage.prototype.component4 = function () {
    return this.writtenBy;
  };
  ClientStatusMessage.prototype.copy_f372nr$ = function (title, text, isPublic, writtenBy) {
    return new ClientStatusMessage(title === void 0 ? this.title : title, text === void 0 ? this.text : text, isPublic === void 0 ? this.isPublic : isPublic, writtenBy === void 0 ? this.writtenBy : writtenBy);
  };
  ClientStatusMessage.prototype.toString = function () {
    return 'ClientStatusMessage(title=' + Kotlin.toString(this.title) + (', text=' + Kotlin.toString(this.text)) + (', isPublic=' + Kotlin.toString(this.isPublic)) + (', writtenBy=' + Kotlin.toString(this.writtenBy)) + ')';
  };
  ClientStatusMessage.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.title) | 0;
    result = result * 31 + Kotlin.hashCode(this.text) | 0;
    result = result * 31 + Kotlin.hashCode(this.isPublic) | 0;
    result = result * 31 + Kotlin.hashCode(this.writtenBy) | 0;
    return result;
  };
  ClientStatusMessage.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.title, other.title) && Kotlin.equals(this.text, other.text) && Kotlin.equals(this.isPublic, other.isPublic) && Kotlin.equals(this.writtenBy, other.writtenBy)))));
  };
  Object.defineProperty(_, 'HomePage', {
    get: HomePage_getInstance
  });
  Object.defineProperty(_, 'LayoutScripts', {
    get: LayoutScripts_getInstance
  });
  _.MyToastOptions = MyToastOptions;
  Object.defineProperty(_, 'Message', {
    get: Message_getInstance
  });
  Object.defineProperty(_, 'SecretPage', {
    get: SecretPage_getInstance
  });
  Object.defineProperty(_, 'VisitorPage', {
    get: VisitorPage_getInstance
  });
  _.ClientVisitor = ClientVisitor;
  _.ClientStatusMessage = ClientStatusMessage;
  Kotlin.defineModule('frontend', _);
  return _;
}));

//# sourceMappingURL=frontend.js.map
