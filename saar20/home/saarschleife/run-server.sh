#!/usr/bin/env bash
if [ ! -f /home/saarschleife/backend/build/libs/Saarschleife-Server.jar ]; then
    echo "Building Server..."
    pushd .
    cd /home/saarschleife/backend/
    gradle shadowJar --no-daemon
    gradle --stop
    popd
fi

exec java -jar /home/saarschleife/backend/build/libs/Saarschleife-Server.jar
