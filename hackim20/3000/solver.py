import angr
import sys
from pwn import *
import binascii

def write_file(dat):
    fil = open("inp","w")
    fil.write(dat)
    fil.close()


def solve(argv):
    proj = angr.Project("/home/silv3r/Documents/re_stuff/ctfs/linux/hackim20/3000/"+argv)
    ini_state = proj.factory.entry_state()

    sim = proj.factory.simgr(ini_state)

    reach = 0x804952c
    #no_reach = 0x80494fc
    #sim.explore(find = reach)
    sim.explore(find=lambda s: b"Well done" in s.posix.dumps(1),avoid=lambda b: b"You have failed" in b.posix.dumps(1))
    if sim.found:
        solution_state = sim.found[0]
        sol = solution_state.posix.dumps(sys.stdin.fileno())
        return sol
    else:
        print("Loser!")

dic={}
def main():
    for i in range(1,5):
        a = binascii.b2a_base64(solve(str(i)+".bin"))
        print(i,"---------",a)
        dic[i]=a
    print(dic)

if __name__ == '__main__':
    #main(sys.argv)
    #main()
    print(binascii.b2a_base64(solve("1.bin")))
