import angr
import sys
from pwn import *
import binascii

def write_file(dat):
    fil = open("inp","w")
    fil.write(dat)
    fil.close()


def solve(argv):
    proj = angr.Project("/home/silv3r/Documents/re_stuff/ctfs/hackim20/3000/"+argv)
    ini_state = proj.factory.entry_state()

    sim = proj.factory.simgr(ini_state)

    reach = 0x804952c
    #no_reach = 0x80494fc
    #sim.explore(find = reach)
    sim.explore(find=lambda s: b"Well done" in s.posix.dumps(1))
    if sim.found:
        solution_state = sim.found[0]
        sol = solution_state.posix.dumps(sys.stdin.fileno())
        return sol
    else:
        print("Loser!")

def main():
    io = remote("re.ctf.nullcon.net" ,1234)
    while(True):
        fname = str(io.recvuntil(">")).replace("\\n","").replace(">","").replace("b'","").replace("'","")
        print(fname)
        io.send(binascii.b2a_base64(solve(fname)))
        print(io.recvline())

if __name__ == '__main__':
    #main(sys.argv)
    main()
