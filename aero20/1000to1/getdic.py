import gdb
import binascii
import subprocess
from os import listdir
from os.path import isfile, join

add_addr = 0x0000555555555301
xor_addr = 0x0000555555555304
sub_addr = 0x0000555555555307

tok_list = {}
count=0
def solve(filename):
    global count
    gdb.execute("set disassembly-flavor intel")
    gdb.execute("set pagination off")
    gdb.execute("file PATH"+filename)
    gdb.execute("r < inp")
    gdb.execute("b*0x5555555552ef")
    
    xor = gdb.execute("x/i "+str(xor_addr),to_string=True).split(",")[1]
    add = gdb.execute("x/i "+str(add_addr),to_string=True).split(",")[1]
    sub = gdb.execute("x/i "+str(sub_addr),to_string=True).split("-")[1].replace("]","")
    
    gdb.execute("r < inp")
    a = gdb.execute("x/4gx $rbp-0x30",to_string=True).split("\n")[:2]
    fin=b""
    for i in a:
        fin
        temp=i.split(":")[1].split("\t0x")
        for i in temp:
            fin+=binascii.unhexlify(i)[::-1]
    f=""
    for i in fin:
        f+=chr(((i+int(sub,16))^int(xor,16))-int(add,16))
    print(count,"---",filename,"----",f)
    count = count +1
    return f

def main():
    onlyfiles = [f for f in listdir("PATH") if isfile(join("PATH", f))]
    for i in onlyfiles:
        temp = solve(i)
        tok_list[i] = temp
        
main()

print(tok_list)

fil = open("finaldic","w")
fil.write(str(tok_list))
fil.close()