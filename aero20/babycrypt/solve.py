from itertools import cycle

def xor(a,b):
    return ''.join(chr((ord(i)^ord(j))+ord(j)) for i,j in zip(a,cycle(b)))


