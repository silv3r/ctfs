#!/bin/bash

for i in {a..z}
do
	echo -n $i > inp
	./luajit -jdump=T -O0 ../../task.lua ../../lua_task_data < inp &> inp$i
done
