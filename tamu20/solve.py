import angr
import sys
from pwn import *
import binascii
import logging

logging.getLogger('angr').setLevel('DEBUG')

def solve(argv):
    proj = angr.Project("/home/silv3r/Documents/re_stuff/ctfs/tamu20/"+argv)
    ini_state = proj.factory.entry_state()

    sim = proj.factory.simgr(ini_state)

    sim.explore(find=lambda s: b"Correct!" in s.posix.dumps(1),avoid=lambda b: b"Oops" in b.posix.dumps(1))
    if sim.found:
        solution_state = sim.found[0]
        sol = solution_state.posix.dumps(sys.stdin.fileno())
        return sol
    else:
        print("Loser!")

if __name__ == '__main__':
    ans = solve("splatter-calc")
    p = remote("challenges.tamuctf.com", 4322)
    p.recv()
    p.send(ans)
    p.interactive()
