import angr
import sys
from pwn import *
import binascii
import logging

logging.getLogger('angr').setLevel('DEBUG')

def solve(argv):
    proj = angr.Project("/home/silv3r/Documents/re_stuff/ctfs/tamu20/"+argv)
    ini_state = proj.factory.entry_state(angr.options.unicorn)

    sim = proj.factory.simgr(ini_state)

    sim.explore(find=0x405ADA,avoid=0x405B62)
    if sim.found:
        solution_state = sim.found[0]
        sol = solution_state.posix.dumps(sys.stdin.fileno())
        return sol
    else:
        print("Loser!")

if __name__ == '__main__':
    ans = solve("splatter-calc")
    print(ans)
