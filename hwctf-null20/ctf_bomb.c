#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

// Should the toy be used?
#define DRIVEBOMB 1
#define BRIGHTNESS 20
// DEBUG will set a fixed challenge: just swap first two wires
#define DEBUG 0
// Dummy flag to generate HEX to be reversed?
#define DUMMY 0
// How many times to cycle the flag banner?
#define FLAGCYCLE 2


#define RGBPIN 13
#define BTIME_PIN 10
// TRIG_PIN needs PWM: pins 3, 5, 6, 9, 10, and 11
// Let's share it with BTIME
#define TRIG_PIN 10
#define BSTART_PIN 11
#define BFINISH_PIN 12
#define ABUTTON_PIN 0
#define ALED_PIN 1
#define ARANDOM_PIN 5
uint8_t randNumber[5];
uint8_t wiring[10];
uint8_t original_wiring[10]={5, 6, 7, 8, 9, 0, 1, 2, 3, 4};
uint8_t debug_wiring[10]={6, 5, 7, 8, 9, 1, 0, 2, 3, 4}; // swap first two wires

#if DUMMY
// "Flag goes here"
uint8_t flag[50]={0x46, 0x6c, 0x61, 0x67, 0x20, 0x67, 0x6f, 0x65, 0x73, 0x20, 0x68, 0x65, 0x72, 0x65, 0x0a};
#else
// yep the real flag goes here...
#endif

uint8_t brightness;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(40, RGBPIN, NEO_GRB + NEO_KHZ800);

void setup() {
 uint8_t i;
 // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
 #if defined (__AVR_ATtiny85__)
   if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
 #endif
 // End of trinket special code
 /*Serial.begin(9600); // Serial interferes with pins 0 & 1 so with check_wiring(), use it carefully*/
 strip.begin();
 strip.show(); // Initialize all pixels to 'off'

 pinMode(BSTART_PIN, INPUT);
 digitalWrite(BSTART_PIN, LOW);
 pinMode(BFINISH_PIN, INPUT);
 digitalWrite(BFINISH_PIN, LOW);
 pinMode(BTIME_PIN, INPUT);
 digitalWrite(BTIME_PIN, LOW);
 randomSeed((analogRead(ARANDOM_PIN) << 8) + analogRead(ARANDOM_PIN));
 for (i=0;i<10;i++) {
   pinMode(i, INPUT);
   digitalWrite(i, HIGH);
 }

 // first time is just to wake up timer:
 // default is 1m
 timerAdd30s();
// The problem of setting it here is that resetting Arduino alone is not enough, the bomb remembers the timer
// Better to configure timer manually
// timerAdd30s();
// timerAdd30s();
}
void loop() {
 uint16_t i;
 Serial.println(F("Wait for initial wiring..."));
 brightness=BRIGHTNESS;
 strip.setBrightness(brightness);
 while(check_wiring(original_wiring)!=1) {
   colorWipe(strip.Color(0, 0, 0), 0);
   delay(200);
   colorWipe(strip.Color(255, 0, 0), 0);
   delay(200);
 }
 Serial.println(F("Entertaining till someone presses the button..."));
 rainbowWait(5);
#if DRIVEBOMB
 Serial.println(F("Starting the bomb timer..."));
 timerStart();
#endif
 Serial.println(F("Picking and showing internal randoms, shuffling wiring..."));
 update_wiring();
 colorSet(strip.Color(255, 0, 0), randNumber, 50);
 Serial.println(F("Waiting for first wire to be removed..."));
 while((check_wiring(original_wiring)==1) && (analogRead(ALED_PIN) > 100));
 
 Serial.println(F("Now the real game started..."));
 int result, prev_result=0;
 while(1){
  fireFrame();
  if (brightness>BRIGHTNESS) {
    brightness-=10;
    strip.setBrightness(brightness);
  }
  Serial.println(F("Checking wiring..."));
#if DEBUG
  result = check_wiring(debug_wiring);
#else
  result = check_wiring(wiring);
#endif
  Serial.println(result);
  if ((result == -1) && (prev_result != -1)) { // wiring finished, but wrong
  Serial.println(F("wrong"));
    trigger();
  }
  if (result == 1) { // wiring finished and correct
  Serial.println(F("correct"));
    defused();
    colorWipe(strip.Color(0, 0, 0), 0);
    delay(1000);
    break;
  }

  if ((analogRead(ABUTTON_PIN) < 100)||(analogRead(ALED_PIN) < 100))break;
  prev_result=result;
 }
}

void fireFrame(){
  uint8_t i;
  for(i=0; i<strip.numPixels(); i++) {
    int flicker = random(0,150);
    int r1 = 226-flicker;
    int g1 = 121-flicker;
    int b1 = 35-flicker;
    if(g1<0) g1=0;
    if(r1<0) r1=0;
    if(b1<0) b1=0;
    strip.setPixelColor(i,r1,g1, b1);
  }
  strip.show();
  delay(random(10,113));
}


int check_wiring(uint8_t w[10]){
 uint8_t i, j;
 uint8_t tmpw[10];
 for (i=0;i<10;i++) {
   tmpw[i]=0xff;
 }
 for (i=0;i<9;i++) {
   pinMode(i, OUTPUT);
   digitalWrite(i, LOW);
   for (j=i+1;j<10;j++) {
    if (digitalRead(j)==0) {
     tmpw[i]=j;
     tmpw[j]=i;
    }
   }
   pinMode(i, INPUT);
   digitalWrite(i, HIGH);
 }
 for (i=0;i<10;i++) {
   if (tmpw[i]==0xff) return 0;
 }
 for (i=0;i<10;i++) {
   if (tmpw[i]!=w[i]) return -1;
 }
 return 1; 
}

void update_wiring(){
 uint8_t i, off;
 for (i=0;i<5;i++) {
   randNumber[i] = random(256);
 } 

 // Oops, we lost the interesting part of the source code :(
 // ...wiring[]=...

 // don't leak the flag so easily...
 uint8_t diff=0; 
 for (i=0;i<10;i++) {
   diff|=wiring[i]^original_wiring[i];
 }
 if (diff==0) update_wiring();
}

void timerAdd30s() {
 pinMode(BTIME_PIN, OUTPUT);
 delay(200);
 digitalWrite(BTIME_PIN, HIGH);
 delay(300);
 // Set it back to HighZ
 digitalWrite(BTIME_PIN, LOW);
 pinMode(BTIME_PIN, INPUT);
}

void timerStart() {
 pinMode(BSTART_PIN, OUTPUT);
 digitalWrite(BSTART_PIN, HIGH);
 delay(500);
 // Set it back to HighZ
 digitalWrite(BSTART_PIN, LOW);
 pinMode(BSTART_PIN, INPUT);
}

void trigger() {
 uint8_t i;
 for(i=0; i<strip.numPixels(); i++) {
   strip.setPixelColor(i,0,0,0);
 }
 strip.show();
 brightness=200;
 strip.setBrightness(brightness);
 pinMode(TRIG_PIN, OUTPUT);
 analogWrite(TRIG_PIN, 100);
 delay(500);
 // Set it back to HighZ
 digitalWrite(TRIG_PIN, LOW);
 pinMode(TRIG_PIN, INPUT);
}

void timerFinish() {
 pinMode(BFINISH_PIN, OUTPUT);
 digitalWrite(BFINISH_PIN, HIGH);
 delay(500);
 // Set it back to HighZ
 digitalWrite(BFINISH_PIN, LOW);
 pinMode(BFINISH_PIN, INPUT);
}

// You managed to defuse the bomb!
void defused() {
 uint32_t c=strip.Color(0, 255, 0);
 timerFinish();
 theaterChase(strip.Color(127, 127, 127), 50);
 colorWipe(strip.Color(0, 0, 0), 0);
 // flag banner
 uint32_t i, j, k;
 for (j=0;j<50*FLAGCYCLE;j++) {
   for (i=0;i<8;i++) {
     //col i will contain data from col (i+j)%50
     for (k=0;k<5;k++) {
       strip.setPixelColor(i+k*8,flag[(i+j)%50] & (1<<(4-k)) ? c : 0);
     }
   }
  strip.show();
  delay(200);
 }
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
 for(uint16_t i=0; i<strip.numPixels(); i++) {
   strip.setPixelColor(i, c);
   strip.show();
   delay(wait);
 }
}

// Set binary dots according to bitmap
void colorSet(uint32_t c, uint8_t m[5], uint8_t wait) {
 for(uint16_t i=0; i<40; i++) {
   strip.setPixelColor(i, m[i/8]&(1<<(i%8)) ? c : 0);
   strip.show();
   delay(wait);
 }
}

void rainbowWait(uint8_t wait) {
 uint16_t i, j=0;
 while(1) {
   j=(j+1)%256;
   for(i=0; i<strip.numPixels(); i++) {
     strip.setPixelColor(i, Wheel((i+j) & 255));
   }
   strip.show();
   if (analogRead(ABUTTON_PIN) < 100)break;
   delay(wait);
 }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
 for (int j=0; j<10; j++) {  //how many cycles of chasing
   for (int q=0; q < 3; q++) {
     for (int i=0; i < strip.numPixels(); i=i+3) {
       strip.setPixelColor(i+q, c);    //turn every third pixel on
     }
     strip.show();
     delay(wait);
     for (int i=0; i < strip.numPixels(); i=i+3) {
       strip.setPixelColor(i+q, 0);        //turn every third pixel off
     }
   }
 }
}

uint32_t Wheel(byte WheelPos) {
 WheelPos = 255 - WheelPos;
 if(WheelPos < 85) {
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
 }
 if(WheelPos < 170) {
   WheelPos -= 85;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
 }
 WheelPos -= 170;
 return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

