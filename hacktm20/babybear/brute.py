import gdb
import string
import os
import logging

gdb.execute("set pagination off")
gdb.execute("set print pretty")
gdb.execute("file baby_bear")
gdb.execute("set logging off")
start_address = 0x600768
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
#logging.basicConfig(filename="newfile.log", format='%(asctime)s %(message)s', filemode='w', level=logging.DEBUG)
logger=logging.getLogger()

uniq_dict = {}

def write_file(inp):
    fp = open("/tmp/test", "w")
    fp.write(inp + "\n")
    fp.close()

def copy_file():
    #replace the urandom with our input.
    os.system("sudo cp /tmp/test /dev/urandom")

def conv_bin(inp):
    bin_string = ""
    for i in inp:
        bin_string += bin(ord(i)).replace('0b','').zfill(8)[::-1]
    return bin_string

def find_bins(inp):
    global start_address
    inp_bin = conv_bin(inp)
    logger.debug("binary of input is %s" % (inp_bin))
    gdb.execute("r")
    string = ""
    current_address = start_address
    while True:
        rip = gdb.execute("p/x $rip", to_string=True).split("=")[1].strip()	
        if rip == "0x40035c":
            logger.info(string)
            rsi = int(gdb.execute("p/x $rsi", to_string=True).split("=")[1].strip(), 16)
            start_index = current_address - start_address
            size_index = rsi - current_address
            logger.info(inp_bin[start_index: start_index + size_index])
            current_address = rsi
            uniq_dict[string] = inp_bin[start_index: start_index + size_index]
            string = ""
        elif rip == "0x4000b0":
            string += chr(int(gdb.execute("p/d $al", to_string=True).split("=")[1].strip()) + 48)
        else:
            break
        gdb.execute("c")


def main():
    gdb.execute("b * 0x40035c")
    gdb.execute("b * 0x4000B0")
    gdb.execute("b * 0x040054F")

    #Find the encoding for all the characters upto 255
    for i in range(0,0xff):
        input_string = chr(i)*10
        logger.info("Writing to file")
        write_file(input_string)	#Write i into a file
        logger.info("Copying file")
        copy_file()			#Replace /dev/urandom with our file.0
        find_bins(input_string)		#Find the encoding of the input file.
        print(uniq_dict)		#Print the dictionary of populated values.
main()
