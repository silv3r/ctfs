#!/usr/bin/env python
a={}
for i in range(0xff):
    enc=""
    tmp = i
    while(tmp):
        if(tmp & 1):
            enc += "1"
        else:
            enc += "0"
        tmp = (tmp >> 1) & 0xff
    enc += "0"
    a[i]=enc

print(a)
