import gdb
import string
import os
import logging

gdb.execute("set pagination off")
gdb.execute("set print pretty")
gdb.execute("file plop")
gdb.execute("set logging off")
gdb.execute("handle SIGSEGV nostop")
gdb.execute("handle SIGALRM pass")
gdb.execute("set disassembly-flavor intel")

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
#logging.basicConfig(filename="newfile.log", format='%(asctime)s %(message)s', filemode='w', level=logging.DEBUG)
logger=logging.getLogger()

ins_list = []

def get_ins():
    gdb.execute("si")
    ins = gdb.execute("x/40i $rip", to_string=True)
    ins_list.append(ins)
    gdb.execute("ni")
    print(gdb.execute("p/x $rax",to_string = True))
    print(gdb.execute("x/gx 0x1337000",to_string = True)) 
    print(gdb.execute("x/gx 0x1337080",to_string = True))     
    gdb.execute("c")


def main():
    gdb.execute("b * 0x5555555555B2")
    gdb.execute("r < inp")

    while True:
        get_ins()
        print("#" * 100)
        [print(ins) for ins in ins_list]
main()
