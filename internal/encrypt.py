from Crypto.Cipher import AES
import os

def xor(a,b):
    return "".join(chr(ord(i)^ord(j)) for i,j in zip(a,b))

def encrypt(message,key):
    aes = AES.new(key,AES.MODE_ECB)
    iv = os.urandom(16)
    ct = xor(message,iv)
    ct = aes.encrypt(ct)
    ct = xor(ct,iv)
    return ct.encode('hex'),iv.encode('hex')


if __name__ == "__main__":

    key = 'ThisistheAESkey!'
    flag = open('flag.txt').read()
    assert(len(flag)==16)
    enc ,iv = encrypt(flag,key)
    f = open('parameter.txt','a')
    f.write('c : ' + enc+'\n')
    f.write('iv : ' + iv[:-2])

