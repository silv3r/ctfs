#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


void read_input( int size , char * buffer)
{
  char buf[0x10];
  int i = 0;
  char * dest = buffer;
  for(; i <size ; i++)
    {
      read(0,&buf[i],1);
      if(buf[i] == '\n')
        {
          buf[i] = '\x00';
          break;
        }
    }

  strncpy(dest,buf,size);
}

// Just call this with right argument to pop the shell and get
// the flag .
void call_me(unsigned int arg1,unsigned int arg2)
{

  int (*func)(const char *) = &system;
  if(arg1 != 0xdeadbeef || arg2 != 0xcafebabe)
    {
      return;
    }
  func("/bin/sh");
}


int main()
{
  char buf[32];
  puts("\nEnter a string\n(I really don't know why but you just have to)\n");
  read_input(32,buf);

  exit(0);
}

